<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Paypal extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('paypal_lib');
        $this->load->model('Payment_model');
        $this->load->model('Form_model');
    }

    function success() {
        //get the transaction data
        if (isset($_REQUEST['payment_status']) && $_REQUEST['payment_status'] == 'Completed') {
            $paypalInfo = $_REQUEST;
// 		  	echo '<pre>';print_r($paypalInfo);exit;
            $data['txn_id'] = $paypalInfo["txn_id"];
            $data['user_id'] = $paypalInfo['custom'];
            $data['product_id'] = $paypalInfo["item_number"];
            $data['payment_gross'] = $paypalInfo["payment_gross"];
            $data['currency_code'] = $paypalInfo["mc_currency"];
            $data['payer_email'] = $paypalInfo["payer_email"];
            $data['payment_status'] = $paypalInfo["payment_status"];
            /** get membership info and set payment date and expiry date */
            $membership = $this->Form_model->getMembershipInfo($paypalInfo["item_number"]);
            $month = (!empty($membership[0]['month']) ? $membership[0]['month'] : 3);
            $purchase_date = date(DATE_TIME_FORMAT);
            $purchase_date_timestamp = strtotime($purchase_date);
            $expiry_date = date(DATE_TIME_FORMAT, strtotime("+" . $month . " months", $purchase_date_timestamp));

            $data['payment_date'] = $purchase_date;
            $data['expiry_date'] = $expiry_date;
// 			echo '<pre>';print_r($data);exit;
            //insert the transaction data into the database
            $this->Payment_model->insertTransaction($data);

            //pass the transaction data to view
            $this->load->view('paypal/success', $data);
        }
    }

    function cancel() {
        $this->load->view('paypal/cancel');
    }

    function ipn() {
        //paypal return transaction details array
        $paypalInfo = $_REQUEST;
        $data['user_id'] = $paypalInfo['custom'];
        $data['product_id'] = $paypalInfo["item_number"];
        $data['txn_id'] = $paypalInfo["txn_id"];
        $data['payment_gross'] = $paypalInfo["payment_gross"];
        $data['currency_code'] = $paypalInfo["mc_currency"];
        $data['payer_email'] = $paypalInfo["payer_email"];
        $data['payment_status'] = $paypalInfo["payment_status"];

        $paypalURL = $this->paypal_lib->paypal_url;
        $result = $this->paypal_lib->curlPost($paypalURL, $paypalInfo);

        //check whether the payment is verified
        if (eregi("VERIFIED", $result)) {
            //insert the transaction data into the database
            $this->Payment_model->insertTransaction($data);
        }
    }

}
