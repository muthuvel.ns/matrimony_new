<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Matches extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper(array('url', 'language'));
        $this->load->helper('form');
        $this->load->model('User_model', '', TRUE);
        $this->load->model('Prefrence_model', '', TRUE);
        $this->load->model('Profilemanager_model', '', TRUE);
        $this->load->model('Form_model', '', TRUE);
        $this->load->model('Payment_model', '', TRUE);
        $this->load->library('session');
        $this->load->model('Success_model', '', TRUE);

        header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1.
        header("Pragma: no-cache"); // HTTP 1.0.
        header("Expires: 0"); // Proxies.
        if (!isset($this->session->userdata['roleguid']) || $this->session->userdata['roleguid'] != MEMBER_ROLE_ID) {
            redirect(base_url());
        }

        if (empty($this->session->userdata['gender'])) {
            redirect(base_url() . 'index.php/common/addgender');
        }
    }

    public function index() {
        $userguid = $this->session->userdata['userguid'];

        $prefrenceInfo = array();
        $this->data = array();
        $caste = '';
        $partnerInfo = $this->User_model->getUserPartnerInfo($userguid);
        if (!empty($partnerInfo)) {
            $prefrence = (!empty($partnerInfo[0]['prefrence']) ? json_decode($partnerInfo[0]['prefrence'], true) : '');
            if (!empty($prefrence)) {
                $gender = GENDER_MALE;
                if (strtoupper($this->session->userdata['gender']) == GENDER_MALE) {
                    $gender = GENDER_FEMALE;
                }
                if (!empty($prefrence['caste'])) {
                    $this->data['caste'] = $prefrence['caste'];
                    $caste = $prefrence['caste'];
                }
                /** get block and ignore user list */
                $id = array(REQUEST_IGNORE, REQUEST_NOT_INETREST, REQUEST_BLOCK);
                /*                 * get login user blocked user list  (Me blocked from others) * */
                $exists = $this->User_model->ProfileRequestInfo($userguid, '', '', $id);
                /*                 * Any one blocked from login user list (Others blocked from me) * */
                $exists1 = $this->User_model->ProfileRequestInfo('', $userguid, '', $id);

                $userList = array();
                $blockList = array();
                $otherBlockList = array();
                if ($exists) {
                    foreach ($exists as $user) {
                        $blockList[] = $user['request_to'];
                    }
                }

                if ($exists1) {
                    foreach ($exists1 as $user) {
                        $otherBlockList[] = $user['request_from'];
                    }
                }

                $userList = array_merge($blockList, $otherBlockList);

                $membership = $this->Payment_model->getPaymentMembershipInfo($userguid);
                //echo "<pre>";print_r($membership );exit;
                $this->data['offer'] = 2;
                if (!empty($membership) && ( $membership[0]['payment_status'] == TEXT_COMPLETED )) {
                    $this->data['offer'] = 1;
                }

                /** partner prefrence */
                $getUserList = $this->Prefrence_model->getUserPrefrenceDetails($prefrence, $gender, $userList);

                if (!empty($getUserList)) {
                    foreach ($getUserList as $key => $data) {
                        $this->data['new'][$data['userGuid']] = $this->Profilemanager_model->userRequestViewData($data['userGuid']);
                        $this->data['new'][$data['userGuid']]['userId'] = $data['userId'];
                        $this->data['new'][$data['userGuid']]['userGuid'] = $data['userGuid'];
                        $this->data['new'][$data['userGuid']]['username'] = $data['username'];
                        $this->data['new'][$data['userGuid']]['age'] = $data['age'];
                        $this->data['new'][$data['userGuid']]['height'] = $data['height'];
                        $this->data['new'][$data['userGuid']]['gender'] = $data['gender'];
                        $this->data['new'][$data['userGuid']]['religion'] = $data['religion'];
                        $this->data['new'][$data['userGuid']]['education'] = $data['education'];
                        $this->data['new'][$data['userGuid']]['qualification'] = $data['qualification'];
                        $this->data['new'][$data['userGuid']]['occupation'] = $data['occupation'];
                        $this->data['new'][$data['userGuid']]['martial_status'] = $data['martial_status'];
                        $this->data['new'][$data['userGuid']]['physical_status'] = $data['physical_status'];
                        $this->data['new'][$data['userGuid']]['profile_created'] = $data['profile_created'];
                        $this->data['new'][$data['userGuid']]['summary'] = $data['summary'];
                        $this->data['new'][$data['userGuid']]['log_in'] = 'Not Login';

                        $loginActivity = $this->User_model->getLoginActivityInfo($data['userGuid'], 1);
                        if (!empty($loginActivity[0]['client_date'])) {
                            $this->data['new'][$data['userGuid']]['log_in'] = $this->User_model->relativeTime(strtotime($loginActivity[0]['client_date']));
                        }

                        $shortList = $this->User_model->ProfileRequestInfo($userguid, $data['userGuid'], '', REQUEST_SHORTLIST);
                        if (!empty($shortList[0]['guid'])) {
                            $this->data['new'][$data['userGuid']]['shortlist'] = 1;
                        }
                        $imagerequest = $this->User_model->ProfileRequestInfo($userguid, $data['userGuid'], '', REQUEST_IMAGE);
                        if (!empty($imagerequest[0]['guid'])) {
                            $this->data['new'][$data['userGuid']]['imagerequest'] = 1;
                        }
                        $educationDetails = $this->Form_model->educationInfo($data['education']);
                        if (!empty($educationDetails[0]['name'])) {
                            $this->data['new'][$data['userGuid']]['edu_name'] = $educationDetails[0]['name'];
                        }

                        $casteDetails = $this->Form_model->casteDetails($data['caste']);
                        if (!empty($casteDetails[0]['name'])) {
                            $this->data['new'][$data['userGuid']]['caste_name'] = $casteDetails[0]['name'];
                        }

                        $cityDetails = $this->Form_model->cityDetails('', $data['city']);
                        if (!empty($cityDetails[0]['name'])) {
                            $this->data['new'][$data['userGuid']]['city_name'] = $cityDetails[0]['name'];
                        }

                        /** get active  and verified image  if not active then get default image */
                        $getUserImagesInfo = $this->Profilemanager_model->getActiveProfileImage($data['userGuid'], $active = 1, $verified = 1);
                        $this->data['new'][$data['userGuid']]['image'] = $getUserImagesInfo['image'];
                        $this->data['new'][$data['userGuid']]['image_request'] = $getUserImagesInfo['image_request'];
                    }
                }
            }
        } else {
            $this->session->set_flashdata('message', $this->lang->line("text_add_profile_prefrence"));
            redirect(base_url() . 'index.php/profile/#pro_prefrence');
        }
        $this->data['counts'] = $this->Prefrence_model->getCountList($gender, $userList, $caste);
        $this->load->view('layout/header');
        $this->load->view('matches/new_match', $this->data);
        $this->load->view('layout/footer');
    }

    public function shortlist() {
        $userguid = $this->session->userdata['userguid'];
        $this->data = array();
        $gender = GENDER_MALE;
        if (strtoupper($this->session->userdata['gender']) == GENDER_MALE) {
            $gender = GENDER_FEMALE;
        }

        $id = REQUEST_SHORTLIST;
        /*         * get shortlist user details * */
        $exists = $this->User_model->ProfileRequestInfo($userguid, '', '', $id);
        $userList = array();
        if ($exists) {
            foreach ($exists as $user) {
                $userList[] = $user['request_to'];
            }
        }

        $membership = $this->Payment_model->getPaymentMembershipInfo($userguid);
        $this->data['offer'] = 2;
        if (!empty($membership) && ( $membership[0]['payment_status'] == TEXT_COMPLETED )) {
            $this->data['offer'] = 1;
        }

        if (!empty($userList)) {
            /** ignore maches */
            $getShortListUser = $this->User_model->getUserinformation($userList, '', $gender);
            if (!empty($getShortListUser)) {
                $j = 0;
                foreach ($getShortListUser as $key => $matches) {
                    $this->data['shortList'][$j] = $this->Profilemanager_model->userRequestViewData($matches['userGuid']);
                    $this->data['shortList'][$j]['userId'] = $matches['userId'];
                    $this->data['shortList'][$j]['username'] = $matches['username'];
                    $this->data['shortList'][$j]['userGuid'] = $matches['userGuid'];
                    $this->data['shortList'][$j]['age'] = $matches['age'];
                    $this->data['shortList'][$j]['gender'] = $matches['gender'];
                    $this->data['shortList'][$j]['height'] = $matches['height'];
                    $this->data['shortList'][$j]['qualification'] = $matches['qualification'];
                    $this->data['shortList'][$j]['religion'] = $matches['religion'];
                    $this->data['shortList'][$j]['martialstatus'] = constant("MARITAL_STATUS_" . $matches['martial_status']);
                    $created = $matches['profile_created'];
                    if ($matches['profile_created'] == TEXT_SON || $matches['profile_created'] == TEXT_DAUGHTER) {
                        $created = TEXT_PARENT;
                    }

                    $this->data['shortList'][$j]['profile_created'] = $created;
                    $this->data['shortList'][$j]['log_in'] = 'Not Login';

                    $loginActivity = $this->User_model->getLoginActivityInfo($matches['userGuid'], 1);
                    if (!empty($loginActivity[0]['client_date'])) {
                        $this->data['shortList'][$j]['log_in'] = $this->User_model->relativeTime(strtotime($loginActivity[0]['client_date']));
                    }
                    $casteDetail = $this->Form_model->casteDetails($matches['caste']);
                    if (!empty($casteDetail[0]['name'])) {
                        $this->data['shortList'][$j]['caste_name'] = $casteDetail[0]['name'];
                    }

                    $educationDetails = $this->Form_model->educationInfo($matches['education']);
                    if (!empty($educationDetails[0]['name'])) {
                        $this->data['shortList'][$j]['edu_name'] = $educationDetails[0]['name'];
                    }
                    $imagerequest = $this->User_model->ProfileRequestInfo($userguid, $matches['userGuid'], '', REQUEST_IMAGE);
                    if (!empty($imagerequest[0]['guid'])) {
                        $this->data['shortList'][$j]['imagerequest'] = 1;
                    }

                    /** get active  and verified image  if not active then get default image */
                    $getUserImagesInfo = $this->Profilemanager_model->getActiveProfileImage($matches['userGuid'], $active = 1, $verified = 1);
                    $this->data['shortList'][$j]['image'] = $getUserImagesInfo['image'];
                    $this->data['shortList'][$j]['image_request'] = $getUserImagesInfo['image_request'];

                    $j++;
                }
            }
        }
        //echo "<pre>";print_r($this->data);exit;
        $this->load->view('layout/header');
        $this->load->view('matches/shortlist', $this->data);
        $this->load->view('layout/footer');
    }

    public function ignore_list() {
        $userguid = $this->session->userdata['userguid'];
        $this->data = array();
        $gender = GENDER_MALE;
        if (strtoupper($this->session->userdata['gender']) == GENDER_MALE) {
            $gender = GENDER_FEMALE;
        }

        /** get ignore user list */
        $id = REQUEST_IGNORE;
        /*         * get login user ignore user list  (Me ignore from others) * */
        $exists = $this->User_model->ProfileRequestInfo($userguid, '', '', $id);

        $userList = array();
        if ($exists) {
            foreach ($exists as $user) {
                $userList[] = $user['request_to'];
            }
        }
        $membership = $this->Payment_model->getPaymentMembershipInfo($userguid);
        $this->data['offer'] = 2;
        if (!empty($membership) && ( $membership[0]['payment_status'] == TEXT_COMPLETED )) {
            $this->data['offer'] = 1;
        }
        if (!empty($userList)) {
            /** ignore maches */
            $getIgnoreUserList = $this->User_model->getUserinformation($userList, '', $gender);

            if (!empty($getIgnoreUserList)) {
                $j = 0;
                foreach ($getIgnoreUserList as $key => $matches) {
                    $this->data['ignoreList'][$j]['userId'] = $matches['userId'];
                    $this->data['ignoreList'][$j]['username'] = $matches['username'];
                    $this->data['ignoreList'][$j]['userGuid'] = $matches['userGuid'];
                    $this->data['ignoreList'][$j]['gender'] = $matches['gender'];
                    $this->data['ignoreList'][$j]['age'] = $matches['age'];
                    $this->data['ignoreList'][$j]['height'] = $matches['height'];
                    $this->data['ignoreList'][$j]['qualification'] = $matches['qualification'];
                    $this->data['ignoreList'][$j]['religion'] = $matches['religion'];
                    $this->data['ignoreList'][$j]['martialstatus'] = constant("MARITAL_STATUS_" . $matches['martial_status']);

                    $created = $matches['profile_created'];
                    if ($matches['profile_created'] == TEXT_SON || $matches['profile_created'] == TEXT_DAUGHTER) {
                        $created = TEXT_PARENT;
                    }
                    $this->data['ignoreList'][$j]['profile_created'] = $created;
                    $this->data['ignoreList'][$j]['log_in'] = 'Not Login';

                    $loginActivity = $this->User_model->getLoginActivityInfo($matches['userGuid'], 1);
                    if (!empty($loginActivity[0]['client_date'])) {
                        $this->data['ignoreList'][$j]['log_in'] = $this->User_model->relativeTime(strtotime($loginActivity[0]['client_date']));
                    }
                    $casteDetail = $this->Form_model->casteDetails($matches['caste']);
                    if (!empty($casteDetail[0]['name'])) {
                        $this->data['ignoreList'][$j]['caste_name'] = $casteDetail[0]['name'];
                    }

                    $educationDetails = $this->Form_model->educationInfo($matches['education']);
                    if (!empty($educationDetails[0]['name'])) {
                        $this->data['ignoreList'][$j]['edu_name'] = $educationDetails[0]['name'];
                    }

                    /** get active  and verified image  if not active then get default image */
                    $getUserImagesInfo = $this->Profilemanager_model->getActiveProfileImage($matches['userGuid'], $active = 1, $verified = 1);
                    $this->data['ignoreList'][$j]['image'] = $getUserImagesInfo['image'];
                    $this->data['ignoreList'][$j]['image_request'] = $getUserImagesInfo['image_request'];

                    $j++;
                }
            }
        }

        $this->load->view('layout/header');
        $this->load->view('matches/ignore_list', $this->data);
        $this->load->view('layout/footer');
    }

    public function mutual_matches() {
        $userguid = $this->session->userdata['userguid'];
        $partnerInfo = $this->User_model->getUserPartnerInfo($userguid);
        $prefrenceInfo = array();
        if (!empty($partnerInfo)) {
            $prefrence = (!empty($partnerInfo[0]['prefrence']) ? json_decode($partnerInfo[0]['prefrence'], true) : '');
            if (!empty($prefrence)) {
                $gender = GENDER_MALE;
                if (strtoupper($this->session->userdata['gender']) == GENDER_MALE) {
                    $gender = GENDER_FEMALE;
                }
                /** get block and ignore user list */
                $id = array(REQUEST_IGNORE, REQUEST_NOT_INETREST, REQUEST_BLOCK);
                /*                 * get login user blocked user list  (Me blocked from others) * */
                $exists = $this->User_model->ProfileRequestInfo($userguid, '', '', $id);
                /*                 * Any one blocked from login user list (Others blocked from me) * */
                $exists1 = $this->User_model->ProfileRequestInfo('', $userguid, '', $id);

                $userList = array();
                $blockList = array();
                $otherBlockList = array();
                if ($exists) {
                    foreach ($exists as $user) {
                        $blockList[] = $user['request_to'];
                    }
                }

                if ($exists1) {
                    foreach ($exists1 as $user) {
                        $otherBlockList[] = $user['request_from'];
                    }
                }
                $membership = $this->Payment_model->getPaymentMembershipInfo($userguid);

                $this->data['offer'] = 2;
                if (!empty($membership) && ( $membership[0]['payment_status'] == TEXT_COMPLETED )) {
                    $this->data['offer'] = 1;
                }
                $userList = array_merge($blockList, $otherBlockList);

                /** newly added maches */
                $added = array('maritalstatus' => $prefrence['maritalstatus'], 'caste' => $prefrence['caste']);
                $getPrefrencetUserList = $this->Prefrence_model->getUserPrefrenceDetails($added, $gender, $userList);
                
                if (!empty($getPrefrencetUserList)) {
                    $j = 0;
                    foreach ($getPrefrencetUserList as $key => $matches) {
                        $this->data['mutualmatches'][$j] = $this->Profilemanager_model->userRequestViewData($matches['userGuid']);
                        $this->data['mutualmatches'][$j]['userId'] = $matches['userId'];
                        $this->data['mutualmatches'][$j]['username'] = $matches['username'];
                        $this->data['mutualmatches'][$j]['userGuid'] = $matches['userGuid'];
                        $this->data['mutualmatches'][$j]['gender'] = $matches['gender'];
                        $this->data['mutualmatches'][$j]['age'] = $matches['age'];
                        $this->data['mutualmatches'][$j]['height'] = $matches['height'];
                        $this->data['mutualmatches'][$j]['qualification'] = $matches['qualification'];
                        $this->data['mutualmatches'][$j]['religion'] = $matches['religion'];
                        $this->data['mutualmatches'][$j]['martialstatus'] = constant("MARITAL_STATUS_" . $matches['martial_status']);

                        $created = $matches['profile_created'];
                        if ($matches['profile_created'] == TEXT_SON || $matches['profile_created'] == TEXT_DAUGHTER) {
                            $created = TEXT_PARENT;
                        }

                        $this->data['mutualmatches'][$j]['profile_created'] = $created;

                        $casteDetail = $this->Form_model->casteDetails($matches['caste']);
                        if (!empty($casteDetail[0]['name'])) {
                            $this->data['mutualmatches'][$j]['caste_name'] = $casteDetail[0]['name'];
                        }

                        $shortList = $this->User_model->ProfileRequestInfo($userguid, $matches['userGuid'], '', REQUEST_SHORTLIST);
                        if (!empty($shortList[0]['guid'])) {
                            $this->data['mutualmatches'][$j]['shortlist'] = 1;
                        }
                        $imagerequest = $this->User_model->ProfileRequestInfo($userguid, $matches['userGuid'], '', REQUEST_IMAGE);
                        if (!empty($imagerequest[0]['guid'])) {
                            $this->data['mutualmatches'][$j]['imagerequest'] = 1;
                        }
                        $educationDetails = $this->Form_model->educationInfo($matches['education']);
                        if (!empty($educationDetails[0]['name'])) {
                            $this->data['mutualmatches'][$j]['edu_name'] = $educationDetails[0]['name'];
                        }
                        /** get active  and verified image  if not active then get default image */
                        $getUserImagesInfo = $this->Profilemanager_model->getActiveProfileImage($matches['userGuid'], $active = 1, $verified = 1);
                        $this->data['mutualmatches'][$j]['image'] = $getUserImagesInfo['image'];
                        $this->data['mutualmatches'][$j]['image_request'] = $getUserImagesInfo['image_request'];
                        $j++;
                    }
                }
            }
        } else {
            $this->session->set_flashdata('message', $this->lang->line("text_add_profile_prefrence"));
            redirect(base_url() . 'index.php/profile/#pro_prefrence');
        }
        $this->data['imagemain'] = $this->session->userdata['image'];
        $this->load->view('layout/header');
        $this->load->view('matches/mutual_match', $this->data);
        $this->load->view('layout/footer');
    }

    public function view() {
        $userguid = $this->session->userdata['userguid'];
        $prefrenceInfo = array();
        $recentList = array();
        $caste = '';
        $userList = array();

        $gender = 'M';
        if (strtoupper($this->session->userdata['gender']) == 'M') {
            $gender = 'F';
        }
        $membership = $this->Payment_model->getPaymentMembershipInfo($userguid);
        $this->data['offer'] = 2;
        if (!empty($membership) && ( $membership[0]['payment_status'] == TEXT_COMPLETED )) {
            $this->data['offer'] = 1;
        }
        $partnerInfo = $this->User_model->getUserPartnerInfo($userguid);
        if (!empty($partnerInfo)) {
            $prefrence = (!empty($partnerInfo[0]['prefrence']) ? json_decode($partnerInfo[0]['prefrence'], true) : '');
            if (!empty($prefrence)) {

                if (!empty($prefrence['caste'])) {
                    $this->data['caste'] = $prefrence['caste'];
                    $caste = $prefrence['caste'];
                }
                /** get block and ignore user list */
                $id = array(REQUEST_IGNORE, REQUEST_NOT_INETREST, REQUEST_BLOCK);
                /*                 * get login user blocked user list  (Me blocked from others) * */
                $exists = $this->User_model->ProfileRequestInfo($userguid, '', '', $id);
                /*                 * Any one blocked from login user list (Others blocked from me) * */
                $exists1 = $this->User_model->ProfileRequestInfo('', $userguid, '', $id);

                $blockList = array();
                $otherBlockList = array();
                if ($exists) {
                    foreach ($exists as $user) {
                        $blockList[] = $user['request_to'];
                    }

                    $blockList = array_unique($blockList);
                }

                if ($exists1) {
                    foreach ($exists1 as $user) {
                        $otherBlockList[] = $user['request_from'];
                    }
                    $otherBlockList = array_unique($otherBlockList);
                }

                $userList = array_merge($blockList, $otherBlockList);

                /** partner prefrence */
                $prefrenceInfo['matches'] = $this->Prefrence_model->getUserPrefrenceDetails($prefrence, $gender, $userList);

                $recentExists = $this->User_model->getRecentlyUpdatedProfile('', $caste);
                $recentList = array();
                if (!empty($recentExists)) {
                    foreach ($recentExists as $user1) {
                        $recentList[] = $user1['user_guid'];
                    }
                }
                $limits = 4;
                $recentUpdated = array('maritalstatus' => $prefrence['maritalstatus'], 'caste' => $prefrence['caste'], 'eduction' => $prefrence['education']);
                /*                 * recently updated profile */
                $recent = $this->Prefrence_model->getUserPrefrenceDetails($recentUpdated, $gender, $userList, $recentList, $limits);
                if (!empty($recent)) {
                    foreach ($recent as $key => $data2) {
                        $this->data['recent'][$data2['userGuid']] = $this->Profilemanager_model->userRequestViewData($data2['userGuid']);
                        $this->data['recent'][$data2['userGuid']]['userId'] = $data2['userId'];
                        $this->data['recent'][$data2['userGuid']]['userGuid'] = $data2['userGuid'];
                        $this->data['recent'][$data2['userGuid']]['username'] = $data2['username'];

                        /** get active  and verified image  if not active then get default image */
                        $getUserImagesInfo = $this->Profilemanager_model->getActiveProfileImage($data2['userGuid'], $active = 1, $verified = 1);
                        $this->data['recent'][$data2['userGuid']]['image'] = $getUserImagesInfo['image'];
                        $this->data['recent'][$data2['userGuid']]['image_request'] = $getUserImagesInfo['image_request'];
                    }
                    $this->data['recentcount'] = 3;
                    if (count($this->data['recent']) < 3) {
                        $this->data['recentcount'] = count($this->data['recent']);
                    }
                }

                /** similar matches */
                $newlyAdded = array('maritalstatus' => $prefrence['maritalstatus'], 'caste' => $prefrence['caste'], 'eduction' => $prefrence['education']);
                $newmatches = $this->Prefrence_model->getUserPrefrenceDetails($newlyAdded, $gender, $userList, '', $limits);

                if (!empty($newmatches)) {
                    foreach ($newmatches as $key => $data1) {
                        $this->data['similar'][$data1['userGuid']] = $this->Profilemanager_model->userRequestViewData($data1['userGuid']);
                        $this->data['similar'][$data1['userGuid']]['userId'] = $data1['userId'];
                        $this->data['similar'][$data1['userGuid']]['userGuid'] = $data1['userGuid'];
                        $this->data['similar'][$data1['userGuid']]['username'] = $data1['username'];

                        /** get active  and verified image  if not active then get default image */
                        $getUserImagesInfo = $this->Profilemanager_model->getActiveProfileImage($data1['userGuid'], $active = 1, $verified = 1);
                        $this->data['similar'][$data1['userGuid']]['image'] = $getUserImagesInfo['image'];
                        $this->data['similar'][$data1['userGuid']]['image_request'] = $getUserImagesInfo['image_request'];
                    }
                    $this->data['similarcount'] = 3;
                    if (count($this->data['similar']) < 3) {
                        $this->data['similarcount'] = count($this->data['similar']);
                    }
                }
                /** newly added maches */
                $newmatches = $this->Prefrence_model->getUserPrefrenceDetails($prefrence, $gender, $userList, '', $limits);

                if (!empty($newmatches)) {
                    foreach ($newmatches as $key => $data3) {
                        $this->data['newmatches'][$data3['userGuid']] = $this->Profilemanager_model->userRequestViewData($data3['userGuid']);
                        $this->data['newmatches'][$data3['userGuid']]['userId'] = $data3['userId'];
                        $this->data['newmatches'][$data3['userGuid']]['userGuid'] = $data3['userGuid'];
                        $this->data['newmatches'][$data3['userGuid']]['username'] = $data3['username'];

                        /** get active  and verified image  if not active then get default image */
                        $getUserImagesInfo = $this->Profilemanager_model->getActiveProfileImage($data3['userGuid'], $active = 1, $verified = 1);
                        $this->data['newmatches'][$data3['userGuid']]['image'] = $getUserImagesInfo['image'];
                        $this->data['newmatches'][$data3['userGuid']]['image_request'] = $getUserImagesInfo['image_request'];
                    }
                    $this->data['newcount'] = 3;
                    if (count($this->data['newmatches']) < 3) {
                        $this->data['newcount'] = count($this->data['newmatches']);
                    }
                }

                /** who viewed login user list */
                $viewedUserList = array();
                $viewedInfo = $this->User_model->viwedProfileInfo('', $userguid);

                if (!empty($viewedInfo)) {
                    $viewedUserList = array();
                    foreach ($viewedInfo as $key => $value) {
                        $viewedUserList[] = $value['viewed_from'];
                    }
                }
                /** removed ignore user list and get viewed user list * */
                $userGuidList = array();
                if (!empty($viewedUserList)) {
                    $userGuidList = array_diff($viewedUserList, $userList);
                }

                $getUserList = array();
                if (!empty($userGuidList)) {
                    $getUserList = $this->User_model->getUserinformation($userGuidList, '', $gender, $limits);
                    if (!empty($getUserList)) {
                        foreach ($getUserList as $key => $data4) {
                            $this->data['viewed'][$data4['userGuid']] = $this->Profilemanager_model->userRequestViewData($data4['userGuid']);
                            $this->data['viewed'][$data4['userGuid']]['userId'] = $data4['userId'];
                            $this->data['viewed'][$data4['userGuid']]['username'] = $data4['username'];

                            /** get active  and verified image  if not active then get default image */
                            $getUserImagesInfo = $this->Profilemanager_model->getActiveProfileImage($data4['userGuid'], $active = 1, $verified = 1);
                            $this->data['viewed'][$data4['userGuid']]['image'] = $getUserImagesInfo['image'];
                            $this->data['viewed'][$data4['userGuid']]['image_request'] = $getUserImagesInfo['image_request'];
                        }

                        $this->data['viewedcount'] = 3;
                        if (count($this->data['viewed']) < 3) {
                            $this->data['viewedcount'] = count($this->data['viewed']);
                        }
                    }
                }

                /** login user viewed user list */
                $userViewedList = array();
                $viewedInfo = $this->User_model->viwedProfileInfo($userguid);
                if (!empty($viewedInfo)) {
                    foreach ($viewedInfo as $key => $value) {
                        $userViewedList[] = $value['viewed_to'];
                    }
                }

                /** removed ignore user list and get viewed user list * */
                $userGuidViewedList = array();
                if (!empty($userViewedList)) {
                    $userList[] = $userguid;
                    $userGuidViewedList = array_diff($userViewedList, $userList);
                }

                $getUserList = array();
                if (!empty($userGuidViewedList)) {
                    $getUserList = $this->User_model->getUserinformation($userGuidViewedList, '', $gender, $limits);
                    if (!empty($getUserList)) {
                        foreach ($getUserList as $key => $data5) {
                            $this->data['profile_viewed'][$data5['userGuid']] = $this->Profilemanager_model->userRequestViewData($data5['userGuid']);
                            $this->data['profile_viewed'][$data5['userGuid']]['userId'] = $data5['userId'];
                            $this->data['profile_viewed'][$data5['userGuid']]['username'] = $data5['username'];
                            $this->data['profile_viewed'][$data5['userGuid']]['userGuid'] = $data5['userGuid'];

                            /** get active  and verified image  if not active then get default image */
                            $getUserImagesInfo = $this->Profilemanager_model->getActiveProfileImage($data5['userGuid'], $active = 1, $verified = 1);
                            $this->data['profile_viewed'][$data5['userGuid']]['image'] = $getUserImagesInfo['image'];
                            $this->data['profile_viewed'][$data5['userGuid']]['image_request'] = $getUserImagesInfo['image_request'];
                        }
                        $this->data['viewedcount'] = 3;
                        if (count($this->data['profile_viewed']) < 3) {
                            $this->data['viewedcount'] = count($this->data['profile_viewed']);
                        }
                    }
                }
            }
        } else {
            $this->session->set_flashdata('message', $this->lang->line("text_add_profile_prefrence"));
            redirect(base_url() . 'index.php/profile/#pro_prefrence');
        }

        $successInfo = $this->Success_model->getUserSuccessInfo('', '', '', $limits);
        if (!empty($successInfo)) {
            $this->data['success'] = $successInfo;
        }
        $this->data['counts'] = $this->Prefrence_model->getCountList($gender, $userList, $caste);
        $this->load->view('layout/header');
        $this->load->view('matches/matches_view1', $this->data);
        //$this->load->view('matches/matches_view',$this->data);
        $this->load->view('layout/footer');
    }

    public function modaldata() {
        if (empty($_REQUEST['uid'])) {
            echo $this->lang->line('detail_not_found');
            exit;
        }

        $profileid = $_REQUEST['uid'];
        $getUserInfo = $this->User_model->getUserinformation($profileid);

        $profile = array();
        $image = '';
        if (!empty($getUserInfo[0])) {
            $profile = $this->Profilemanager_model->profileEditData($getUserInfo[0]);

            $getUserImagesInfo = $this->User_model->getUserImageInfo($profileid, '', '', '', $verify = 1);
            $url = '';
            if (!empty($getUserImagesInfo[0])) {
                foreach ($getUserImagesInfo as $key => $value) {//echo '<pre>';print_r();exit;
                    if (!empty($value['image'])) {
                        if (file_exists(dirname($_SERVER["SCRIPT_FILENAME"]) . "/assets/upload_images/" . $value['image'])) {
                            $url = base_url() . 'assets/upload_images/' . $value['image'];
                        }
                    }
                    if (!empty($url)) {
                        $style = 'style="display: none;"';
                        if ($value['active'] == 1) {
                            $style = 'style="display: block;"';
                            $image .= '<img class="mySlides" src="' . $url . '" ' . $style . '>';
                        } else {
                            $image .= '<img class="mySlides" src="' . $url . '" ' . $style . '>';
                        }
                    }
                }
            }
            if (empty($url)) {
                $url = base_url() . 'assets/upload_images/default.png';
                $image = '<img class="mySlides" src="' . $url . '" height="170" width="170">';
            }
        }
        $html = $this->lang->line('detail_not_found');
        if (!empty($profile)) {

            $html = '	<div class="row">
						<div class="col-md-7">
							<div class="myprofile_slider">
								<div class="w3-content" style="position:relative">
									' . $image . '
									<a class="w3-btn-floating" style="position:absolute;top:45%;left:0" onclick="plusDivs(-1)">❮</a>
									<a class="w3-btn-floating" style="position:absolute;top:45%;right:0" onclick="plusDivs(1)">❯</a>
								</div>
							</div>
						</div>
					<script type="text/javascript">
					$("img").on("contextmenu",function(){
		    	       return false;
		    	    }); 
					var slideIndex = 1;
					showDivs(slideIndex);
					
					function plusDivs(n) {
						showDivs(slideIndex += n);
					}
					
					function showDivs(n) {
						var i;
						var x = document.getElementsByClassName("mySlides");
						if (n > x.length) {slideIndex = 1}
						if (n < 1) {slideIndex = x.length} ;
						for (i = 0; i < x.length; i++) {
							x[i].style.display = "none";
						}
						x[slideIndex-1].style.display = "block";
					}
					</script>
					<div class="col-md-5 personal_inform">
					<h2 class="name_text">' . (!empty($profile['username']) ? $profile['username'] : 'Not Specified') . '</h2>
					<table style="width:100%">
						<tr>
							<td class="det_pro">' . $this->lang->line('register_profile_for_name') . ' </td>
							<td>:</td>
							<td>' . (!empty($profile['username']) ? $profile['username'] : 'Not Specified') . '</td>
						</tr>
						<tr>
							<td class="det_pro">' . $this->lang->line('search_profile_for_age') . '</td>
							<td>:</td>		
							<td>' . (!empty($profile['age']) ? $profile['age'] . 'Years' : 'Not Specified') . '</td>
						</tr>
						<tr>
							<td class="det_pro">' . $this->lang->line('register_profile_for_height') . '</td>
							<td>:</td>		
							<td>' . (!empty($profile['height']) ? $profile['height'] . ' cm' : 'Not Specified') . '</td>
						</tr>
						<tr>
							<td class="det_pro">' . $this->lang->line('register_profile_for_marriagestatus') . '</td>
							<td>:</td>		
							<td>' . (!empty($profile['martial_status']) ? constant("MARITAL_STATUS_" . $profile['martial_status']) : 'Not Specified') . '</td>
						</tr>
						<tr>
							<td class="det_pro">' . $this->lang->line('register_profile_for_physicalstatus') . '</td>
							<td>:</td>		
							<td>' . (!empty($profile['physical_status']) ? constant("PHYSICAL_STATUS_" . $profile['physical_status']) : 'Not Specified') . '</td>
						</tr>
						
							<td class="det_pro">' . $this->lang->line('register_profile_for_caste') . ' </td>
							<td>:</td>		
							<td>' . (!empty($profile['caste_name']) ? $profile['caste_name'] : 'Not Specified') . '</td>
						</tr>
						<tr>
							<td class="det_pro">' . $this->lang->line('location_text') . '  </td>
							<td>:</td>		
							<td>' . (!empty($profile['city_name']) ? $profile['city_name'] : 'Not Specified') . '</td>
						</tr>
						<tr>
							<td class="det_pro">' . $this->lang->line('search_education_title') . '   </td>
							<td>:</td>		
							<td>' . (!empty($profile['edu_name']) ? $profile['edu_name'] : 'Not Specified') . ' - ' . (!empty($profile['qualification']) ? ucfirst($profile['qualification']) : 'Not Specified') . '</td>
						</tr>
						<tr>
							<td class="det_pro">' . $this->lang->line('register_profile_for_occupation') . '    </td>
							<td>:</td>		
							<td>' . (!empty($profile['occupation']) ? $profile['occupation'] : 'Not Specified') . '</td>
						</tr>
					</table>
				</div>
			</div>';
        }

        echo $html;
        exit;
    }

}
