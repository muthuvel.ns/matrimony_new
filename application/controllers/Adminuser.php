<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Adminuser extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Admin_model', '', TRUE);
        $this->load->model('Login_model', '', TRUE);
        $this->load->model('User_model', '', TRUE);
        $this->load->model('Admin_manager', '', TRUE);
        $this->load->model('Payment_model', '', TRUE);
        $this->load->library('session');

        header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1.
        header("Pragma: no-cache"); // HTTP 1.0.
        header("Expires: 0"); // Proxies.

        if (!isset($this->session->userdata['admin_roleguid']) || empty($this->session->userdata['admin_roleguid'])) {
            redirect(base_url() . 'index.php/admin');
        }
    }

    public function activeDeactiveuser() { 
        $userguid = $_REQUEST['userGuid'];
        $status = $_REQUEST['status'];
        if (!empty($userguid)) {
            $data = array('status' => $status);
            $userUpdate = $this->User_model->updateUser($_REQUEST['userGuid'], $data);
            $response = array('msg' => 'success');
        } else {
            $response = array('msg' => 'failure');
        }
        echo json_encode($response);
        exit;
    }

    public function profiledeleteuser() {
        $userguid = $_REQUEST['userGuid'];
        $delete = $_REQUEST['delete'];
        if (!empty($userguid)) {
            $data = array('deleted' => $delete);
            $userUpdate = $this->User_model->updateUser($_REQUEST['userGuid'], $data);
            $response = array('msg' => 'success');
        } else {
            $response = array('msg' => 'failure');
        }
        echo json_encode($response);
        exit;
    }

    public function premiummember() {

        if (empty($_REQUEST['type'])) {
            redirect(base_url() . 'index.php/admin/dashboard');
        }
        $data = $this->Admin_manager->getTypeBasedUserDetails($_REQUEST['type']);
        //$type = $_GET['type'];
        /*
          if($premium==8){
          $userData['payment']=1;
          $this->data['payinfo']	 = $this->Admin_manager->getUserAndProfileDetails($userData);
          }elseif($premium==9){
          $userData['payment']=2;
          $this->data['payinfo']	 = $this->Admin_manager->getUserAndProfileDetails($userData);
          }elseif($premium==10){
          $userData['payment']=3;
          $this->data['payinfo']	 = $this->Admin_manager->getUserAndProfileDetails($userData);
          }
         */
        //echo "<pre>";print_r($data);exit;
        $this->load->view('layout/admin/header');
        $this->load->view('admin/profileview', $data);
        $this->load->view('layout/admin/footer');
    }

}
