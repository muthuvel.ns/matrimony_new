<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Common extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper(array('url', 'language'));
        $this->load->helper('form');
        $this->load->model('User_model', '', TRUE);
        $this->load->model('Usermanager_model', '', TRUE);
        $this->load->model('Form_model', '', TRUE);
        $this->load->model('Utils', '', TRUE);
        $this->load->library('session');
    }

    public function statelist($coutryID) {
        $stateInfo = $this->Form_model->stateDetails($coutryID);
//		$options 	= '<select class="form-control js-example-basic-single"  name="state">';
        /* $options 	= '<option value="0"> -- State -- </option>';
          if ( !empty( $countryInfo )) {
          foreach ( $countryInfo as $key=>$val){
          $options .= '<option value="'.$val['id'].'">'.$val['name'].'</option>';
          }
          }
          //$options 	.= '</select>';
          echo $options; */

        if (!empty($stateInfo)) {
            $state[] = array('id' => '0', 'text' => '-- select state --');
            foreach ($stateInfo as $key => $val) {
                $state[] = array('id' => $val['id'], 'text' => $val['name']);
            }
        } else {
            $state = array('status' => 0);
        }
        echo json_encode($state);
    }

    public function citylist($cityID) {
        $cityInfo = $this->Form_model->cityDetails($cityID);
        //		$options 	= '<select class="form-control js-example-basic-single"  name="state">';
        /* $options 	= '<option value="0"> -- State -- </option>';
          if ( !empty( $countryInfo )) {
          foreach ( $countryInfo as $key=>$val){
          $options .= '<option value="'.$val['id'].'">'.$val['name'].'</option>';
          }
          }
          //$options 	.= '</select>';
          echo $options; */

        if (!empty($cityInfo)) {
            $city[] = array('id' => '0', 'text' => '-- select city --');
            foreach ($cityInfo as $key => $val) {
                $city[] = array('id' => $val['id'], 'text' => $val['name']);
            }
        } else {
            $city = array('status' => 0);
        }
        echo json_encode($city);
    }

    public function addfeedback() {
        $msg = 'Please try agin after some time';
        if (!empty($_REQUEST)) {
            $data = array(
                'subject' => (!empty($_REQUEST['subject']) ? $_REQUEST['subject'] : ''),
                'username' => (!empty($_REQUEST['username']) ? $_REQUEST['username'] : ''),
                'email' => (!empty($_REQUEST['email']) ? $_REQUEST['email'] : ''),
                'country' => (!empty($_REQUEST['country']) ? $_REQUEST['country'] : ''),
                'category' => (!empty($_REQUEST['country']) ? $_REQUEST['country'] : ''),
                'message' => (!empty($_REQUEST['message']) ? $_REQUEST['message'] : '')
            );
            $result = $this->Form_model->insertFeedbackInfo($data);
            if ($result) {
                $msg = 'Successfully added';
            }
        }

        $this->session->set_flashdata('message', $msg);
        redirect('index.php/footer/feedback');
    }

    public function addgender() {
        if (empty($this->session->userdata['gender'])) {
            $sessionUserGuid = $this->session->userdata['userguid'];
            if (!empty($_POST['gender'])) {
                $gender = $_POST['gender'];
                $data = array('gender' => $gender, 'last_updated' => date(DATE_TIME_FORMAT), 'last_updated_by' => $sessionUserGuid);
                $update = $this->User_model->updateUserProfile($sessionUserGuid, $data);
                if ($update) {
                    $img_info = $this->Usermanager_model->getProfileAndDefaultImage($sessionUserGuid);

                    $data['image'] = (!empty($img_info['image']) ? $img_info['image'] : DEFAULT_IMAGE);
                    $data['default'] = (!empty($img_info['default']) ? $img_info['default'] : DEFAULT_IMAGE);
                    $data['gender'] = $gender;
                    $this->session->set_userdata($data);
                }
                redirect(base_url());
            }
            $this->load->view('layout/header');
            $this->load->view('gender');
        } else {
            redirect(base_url());
        }
    }

}
