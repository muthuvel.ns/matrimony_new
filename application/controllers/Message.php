<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Message extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper(array('url', 'language'));
        $this->load->helper('form');
        $this->load->model('User_model', '', TRUE);
        $this->load->model('Usermanager_model', '', TRUE);
        $this->load->model('Form_model', '', TRUE);
        $this->load->model('Utils', '', TRUE);
        $this->load->model('Profilemanager_model', '', TRUE);
        $this->load->model('Payment_model', '', TRUE);
        $this->load->model('Mail', '', TRUE);

        $this->load->library('session');
        if (!isset($this->session->userdata['roleguid']) || $this->session->userdata['roleguid'] != MEMBER_ROLE_ID) {
            redirect(base_url());
        }
        if (empty($this->session->userdata['gender'])) {
            redirect(base_url() . 'index.php/common/addgender');
        }
    }

    public function index() {
        redirect(base_url() . 'index.php/message/inbox');
    }

    public function notification() {
        $this->load->view('layout/header', $this->data);
        $this->load->view('massage/notification');
        $this->load->view('layout/footer');
    }

    public function inbox() {
        $sessionUserGuid = $this->session->userdata['userguid'];

        $membership = $this->Payment_model->getPaymentMembershipInfo($sessionUserGuid);
        $data['offer'] = 2;
        if (!empty($membership) && ( $membership[0]['payment_status'] == TEXT_COMPLETED )) {
            $data['offer'] = 1;
        }

        /** get block and ignore user list */
        $id = REQUEST_IGNORE . ',' . REQUEST_BLOCK;
        /*         * get login user blocked user list  (Me blocked from others) * */
        $exists = $this->User_model->ProfileRequestInfo($sessionUserGuid, '', '', $id);
        /*         * Any one blocked from login user list (Others blocked from me) * */
        $exists1 = $this->User_model->ProfileRequestInfo('', $sessionUserGuid, '', $id);

        $IgnoreUserList = array();
        $blockList = array();
        $otherBlockList = array();
        if ($exists) {
            foreach ($exists as $user) {
                $blockList[] = $user['request_to'];
            }
        }

        if ($exists1) {
            foreach ($exists1 as $user) {
                $otherBlockList[] = $user['request_from'];
            }
        }

        $IgnoreUserList = array_unique(array_merge($blockList, $otherBlockList));
        $id1 = array(REQUEST_INTEREST, REQUEST_ACCEPT, REQUEST_SEND, REQUEST_ARRANGED, REQUEST_IMAGE, REQUEST_NOT_INETREST);
        /** inbox conversation and activity get form ProfileRequestInfo and mail table 
         * if $to =1 means get active message and activtiy form ProfileRequestInfo and mail table based on to filed 
         * * */
        $to = 1;
        /** get profile request info based on activity id */
        $receive = $this->User_model->ProfileRequestInfo('', $sessionUserGuid, '', $id1, $IgnoreUserList, '', $to);

        $userListData = array();
        $activity = array();
        $inbox = array();
        if (!empty($receive)) {
            foreach ($receive as $val) {
                $userListData[] = $val['request_from'];
            }
            $userListData = array_unique($userListData);
            if (!empty($userListData)) {
                $inbox['inbox'] = $this->Profilemanager_model->getInboxMessageAndUserDetails($sessionUserGuid, $userListData, $to);
                $activity = $this->Profilemanager_model->getInboxActivityDetails($sessionUserGuid, $userListData, $to);
            }
        }

        /** merge the inbox and activity array */
        $data = array_merge($inbox, $activity);
        /** validate login user paid member or not  */
        $membership = $this->Payment_model->getPaymentMembershipInfo($sessionUserGuid);
        $data['offer'] = 2;
        if (!empty($membership) && ( $membership[0]['payment_status'] == TEXT_COMPLETED )) {
            $data['offer'] = 1;
        }
        /*         * get active profile image */
        $data['profile'] = $this->Profilemanager_model->getActiveProfileImage($sessionUserGuid);

        $this->load->view('layout/header');
        $this->load->view('massage/inbox', $data);
        $this->load->view('layout/footer');
    }

    public function send() {

        $sessionUserGuid = $this->session->userdata['userguid'];
        $data = array();
        /** get block and ignore user list */
        $id = REQUEST_IGNORE . ',' . REQUEST_BLOCK;
        /*         * get login user blocked user list  (Me blocked from others) * */
        $exists = $this->User_model->ProfileRequestInfo($sessionUserGuid, '', '', $id);
        /*         * Any one blocked from login user list (Others blocked from me) * */
        $exists1 = $this->User_model->ProfileRequestInfo('', $sessionUserGuid, '', $id);

        $IgnoreUserList = array();
        $blockList = array();
        $otherBlockList = array();
        if ($exists) {
            foreach ($exists as $user) {
                $blockList[] = $user['request_to'];
            }
        }
        if ($exists1) {
            foreach ($exists1 as $user) {
                $otherBlockList[] = $user['request_from'];
            }
        }
        $IgnoreUserList = array_unique(array_merge($blockList, $otherBlockList));

        $id1 = array(REQUEST_INTEREST, REQUEST_ACCEPT, REQUEST_SEND, REQUEST_ARRANGED, REQUEST_IMAGE, REQUEST_NOT_INETREST);
        /** send conversation and activity get form ProfileRequestInfo and mail table
         * if $from =1 means get active message and activtiy form ProfileRequestInfo and mail table based on from filed
         * * */
        $from = 1;

        $send = $this->User_model->ProfileRequestInfo($sessionUserGuid, '', '', $id1, $IgnoreUserList, $from);

        $userListData = array();
        $send_info = array();
        $activity = array();
        if (!empty($send)) {
            foreach ($send as $val) {
                $userListData[] = $val['request_to'];
            }
            if (!empty($userListData)) {
                $send_info['send'] = $this->Profilemanager_model->getSendMessageAndUserDetails($sessionUserGuid, array_unique($userListData), $from);
                $activity = $this->Profilemanager_model->getSendActivityDetails($sessionUserGuid, array_unique($userListData), $from);
            }
        }

        /** merge the inbox and activity array */
        $data = array_merge($send_info, $activity);

        /** validate login user paid member or not  */
        $membership = $this->Payment_model->getPaymentMembershipInfo($sessionUserGuid);
        $data['offer'] = 2;
        if (!empty($membership) && ( $membership[0]['payment_status'] == TEXT_COMPLETED )) {
            $data['offer'] = 1;
        }
        /*         * get active profile image */
        $data['profile'] = $this->Profilemanager_model->getActiveProfileImage($sessionUserGuid);

        $this->load->view('layout/header');
        $this->load->view('massage/send', $data);
        $this->load->view('layout/footer');
    }

    public function inboxconversation() {
        $sessionUserGuid = $this->session->userdata['userguid'];
        if (empty($_GET['uid'])) {
            redirect(base_url() . 'index.php/message/');
        }

        $userid = $_GET['uid'];
        $data = $this->Profilemanager_model->getUserInboxConversationInformation($sessionUserGuid, $userid, '', $to_msg = 1);

        $membership = $this->Payment_model->getPaymentMembershipInfo($sessionUserGuid);
        $data['offer'] = 2;
        if (!empty($membership) && ( $membership[0]['payment_status'] == TEXT_COMPLETED )) {
            $data['offer'] = 1;
        }

        /*         * get active profile image */
        $data['profile'] = $this->Profilemanager_model->getActiveProfileImage($sessionUserGuid);

        /*         * get login user blocked user list  (Me blocked from others) * */
        $blockexists = $this->User_model->ProfileRequestInfo($sessionUserGuid, $userid, '', REQUEST_IGNORE);

        $data['profile']['ignore'] = 0;
        if (!empty($blockexists)) {
            $data['profile']['ignore'] = 1;
        }

        /*         * get login user blocked user list  (Me blocked from others) * */
        $blockexists = $this->User_model->ProfileRequestInfo($sessionUserGuid, $userid, '', REQUEST_BLOCK);

        $data['profile']['block'] = 0;
        if (!empty($blockexists)) {
            $data['profile']['block'] = 1;
        }

        $this->load->view('layout/header');
        $this->load->view('massage/conversation', $data);
        $this->load->view('layout/footer');
    }

    public function inboxmailconversation() {
        $sessionUserGuid = $this->session->userdata['userguid'];
        if (empty($_GET['uid'])) {
            redirect(base_url() . 'index.php/message/');
        }

        $userid = $_GET['uid'];
        $mailconversation = 1;
        $conversationInfo = $this->Profilemanager_model->getUserInboxConversationInformation($sessionUserGuid, $userid, $mailconversation, $to_msg = 1);

        if (!empty($conversationInfo['conversation'][0])) {
            $this->data['conversation'] = $conversationInfo['conversation'][0];
        }
        //	echo '<pre>';print_r($this->data);exit;
        $membership = $this->Payment_model->getPaymentMembershipInfo($sessionUserGuid);
        $this->data['offer'] = 2;
        if (!empty($membership) && ( $membership[0]['payment_status'] == TEXT_COMPLETED )) {
            $this->data['offer'] = 1;
        }

        /*         * get active profile image */
        $this->data['profile'] = $this->Profilemanager_model->getActiveProfileImage($sessionUserGuid);

        /*         * get login user blocked user list  (Me blocked from others) * */
        $blockexists = $this->User_model->ProfileRequestInfo($sessionUserGuid, $userid, '', REQUEST_IGNORE);

        $this->data['profile']['ignore'] = 0;
        if (!empty($blockexists)) {
            $this->data['profile']['ignore'] = 1;
        }

        /*         * get login user blocked user list  (Me blocked from others) * */
        $blockexists = $this->User_model->ProfileRequestInfo($sessionUserGuid, $userid, '', REQUEST_BLOCK);

        $this->data['profile']['block'] = 0;
        if (!empty($blockexists)) {
            $this->data['profile']['block'] = 1;
        }

        $this->load->view('layout/header');
        $this->load->view('massage/inboxreplyconversation', $this->data);
        $this->load->view('layout/footer');
    }

    public function sendconversation() {
        $sessionUserGuid = $this->session->userdata['userguid'];
        if (empty($_GET['uid'])) {
            redirect(base_url() . 'index.php/message/');
        }

        $userid = $_GET['uid'];
        $data = $this->Profilemanager_model->getUserSendConversationInformation($sessionUserGuid, $userid, '', $from_msg = 1);

        $membership = $this->Payment_model->getPaymentMembershipInfo($sessionUserGuid);
        $data['offer'] = 2;
        if (!empty($membership) && ( $membership[0]['payment_status'] == TEXT_COMPLETED )) {
            $data['offer'] = 1;
        }
        /*         * get active profile image */
        $data['profile'] = $this->Profilemanager_model->getActiveProfileImage($sessionUserGuid);

        /*         * get login user blocked user list  (Me blocked from others) * */
        $blockexists = $this->User_model->ProfileRequestInfo($sessionUserGuid, $userid, '', REQUEST_IGNORE);

        $data['profile']['ignore'] = 0;
        if (!empty($blockexists)) {
            $data['profile']['ignore'] = 1;
        }

        /*         * get login user blocked user list  (Me blocked from others) * */
        $blockexists = $this->User_model->ProfileRequestInfo($sessionUserGuid, $userid, '', REQUEST_BLOCK);

        $data['profile']['block'] = 0;
        if (!empty($blockexists)) {
            $data['profile']['block'] = 1;
        }
        $this->load->view('layout/header');
        $this->load->view('massage/sendconversation', $data);
        $this->load->view('layout/footer');
    }

    public function sendmailconversation() {
        $sessionUserGuid = $this->session->userdata['userguid'];
        if (empty($_GET['uid'])) {
            redirect(base_url() . 'index.php/message/');
        }

        $userid = $_GET['uid'];
        $mailconversation = 1;
        $conversationInfo = $this->Profilemanager_model->getUserSendConversationInformation($sessionUserGuid, $userid, $mailconversation, $from_msg = 1);

        if (!empty($conversationInfo['conversation'][0])) {
            $this->data['conversation'] = $conversationInfo['conversation'][0];
        }

        $membership = $this->Payment_model->getPaymentMembershipInfo($sessionUserGuid);
        $this->data['offer'] = 2;
        if (!empty($membership) && ( $membership[0]['payment_status'] == TEXT_COMPLETED )) {
            $this->data['offer'] = 1;
        }

        /*         * get active profile image */
        $this->data['profile'] = $this->Profilemanager_model->getActiveProfileImage($sessionUserGuid);

        /*         * get login user blocked user list  (Me blocked from others) * */
        $blockexists = $this->User_model->ProfileRequestInfo($sessionUserGuid, $userid, '', REQUEST_IGNORE);

        $this->data['profile']['ignore'] = 0;
        if (!empty($blockexists)) {
            $this->data['profile']['ignore'] = 1;
        }

        /*         * get login user blocked user list  (Me blocked from others) * */
        $blockexists = $this->User_model->ProfileRequestInfo($sessionUserGuid, $userid, '', REQUEST_BLOCK);

        $this->data['profile']['block'] = 0;
        if (!empty($blockexists)) {
            $this->data['profile']['block'] = 1;
        }
        $this->load->view('layout/header');
        $this->load->view('massage/replyconversation', $this->data);
        $this->load->view('layout/footer');
    }

    /**
     * Inbox and send deleted over all message and individual message also
     * This function used only All new ,accepted, replied and notinterest
     * This not used for conversation message removing
     */
    public function removemessage() {
        $sessionUserGuid = $this->session->userdata['userguid'];
        $updateRequest = 0;
        $reponse = $this->lang->line("failure_msg");
        if (empty($_REQUEST['id'])) {
            echo json_encode($reponse);
            exit;
        }
        $id = $_REQUEST['id'];
        $flag = 0;
        if (!empty($_REQUEST['inboxlist'])) {
            $userList = $_REQUEST['inboxlist'];
            $activityList = array(REQUEST_ACCEPT, REQUEST_NOT_INETREST, REQUEST_SEND, REQUEST_INTEREST, REQUEST_IMAGE);
            $flag = 1;
        } elseif (!empty($_REQUEST['acceptlist'])) {
            $userList = $_REQUEST['acceptlist'];
            $activityList = REQUEST_ACCEPT;
        } elseif (!empty($_REQUEST['notlist'])) {
            $userList = $_REQUEST['notlist'];
            $activityList = REQUEST_NOT_INETREST;
        } elseif (!empty($_REQUEST['replylist'])) {
            $userList = $_REQUEST['replylist'];
            $activityList = REQUEST_SEND;
            $flag = 1;
        }

        $guidList = array();
        $mailIdList = array();
        $updateData = array();
        /** $id==1 remove inbox conversation $id==2 remove send conversation */
        if (!empty($userList) && !empty($activityList)) {
            if ($id == 1) {
                $reqInfo = $this->User_model->ProfileRequestInfo($userList, $sessionUserGuid, '', $activityList);
                if (!empty($reqInfo)) {
                    foreach ($reqInfo as $key => $val) {
                        $guidList[] = $val['guid'];
                    }
                }

                if ($flag == 1) {
                    /** get send mail information */
                    $mailInfo = $this->Mail->userMailConversationInfo($userList, $sessionUserGuid);
                    if (!empty($mailInfo)) {
                        foreach ($mailInfo as $value) {
                            $mailIdList[] = $value['id'];
                        }
                    }
                }
                $updateData = array('to' => 1, 'last_updated' => date(DATE_TIME_FORMAT), 'last_updated_by' => $sessionUserGuid);
            }

            if ($id == 2) {
                $reqInfo = $this->User_model->ProfileRequestInfo($sessionUserGuid, $userList, '', $activityList);
                if (!empty($reqInfo)) {
                    foreach ($reqInfo as $key => $val) {
                        $guidList[] = $val['guid'];
                    }
                }

                if ($flag == 1) {
                    /** get send mail information */
                    $mailInfo = $this->Mail->userMailConversationInfo($sessionUserGuid, $userList);
                    if (!empty($mailInfo)) {
                        foreach ($mailInfo as $value) {
                            $mailIdList[] = $value['id'];
                        }
                    }
                }
                $updateData = array('from' => 1, 'last_updated' => date(DATE_TIME_FORMAT), 'last_updated_by' => $sessionUserGuid);
            }
        }

        if (!empty($guidList)) {
            $updateRequest = $this->User_model->updateProfileRequestInfo($guidList, $updateData);
        }
        if (!empty($mailIdList)) {
            $updateRequest = $this->Mail->updateMailInfo($mailIdList, $updateData);
        }
        $reponse = $this->lang->line("failure_msg");
        if ($updateRequest) {
            $reponse = $this->lang->line("success_msg");
        }
        echo json_encode($reponse);
        exit;
    }

    /**
     * remove inbox conversation messages 
     */
    public function removeinboxmessage() {
        $sessionUserGuid = $this->session->userdata['userguid'];
        $response = $this->Usermanager_model->removeInboxMessageInforamtion($_REQUEST, $sessionUserGuid);
        echo json_encode($response);
    }

    /**
     * remove send conversation messages 
     */
    public function removesendmessage() {
        $sessionUserGuid = $this->session->userdata['userguid'];
        $response = $this->Usermanager_model->removeSendMessageInforamtion($_REQUEST, $sessionUserGuid);
        echo json_encode($response);
    }

}
