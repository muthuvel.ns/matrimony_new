<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Search extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper(array('url', 'language'));
        $this->load->helper('form');
        $this->load->model('User_model', '', TRUE);
        $this->load->model('Search_model', '', TRUE);
        $this->load->model('Payment_model', '', TRUE);
        $this->load->model('Prefrence_model', '', TRUE);
        $this->load->model('Usermanager_model', '', TRUE);
        $this->load->model('Form_model', '', TRUE);
        $this->load->model('Profilemanager_model', '', TRUE);
        $this->load->library('session');
        if (!isset($this->session->userdata['roleguid']) || $this->session->userdata['roleguid'] != MEMBER_ROLE_ID) {
            redirect(base_url());
        }
        if (empty($this->session->userdata['gender'])) {
            redirect(base_url() . 'index.php/common/addgender');
        }
    }

    public function index() {
        $casteInfo = $this->Form_model->casteDetails();

        if (!empty($casteInfo)) {
            foreach ($casteInfo as $key => $val) {
                $caste[] = array('id' => $val['id'], 'text' => $val['name']);
            }
            $this->data['caste'] = json_encode($caste);
        }

        $country = array();
        $state = array();
        $city = array();
        $edu = array();
        $countryInfo = $this->Form_model->countryDetails();
        if (!empty($countryInfo)) {
            foreach ($countryInfo as $key => $val) {
                $country[] = array('id' => $val['id'], 'text' => $val['name']);
            }
            $this->data['country'] = json_encode($country);
            $this->data['state'] = json_encode($state);
            $this->data['city'] = json_encode($city);
        }
        $eduInfo = $this->Form_model->educationInfo();
        if (!empty($eduInfo)) {
            foreach ($eduInfo as $k => $v) {
                $edu[] = array('id' => $v['id'], 'text' => $v['name']);
            }
            $this->data['education'] = json_encode($edu);
        }
        $userguid = $this->session->userdata['userguid'];
        $membership = $this->Payment_model->getPaymentMembershipInfo($userguid);
        $this->data['offer'] = 2;
        if (!empty($membership) && ( $membership[0]['payment_status'] == TEXT_COMPLETED )) {
            $this->data['offer'] = 1;
        }
        //echo "<pre>";print_r($membership);exit;
        $this->load->view('layout/header');
        $this->load->view('search/index', $this->data);
        $this->load->view('layout/footer');
    }

    public function search() {
        redirect(base_url() . 'index.php/search');
    }

    public function filtered() {
        if (empty($_REQUEST)) {
            redirect(base_url());
        }
        $gender = GENDER_MALE;
        if (strtoupper($this->session->userdata['gender']) == GENDER_MALE) {
            $gender = GENDER_FEMALE;
        }
        $userguid = $this->session->userdata['userguid'];
        /** get block and ignore user list */
        $id = REQUEST_IGNORE . ',' . REQUEST_NOT_INETREST . ',' . REQUEST_BLOCK;
        /*         * get login user blocked user list  (Me blocked from others) * */
        $exists = $this->User_model->ProfileRequestInfo($userguid, '', '', $id);
        /*         * Any one blocked from login user list (Others blocked from me) * */
        $exists1 = $this->User_model->ProfileRequestInfo('', $userguid, '', $id);

        $userList = array();
        $blockList = array();
        $otherBlockList = array();
        if ($exists) {
            foreach ($exists as $user) {
                $blockList[] = $user['request_to'];
            }
        }

        if ($exists1) {
            foreach ($exists1 as $user) {
                $otherBlockList[] = $user['request_from'];
            }
        }

        $userList = array_merge($blockList, $otherBlockList);

        $searchInfos = array();
        if (isset($_REQUEST['keySearch']) && !empty($_REQUEST['keyword'])) {
            /** get keyword serach based on details */
            $searchKeyword = $this->Prefrence_model->getUserKeywordSearchDetails($_REQUEST['keyword'], $gender, $userList);
            /** validate user blocked and deleted or not and get not blocked and not deleted user list  */
            if (!empty($searchKeyword)) {
                foreach ($searchKeyword as $val) {
                    if (!in_array($val['userGuid'], $userList) && $val['deleted'] == 0 && $val['gender'] == $gender && $val['roleGuid'] == MEMBER_ROLE_ID) {
                        $searchInfos[] = $val;
                    }
                }
            }
        } else {
            /** partner prefrence */
            $searchInfos = $this->Prefrence_model->getUserPrefrenceDetails($_REQUEST, $gender, $userList);
        }

        if (!empty($searchInfos)) {

            foreach ($searchInfos as $key => $data) {
                $this->data['searchInfo'][$data['userGuid']]['userId'] = $data['userId'];
                $this->data['searchInfo'][$data['userGuid']]['userGuid'] = $data['userGuid'];
                $this->data['searchInfo'][$data['userGuid']]['username'] = $data['username'];
                $this->data['searchInfo'][$data['userGuid']]['age'] = $data['age'];
                $this->data['searchInfo'][$data['userGuid']]['height'] = $data['height'];
                $this->data['searchInfo'][$data['userGuid']]['gender'] = $data['gender'];
                $this->data['searchInfo'][$data['userGuid']]['religion'] = $data['religion'];

                $casteDetails = $this->Form_model->casteDetails($data['caste']);
                if (!empty($casteDetails[0]['name'])) {
                    $this->data['searchInfo'][$data['userGuid']]['caste_name'] = $casteDetails[0]['name'];
                }

                /** get active  and verified image  if not active then get default image */
                $getUserImagesInfo = $this->Profilemanager_model->getActiveProfileImage($data['userGuid'], $active = 1, $verified = 1);
                $this->data['searchInfo'][$data['userGuid']]['image'] = $getUserImagesInfo['image'];
                $this->data['searchInfo'][$data['userGuid']]['image_request'] = $getUserImagesInfo['image_request'];
            }
        }

        $caste = '';
        if (!empty($_REQUEST['caste'])) {
            $caste = $_REQUEST['caste'];
            $this->data['caste'] = $_REQUEST['caste'];
        }
        $this->data['counts'] = $this->Prefrence_model->getCountList($gender, $userList, $caste);
        // echo "<pre>";print_r($this->data);exit;     	

        $this->load->view('layout/header');
        $this->load->view('search/filter', $this->data);
        $this->load->view('layout/footer');
    }

    public function request() {
        if (empty($_REQUEST)) {
            redirect(base_url());
        }

        $gender = 'M';
        if (strtoupper($this->session->userdata['gender']) == 'M') {
            $gender = 'F';
        }
        $userguid = $this->session->userdata['userguid'];
        /** get block and ignore user list */
        $id = REQUEST_IGNORE . ',' . REQUEST_NOT_INETREST . ',' . REQUEST_BLOCK;
        /*         * get login user blocked user list  (Me blocked from others) * */
        $exists = $this->User_model->ProfileRequestInfo($userguid, '', '', $id);
        /*         * Any one blocked from login user list (Others blocked from me) * */
        $exists1 = $this->User_model->ProfileRequestInfo('', $userguid, '', $id);

        $userList = array();
        $blockList = array();
        $otherBlockList = array();
        if ($exists) {
            foreach ($exists as $user) {
                $blockList[] = $user['request_to'];
            }
        }

        if ($exists1) {
            foreach ($exists1 as $user) {
                $otherBlockList[] = $user['request_from'];
            }
        }

        $userList = array_merge($blockList, $otherBlockList);

        $searchInfo = $this->Prefrence_model->getUserPrefrenceDetails($_REQUEST, $gender, $userList);
        $response = 'Matches Not Found';
        if (!empty($searchInfo)) {
            $response = $this->Search_model->template($searchInfo);
        }
        echo $response;
    }

}
