<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Adminpayment extends CI_Controller {

    function __construct() {
        parent::__construct();

        parent::__construct();
        $this->load->helper(array('url', 'language'));
        $this->load->helper('form');
        $this->load->model('User_model', '', TRUE);
        $this->load->model('Form_model', '', TRUE);
        $this->load->model('Admin_manager', '', TRUE);
        $this->load->library('session');
        $this->load->library('paypal_lib');
        //$this->load->model('product');

        header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1.
        header("Pragma: no-cache"); // HTTP 1.0.
        header("Expires: 0"); // Proxies.
        if (!isset($this->session->userdata['admin_roleguid']) || $this->session->userdata['admin_roleguid'] != ADMIN_ROLE_ID) {
            redirect(base_url() . 'index.php/admin');
        }
    }

    function index() {
        if (empty($_REQUEST['uid'])) {
            redirect(base_url() . 'index.php/admin');
        }
        $this->data = array();
        $membership = $this->Form_model->getMembershipInfo();
        if (!empty($membership)) {
            $this->data['membership'] = $membership;
        }
        $this->data['userguid'] = $_REQUEST['uid'];
        $this->load->view('layout/admin/header');
        $this->load->view('admin/payment/index', $this->data);
        $this->load->view('layout/admin/footer');
    }

    function buy() {
        if (empty($_REQUEST['productcode']) || empty($_REQUEST['uid'])) {
            redirect(base_url() . 'index.php/adminpayment');
        }

        $membership = $this->Form_model->getMembershipInfo($_REQUEST['productcode']);

        $name = (!empty($membership[0]['name']) ? $membership[0]['name'] . ' membership' : 'membership');
        $id = $_REQUEST['productcode'];
        $price = (!empty($membership[0]['usd_currency']) ? $membership[0]['usd_currency'] : '');
        $userID = $_REQUEST['uid'];

        if (!empty($price)) {
            //Set variables for paypal form
            $paypalURL = 'https://www.sandbox.paypal.com/cgi-bin/webscr'; //test PayPal api url
            $paypalID = 'paul_busi@gmail.com'; //business email
            $returnURL = base_url() . 'index.php/adminpayment/success'; //payment success url
            $cancelURL = base_url() . 'index.php/adminpayment/cancel'; //payment cancel url
            $notifyURL = base_url() . 'index.php/adminpayment/ipn'; //ipn url

            $logo = base_url() . 'assets/images/1.png';

            $this->paypal_lib->add_field('business', $paypalID);
            $this->paypal_lib->add_field('return', $returnURL);
            $this->paypal_lib->add_field('cancel_return', $cancelURL);
            $this->paypal_lib->add_field('notify_url', $notifyURL);
            $this->paypal_lib->add_field('currency_code', 'USD');
            $this->paypal_lib->add_field('item_name', $name);
            $this->paypal_lib->add_field('custom', $userID);
            $this->paypal_lib->add_field('item_number', $id);
            $this->paypal_lib->add_field('amount', $price);
            $this->paypal_lib->image($logo);

            $this->paypal_lib->paypal_auto_form();
        } else {
            redirect(base_url() . 'index.php/adminpayment');
        }
    }

    public function membership() {
        $userguid = $this->session->userdata['admin_userguid'];
        $this->data = '';
        if (!empty($_REQUEST['member']) && !empty($_REQUEST['month']) && !empty($_REQUEST['ind_currency']) && !empty($_REQUEST['usd_currency'])) {

            $data = array(
                'month' => $_REQUEST['month'],
                'default_currency' => $_REQUEST['default'],
                'ind_currency' => $_REQUEST['ind_currency'],
                'usd_currency' => $_REQUEST['usd_currency'],
                'last_updated' => date(DATE_TIME_FORMAT),
                'last_updated_by' => $userguid
            );
            $exists = $this->Form_model->getMembershipInfo($_REQUEST['member']);

            if (empty($exists)) {
// 				$data['last_updated'] 		= date(DATE_TIME_FORMAT);
// 				$data['last_updated_by']	= $userGuid;
// 				$insert = $this->Form_model->insertMembershipInfo($data);
                $this->data['message'] = 'please insert membership info';
            } else {
                $insert = $this->Form_model->updateMembershipInfo($_REQUEST['member'], $data);
                $this->data['message'] = 'Failure to Update';
                if ($insert) {
                    $this->data['message'] = 'Successfully Updated';
                }
            }
        }

        $this->load->view('layout/admin/header');
        $this->load->view('admin/payment/currency', $this->data);
        $this->load->view('layout/admin/footer');
    }

    public function currencyconvertion() {
        $converted_amount = 0;
        if (!empty($_REQUEST['price'])) {
            $from = 'INR';
            $to = 'USD';
            $amount = $_REQUEST['price'];

            $amount = urlencode($amount);
            $from_Currency = urlencode($from);
            $to_Currency = urlencode($to);
            $get = file_get_contents("https://www.google.com/finance/converter?a=$amount&from=$from_Currency&to=$to_Currency");
            $get = explode("<span class=bld>", $get);
            $get = explode("</span>", $get[1]);
            $converted_amount = preg_replace("/[^0-9\.]/", null, $get[0]);
        }
        echo $converted_amount;
        exit;
    }

    function success() {
        //get the transaction data
        if (isset($_REQUEST['payment_status']) && $_REQUEST['payment_status'] == 'Completed') {
            $paypalInfo = $_REQUEST;
            $data['txn_id'] = $paypalInfo["txn_id"];
            $data['user_id'] = $paypalInfo['custom'];
            $data['product_id'] = $paypalInfo["item_number"];
            $data['payment_gross'] = $paypalInfo["payment_gross"];
            $data['currency_code'] = $paypalInfo["mc_currency"];
            $data['payer_email'] = $paypalInfo["payer_email"];
            $data['payment_status'] = $paypalInfo["payment_status"];
            /** get membership info and set payment date and expiry date */
            $membership = $this->Form_model->getMembershipInfo($paypalInfo["item_number"]);
            $month = (!empty($membership[0]['month']) ? $membership[0]['month'] : 3);
            $purchase_date = date(DATE_TIME_FORMAT);
            $purchase_date_timestamp = strtotime($purchase_date);
            $expiry_date = date(DATE_TIME_FORMAT, strtotime("+" . $month . " months", $purchase_date_timestamp));

            $data['payment_date'] = $purchase_date;
            $data['expiry_date'] = $expiry_date;

            //insert the transaction data into the database
            $this->Payment_model->insertTransaction($data);

            //pass the transaction data to view
            $this->load->view('admin/paypal/success', $data);
        }
    }

    function cancel() {
        $this->load->view('admin/paypal/cancel');
    }

    function ipn() {
        //paypal return transaction details array
        $paypalInfo = $_REQUEST;
        $data['user_id'] = $paypalInfo['custom'];
        $data['product_id'] = $paypalInfo["item_number"];
        $data['txn_id'] = $paypalInfo["txn_id"];
        $data['payment_gross'] = $paypalInfo["payment_gross"];
        $data['currency_code'] = $paypalInfo["mc_currency"];
        $data['payer_email'] = $paypalInfo["payer_email"];
        $data['payment_status'] = $paypalInfo["payment_status"];

        $paypalURL = $this->paypal_lib->paypal_url;
        $result = $this->paypal_lib->curlPost($paypalURL, $paypalInfo);

        //check whether the payment is verified
        if (eregi("VERIFIED", $result)) {
            //insert the transaction data into the database
            $this->Payment_model->insertTransaction($data);
        }
    }

    public function userdetails() {

        if (empty($_REQUEST['type'])) {
            redirect(base_url() . 'index.php/admin/dashboard');
        }

        $data = $this->Admin_manager->getTypeBasedUserDetails($_REQUEST['type']);

        $this->load->view('layout/admin/header');
        $this->load->view('admin/profileview', $data);
        $this->load->view('layout/admin/footer');
    }

}
