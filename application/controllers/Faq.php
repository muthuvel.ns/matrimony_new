<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Faq extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper(array('url', 'language'));
        $this->load->helper('form');
        $this->load->library('session');
    }

    public function faqs() {
        $this->data['log'] = 0;
        $this->load->view('layout/header', $this->data);
        $this->load->view('faq');
        $this->load->view('layout/footer');
    }

}
