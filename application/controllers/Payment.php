<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Payment extends CI_Controller {

    function __construct() {
        parent::__construct();

        parent::__construct();
        $this->load->helper(array('url', 'language'));
        $this->load->helper('form');
        $this->load->model('User_model', '', TRUE);
        $this->load->model('Form_model', '', TRUE);
        $this->load->library('session');
        $this->load->library('paypal_lib');
        //$this->load->model('product');

        header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1.
        header("Pragma: no-cache"); // HTTP 1.0.
        header("Expires: 0"); // Proxies.
        if (!isset($this->session->userdata['roleguid']) || $this->session->userdata['roleguid'] != MEMBER_ROLE_ID) {
            redirect(base_url());
        }
    }

    function index() {
        $this->data = array();
        $membership = $this->Form_model->getMembershipInfo();
        if (!empty($membership)) {
            $this->data['membership'] = $membership;
        }

        $this->load->view('payment/index', $this->data);
        $this->load->view('layout/footer');
    }

    function buy() {
        if (empty($_REQUEST['productcode'])) {
            redirect(base_url() . 'index.php/payment');
        }

        $membership = $this->Form_model->getMembershipInfo($_REQUEST['productcode']);

        $name = (!empty($membership[0]['name']) ? $membership[0]['name'] . ' membership' : 'membership');
        $id = $_REQUEST['productcode'];
        $price = (!empty($membership[0]['usd_currency']) ? $membership[0]['usd_currency'] : '');

        if (!empty($price)) {
            //Set variables for paypal form
            $paypalURL = 'https://www.sandbox.paypal.com/cgi-bin/webscr'; //test PayPal api url
            $paypalID = 'paul_busi@gmail.com'; //business email
            $returnURL = base_url() . 'index.php/paypal/success'; //payment success url
            $cancelURL = base_url() . 'index.php/paypal/cancel'; //payment cancel url
            $notifyURL = base_url() . 'index.php/paypal/ipn'; //ipn url

            $userID = $this->session->userdata['userguid']; //current user id
            $logo = base_url() . 'assets/images/1.png';

            $this->paypal_lib->add_field('business', $paypalID);
            $this->paypal_lib->add_field('return', $returnURL);
            $this->paypal_lib->add_field('cancel_return', $cancelURL);
            $this->paypal_lib->add_field('notify_url', $notifyURL);
            $this->paypal_lib->add_field('currency_code', 'USD');
            $this->paypal_lib->add_field('item_name', $name);
            $this->paypal_lib->add_field('custom', $userID);
            $this->paypal_lib->add_field('item_number', $id);
            $this->paypal_lib->add_field('amount', $price);
            $this->paypal_lib->image($logo);

            $this->paypal_lib->paypal_auto_form();
        } else {
            redirect(base_url() . 'index.php/payment');
        }
    }

}
