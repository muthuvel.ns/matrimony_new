<?php

Class User_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function insertUser($data) {
        $result = 0;
        if (empty($data)) {
            return $result;
        }

        $result = $this->db->insert('user', $data);
        return $result;
    }

    function insertUserProfile($data) {
        $result = 0;
        if (empty($data)) {
            return $result;
        }

        $result = $this->db->insert('user_profile', $data);
        return $result;
    }

    function updateUser($userGuid, $data) {
        $result = 0;
        if (empty($userGuid) || empty($data)) {
            return $result;
        }

        $this->db->where('guid', $userGuid);
        $result = $this->db->update('user', $data);
        return $result;
    }

    function updateUserProfile($userGuid = '', $data = '', $guid = '') {
        $result = 0;
        if (empty($guid) && empty($data) || empty($userGuid) && empty($data)) {
            return $result;
        }
        if ($guid) {
            $this->db->where('guid', $guid);
        }
        if ($userGuid) {
            $this->db->where('user_guid', $userGuid);
        }
        $result = $this->db->update('user_profile', $data);
        return $result;
    }

    function insertRecentlyUpdated($data) {
        $result = 0;
        if (empty($data)) {
            return $result;
        }

        $result = $this->db->insert('recently_updated_profile', $data);
        return $result;
    }

    function updateRecentlyUpdated($userGuid, $data) {
        $result = 0;
        if (empty($userGuid) || empty($data)) {
            return $result;
        }

        $this->db->where('user_guid', $userGuid);
        $result = $this->db->update('recently_updated_profile', $data);
        return $result;
    }

    function insertUserRole($data) {
        $result = 0;
        if (empty($data)) {
            return $result;
        }

        $result = $this->db->insert('user_role', $data);
        return $result;
    }

    function insertReligionInfo($data) {
        $result = 0;
        if (empty($data)) {
            return $result;
        }

        $insert = $this->db->insert('user_religion_info', $data);
        $result = $this->db->insert_id();
        return $result;
    }

    function insertCasteInfo($data) {
        $result = 0;
        if (empty($data)) {
            return $result;
        }

        $insert = $this->db->insert('caste_info', $data);
        $result = $this->db->insert_id();
        return $result;
    }

    function updateReligionInfo($guid = '', $data, $userGuid = '') {
        $result = 0;
        if (empty($guid) && empty($data) || empty($userGuid) && empty($data)) {
            return $result;
        }

        if ($guid) {
            $this->db->where('guid', $guid);
        }

        if ($userGuid) {
            $this->db->where('user_guid', $userGuid);
        }
        $result = $this->db->update('user_religion_info', $data);
        return $result;
    }

    function insertFamilyInfo($data) {
        $result = 0;
        if (empty($data)) {
            return $result;
        }

        $result = $this->db->insert('user_family_details', $data);
        return $result;
    }

    function updateFamilyInfo($guid = '', $data, $userGuid = '') {
        $result = 0;
        if (empty($guid) && empty($data) || empty($userGuid) && empty($data)) {
            return $result;
        }

        if ($guid) {
            $this->db->where('guid', $guid);
        }

        if ($userGuid) {
            $this->db->where('user_guid', $userGuid);
        }
        $result = $this->db->update('user_family_details', $data);
        return $result;
    }

    function insertHabbitsInfo($data) {
        $result = 0;
        if (empty($data)) {
            return $result;
        }

        $result = $this->db->insert('user_food_habbits', $data);
        return $result;
    }

    function updateHabbitsInfo($guid = '', $data, $userGuid = '') {
        $result = 0;
        if (empty($guid) && empty($data) || empty($userGuid) && empty($data)) {
            return $result;
        }

        if ($guid) {
            $this->db->where('guid', $guid);
        }

        if ($userGuid) {
            $this->db->where('user_guid', $userGuid);
        }
        $result = $this->db->update('user_food_habbits', $data);
        return $result;
    }

    function insertOtherInfo($data) {
        $result = 0;
        if (empty($data)) {
            return $result;
        }

        $result = $this->db->insert('user_other_details', $data);
        return $result;
    }

    function updateOtherInfo($guid = '', $data, $userGuid = '') {
        $result = 0;
        if (empty($guid) && empty($data) || empty($userGuid) && empty($data)) {
            return $result;
        }

        if ($guid) {
            $this->db->where('guid', $guid);
        }

        if ($userGuid) {
            $this->db->where('user_guid', $userGuid);
        }
        $result = $this->db->update('user_other_details', $data);
        return $result;
    }

    function insertStateInfo($data) {
        $result = 0;
        if (empty($data)) {
            return $result;
        }

        $insert = $this->db->insert('states', $data);
        $result = $this->db->insert_id();
        return $result;
    }

    function insertCityInfo($data) {
        $result = 0;
        if (empty($data)) {
            return $result;
        }

        $insert = $this->db->insert('cities', $data);
        $result = $this->db->insert_id();
        return $result;
    }

    function insertPartnerInfo($data) {
        $result = 0;
        if (empty($data)) {
            return $result;
        }

        $insert = $this->db->insert('user_partner_info', $data);
        $result = $this->db->insert_id();
        return $result;
    }

    function updatePartnerInfo($guid, $data) {
        $result = 0;
        if (empty($guid) || empty($data)) {
            return $result;
        }

        $this->db->where('guid', $guid);
        $result = $this->db->update('user_partner_info', $data);
        return $result;
    }

    function insertImangeInfo($data) {
        $result = 0;
        if (empty($data)) {
            return $result;
        }

        $insert = $this->db->insert('user_image_info', $data);
        $result = $this->db->insert_id();
        return $result;
    }

    function updateImangeInfo($guid = '', $data, $userGuid = '') {
        $result = 0;
        if (empty($guid) && empty($data) || empty($userGuid) && empty($data)) {
            return $result;
        }

        if ($guid) {
            $this->db->where_in('guid', $guid);
        }

        if ($userGuid) {
            $this->db->where_in('user_guid', $userGuid);
        }
        $result = $this->db->update('user_image_info', $data);
        return $result;
    }

    function insertUserViewedProfileInfo($data) {
        $result = 0;
        if (empty($data)) {
            return $result;
        }

        $result = $this->db->insert('user_viewed_profile', $data);
        return $result;
    }

    function updateUserViewedProfileInfo($guid = '', $data, $userGuid = '') {
        $result = 0;
        if (empty($guid) || empty($data)) {
            return $result;
        }

        if ($guid) {
            $this->db->where('guid', $guid);
        }

        $result = $this->db->update('user_viewed_profile', $data);
        return $result;
    }

    function insertProfileRequestInfo($data) {
        $result = 0;
        if (empty($data)) {
            return $result;
        }

        $result = $this->db->insert('profile_request', $data);
        return $result;
    }

    function updateProfileRequestInfo($guid, $data) {
        $result = 0;
        if (empty($guid) || empty($data)) {
            return $result;
        }
        /* if ( !empty( $activityId ) ){
          $this->db->where_in('activity', $activityId);
          } */
        $this->db->where_in('guid', $guid);
        $result = $this->db->update('profile_request', $data);
        return $result;
    }

    function insertUserDeactivationInfo($data) {
        $result = 0;
        if (empty($data)) {
            return $result;
        }

        $result = $this->db->insert('user_deactivation_info', $data);
        return $result;
    }

    function updateUserDeactivationInfo($id, $data) {
        $result = 0;
        if (empty($id) || empty($data)) {
            return $result;
        }
        $this->db->where('id', $id);
        $result = $this->db->update('user_deactivation_info', $data);
        return $result;
    }

    function userDetails($email = '', $guid = '') {
        $this->db->select('id, password, username, email, guid, status, resetpwd');

        if ($email) {
            $this->db->where('email', $email);
        }
        if ($guid) {
            $this->db->where('guid', $guid);
        }
        $this->db->where('deleted', 0);
        $query = $this->db->get('user');
        return $query->result_array();
    }

    function userProfileDetails($userGuid = "", $guid = "") {

        $this->db->select('user_guid, mobile, photo, guid');

        if ($userGuid) {
            $this->db->where_in('user_guid', $userGuid);
        }
        if ($guid) {
            $this->db->where_in('guid', $guid);
        }

        $this->db->where('deleted', 0);
        $query = $this->db->get('user_profile');

        return $query->result_array();
    }

    function getUserRoleInfo($userGuid = '') {

        $this->db->select('role.name, ur.user_guid AS userGuid')
                ->join('user_role as ur', 'role.guid = ur.role_guid');
        if ($userGuid) {
            $this->db->where('guid', $userGuid);
        }

        $query = $this->db->get('roles AS role');
        return $query->result_array();
    }

    function getUserAndRoleDetails($userGuid = '', $roleGuid = '', $email = '', $gender = '', $notInUserGuid = '') {

        $this->db->select('user.guid AS userGuid, user.username, user.id as userId, user.email, profile.mobile, profile.photo, profile.guid AS profileGuid, profile.physical_status, profile.height, profile.weight, profile.martial_status, profile.gender, profile.age, profile.bodytype, profile.complexion, profile.address, profile.country, profile.state, profile.city, profile.profile_created, profile.dob, role.name AS roleName,role.guid AS roleGuid')
                ->join('user_profile AS profile', 'profile.user_guid = user.guid')
                ->join('user_role as ur', 'ur.user_guid = user.guid')
                ->join('roles AS role', 'role.guid = ur.role_guid');

        if ($userGuid) {
            $this->db->where_in('user.guid', $userGuid);
        }

        if ($email) {
            $this->db->where_in('user.email', $email);
        }

        if ($gender) {
            $this->db->where_in('profile.gender', $gender);
        }

        if ($roleGuid) {
            $this->db->where_in('ur.role_guid', $roleGuid);
        }

        if ($notInUserGuid) {
            $this->db->where_not_in('user.guid', $notInUserGuid);
        }

        $this->db->where('user.deleted', 0);
        $query = $this->db->get('user');
        return $query->result_array();
    }

    function getUserReligionInfo($userGuid = '', $guid = '') {

        $this->db->select('reg.user_guid, reg.guid, reg.caste, reg.sub_caste, reg.religion, reg.dhosam_type, reg.mother_tongue, reg.gothram, reg.dhosam, reg.raasi, reg.star');
        if ($userGuid) {
            $this->db->where('reg.user_guid', $userGuid);
        }

        if ($guid) {
            $this->db->where('reg.guid', $guid);
        }
        $this->db->where('deleted', 0);
        $query = $this->db->get('user_religion_info AS reg');
        return $query->result_array();
    }

    function getUserFamilyInfo($userGuid = '', $guid = '') {

        $this->db->select('user_guid, guid');
        if ($userGuid) {
            $this->db->where('user_guid', $userGuid);
        }

        if ($guid) {
            $this->db->where('guid', $guid);
        }
        $this->db->where('deleted', 0);
        $query = $this->db->get('user_family_details');
        //echo $this->db->last_query();exit;
        return $query->result_array();
    }

    function getUserHabbitsInfo($userGuid = '', $guid = '') {

        $this->db->select('user_guid, guid');
        if ($userGuid) {
            $this->db->where('user_guid', $userGuid);
        }

        if ($guid) {
            $this->db->where('guid', $guid);
        }
        $this->db->where('deleted', 0);
        $query = $this->db->get('user_food_habbits');
        return $query->result_array();
    }

    function getUserOtherInfo($userGuid = '', $guid = '') {

        $this->db->select('user_guid, guid');
        if ($userGuid) {
            $this->db->where('user_guid', $userGuid);
        }

        if ($guid) {
            $this->db->where('guid', $guid);
        }
        $this->db->where('deleted', 0);
        $query = $this->db->get('user_other_details');
        return $query->result_array();
    }

    function getUserPartnerInfo($userGuid = '', $guid = '') {
        $this->db->select('user_guid, guid, preference_info as prefrence');
        if ($userGuid) {
            $this->db->where('user_guid', $userGuid);
        }

        if ($guid) {
            $this->db->where('guid', $guid);
        }
        $this->db->where('deleted', 0);
        $query = $this->db->get('user_partner_info');
        return $query->result_array();
    }

    function getUserImageInfo($userGuid = '', $guid = '', $active = 0, $imageId = '', $verify = '') {

        $this->db->select('user_guid, image, guid, active, verify');
        if ($userGuid) {
            $this->db->where_in('user_guid', $userGuid);
        }

        if ($guid) {
            $this->db->where_in('guid', $guid);
        }

        if ($imageId) {
            $this->db->where_in('image', $imageId);
        }

        if ($active) {
            $this->db->where('active', $active);
        }

        if ($verify) {
            $this->db->where('verify', 1);
        }

        $this->db->where('deleted', 0);
        $query = $this->db->get('user_image_info');
        return $query->result_array();
    }

    function getUserinformation($userGuid = '', $email = '', $gender = '', $limits = '', $roleGuid = MEMBER_ROLE_ID) {
        $this->db->select('user.guid AS userGuid, user.username, user.email, user.id as userId')
                ->select('profile.profile_created, profile.mobile, profile.dob, profile.age,profile.martial_status, profile.childs, profile.gender, profile.physical_status, profile.bodytype, profile.complexion, profile.height, profile.weight, profile.country, profile.state, profile.city')
                ->select('edu.education, edu.qualification, edu.occupation,edu.employeed_in, edu.income, edu.salary')
                ->select('reg.caste, reg.sub_caste, reg.religion,reg.mother_tongue, reg.gothram, reg.dhosam, reg.dhosam_type, reg.star, reg.raasi ')
                ->select('habbit.food, habbit.drink, habbit.smoke')
                ->select('family.family_type, family.family_values, family.family_status, family.summary')
                ->join('user_role AS role', 'role.user_guid = user.guid')
                ->join('user_profile AS profile', 'profile.user_guid = user.guid')
                ->join('user_other_details AS edu', 'edu.user_guid = user.guid')
                ->join('user_religion_info AS reg', 'reg.user_guid = user.guid')
                ->join('user_food_habbits AS habbit', 'habbit.user_guid = user.guid', 'left')
                ->join('user_family_details AS family', 'family.user_guid = user.guid');
        if ($userGuid) {
            $this->db->where_in('user.guid', $userGuid);
        }
        if ($roleGuid) {
            $this->db->where_in('role.role_guid', $roleGuid);
        }
        if ($email) {
            $this->db->where_in('user.email', $email);
        }
        if ($gender) {
            $this->db->where_in('profile.gender', $gender);
        }
        if ($limits) {
            $this->db->limit($limits);
        }

        $this->db->where('user.deleted', 0);
        $query = $this->db->get('user as user');
        return $query->result_array();
        //echo $this->db->last_query();exit;
    }

    public function viwedProfileInfo($from = '', $to = '', $limits = '') {
        $this->db->select('*');
        if ($from) {
            $this->db->where('viewed_from', $from);
        }
        if ($to) {
            $this->db->where('viewed_to', $to);
        }
        if ($limits) {
            $this->db->limit($limits);
        }
        $this->db->where('deleted', 0);
        $this->db->order_by('last_updated', 'DESC');
        $query = $this->db->get('user_viewed_profile');
        return $query->result_array();
    }

    public function ProfileRequestInfo($from = '', $to = '', $limits = '', $request = '', $notIn = '', $from_msg = '', $to_msg = '') {
        $this->db->select('*');
        if ($from) {
            $this->db->where_in('request_from', $from);
        }

        if ($to) {
            $this->db->where_in('request_to', $to);
        }

        if ($request) {
            $this->db->where_in('activity', $request);
        }

        if ($notIn) {
            $this->db->where_not_in('request_to', $notIn);
        }
        if ($from_msg) {
            $this->db->where('from', 0);
        }
        if ($to_msg) {
            $this->db->where('to', 0);
        }

        if ($limits) {
            $this->db->limit($limits);
        }


        $this->db->where('deleted', 0);
        $this->db->order_by('last_updated', 'DESC');
        $query = $this->db->get('profile_request');
//  	echo $this->db->last_query($query);exit;
        return $query->result_array();
    }

    public function getLoginActivityInfo($userGuid = '', $active = 0) {
        $this->db->select('*');
        if ($userGuid) {
            $this->db->where_in('user_guid', $userGuid);
        }
        if ($active) {
            $this->db->where('active', $active);
        }
        $this->db->order_by('created', 'DESC');
        $query = $this->db->get('activity_log');
        return $query->result_array();
    }

    function relativeTime($time, $short = false) {
        $SECOND = 1;
        $MINUTE = 60 * $SECOND;
        $HOUR = 60 * $MINUTE;
        $DAY = 24 * $HOUR;
        $MONTH = 30 * $DAY;
        $before = time() - $time;

        if ($before < 0) {
            return "not yet";
        }

        if ($short) {
            if ($before < 1 * $MINUTE) {
                return ($before < 5) ? "just now" : $before . " ago";
            }

            if ($before < 2 * $MINUTE) {
                return "1m ago";
            }

            if ($before < 45 * $MINUTE) {
                return floor($before / 60) . "m ago";
            }

            if ($before < 90 * $MINUTE) {
                return "1h ago";
            }

            if ($before < 24 * $HOUR) {

                return floor($before / 60 / 60) . "h ago";
            }

            if ($before < 48 * $HOUR) {
                return "1d ago";
            }

            if ($before < 30 * $DAY) {
                return floor($before / 60 / 60 / 24) . "d ago";
            }


            if ($before < 12 * $MONTH) {
                $months = floor($before / 60 / 60 / 24 / 30);
                return $months <= 1 ? "1mo ago" : $months . "mo ago";
            } else {
                $years = floor($before / 60 / 60 / 24 / 30 / 12);
                return $years <= 1 ? "1y ago" : $years . "y ago";
            }
        }

        if ($before < 1 * $MINUTE) {
            return ($before <= 1) ? "just now" : $before . " seconds ago";
        }

        if ($before < 2 * $MINUTE) {
            return "a minute ago";
        }

        if ($before < 45 * $MINUTE) {
            return floor($before / 60) . " minutes ago";
        }

        if ($before < 90 * $MINUTE) {
            return "an hour ago";
        }

        if ($before < 24 * $HOUR) {

            return (floor($before / 60 / 60) == 1 ? 'about an hour' : floor($before / 60 / 60) . ' hours') . " ago";
        }

        if ($before < 48 * $HOUR) {
            return "yesterday";
        }

        if ($before < 30 * $DAY) {
            return floor($before / 60 / 60 / 24) . " days ago";
        }

        if ($before < 12 * $MONTH) {
            $months = floor($before / 60 / 60 / 24 / 30);
            return $months <= 1 ? "1 month ago" : $months . " months ago";
        } else {
            $years = floor($before / 60 / 60 / 24 / 30 / 12);
            return $years <= 1 ? "1 year ago" : $years . " years ago";
        }

        return "$time";
    }

    function getRecentlyUpdatedProfile($userGuid = '', $casteId = '') {

        $this->db->select('*');
        if ($userGuid) {
            $this->db->where('user_guid', $userGuid);
        }

        if ($casteId) {
            $this->db->where('caste_id', $casteId);
        }

        $query = $this->db->get('recently_updated_profile');
        return $query->result_array();
    }

    function getUserDeactivationInfo($userGuid = '') {
        $this->db->select('id, username, email, guid');

        if ($userGuid) {
            $this->db->where('user_guid', $userGuid);
        }
        $query = $this->db->get('user_deactivation_info');
        return $query->result_array();
    }

    /**
     * get active or inactive records based on user inputs
     * @param string $from
     * @param string $to
     * @param string $activity
     */
    public function ProfileRequestDetails($from = '', $to = '', $activity = '') {
        $this->db->select('*');
        if ($from) {
            $this->db->where_in('request_from', $from);
        }

        if ($to) {
            $this->db->where_in('request_to', $to);
        }

        if ($activity) {
            $this->db->where_in('activity', $activity);
        }
        $this->db->order_by('last_updated', 'DESC');
        $query = $this->db->get('profile_request');
        // 	echo $this->db->last_query($query);
        return $query->result_array();
    }

    /**
     * get userinformation query used for admin and member panel 
     * @param string $userGuid
     * @author Muthuvel N
     */
    function getUserinformationData($userGuid = '', $roleGuid = '', $notUserGuid = '', $deleted = '') {
        $this->db->select('user.guid AS userGuid, user.username, user.email,user.status,user.last_updated,user.id as userId, user.deleted')
                ->select('profile.last_updated AS date, profile.profile_created, profile.mobile, profile.dob, profile.age,profile.martial_status, profile.childs, profile.gender, profile.physical_status, profile.bodytype, profile.complexion, profile.height, profile.weight, profile.country, profile.state, profile.city')
                ->select('edu.education, edu.qualification, edu.occupation,edu.employeed_in, edu.income, edu.salary')
                ->select('reg.caste, reg.sub_caste, reg.religion,reg.mother_tongue, reg.gothram, reg.dhosam, reg.dhosam_type, reg.star, reg.raasi ')
                ->select('habbit.food, habbit.drink, habbit.smoke')
                ->select('family.family_type, family.family_values, family.family_status, family.summary')
                ->select('payment.product_id As paymentId')
                ->select('role.role_guid As roleGuid')
                ->join('user_role AS role', 'role.user_guid = user.guid')
                ->join('user_profile AS profile', 'profile.user_guid = user.guid')
                ->join('user_other_details AS edu', 'edu.user_guid = user.guid')
                ->join('user_religion_info AS reg', 'reg.user_guid = user.guid')
                ->join('user_food_habbits AS habbit', 'habbit.user_guid = user.guid', 'left')
                ->join('user_family_details AS family', 'family.user_guid = user.guid')
                ->join('payment_details AS payment', 'payment.user_id = user.guid', 'left');
        if ($userGuid) {
            $this->db->where_in('user.guid', $userGuid);
        }

        if ($roleGuid) {
            $this->db->where_in('role.role_guid', $roleGuid);
        }

        if ($notUserGuid) {
            $this->db->where_not_in('user.guid', $notUserGuid);
        }
        if ($deleted) {
            $this->db->where('user.deleted', 1);
        } else {
            $this->db->where('user.deleted', 0);
        }

        $query = $this->db->get('user as user');

        return $this->db->last_query();
    }

}

?>
