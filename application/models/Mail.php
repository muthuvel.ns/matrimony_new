<?php

Class Mail extends CI_Model {

    function sendMail($from, $to = '', $subject = '', $message = '', $attachment = '', $cc = '') {
        if (empty($from)) {
            $from = 'noreply.kevellcorp@gmail.com';
        }
        // load Email Library
        $ci = get_instance();
        $ci->load->library('email');
        $ci->email->initialize();
        $ci->email->from($from);
        $list = array($to);
        $ci->email->to($list);
        $ci->email->subject($subject);
        $ci->email->message($message);

        if ($attachment == 1) {
            $ci->email->attach('invoice.pdf');
        }
        if (!$ci->email->send()) {
            echo $ci->email->print_debugger();
            exit;
        }
        return true;
    }

    public function insertMailInfo($data) {
        $result = 0;
        if (empty($data)) {
            return $result;
        }

        $result = $this->db->insert('user_mail', $data);
        return $result;
    }

    public function updateMailInfo($id, $data) {
        $result = 0;
        if (empty($data) || empty($id)) {
            return $result;
        }
        $this->db->where_in('id', $id);
        $result = $this->db->update('user_mail', $data);
        return $result;
    }

    public function userMailConversationInfo($from = '', $to = '', $id = '', $from_msg = '', $to_msg = '') {
        $this->db->select('*');
        if ($from) {
            $this->db->where_in('mail_from', $from);
        }

        if ($to) {
            $this->db->where_in('mail_to', $to);
        }

        if ($id) {
            $this->db->where_in('id', $id);
        }
        if ($from_msg) {
            $this->db->where('from', 0);
        }
        if ($to_msg) {
            $this->db->where('to', 0);
        }

        $this->db->where('deleted', 0);
        $this->db->order_by('created', 'DESC');
        $query = $this->db->get('user_mail');
        // 	echo $this->db->last_query($query);
        return $query->result_array();
    }

    function indiaSms($phone, $msg) {
        $username = "karthik";
        $pwd = "123456";

        $message = urlencode($msg);
        $data = "username=" . $username . "&password=" . $pwd . "&to=" . $phone . "&message=" . $message;
        //echo '<pre>';print_r($data);exit;
        $ch = curl_init('http://119.81.202.40/api/http_sms_api.php?' . $data);
        //  		curl_setopt($ch, CURLOPT_POST, true);
        //  		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch); // This is the result from the API
        curl_close($ch);
        if (strpos($result, 'SMS is sent successfully') !== false) {
            return true;
        } else {
            return false;
        }
    }

}

?>
