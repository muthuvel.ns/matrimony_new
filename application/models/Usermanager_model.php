<?php

Class Usermanager_model extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->model('Utils', '', TRUE);
        $this->load->model('User_model', '', TRUE);
    }

    public function userRegistrationStep1($requestData) {
        $result = '';
        if (isset($requestData['step1']) && !empty($requestData['username'])) {
            $gender = (!empty($requestData['gender']) ? $requestData['gender'] : '');
            if (empty($gender)) {
                $gender = (!empty($requestData['gender_hidden']) ? $requestData['gender_hidden'] : '');
            }
            $uname = $requestData['username'];
            $dob = $requestData['year'] . '-' . $requestData['month'] . '-' . $requestData['date'];
            $religion = $requestData['religion'];
            $tongue = $requestData['mother_tongue'];
            $email = $requestData['email'];
            $pass = $requestData['password'];
            $mobile = $requestData['mobile'];
            $profilecreated = $requestData['profile_created'];

            $insertUser = $insertProfile = $insertuserRole = $insertReligionInfo = 0;
            $userExists = $this->User_model->userDetails($email);
            if (empty($userExists)) {
                $this->db->trans_begin();
                $userGuid = $this->Utils->getGuid();
                $created = date(DATE_TIME_FORMAT);
                $insertUserData = array('email' => $email,
                    'password' => md5($pass),
                    'username' => $uname,
                    'created' => $created,
                    'created_by' => $userGuid,
                    'last_updated' => $created,
                    'last_updated_by' => $userGuid,
                    'guid' => $userGuid,
                );
                $insertUser = $this->User_model->insertUser($insertUserData);
                if ($insertUser) {
                    $profileGuid = $this->Utils->getGuid();
                    $insertProfileData = array(
                        'user_guid' => $userGuid,
                        'profile_created' => $profilecreated,
                        'mobile' => $mobile,
                        'dob' => $dob,
                        'age' => date_diff(date_create($dob), date_create('today'))->y,
                        'gender' => $gender,
                        'created' => $created,
                        'created_by' => $userGuid,
                        'last_updated' => $created,
                        'last_updated_by' => $userGuid,
                        'guid' => $profileGuid,
                    );

                    $insertProfile = $this->User_model->insertUserProfile($insertProfileData);

                    if (!empty($insertProfile)) {

                        $roleGuid = $this->Utils->getGuid();
                        $insertRoleData = array(
                            'user_guid' => $userGuid,
                            'role_guid' => MEMBER_ROLE_ID,
                            'created' => $created,
                            'created_by' => $userGuid,
                            'last_updated' => $created,
                            'last_updated_by' => $userGuid,
                            'guid' => $roleGuid,
                        );

                        $insertuserRole = $this->User_model->insertUserRole($insertRoleData);
                        if (!empty($insertuserRole)) {

                            $religionGuid = $this->Utils->getGuid();
                            $insertReligionData = array(
                                'user_guid' => $userGuid,
                                'religion' => $religion,
                                'mother_tongue' => $tongue,
                                'created' => $created,
                                'created_by' => $userGuid,
                                'last_updated' => $created,
                                'last_updated_by' => $userGuid,
                                'guid' => $religionGuid,
                            );

                            $insertReligionInfo = $this->User_model->insertReligionInfo($insertReligionData);
                        }
                    }
                }

                if (empty($insertUser) || empty($insertProfile) || empty($insertuserRole) || empty($insertReligionInfo)) {
                    $this->db->trans_rollback();
                    $result['message'] = $this->lang->line("registration_failed");
                    $result['url'] = base_url() . 'index.php/home/index';
                } else {
                    $this->db->trans_commit();
                    $result['message'] = $this->lang->line("text_success");
                    $result['url'] = base_url() . 'index.php/home/register?uid=' . $userGuid;
                    $result['uid'] = $userGuid;
                }
            } else {
                $result['message'] = $this->lang->line("text_already_exits_email");
                $result['url'] = base_url() . 'index.php/home/index';
            }
        }
        return $result;
    }

    public function userRegistrationStep2($requestData) {
        $result = '';
        if (empty($requestData['uid'])) {
            return $result;
        }

        $userGuid = $requestData['uid'];
        $lastUpdatedUser = $requestData['uid'];
        if (!empty($requestData['sessionGuid'])) {
            $lastUpdatedUser = $requestData['sessionGuid'];
        }
        $userProfileInfo = $this->User_model->userProfileDetails($userGuid);
        if (!empty($userProfileInfo)) {
            $this->db->trans_begin();
            $created = date(DATE_TIME_FORMAT);
            if (isset($requestData['state_text']) && isset($requestData['city_text'])) {

                $stateInfo = array('name' => $requestData['state_text'], 'country_id' => $requestData['country']);
                $stateId = $this->User_model->insertStateInfo($stateInfo);
                $requestData['state'] = $stateId;

                $cityInfo = array('name' => $requestData['city_text'], 'state_id' => $stateId);
                $cityId = $this->User_model->insertCityInfo($cityInfo);
                $requestData['city'] = $cityId;
            } elseif (!isset($requestData['state_text']) && isset($requestData['city_text'])) {

                $cityInfo = array('name' => $requestData['city_text'], 'state_id' => $requestData['state']);
                $cityId = $this->User_model->insertCityInfo($cityInfo);
                $requestData['city'] = $cityId;
            }

            if (isset($requestData['caste_text']) && !empty($requestData['caste_text'])) {
                $casteInfo = array('name' => $requestData['caste_text'], 'created' => date(DATE_TIME_FORMAT));
                $casteId = $this->User_model->insertCasteInfo($casteInfo);
                $requestData['caste'] = $casteId;
            }

            $userProfile = array('martial_status' => (isset($requestData['maritalstatus']) ? $requestData['maritalstatus'] : ''),
                'childs' => (isset($requestData['child']) ? $requestData['child'] : ''),
                'physical_status' => (isset($requestData['physicalstatus']) ? $requestData['physicalstatus'] : ''),
                'height' => (isset($requestData['height']) ? $requestData['height'] : ''),
                'bodytype' => (isset($requestData['bodytype']) ? $requestData['bodytype'] : ''),
                'complexion' => (isset($requestData['complexion']) ? $requestData['complexion'] : ''),
                'weight' => (isset($requestData['weight']) ? $requestData['weight'] : ''),
                'country' => (isset($requestData['country']) ? $requestData['country'] : ''),
                'state' => (isset($requestData['state']) ? $requestData['state'] : ''),
                'city' => (isset($requestData['city']) ? $requestData['city'] : ''),
                'last_updated' => $created,
                'last_updated_by' => $lastUpdatedUser
            );

            $updateProfile = $this->User_model->updateUserProfile($userGuid, $userProfile);

            if ($updateProfile) {
                $religionInfo = array('caste' => (isset($requestData['caste']) ? $requestData['caste'] : ''),
                    'sub_caste' => (isset($requestData['subcaste']) ? $requestData['subcaste'] : ''),
                    'gothram' => (isset($requestData['gothram']) ? $requestData['gothram'] : ''),
                    'dhosam' => (isset($requestData['dhosam']) ? $requestData['dhosam'] : ''),
                    'dhosam_type' => (isset($requestData['dhosam_type']) ? $requestData['dhosam_type'] : ''),
                    'star' => (isset($requestData['star']) ? $requestData['star'] : ''),
                    'raasi' => (isset($requestData['raasi']) ? $requestData['raasi'] : ''),
                    'last_updated' => $created,
                    'last_updated_by' => $lastUpdatedUser
                );

                $religionExists = $this->User_model->getUserReligionInfo($userGuid);

                if (!empty($religionExists[0]['guid'])) {
                    $insertReligion = $this->User_model->updateReligionInfo($religionExists[0]['guid'], $religionInfo);
                } else {
                    $religionInfo['user_guid'] = $userGuid;
                    $religionInfo['created'] = $created;
                    $religionInfo['created_by'] = $userGuid;
                    $religionInfo['guid'] = $this->Utils->getGuid();
                    $insertReligion = $this->User_model->insertReligionInfo($religionInfo);
                }

                $userFamily = array('father_status' => (isset($requestData['father_status']) ? $requestData['father_status'] : ''),
                    'mother_status' => (isset($requestData['mother_status']) ? $requestData['mother_status'] : ''),
                    'family_type' => (isset($requestData['ftype']) ? $requestData['ftype'] : ''),
                    'family_values' => (isset($requestData['fvalues']) ? $requestData['fvalues'] : ''),
                    'family_status' => (isset($requestData['fstatus']) ? $requestData['fstatus'] : ''),
                    'brother' => (isset($requestData['brother']) ? $requestData['brother'] : ''),
                    'sister' => (isset($requestData['sister']) ? $requestData['sister'] : ''),
                    'summary' => (isset($requestData['summary']) ? $requestData['summary'] : ''),
                    'last_updated' => $created,
                    'last_updated_by' => $lastUpdatedUser
                );

                $familyExists = $this->User_model->getUserFamilyInfo($userGuid);
                if (!empty($familyExists[0]['guid'])) {
                    $insertFamily = $this->User_model->updateFamilyInfo($familyExists[0]['guid'], $userFamily);
                } else {
                    $userFamily['user_guid'] = $userGuid;
                    $userFamily['created'] = $created;
                    $userFamily['created_by'] = $userGuid;
                    $userFamily['guid'] = $this->Utils->getGuid();
                    $insertFamily = $this->User_model->insertFamilyInfo($userFamily);
                }

                $userHabbits = array('food' => (isset($requestData['food']) ? $requestData['food'] : ''),
                    'drink' => (isset($requestData['smoking']) ? $requestData['smoking'] : ''),
                    'smoke' => (isset($requestData['drinking']) ? $requestData['drinking'] : ''),
                    'last_updated' => $created,
                    'last_updated_by' => $lastUpdatedUser
                );
                $habbitsExists = $this->User_model->getUserHabbitsInfo($userGuid);
                if (!empty($habbitsExists[0]['guid'])) {
                    $insertHabbits = $this->User_model->updateHabbitsInfo($habbitsExists[0]['guid'], $userHabbits);
                } else {
                    $userHabbits['user_guid'] = $userGuid;
                    $userHabbits['created'] = $created;
                    $userHabbits['created_by'] = $userGuid;
                    $userHabbits['guid'] = $this->Utils->getGuid();
                    $insertHabbits = $this->User_model->insertHabbitsInfo($userHabbits);
                }

                $userOthers = array(
                    'education' => (isset($requestData['education']) ? $requestData['education'] : ''),
                    'qualification' => (isset($requestData['qualification']) ? $requestData['qualification'] : ''),
                    'occupation' => (isset($requestData['occupation']) ? $requestData['occupation'] : ''),
                    'employeed_in' => (isset($requestData['emp_in']) ? $requestData['emp_in'] : ''),
                    'income' => (isset($requestData['income']) ? $requestData['income'] : ''),
                    'salary' => (isset($requestData['salary']) ? $requestData['salary'] : ''),
                    'last_updated' => $created,
                    'last_updated_by' => $lastUpdatedUser
                );
                $otherExists = $this->User_model->getUserOtherInfo($userGuid);
                if (!empty($otherExists[0]['guid'])) {
                    $insertOther = $this->User_model->updateOtherInfo($otherExists[0]['guid'], $userOthers);
                } else {
                    $userOthers['user_guid'] = $userGuid;
                    $userOthers['created'] = $created;
                    $userOthers['created_by'] = $userGuid;
                    $userOthers['guid'] = $this->Utils->getGuid();
                    $insertOther = $this->User_model->insertOtherInfo($userOthers);
                }
            }
            if (empty($updateProfile) || empty($insertReligion) || empty($insertFamily) || empty($insertHabbits) || empty($insertOther)) {
                $this->db->trans_rollback();
                $result['message'] = $this->lang->line("registration_failed");
                $result['url'] = base_url() . 'index.php/home/register?uid=' . $userGuid;
                $result['uid'] = $userGuid;
            } else {
                $this->db->trans_commit();
                $result['message'] = $this->lang->line("text_success");
                $result['url'] = base_url() . 'index.php/home/photo?uid=' . $userGuid;
                $result['uid'] = $userGuid;
            }
        } else {
            $result['message'] = $this->lang->line("text_profile_not");
            $result['url'] = 'home/index';
        }
        return $result;
    }

    public function userRegistrationStep3($requestData) {
        $result = '';
        if (empty($requestData['uid'])) {
            return $result;
        }
        //echo "<pre>";print_r($requestData);exit;
        $userGuid = $requestData['uid'];
        
        $lastUpdatedUser = $requestData['uid'];
        if (!empty($requestData['sessionGuid'])) {
            $lastUpdatedUser = $requestData['sessionGuid'];
        }
        if (isset($requestData['state_text']) && isset($requestData['city_text'])) {

            $stateInfo = array('name' => $requestData['state_text'], 'country_id' => $requestData['country']);
            $stateId = $this->User_model->insertStateInfo($stateInfo);
            $requestData['state'] = $stateId;

            $cityInfo = array('name' => $requestData['city_text'], 'state_id' => $stateId);
            $cityId = $this->User_model->insertCityInfo($cityInfo);
            $requestData['city'] = $cityId;
        } elseif (!isset($requestData['state_text']) && isset($requestData['city_text'])) {

            $cityInfo = array('name' => $requestData['city_text'], 'state_id' => $requestData['state']);
            $cityId = $this->User_model->insertCityInfo($cityInfo);
            $requestData['city'] = $cityId;
        }

        $partnerInfo = array('age_from' => $requestData['age_from'],
            'age_to' => $requestData['age_to'],
            'maritalstatus' => $requestData['maritalstatus'],
            'height' => $requestData['height'],
            'physicalstatus' => $requestData['physicalstatus'],
            'food' => isset($requestData['food']) ? $requestData['food'] : 0,
            'smoking' => isset($requestData['smoking']) ? $requestData['smoking'] : 0,
            'drinking' => isset($requestData['drinking']) ? $requestData['drinking'] : 0,
            'caste' => $requestData['caste'],
            'dhosam' => isset($requestData['dhosam']) ? $requestData['dhosam'] : 0,
            'dhosam_type' => isset($requestData['dhosam_type']) ? $requestData['dhosam_type'] : '',
            'star' => isset($requestData['star']) ? $requestData['star'] : '',
            'country' => isset($requestData['country']) ? $requestData['country'] : 0,
            'state' => isset($requestData['state']) ? $requestData['state'] : 0,
            'city' => isset($requestData['city']) ? $requestData['city'] : 0,
            'education' => $requestData['education'],
            'qualification' => $requestData['qualification'],
            'occupation' => $requestData['occupation'],
            'employeed_in' => isset($requestData['emp_in']) ? $requestData['emp_in'] : 0,
            'salary' => isset($requestData['salary']) ? $requestData['salary'] : ''
        );
        $created = date(DATE_TIME_FORMAT);
        $insertPartnerInfo = array(
            'preference_info' => json_encode($partnerInfo),
            'last_updated' => $created,
            'last_updated_by' => $lastUpdatedUser
        );
        $partnerExists = $this->User_model->getUserPartnerInfo($userGuid);

        if (!empty($partnerExists[0]['guid'])) {
            $insert = $this->User_model->updatePartnerInfo($partnerExists[0]['guid'], $insertPartnerInfo);
        } else {
            $insertPartnerInfo['user_guid'] = $userGuid;
            $insertPartnerInfo['created'] = $created;
            $insertPartnerInfo['created_by'] = $userGuid;
            $insertPartnerInfo['guid'] = $this->Utils->getGuid();
            $insert = $this->User_model->insertPartnerInfo($insertPartnerInfo);
        }
        if ($insert) {
            $result['message'] = $this->lang->line("text_success_add");
            $result['url'] = base_url() . 'index.php/home/success?uid=' . $userGuid;
            $result['uid'] = $userGuid;
        } else {
            $result['message'] = $this->lang->line("photo_add_failed");
            $result['url'] = base_url() . 'index.php/home/partnerinfo?uid=' . $userGuid;
            $result['uid'] = $userGuid;
        }
        return $result;
    }

    public function fileupload($userGuid) {
        $message = array('error' => 'request empty');
        if (empty($userGuid)) {
            return json_encode($message);
        }

        $exists = $this->User_model->getUserImageInfo($userGuid);
        if (count($exists) < 5) {

            if (isset($_FILES['upfile']['error']) && $_FILES['upfile']['error'] == 0) {
                $path = dirname($_SERVER["SCRIPT_FILENAME"]) . "/assets/upload_images/";
                $pathImage = dirname($_SERVER["SCRIPT_FILENAME"]) . "/assets/upload_images/images/";
                $config['upload_path'] = $pathImage;
                $config['allowed_types'] = '*';
                // 	        $config['max_size'] = '100';
                // 	        $config['max_width']  = '1024';
                // 	        $config['max_height']  = '768';
                $config['file_name'] = $userGuid . '_' . strtotime(date(DATE_TIME_FORMAT));
                $this->load->library('upload', $config);

                /** create folder and set permission */
                $this->Utils->folderpermission($path);
                $this->Utils->folderpermission($pathImage);

                if (!$this->upload->do_upload('upfile')) {
                    $message = array('error' => $this->upload->display_errors());
                    return json_encode($message);
                } else {
                    $upload_data = $this->upload->data();
                    $filename = ImageJPEG(ImageCreateFromString(file_get_contents($upload_data['full_path'])), $path . $upload_data['file_name'], 30);
                }

                $created = date(DATE_TIME_FORMAT);
                $insertImage = array(
                    'user_guid' => $userGuid,
                    'image' => (!empty($upload_data['file_name']) ? $upload_data['file_name'] : ''),
                    'created' => $created,
                    'created_by' => $userGuid,
                    'last_updated' => $created,
                    'last_updated_by' => $userGuid,
                    'active' => 1,
                    'guid' => $this->Utils->getGuid()
                );

                /** exits image update active =0 newly added image will be active */
                if ($exists) {
                    $update = array('last_updated' => $created,
                        'last_updated_by' => $userGuid,
                        'active' => 0,
                    );
                    $updateInfo = $this->User_model->updateImangeInfo('', $update, $userGuid);
                }

                $insert = $this->User_model->insertImangeInfo($insertImage);

                $message = json_encode(array('success' => $this->lang->line('upload_failure')));
                if ($insert) {
                    $data['image'] = '';
                    $this->session->unset_userdata($data);
                    $data['image'] = (!empty($upload_data['file_name']) ? $upload_data['file_name'] : '');
                    $this->session->set_userdata($data);
                    if (file_exists($path . $upload_data['file_name'])) {
                        unlink($upload_data['full_path']);
                    }
                    $message = json_encode(array('success' => $this->lang->line('upload_success')));
                }
            } else {
                $message = json_encode(array('error' => $this->lang->line('upload_after_some_time')));
            }
        } else {
            $message = json_encode(array('error' => $this->lang->line('only_allowed_to_upload')));
        }
        return $message;
    }

    public function updateUserRegistrationStep1($requestData) {
        $result = '';
        if (empty($requestData['uid'])) {
            return $result;
        }

        $userGuid = $requestData['uid'];
        $lastUpdatedUser = $requestData['uid'];
        if (!empty($requestData['sessionGuid'])) {
            $lastUpdatedUser = $requestData['sessionGuid'];
        }
        if (isset($requestData['step1']) && !empty($requestData['username'])) {
            $gender = (!empty($requestData['gender']) ? $requestData['gender'] : '');
            if (empty($gender)) {
                $gender = (!empty($requestData['gender_hidden']) ? $requestData['gender_hidden'] : '');
            }

            $uname = $requestData['username'];
            $dob = $requestData['year'] . '-' . $requestData['month'] . '-' . $requestData['date'];
            $religion = $requestData['religion'];
            $tongue = $requestData['mother_tongue'];
            $mobile = $requestData['mobile'];
            $profilecreated = $requestData['profile_created'];

            $insertUser = $insertProfile = $insertuserRole = $insertReligionInfo = 0;

            /** transaction start */
            $this->db->trans_begin();

            $created = date(DATE_TIME_FORMAT);
            /** user table */
            $updateUserData = array(
                'username' => $uname,
                'last_updated' => $created,
                'last_updated_by' => $lastUpdatedUser,
            );
            $updateuser = $this->User_model->updateUser($userGuid, $updateUserData);

            /** user_profile table */
            $insertProfileData = array(
                'mobile' => $mobile,
                'dob' => $dob,
                'age' => date_diff(date_create($dob), date_create('today'))->y,
                'gender' => $gender,
                'last_updated' => $created,
                'last_updated_by' => $lastUpdatedUser,
            );

            $updateProfile = $this->User_model->updateUserProfile($userGuid, $insertProfileData);

            /** user_religion table */
            $insertReligionData = array(
                'religion' => $religion,
                'mother_tongue' => $tongue,
                'last_updated' => $created,
                'last_updated_by' => $lastUpdatedUser,
            );

            $exitReg = $this->User_model->getUserReligionInfo($userGuid);

            if (!empty($exitReg[0]['guid'])) {
                $updateReligionInfo = $this->User_model->updateReligionInfo($exitReg[0]['guid'], $insertReligionData);
            } else {
                $regData['user_guid'] = $userGuid;
                $regData['created'] = $created;
                $regData['created_by'] = $userGuid;
                $regData['guid'] = $this->Utils->getGuid();
                $updateReligionInfo = $this->User_model->insertReligionInfo($regData);
            }

            if (empty($updateuser) || empty($updateProfile) || empty($updateReligionInfo)) {
                $this->db->trans_rollback();
                $result['message'] = $this->lang->line("registration_failed");
            } else {
                $this->db->trans_commit();
                $result['message'] = $this->lang->line("text_success");
            }
        }
        return $result;
    }

    public function adminRegistration($requestData) {
        $result = '';
        if (!empty($requestData['username'])) {
            $uname = $requestData['username'];
            $gender = $requestData['gender'];
            $dob = $requestData['year'] . '-' . $requestData['month'] . '-' . $requestData['date'];
            $email = $requestData['email'];
            $pass = $requestData['password'];
            $mobile = $requestData['mobile'];

            $insertUser = $insertProfile = $insertuserRole = 0;
            $userExists = $this->User_model->userDetails($email);
            if (empty($userExists)) {
                $this->db->trans_begin();
                $userGuid = $this->Utils->getGuid();
                $created = date(DATE_TIME_FORMAT);
                $insertUserData = array('email' => $email,
                    'password' => md5($pass),
                    'username' => $uname,
                    'created' => $created,
                    'created_by' => $userGuid,
                    'last_updated' => $created,
                    'last_updated_by' => $userGuid,
                    'guid' => $userGuid,
                );
                $insertUser = $this->User_model->insertUser($insertUserData);
                if ($insertUser) {
                    $profileGuid = $this->Utils->getGuid();
                    $insertProfileData = array(
                        'user_guid' => $userGuid,
                        'mobile' => $mobile,
                        'dob' => $dob,
                        'age' => date_diff(date_create($dob), date_create('today'))->y,
                        'gender' => $gender,
                        'created' => $created,
                        'created_by' => $userGuid,
                        'last_updated' => $created,
                        'last_updated_by' => $userGuid,
                        'guid' => $profileGuid,
                    );

                    $insertProfile = $this->User_model->insertUserProfile($insertProfileData);

                    if (!empty($insertProfile)) {

                        $roleGuid = $this->Utils->getGuid();
                        $insertRoleData = array(
                            'user_guid' => $userGuid,
                            'role_guid' => ADMIN_ROLE_ID,
                            'created' => $created,
                            'created_by' => $userGuid,
                            'last_updated' => $created,
                            'last_updated_by' => $userGuid,
                            'guid' => $roleGuid,
                        );

                        $insertuserRole = $this->User_model->insertUserRole($insertRoleData);
                    }
                }

                if (empty($insertUser) || empty($insertProfile) || empty($insertuserRole)) {
                    $this->db->trans_rollback();
                    $result['message'] = $this->lang->line('registration_failed');
                } else {
                    $this->db->trans_commit();
                    $result['message'] = $this->lang->line('text_success');
                }
            } else {
                $result['message'] = $this->lang->line('text_already_exits_email');
            }
        }
        return $result;
    }

    /**
     *  remove inbox conversation messages 
     * @param array $request
     * @param string $sessionUserGuid
     */
    public function removeInboxMessageInforamtion($request, $sessionUserGuid) {
        $response = array('msg' => $this->lang->line("failure_msg"));
        $result = 0;
        if (empty($request) || empty($sessionUserGuid)) {
            return $response;
        }


        $updateData = array('to' => 1, 'last_updated' => date(DATE_TIME_FORMAT), 'last_updated_by' => $sessionUserGuid);

        if (isset($request['id']) && isset($request['pid']) && !empty($request['id']) && !empty($request['pid'])) {
            $to_msg = 1;
            /** get profile request details based on sessionuserguid , profileid and activity id */
            $exists = $this->User_model->ProfileRequestInfo($request['pid'], $sessionUserGuid, '', $request['id']);

            if (!empty($exists[0]['guid'])) {
                $result = $this->User_model->updateProfileRequestInfo($exists[0]['guid'], $updateData);
            }
            if ($request['id'] == REQUEST_SEND) {
                /** get send mail information */
                $mailIdList = array();
                $mailInfo = $this->Mail->userMailConversationInfo($request['pid'], $sessionUserGuid);
                if (!empty($mailInfo)) {
                    foreach ($mailInfo as $value) {
                        $mailIdList[] = $value['id'];
                    }
                }
                if (!empty($mailIdList)) {
                    $result = $this->Mail->updateMailInfo($mailIdList, $updateData);
                }
            }

            if ($result) {
                $response = array('msg' => $this->lang->line("success_msg"));
            }
        }

        /** remove send mail information */
        if (isset($request['msg_id']) && !empty($request['msg_id'])) {

            $exists = $this->Mail->userMailConversationInfo('', '', $request['msg_id']);
            if ($exists[0]) {
                $result = $this->Mail->updateMailInfo($request['msg_id'], $updateData);
            }

            if ($result) {
                $response = array('msg' => $this->lang->line("success_msg"));
            }
        }
        return $response;
    }

    /**
     * remove inbox conversation messages
     * @param array $request
     * @param string $sessionUserGuid
     */
    public function removeSendMessageInforamtion($request, $sessionUserGuid) {
        $response = array('msg' => $this->lang->line("failure_msg"));
        $result = 0;
        if (empty($request) || empty($sessionUserGuid)) {
            return $response;
        }


        $updateData = array('from' => 1, 'last_updated' => date(DATE_TIME_FORMAT), 'last_updated_by' => $sessionUserGuid);

        if (isset($request['id']) && isset($request['pid']) && !empty($request['id']) && !empty($request['pid'])) {
            $to_msg = 1;
            /** get profile request details based on sessionuserguid , profileid and activity id */
            $exists = $this->User_model->ProfileRequestInfo($sessionUserGuid, $request['pid'], '', $request['id']);

            if (!empty($exists[0]['guid'])) {
                $result = $this->User_model->updateProfileRequestInfo($exists[0]['guid'], $updateData);
            }
            if ($request['id'] == REQUEST_SEND) {
                /** get send mail information */
                $mailIdList = array();
                $mailInfo = $this->Mail->userMailConversationInfo($sessionUserGuid, $request['pid']);
                if (!empty($mailInfo)) {
                    foreach ($mailInfo as $value) {
                        $mailIdList[] = $value['id'];
                    }
                }
                if (!empty($mailIdList)) {
                    $result = $this->Mail->updateMailInfo($mailIdList, $updateData);
                }
            }

            if ($result) {
                $response = array('msg' => $this->lang->line("success_msg"));
            }
        }

        /** remove send mail information */
        if (isset($request['msg_id']) && !empty($request['msg_id'])) {

            $exists = $this->Mail->userMailConversationInfo('', '', $request['msg_id']);
            if ($exists[0]) {
                $result = $this->Mail->updateMailInfo($request['msg_id'], $updateData);
            }

            if ($result) {
                $response = array('msg' => $this->lang->line("success_msg"));
            }
        }
        return $response;
    }

    /**
     * get profileimage and default image used for login user only
     * @param string $userGuid
     */
    public function getProfileAndDefaultImage($userGuid) {
        $data['image'] = DEFAULT_IMAGE;
        $data['default'] = DEFAULT_IMAGE;
        if (empty($userGuid)) {
            return $data;
        }

        $profile = $this->User_model->getUserImageInfo($userGuid, '', 1);
        $userdata = $this->User_model->getUserinformation($userGuid);
        $image = '';
        if (!empty($profile[0]['image'])) {
            if (file_exists(dirname($_SERVER["SCRIPT_FILENAME"]) . "/assets/upload_images/" . $profile[0]['image'])) {
                $image = $profile[0]['image'];
            }
        }

        if (!empty($userdata['0']['gender'])) {
            if (strtoupper($userdata['0']['gender']) == GENDER_MALE) {
                $image = (!empty($image) ? $image : MALE_IMAGE);
                $default = MALE_IMAGE;
            } elseif (strtoupper($userdata['0']['gender']) == GENDER_FEMALE) {
                $image = (!empty($image) ? $image : FEMALE_IMAGE);
                $default = FEMALE_IMAGE;
            }
            $data['image'] = $image;
            $data['default'] = $default;
        } else {
            $data['image'] = (!empty($image) ? $image : DEFAULT_IMAGE);
        }

        return $data;
    }

}

?>
