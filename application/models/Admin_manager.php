<?php

Class Admin_manager extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->model('User_model', '', TRUE);
        $this->load->model('Profilemanager_model', '', TRUE);
        $this->load->model('Prefrence_model', '', TRUE);
        $this->load->model('Payment_model', '', TRUE);
        $this->load->model('Success_model', '', TRUE);
        $this->load->model('Login_model', '', TRUE);
    }

    public function getManagedUserDetails($userData = '', $userguid = '') {
//echo "<pre>";print_r($userData);exit;

        //$status	= (isset( $userData['status'] )?$userData['status']:'');
        $email = (!empty($userData['email']) ? $userData['email'] : '');
        $gender = (!empty($userData['gender']) ? $userData['gender'] : '');
        $month = (!empty($userData['month']) ? $userData['month'] : '');
        $week = (!empty($userData['week']) ? $userData['week'] : '');
        $payment = (!empty($userData['payment']) ? $userData['payment'] : '');
        $limits = (!empty($userData['limit']) ? $userData['limit'] : '');
        $userRoleguid = (!empty($userData['roleguid']) ? $userData['roleguid'] : MEMBER_ROLE_ID); //get default member roleguid
        $notUserGuid = (!empty($userData['notuserguid']) ? $userData['notuserguid'] : '');
        $deleted = (!empty($userData['deleted']) ? $userData['deleted'] : '');
        /** get user information query */
        $query = $this->User_model->getUserinformationData($userguid, $userRoleguid, $notUserGuid, $deleted);

        $query1 = '';
        if (isset($userData['status'])) {
            $query1.= ' AND user.status=' . $userData['status'];
        }
        if ($payment) {
            $query1.= ' AND payment.product_id=' . $payment;
        }
        if ($email) {
            $query1.= ' AND user.email="' . $email . '"';
        }
        if (!empty($gender)) {
            $query1.= ' AND profile.gender="' . $gender . '"';
        }
        if ($month) {
            $query1.= ' AND profile.last_updated BETWEEN DATE_SUB( CURDATE() + 1 , INTERVAL ' . $month . ' MONTH ) AND CURDATE() + 1';
        }
        if ($week) {
            $query1.= ' AND profile.last_updated BETWEEN DATE_SUB( CURDATE() + 1 , INTERVAL ' . $week . ' WEEK ) AND CURDATE() + 1';
        }
        $query1.= ' ORDER BY profile.last_updated DESC';
        if ($limits) {
            $query1.= ' LIMIT ' . $limits;
        }
        $res = $this->db->query($query . $query1);
        //echo $this->db->last_query();exit;
        return $res->result_array();
    }

    public function getUserAndProfileDetails($userData = '', $userguid = '') {
        $data = array();
        $userInfo = $this->getManagedUserDetails($userData, $userguid);

        if (!empty($userInfo)) {
            foreach ($userInfo as $val) {
                $data[$val['userGuid']] = $val;

                $data[$val['userGuid']]['log_in'] = 'Not Login';


                $educationDetails = $this->Form_model->educationInfo($val['education']);
                if (!empty($educationDetails[0]['name'])) {
                    $data[$val['userGuid']]['edu_name'] = $educationDetails[0]['name'];
                }

                $casteDetails = $this->Form_model->castedetails($val['caste']);
                if (!empty($casteDetails[0]['name'])) {
                    $data[$val['userGuid']]['caste_name'] = $casteDetails[0]['name'];
                }
                /* 	
                  $stateList = $this->Form_model->stateDetails($val['country']);
                  if ( !empty( $stateList )){
                  $state= array();

                  foreach ( $stateList as $key=>$value){
                  $state[]= array('id'=>$value['id'],'text'=>$value['name']);
                  }
                  $data[$val['userGuid']]['stateList'] = json_encode($state);
                  }

                  $cityList = $this->Form_model->cityDetails($val['state']);
                  if ( !empty( $cityList )){
                  $city= array();

                  foreach ( $cityList as $key=>$value){
                  $city[]= array('id'=>$value['id'],'text'=>$value['name']);
                  }
                  $data[$val['userGuid']]['cityList'] = json_encode($city);
                  }
                 */

                $loginActivity = $this->User_model->getLoginActivityInfo($val['userGuid'], 1);
                if (!empty($loginActivity[0]['client_date'])) {
                    $data[$val['userGuid']]['log_in'] = $this->User_model->relativeTime(strtotime($loginActivity[0]['client_date']));
                }

                if (!empty($val['country'])) {
                    $countryDetails = $this->Form_model->countryDetails($val['country']);
                    if (!empty($countryDetails[0]['name'])) {
                        $data[$val['userGuid']]['country_name'] = $countryDetails[0]['name'];
                    }
                }
                if (!empty($val['state'])) {
                    $stateDetails = $this->Form_model->stateDetails('', $val['state']);
                    if (!empty($stateDetails[0]['name'])) {
                        $data[$val['userGuid']]['state_name'] = $stateDetails[0]['name'];
                    }
                }
                if (!empty($val['city'])) {
                    $cityDetails = $this->Form_model->cityDetails('', $val['city']);
                    if (!empty($cityDetails[0]['name'])) {
                        $data[$val['userGuid']]['city_name'] = $cityDetails[0]['name'];
                    }
                }
                $getUserImagesInfo = $this->User_model->getUserImageInfo($val['userGuid'], '', 1);
                $data[$val['userGuid']]['image'] = 'default.png';
                $data[$val['userGuid']]['image_request'] = 1;
                if (!empty($getUserImagesInfo[0]['image'])) {
                    if (file_exists(dirname($_SERVER["SCRIPT_FILENAME"]) . "/assets/upload_images/" . $getUserImagesInfo[0]['image'])) {
                        $data[$val['userGuid']]['image'] = $getUserImagesInfo[0]['image'];
                        $data[$val['userGuid']]['image_request'] = 0;
                    }
                }


                if (!empty($val['salary'])) {
                    $salary = $val['salary'];
                    if ($salary == 1) {
                        $data[$val['userGuid']]['salary_view'] = '100000 or less';
                    } elseif ($salary == 9) {
                        $data[$val['userGuid']]['salary_view'] = '900000 and above';
                    } elseif ($salary == 10) {
                        $data[$val['userGuid']]['salary_view'] = 'Any';
                    } else {
                        $data[$val['userGuid']]['salary_view'] = $val['salary'] . '00000 To ' . ($val['salary'] + 1) . '00000 Laks';
                    }
                }


                $partnerInfo = $this->User_model->getUserPartnerInfo($val['userGuid']);
                //echo "<pre>";print_r($partnerInfo); exit;
                if (!empty($partnerInfo)) {
                    $prefrenceInfo = array();
                    $prefrence = (!empty($partnerInfo[0]['prefrence']) ? json_decode($partnerInfo[0]['prefrence'], true) : '');
                    if (!empty($prefrence)) {
                        $data[$val['userGuid']]['prefrenceData'] = $this->Profilemanager_model->prefrenceEditData($prefrence);
                    }
                }
            }
        }
        return $data;
    }

    /**
     * Get user basic information it contain user and user_profile and user_role details only
     * because user registering time 1st step finished next step net disconnect or any other issue then user not registering 2nd step and 3 etc.
     * this time user details added only above three table only. so admin side disconnect user details edit and activate the user
     * @param string $roleGuid
     * @param string $notInUserGuid
     */
    public function getInCompletedResgistrationUser($roleGuid) {
        $data = array();
        if (empty($roleGuid)) {
            return $data;
        }
        $userData['roleguid'] = $roleGuid;
        $getUser = $this->getUserAndProfileDetails($userData);
        $notInUserGuid = array();
        if (!empty($getUser)) {
            foreach ($getUser as $value) {
                $notInUserGuid[] = $value['userGuid'];
            }
        }

        if (!empty($notInUserGuid)) {
            $userInfo = $this->User_model->getUserAndRoleDetails('', $roleGuid, '', '', $notInUserGuid);
            if (!empty($userInfo)) {
                foreach ($userInfo as $val) {
                    $data[$val['userGuid']] = $val;

                    $getUserImagesInfo = $this->User_model->getUserImageInfo($val['userGuid'], '', 1);
                    $data[$val['userGuid']]['image'] = 'default.png';
                    $data[$val['userGuid']]['image_request'] = 1;
                    if (!empty($getUserImagesInfo[0]['image'])) {
                        if (file_exists(dirname($_SERVER["SCRIPT_FILENAME"]) . "/assets/upload_images/" . $getUserImagesInfo[0]['image'])) {
                            $data[$val['userGuid']]['image'] = $getUserImagesInfo[0]['image'];
                            $data[$val['userGuid']]['image_request'] = 0;
                        }
                    }
                }
            }
        }
        return $data;
    }

    public function getTypeBasedUserDetails($type) {
        $data = array();
        if (empty($type)) {
            return $data;
        }

        $data['type'] = $type;
        /* Get Type Bases Users */
        switch ($type) {

            case TYPE_1 : {
                    /* Get Over all users */
                    $data['user'] = $this->Admin_manager->getUserAndProfileDetails();
                    $data['title'] = $this->lang->line('users') . ' ' . $this->lang->line('detail_text');
                    break;
                }
            case TYPE_2 : {
                    /* Get Male Users only */
                    $userData['gender'] = GENDER_MALE;
                    $data['user'] = $this->Admin_manager->getUserAndProfileDetails($userData);
                    $data['title'] = constant('GENDER_M') . ' ' . $this->lang->line('users');
                    break;
                }
            case TYPE_3 : {
                    /* Get Female Users only */
                    $userData['gender'] = GENDER_FEMALE;
                    $data['user'] = $this->Admin_manager->getUserAndProfileDetails($userData);
                    $data['title'] = constant('GENDER_F') . ' ' . $this->lang->line('users');
                    break;
                }
            case TYPE_4 : {
                    /* Get Premium Users */
                    $premiumuser = $this->Payment_model->getPaymentMembershipInfo();
                    $userlist = array();
                    foreach ($premiumuser as $value) {
                        $userlist[] = $value['user_id'];
                    }
                    if(!empty($userlist)){
                    $data['user'] = $this->Admin_manager->getUserAndProfileDetails('', $userlist);
				}
                    $data['title'] = $this->lang->line('premium') . ' ' . $this->lang->line('users');
                    break;
                }
            case TYPE_5 : {
                    /* Get Active Users */
                    $userData['status'] = constant('TYPE_1');
                    $data['user'] = $this->Admin_manager->getUserAndProfileDetails($userData);
                    $data['title'] = $this->lang->line('active') . ' ' . $this->lang->line('users');
                    break;
                }
            case TYPE_6 : {
                    /* Get Deactive Users */
                    $userData['status'] = 0;
                    $data['user'] = $this->Admin_manager->getUserAndProfileDetails($userData);
                    $data['title'] = $this->lang->line('deactive') . ' ' . $this->lang->line('users');
                    break;
                }
            case TYPE_7 : {
                    /* Get Deactive Users */
                    $userData['deleted'] = constant('TYPE_1');
                    $data['user'] = $this->Admin_manager->getUserAndProfileDetails($userData);
                    $data['title'] = $this->lang->line('delete') . ' ' . $this->lang->line('users');
                    break;
                }
            case TYPE_8 : {
                    /* Get Platinum Users Only */
                    $userData['payment'] = constant('TYPE_1');
                    $data['user'] = $this->Admin_manager->getUserAndProfileDetails($userData);
                    $data['title'] = $this->lang->line('platinum_member_text');
                    break;
                }
            case TYPE_9 : {
                    /* Get Diamond Users Only */
                    $userData['payment'] = constant('TYPE_2');
                    $data['user'] = $this->Admin_manager->getUserAndProfileDetails($userData);
                    $data['title'] = $this->lang->line('diamond_member_text');
                    break;
                }
            case TYPE_10 : {
                    /* Get Gold Users Only */
                    $userData['payment'] = constant('TYPE_3');
                    $data['user'] = $this->Admin_manager->getUserAndProfileDetails($userData);
                    $data['title'] = $this->lang->line('gold_member_text');
                    break;
                }
            case TYPE_11 : {
                    /* Profile approved */
                    $data['userinfo'] = $this->Admin_manager->getUserAndProfileDetails();
                    $data['title'] = $this->lang->line('profile_approved');
                    break;
                }
            case TYPE_12 : {
                    /* Get success story users */
                    $data['successdata'] = $this->Success_model->getAllUserSuccessInfo();
                    $data['title'] = $this->lang->line('success_story_title');
                    break;
                }
            case TYPE_13 : {
                    /* Get overall un-paid users */
                    $paymentuser = $this->Payment_model->getPaymentMembershipInfo();
                    $userlist = array();
                    foreach ($paymentuser as $value) {
                        $userlist[] = $value['user_id'];
                    }
                    $data['user'] = array();
                    if (!empty($userlist)) {
                        $userData['notuserguid'] = $userlist;
                        $data['user'] = $this->Admin_manager->getUserAndProfileDetails($userData);
                    }
                    $data['title'] = $this->lang->line('unpaid-users');
                    break;
                }
            case TYPE_14 : {
                    /* Get Image not verified Users */
                    $userlist = array();
                    $verify = $this->Admin_model->getNotVerifyUserImageInfo();
                    if (!empty($verify[0]['user_guid'])) {
                        foreach ($verify as $val) {
                            $userlist[] = $val['user_guid'];
                        }
                    }
                    $data['user'] = array();
                    if (!empty($userlist)) {
                        $userData['status'] = 1;
                        $data['user'] = $this->Admin_manager->getUserAndProfileDetails($userData, $userlist);
                    }
                    $data['title'] = $this->lang->line('verify_image');
                    break;
                }
            case TYPE_15 : {
                    /* Get Incomplete Registration Users  */
                    $data['user'] = $this->Admin_manager->getInCompletedResgistrationUser(MEMBER_ROLE_ID);
                    $data['title'] = 'Incomplete' . ' ' . $this->lang->line('users');
                    break;
                }
            
            Default: {
                    $data = array();
                }
        }
        return $data;
    }

}
