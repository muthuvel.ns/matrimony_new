<?php

Class Search_model extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->model('Form_model', '', TRUE);
        $this->load->model('Profilemanager_model', '', TRUE);
    }

    function template($searchInfo) {
        $html = '';
        $caste_name = '';
        if (!empty($searchInfo)) {
            foreach ($searchInfo as $v) {
                $casteDetails = $this->Form_model->casteDetails($v['caste']);
                if (!empty($casteDetails[0]['name'])) {
                    $caste_name = $casteDetails[0]['name'];
                }

                /** get active  and verified image  if not active then get default image */
                $getUserImagesInfo = $this->Profilemanager_model->getActiveProfileImage($v['userGuid'], $active = 1, $verified = 1);
                $imagename = $getUserImagesInfo['image'];
                $image = base_url() . 'assets/upload_images/' . $imagename;
                $profile = base_url() . 'index.php/profile/view?pid=' . $v['userGuid'];
                $html.='  <li class="col-sm-4 ">
                      <div> 
		                    <ul>
                           <li class="profile_item-img">
		                      	  <img src="' . $image . '" width="75" height="75" class="img-responsive img_profile" alt=""/>
		                       </li>
		                       <li class="profile_item-desc">
		                         <h4>M' . $v["userId"] . '</h4>
		                        	  <p>' . $v["age"] . 'years </br>' . $v["height"] . 'cms | ' . $caste_name . '</p>
		                         	  <h5><a href="' . $profile . '">View Full Profile</a></h5>
		                       </li>
                         </ul>
                       </div>		  
                     </li>';
            }
        } else {
            $html.="No Matches";
        }
        echo $html;
        return;
    }

}

?>
