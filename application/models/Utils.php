<?php

Class Utils extends CI_Model {

    function getGuid() {
        $ipaddress = $_SERVER['SERVER_ADDR'];

        /** Get guid with lower case and md5  */
        $guid = strtolower(md5(uniqid((( (double) microtime() * 10000 ) . $ipaddress . 'web'), true)));

        /** Retrun guid */
        return $guid;
    }

    function getRandomString($strLength = 8, $splSymbol = false) {
        $randString = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        $generateRandomString = '';

        if ($splSymbol) {
            $randString .= '!#$%@&*-_=+';
        }

        for ($i = 0; $i < $strLength; $i++) {
            $generateRandomString .= $randString{rand(0, strlen($randString) - 1)};
        }

        /** return as string * */
        return $generateRandomString;
    }

    function sendHtmlMail($to = '', $from = '', $subject = '', $message = '', $attachment = '', $cc = '') {
        // load Email Library
        $this->load->library('email');

        $config['mailtype'] = 'html';
        $config['wordwrap'] = TRUE;

        $this->email->initialize($config);

        $this->email->to($to);
        $this->email->from($from);
        $this->email->cc($cc);
        $this->email->subject($subject);
        $this->email->message($message);
        if ($attachment == 1) {
            $this->email->attach('invoice.pdf');
        }
        if (!$this->email->send()) {
            echo $this->email->print_debugger();
            exit;
        }

        return true;
    }

    /**
     * @uses MultiArray Sorting / arraysorting / Ascending / Descending / sorting case-insensitive
     * 			SORT_DESC 	Descending Order
     * 			SORT_ASC	Ascending Order
     * @example
     * 			$arr1 = array(
     * 							array('id'=>1,'name'=>'A','cat'=>'F','dog'=>'p' ),
     * 							array('id'=>2,'name'=>'b','cat'=>'E','dog'=>'q' ),
     * 							array('id'=>2,'name'=>'B','cat'=>'e','dog'=>'r' ),
     * 							array('id'=>4,'name'=>'D','cat'=>'G','dog'=>'s' )
     * 							);
     *
     * 			$arr2 = array_msort($arr1, array('id'=>SORT_DESC,'name'=>SORT_ASC, 'cat'=>SORT_ASC, 'dog'=>SORT_DESC));
     *
     * 	Output:
     * 			$arr1 = array(
     * 							array('id'=>4,'name'=>'D','cat'=>'G','dog'=>'s' ),
     * 							array('id'=>2,'name'=>'B','cat'=>'e','dog'=>'r' ),
     * 							array('id'=>2,'name'=>'b','cat'=>'E','dog'=>'q' ),
     * 							array('id'=>1,'name'=>'A','cat'=>'F','dog'=>'p' )
     * 							);
     *
     * Ex:	$sortedArray = array_msort($unSortedArray, array('arrayKey1'=>SORT_ASC, 'arrayKey2'=>SORT_DESC,);
     *
     * @param unknown $array	Multi/single array.
     * @param unknown $cols		array('key' => SORT_ASC/SORT_DESC , ..... )
     * @return Sorted Array multitype:unknown
     */
    public function array_msort($array, $cols) {
        $colarr = array();

        foreach ($cols as $col => $order) {
            $colarr[$col] = array();
            foreach ($array as $k => $row) {
                $colarr[$col]['_' . $k] = strtolower($row[$col]);
            }
        }

        $eval = 'array_multisort(';
        foreach ($cols as $col => $order) {
            $eval .= '$colarr[\'' . $col . '\'],' . $order . ',';
        }
        $eval = substr($eval, 0, -1) . ');';
        eval($eval);

        $ret = array();
        foreach ($colarr as $col => $arr) {
            foreach ($arr as $k => $v) {
                $k = substr($k, 1);
                if (!isset($ret[$k]))
                    $ret[$k] = $array[$k];
                $ret[$k][$col] = $array[$k][$col];
            }
        }
        return $ret;
    }

    public function folderpermission($folderPath) {

        $folderGeneration = false;
        /** Validate the inputs * */
        if (empty($folderPath))
            return $folderGeneration;

        /** Create Folder if folder has write permission * */
        if (!file_exists($folderPath)) {
            mkdir($folderPath, 0777, true);
        }

        /**  If create directory exists then $folderGeneration value is true* */
        if (is_dir($folderPath) && file_exists($folderPath)) {
// 			chmod($folderPath, 0777);
// 			chmod($folderPath, 0755);
            $folderGeneration = true;
        }

        return $folderGeneration;
    }

}

?>
