<?php

Class Success_model extends CI_Model {

    public function insertSuccessInfo($data) {
        $result = 0;
        if (empty($data)) {
            return $result;
        }

        $result = $this->db->insert('success_story', $data);
        return $result;
    }

    public function updateSuccessInfo($id, $data) {
        $result = 0;
        if (empty($id) || empty($data)) {
            return $result;
        }

        $this->db->where('id', $id);
        $result = $this->db->update('success_story', $data);
        return $result;
    }

    public function getUserSuccessInfo($memberid = '', $partnerid = '', $id = '', $limits='') {
        $this->db->select('*');

        if ($id) {
            $this->db->where_in('id', $id);
        }
        if ($memberid) {
            $this->db->where_in('member_id', $memberid);
        }

        if ($partnerid) {
            $this->db->where_in('partner_id', $partnerid);
        }
        
        if ($limits) {
        	$this->db->limit($limits);
        }

        $this->db->where('deleted', 0);
        $this->db->order_by('created', 'DESC');
        $query = $this->db->get('success_story');
        return $query->result_array();
    }

    /**
     * get verified and not verified story information
     * @param type $memberid
     * @param type $id
     * @return type
     */
    public function getAllUserSuccessInfo($memberid = '', $id = '', $partnerid = '') {
        $this->db->select('*');

        if ($id) {
            $this->db->where_in('id', $id);
        }
        if ($memberid) {
            $this->db->where_in('member_id', $memberid);
        }
        if ($partnerid) {
            $this->db->where_in('partner_id', $partnerid);
        }
        $this->db->order_by('created', 'DESC');
        $query = $this->db->get('success_story');
        return $query->result_array();
    }

}

?>
