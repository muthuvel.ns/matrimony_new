<!DOCTYPE HTML>
<html>
    <head>
        <title>Matrimonial</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="keywords" content="Marital Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template, 
              Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
        <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
        <link href="<?php echo base_url(); ?>assets/css/bootstrap-3.3.4/dist/css/bootstrap.min.css" rel='stylesheet' type='text/css' />
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="<?php echo base_url(); ?>assets/js/jquery-2.1.4.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
        <!-- Custom Theme files -->
        <link href="<?php echo base_url(); ?>assets/css/setting.css" rel='stylesheet' type='text/css' />
        <link href="<?php echo base_url(); ?>assets/css/shortlist.css" rel='stylesheet' type='text/css' />
        <link href="<?php echo base_url(); ?>assets/css/style.css" rel='stylesheet' type='text/css' />
        <link href="<?php echo base_url(); ?>assets/css/your.css" rel='stylesheet' type='text/css' />
        <link href='//fonts.googleapis.com/css?family=Oswald:300,400,700' rel='stylesheet' type='text/css'>
        <link href='//fonts.googleapis.com/css?family=Ubuntu:300,400,500,700' rel='stylesheet' type='text/css'>
        <!--uploadfile-->
        <link href="<?php echo base_url(); ?>assets/css/fileinput.css" media="all" rel="stylesheet" type="text/css" />

        <!--uploadfileend-->
        <!--top scroller-->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/topscroll/css/style.css"> <!-- Gem style -->

        <!----font-Awesome----->
        <link href="<?php echo base_url(); ?>assets/css/font-awesome.css" rel="stylesheet"> 
        <!----font-Awesome----->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css"> 
    <input type="hidden" id="baseurl" value="<?php echo base_url(); ?>"/>

    <script>
        $(document).ready(function () {
            $(".dropdown").hover(
                    function () {
                        $('.dropdown-menu', this).stop(true, true).slideDown("fast");
                        $(this).toggleClass('open');
                    },
                    function () {
                        $('.dropdown-menu', this).stop(true, true).slideUp("fast");
                        $(this).toggleClass('open');
                    }
            );

            $(function () {
                $(this).bind("contextmenu", function (e) {
                    e.preventDefault();
                });
            });
            var url = window.location;
			    var element = $('ul.nav a').filter(function() {
			        return this.href == url;// || url.href.indexOf(this.href) == 0;
			    }).addClass('navactive').parent().parent().addClass('in').parent();
			    if (element.is('li')) {
			    
			        element.addClass('navactive');
			    }
        });

        var baseurl = $('#baseurl').val();

        function forgetpassword() {
            var email = $('#email_id').val( );
            if (email == '') {
                alertmessage('please enter email id'); //going to function
                return false;
            }
            $.ajax({
                type: "POST",
                url: baseurl + 'index.php/login/fogotpassword',
                data: $("#fgt").serialize(), // serializes the form's elements.
                success: function (data) {
                    $('#email_id').val('');
                    if ($.trim(data) == "mail") {
                        $('#fgt_email').hide();
                        $('#fgt_btn').hide();
                        data = 'Mail send successfully please check your Inbox or Spam Folder';
                        $('#alert_msg').show();
                        $('#alert_msg').html(data);
                        setTimeout(function () {
                            $('#modal').hide();
                            location.reload();
                        }, 3000);

                    } else {
                        $(function () {
                            $('#alert_msg').show();
                            $('#alert_msg').html(data);
                            setTimeout(function () {
                                $('#alert_msg').hide();
                            }, 3000);
                        });
                    }
                }
            });
        }
        function alertmessage(data) {
            $('#alert_msg').show();
            $('#alert_msg').html(data);

            setTimeout(function () {
                $('#alert_msg').hide();
            }, 3000);
        }
    </script>


    <!--forget PW popoup form-->
    <style>
        .email_check{display: none;}
        .email_check > p {
            font-size: 16px;
            margin-left: 5%;
            margin-right: 5%;
            text-align: justify;
        }
    </style>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/pop/rmodal.css" type="text/css" />
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/pop/rmodal.js"></script>

    <!--forget PW popup from end-->

    <!--seeting popup-->
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>/assets/pop/popModal.css">
    <!--script src="http://cdn.jsdelivr.net/jquery/3.0.0-beta1/jquery.min.js"></script-->
    <script src="<?php echo base_url(); ?>/assets/pop/popModal.js"></script>
    <script>
        $(function () {
            $('#popModal_ex1').click(function () {
                $('#popModal_ex1').popModal({
                    html: $('#content'),
                    placement: 'bottomLeft',
                    showCloseBut: true,
                    onDocumentClickClose: true,
                    onDocumentClickClosePrevent: '',
                    overflowContent: false,
                    inline: true,
                    asMenu: false,
                    beforeLoadingContent: 'Please, wait...',
                    onOkBut: function () {
                    },
                    onCancelBut: function () {
                    },
                    onLoad: function () {
                    },
                    onClose: function () {
                    }
                });
            });
            /* tab */
            (function ($) {
                $.fn.tab = function (method) {

                    var methods = {
                        init: function (params) {

                            $('.tab').click(function () {
                                var curPage = $(this).attr('data-tab');
                                $(this).parent().find('> .tab').each(function () {
                                    $(this).removeClass('active');
                                });
                                $(this).parent().find('+ .page_container > .page').each(function () {
                                    $(this).removeClass('active');
                                });
                                $(this).addClass('active');
                                $('.page[data-page="' + curPage + '"]').addClass('active');
                            });

                        }
                    };

                    if (methods[method]) {
                        return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
                    } else if (typeof method === 'object' || !method) {
                        return methods.init.apply(this, arguments);
                    }

                };
                $('html').tab();

            })(jQuery);

        });
    </script>
    <!--tap using jquery-1.9.1.js-->
    <script src="<?php echo base_url(); ?>assets/js/jquery-1.9.1.js"; type="text/javascript"></script>
    <!--setting end-->
    <script>
        /*$(document).ready(function () {
         $(".notificationicon").click(function () {
         $("#notificationMenu").show();
         $(this).toggleClass("open");
         $("#notificationMenu").toggleClass("open");
         });
         });*/

        $(function () {
            $("#showModal").on('click', function () {
                $('#modal').show();
            });

            $("#close_modal").on('click', function () {
                $('#modal').hide();
                $('#email_id').val('');
            });
        });

    </script>

    <!--popup end-->
</head>
<body>
    <main class="cd-container">
        <!-- ============================  Navigation Start =========================== -->
        <div class="navbar navbar-inverse-blue navbar">
            <!--<div class="navbar navbar-inverse-blue navbar-fixed-top">-->
            <div class="container">

                <div class="pull-left"><a class="brand" href=""><img src="<?php echo base_url(); ?>assets/images/1.png" alt="logo" width="125"></a></div>
                <div class="pull-right"> 
                    <div class="row form-inline">
                        <?php
                        if (!isset($this->session->userdata['userguid']) || empty($this->session->userdata['userguid'])) {
                            if (!empty($message)) {
                                ?>
                                <script>
                                    $(function () {
                                        $('#success_msg').show();
                                        setTimeout(function () {
                                            $('#success_msg').hide();
                                        }, 3000);
                                    });
                                </script>
                                <div class="error error_text" id="success_msg" style="text-align: center; display:none; padding-buttom:10px;"><?php echo $message; ?></div>
                            <?php } ?>

                            <!-- message popup-->

                            <div id="modal1" class="modal" style=" width: 100%;">
                                <div class="modal-dialog animated sample" style=" margin-top: 9%;width: 540px;">
                                    <div class="modal-content1">

                                    </div>
                                </div>
                            </div>
                            <!-- message popup end-->

                            <!--forget password popup-->
                            <div id="modal" class="modal" style=" width: 100%;">
                                <div class="modal-dialog animated sample" style=" margin-top: 9%;width: 440px;">
                                    <div class="modal-content forget-content">
                                        <div class="social_login">
                                            <form class="form-horizontal"  action="" method="post" id="fgt">
                                                <div class="modal-header forget_header">
                                                    <strong><?php echo $this->lang->line('forgot_your_password'); ?></strong>
                                                    <span style="float:right;"  id="close_modal"><a href="#"><img src="<?php echo base_url(); ?>assets/images/m/forgot-password-close.gif"></a></span>
                                                </div>
                                                </br>
                                                <div class="form-group" style="">
                                                    <span style="" class="for_txt" id="alert_msg" style="display:none;"></span>
                                                    <div class="col-lg-8" id="fgt_email">
                                                        <input style="width: 330px; text-transform: lowercase;" class="form-control forget_text"  id="email_id" name="email" value="" type="text" placeholder="Registered email-id" required oninvalid="this.setCustomValidity('Please enter valid email address')" oninput="setCustomValidity('')" >
                                                    </div>
                                                </div>
                                                <button class="btn btn-warning forget_btn" type="button" id="fgt_btn" onclick="forgetpassword()"><?php echo $this->lang->line('reset_password'); ?></button>
                                            </form>
                                            </br>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!--forget password end-->

                            <form class="form-inline" action="<?php echo base_url(); ?>index.php/login" method="post">
                                <!--fieldset >
                                <!--legend align="right">Member Login</legend-->
                                <div class="form-group"><input   style="text-transform: lowercase;" type="email" name="username" placeholder="user mail id" class="form-control" required pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" oninvalid="this.setCustomValidity('Please enter valid email address')" oninput="setCustomValidity('')"></div>
                                <div class="form-group"><input type="password" name="password" placeholder="Password" class="form-control" required oninvalid="this.setCustomValidity('Please enter password')" oninput="setCustomValidity('')"></div>
                                <div class="form-group"><input type="submit" value="Login" class="btn btn-default subbtn"></div>
                                <div class="center-middle"><a id="showModal"><?php echo $this->lang->line('forget_password'); ?></a></div>
                                <!--/fieldset-->
                            </form>
                            <?php
                        } elseif (isset($this->session->userdata['roleguid']) && !empty($this->session->userdata['roleguid'])) {
                            $username = (!empty($this->session->userdata['username']) ? $this->session->userdata['username'] : '');
                            $image = (!empty($this->session->userdata['image']) ? $this->session->userdata['image'] : 'default.png');
                            ?>
                            <ul class="">
                                <li class="col-md-2 profile_image">
                                    <?php if (file_exists(dirname($_SERVER["SCRIPT_FILENAME"]) . "/assets/upload_images/" . $image)) { ?>
                                        <img src="<?php echo base_url() . 'assets/upload_images/' . $image; ?>">
                                    <?php } ?>
                                <!--<img src="<?php //echo base_url().'assets/upload_images/'.$image;    ?>">-->

                                </li>
                                <li class="col-md-2 detail textuser"><p style="font-size: 18px;"><?php echo $username; ?></p></li>

                                <!--setting popup-->
                                <li class="col-md-1">
                                    <div class="page active" data-page="popModal">
                                        <div class="exampleContainer">
                                            <div class="exampleLive">
                                                <span id="popModal_ex1" class="pointer"><a href="#"><i class="fa fa-cogs" aria-hidden="true"></i></a></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div style="display:none">
                                        <div id="content">
                                            <div class="pro_edit">
                                                <a href="<?php echo base_url(); ?>index.php/profile/"><i class="fa fa-pencil"></i><span class="set_text"><?php echo $this->lang->line("edit_profile"); ?></span></a>
                                            </div>
                                            <!--<div class="part_edit">
                                            <a href="<?php echo base_url(); ?>index.php/profile/partner_edit"><i class="fa fa-user"></i>
                                            <span class="set_text">Edit Partner Preference</span> </a>
                                            </div>-->
                                            <div class="part_edit">
                                                <a href="<?php echo base_url(); ?>index.php/profile/photo_edit"><i class="fa fa-user"></i>
                                                    <span class="set_text"><?php echo $this->lang->line("edit_photo"); ?> </span></a>
                                            </div>
                                            <div class="part_edit">
                                                <a href="<?php echo base_url(); ?>index.php/profile/setting"><i class="fa fa-cog"></i>
                                                    <span class="set_text"><?php echo $this->lang->line("setting"); ?></span> </a>
                                            </div>
                                            <div class="part_edit">
                                                <a href="<?php echo base_url() . 'index.php/logout' ?>"><i class="fa fa-sign-out"></i><span class="set_text"><?php echo $this->lang->line("logout"); ?> </span></a>
                                            </div>
                                        </div>
                                    </div>
                                </li>

                                <!--setting end-->
                                <!--popup-->
                                <li>
                                    <div class="contain">
                                    <!--    <a href="#" class="button notificationicon on"><i class="fa fa-bell" ></i>10</a>-->

                                        <ul id="notificationMenu" class="notifications" style="display:none">
                                            <li class="titlebar">
                                                <span class="title">Notifications</span>
                                                <span class="settings"><i class="icon-cog"></i>
                                                </span>
                                            </li>
                                            <!-- <div class="notifbox">
                                                    <li class=" notif unread">
                                                    <a href="#">
                                                            <div class="imageblock"> 
                                                            <img src="<?php //echo base_url();    ?>assets/images/p13.jpg" class="notifimage"  />
                                                            </div> 
                                                            <div class="messageblock">
                                                            <div class="message">This guy <strong>Jonesy</strong> has just built a tower.
                                                            </div>
                                                            <div class="messageinfo">
                                                                    <i class="icon-trophy"></i>2 hours ago
                                                            </div>
                                                            </div>
                                                    </a>
                                                    </li>
                                                    <li class=" notif unread">
                                                    <a href="#">
                                                            <div class="imageblock">
                                                            <img src="<?php echo base_url(); ?>assets/images/p13.jpg" class="notifimage"  />
                                                            </div> 
                                                            <div class="messageblock">
                                                            <div class="message">
                                                                    <strong>Pete Nawara</strong> want's to drink beer with you
                                                            </div>
                                                            <div class="messageaction">
                                                                    <a class="button tiny success">accept</a> 
                                                                    <a class="button tiny alert">decline</a>
                                                            </div>
                                                            <div class="messageinfo">
                                                                    <i class="icon-flag"></i>3 hour ago
                                                            </div>
                                                            </div>
                                                    </a>
                                                    </li>
                                                    <li class=" notif">
                                                    <a href="#">
                                                            <div class="imageblock"><img src="<?php echo base_url(); ?>assets/images/p13.jpg" class="notifimage" />
                                                            </div> 
                                                            <div class="messageblock">
                                                            <div class="message"><strong>Gary LaPlante</strong> has talked some trash: "<em>Buncha bitches.</em>
                                                            </div>
                                                            <div class="messageinfo">
                                                                    <i class="icon-comment"></i>4 hours ago
                                                            </div>
                                                            </div>
                                                    </a>
                                                    </li>
                                                    <li class=" notif">
                                                    <a href="#">
                                                            <div class="imageblock"><img src="<?php echo base_url(); ?>assets/images/p13.jpg" class="notifimage" /></div> 
                                                            <div class="messageblock">
                                                            <div class="message"><strong>Jason Nawara</strong> is probably playing <strong>Smite</strong> right now.
                                                            </div>
                                                            <div class="messageinfo"><i class="icon-trophy"></i>Yesterday</div>
                                                            </div>
                                                    </a>
                                                    </li>
                                            
                                                    <li class=" notif">
                                                    <a href="#">
                                                            <div class="imageblock"><img src="<?php echo base_url(); ?>assets/images/p13.jpg" class="notifimage"  /></div> 
                                                            <div class="messageblock">
                                                            <div class="message"><strong>Roidberg</strong> left you a comment: "<em>Hey buddy! Nice toenails!"</em></div>
                                                            <div class="messageinfo"><i class="icon-comment"></i>2 hours ago</div>
                                                            </div>
                                                    </a>
                                                    </li>
                                            </div> -->
                                            <li class="seeall">
                                                <a href="<?php echo base_url(); ?>index.php/massage/notification">See All</a>
                                            </li>
                                        </ul>
                                    </div>
                                </li></ul>
                            <!--popup end-->
                        <?php } ?>
                    </div>
                </div>
            </div>
            <div class="navbar-inner">
                <div class="container">
                    <div class="navigation">
                        <nav id="colorNav">
                            <ul>
                                <!--li class="green">
                                        <a href="#" class="icon-home"></a>
                                        <ul>
                                                <li><a href="login.html">Login</a></li>
                                                <li><a href="register.html">Register</a></li>
                                                <li><a href="index.html">Logout</a></li>
                                        </ul>
                                </li-->
                            </ul>
                        </nav>
                    </div>
                    <!--a class="brand" href="index.html"><img src="images/logo.png" alt="logo"></a-->
                    <div class="pull-left">
                        <nav class="navbar nav_bottom" role="navigation">
                            <!-- Brand and toggle get grouped for better mobile display -->
                            <div class="navbar-header nav_2">
                                <button type="button" class="navbar-toggle collapsed navbar-toggle1" data-toggle="collapse" data-target="#bs-megadropdown-tabs">Menu
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                                <a class="navbar-brand" href="#"></a>
                            </div> 
                            <!-- Collect the nav links, forms, and other content for toggling -->
                            <div class="collapse navbar-collapse" id="bs-megadropdown-tabs">
                                <ul class="nav navbar-nav nav_1">
                                    <li><a href="<?php echo base_url(); ?>index.php/home/index"><?php echo $this->lang->line("header_home"); ?></a></li>
                                    <li><a href="<?php echo base_url(); ?>index.php/about/about_us"><?php echo $this->lang->line("header_about"); ?></a></li>
                                    <?php if (isset($this->session->userdata['userguid']) && !empty($this->session->userdata['userguid'])) { ?>
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo $this->lang->line("header_matches"); ?><span class="caret"></span></a>
                                            <ul class="dropdown-menu" role="menu">
                                                <li><a href="<?php echo base_url(); ?>index.php/matches/index"><?php echo $this->lang->line("header_matches_1"); ?></a></li>
                                                <li><a href="<?php echo base_url(); ?>index.php/profile/viewed"><?php echo $this->lang->line("header_matches_2"); ?></a></li>
                                                <li><a href="<?php echo base_url(); ?>index.php/profile/profileviewed"><?php echo $this->lang->line("header_matches_3"); ?></a></li>
                                                <li><a href="<?php echo base_url(); ?>index.php/matches/shortlist"><?php echo $this->lang->line("header_matches_4"); ?></a></li>
                                                <li><a href="<?php echo base_url(); ?>index.php/matches/ignore_list"><?php echo $this->lang->line("header_matches_5"); ?></a></li>
                                                <li><a href="<?php echo base_url(); ?>index.php/matches/mutual_matches"><?php echo $this->lang->line("header_matches_6"); ?></a></li>
                                            </ul>
                                        </li>
                                        <!--li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Search<span class="caret"></span></a>
                                        <ul class="dropdown-menu" role="menu">
                                                <li><a href="search.html">Regular Search</a></li>
                                                <li><a href="profile.html">Recently Viewed Profiles</a></li>
                                                <li><a href="search-id.html">Search By Profile ID</a></li>
                                                <li><a href="faq.html">Faq</a></li>
                                                <li><a href="shortcodes.html">Shortcodes</a></li>
                                        </ul>
                                        </li>-->
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo $this->lang->line("header_message"); ?><span class="caret"></span></a>
                                            <ul class="dropdown-menu" role="menu">
                                                <li><a href="<?php echo base_url(); ?>index.php/message/inbox"><?php echo $this->lang->line("header_message_1"); ?></a></li>
                                                <!-- <li><a href="#">New</a></li>
                                                        <li><a href="inbox.html">Accepted</a></li>-->
                                                <li><a href="<?php echo base_url(); ?>index.php/message/send"><?php echo $this->lang->line("header_message_2"); ?></a></li>
                                            </ul>
                                        </li>
                                        <li class="last"><a href="<?php echo base_url(); ?>index.php/search"><?php echo $this->lang->line("header_search"); ?></a></li>
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo $this->lang->line("header_upgrade"); ?><span class="caret"></span></a>
                                            <ul class="dropdown-menu" role="menu">
                                                <li><a href="<?php echo base_url(); ?>index.php/payment"><?php echo $this->lang->line("header_payment"); ?></a></li>
                                            </ul>
                                        </li>
                                    <?php } ?>
                                    <li class="last"><a href="<?php echo base_url(); ?>index.php/contact/contact_us"><?php echo $this->lang->line("header_contact"); ?></a></li>
                                    <li class="last"><a href="<?php echo base_url(); ?>index.php/faq/faqs"><?php echo $this->lang->line("header_fqas"); ?></a></li>
                                </ul>
                            </div><!-- /.navbar-collapse -->
                        </nav>
                    </div> <!-- end pull-right -->
                    <div class="clearfix"> </div>
                </div> <!-- end container -->
            </div> <!-- end navbar-inner -->
        </div> <!-- end navbar-inverse-blue -->
        <!-- ============================  Navigation End ============================ -->
