<script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/pagination/js/jquery.pajinate.js"></script>
<script>
    $(document).ready(function () {
        $('#paging_container_ignore').pajinate({
            num_page_links_to_display: 3,
            items_per_page: 2
        });
    });



</script>
<div class="grid_3">
    <div class="container">
        <div class="breadcrumb1">
            <ul>
                <a href="<?php echo base_url(); ?>index.php/matches/view/"><i class="fa fa-home home_1"></i></a>
                <span class="divider">&nbsp;|&nbsp;</span>
                <li class="current-page"><?php echo $this->lang->line('ignored_text'); ?> <?php echo sprintf($this->lang->line('profile_text'), ''); ?> (<?php echo (!empty($ignoreList) ? count($ignoreList) : 0) ?>)</li>
            </ul>
        </div>
        <div class="col-md-3 col_5">
            <ul class="menu">
                <li class="item1"><h3 class="m_2"><?php echo $this->lang->line('activity_text'); ?></h3>
                    <ul class="cute">
                        <li class="subitem1"><a href="<?php echo base_url(); ?>index.php/matches/shortlist"><?php echo $this->lang->line('profiles_text') . ' ' . $this->lang->line('shortlished_text'); ?></a></li>
                        <li class="subitem2"><a href="<?php echo base_url(); ?>index.php/matches/ignore_list"><?php echo $this->lang->line('profiles_text') . ' ' . $this->lang->line('ignored_text'); ?></a></li>
                    </ul>
                </li>

            </ul>
            <?php if ($offer == 2) { ?>		
                <div class="pay left sendleft">
                    <div class="leftpay">
                        <span class="leftpaytxt" style="color: #fff;"><?php echo $this->lang->line("payment_text"); ?> </span>
                    </div>	
                    <div class=" leftpay pointer btnlink" >
                        <span class="btnlinkalign ">
                            <a href="<?php echo base_url(); ?>index.php/payment"><?php echo $this->lang->line('payment'); ?></a></span>
                    </div>
                </div>
            <?php } ?>

        </div>
        <div class="col-md-9 profile_left">
            <div class="col_4">
                <div class="tab_box tab_box1">
                    <!--				      <h1>Ignored Profile</h1>-->
                    <p></p>
                </div>

                <div id="paging_container_ignore">
                    <?php if (!empty($ignoreList)) { ?>
                        <div class="page_navigation pagination pagination_1"></div>	    
                        <!--<div class="short_send">
                                <input value="0" id="selectall" type="checkbox">
                                <div class="ignore_btn_del vertical">delete</div>
                        </div>-->
                        <div class="clearfix"> </div>
                        <div class="content">
                            <?php foreach ($ignoreList as $key => $value) { ?>
                                <div class="jobs-item with-thumb box_profile">
                                    <div class="check_name">
                                        <label class="short_checkbox">
                                        <!--<input value="1"  class="case" type="checkbox">-->
                                            <span class="sh_name"><?php echo (!empty($value['username']) ? $value['username'] : '') ?></span>
                                        </label>
                                    </div>
                                    <div class="thumb_top">
                                        <div class="thumb short_img"><a href="#"><img src="<?php echo base_url() . 'assets/upload_images/' . $value['image'] ?>" class="img-responsive" alt=""/></a></div>
                                        <div class="jobs_right short_right">
                                            <h6 class="title"><a href="<?php echo base_url() . 'index.php/profile/view?pid=' . $value['userGuid'] ?>"><?php echo (!empty($value['username']) ? $value['username'] : '') . ' ( ' . constant('MEMBERID') . $value['userId'] . ' )'; ?></a></h6>
                                            <ul class="top-btns short_top_btn">
                                                <!-- <li><a href="#" class="fa fa-trash fa-short"></a></li>
                                               <li><a href="#" class="fa fa-twitter"></a></li>
                                                <li><a href="#" class="fa fa-google-plus"></a></li>-->
                                            </ul>
                                            <ul class="login_details1">
                                                <li><?php echo sprintf($this->lang->line('last_text'), $this->lang->line('text_login')); ?>  :  <?php echo $value['log_in']; ?></li>
                                            </ul>
                                            <table class="table_short">
                                                <tbody>
                                                    <tr class="opened_1">
                                                        <td class="short_label1"><?php echo $this->lang->line('search_profile_for_age'); ?> / <?php echo $this->lang->line('register_profile_for_height'); ?> </td>
                                                        <td>:</td>
                                                        <td class="short_value"><?php echo (!empty($value['age']) ? $value['age'] . ' Yrs,' : 'not specified') . ' ' . (!empty($value['height']) ? $value['height'] . ' Cm' : 'not specified'); ?> </td>
                                                    </tr>
                                                    <tr class="opened">
                                                        <td class="short_label1"><?php echo $this->lang->line('register_profile_for_religion'); ?>  </td>
                                                        <td>:</td>
                                                        <td class="short_value"><?php echo (!empty($value['religion']) ? $value['religion'] : 'not specified') ?></td>
                                                    </tr>
                                                    <tr class="opened">
                                                        <td class="short_label1"><?php echo $this->lang->line('register_profile_for_gender'); ?> </td>
                                                        <td>:</td>
                                                        <td class="short_value"><?php echo (!empty($value['gender']) ? constant("GENDER_" . strtoupper($value['gender'])) : 'Not Specified'); ?></td>
                                                    </tr>
                                                    <tr class="opened">
                                                        <td class="short_label1"><?php echo $this->lang->line('register_profile_for_marriagestatus'); ?> </td>
                                                        <td>:</td>
                                                        <td class="short_value"><?php echo (!empty($value['martialstatus']) ? $value['martialstatus'] : 'not specified') ?></td>
                                                    </tr>
                                                    <tr class="opened">
                                                        <td class="short_label1"><?php echo $this->lang->line('register_profile_for_caste'); ?> </td>
                                                        <td>:</td>
                                                        <td class="short_value"><?php echo (!empty($value['caste_name']) ? $value['caste_name'] : 'not specified') ?></td>
                                                    </tr>
                                                    <tr class="closed">
                                                        <td class="short_label1"><?php echo sprintf($this->lang->line('search_profile_create'), $this->lang->line('by_text')); ?> </td>
                                                        <td>:</td>
                                                        <td class="short_value "><span><?php echo (!empty($value['profile_created']) ? $value['profile_created'] : 'not specified') ?></span></td>
                                                    </tr>
                                                    <tr class="closed">
                                                        <td class="short_label1"><?php echo $this->lang->line('search_education_title'); ?> </td>
                                                        <td>:</td>
                                                        <td class="short_value "><span><?php echo (!empty($value['edu_name']) ? $value['edu_name'] : 'not specified') . ' - ' . (!empty($value['qualification']) ? $value['qualification'] : 'not specified'); ?></span></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <p class="description"><a  class="m_3" href="<?php echo base_url() . 'index.php/profile/view?pid=' . $value['userGuid'] ?>" class="read-more"><?php echo $this->lang->line('text_view_profile'); ?></a></p>
                                        </div>
                                        <div class="clearfix"> </div>
                                    </div>
                                    <div class="thumb_bottom short_bottom">
                                        <!--<div class="buttons1">
                                           <div class="vertical" id="send">Send Mail</div>
                                           <div class="vertical" id="ignore" onclick="requestInfo('<?php echo $value['userGuid'] ?>', '<?php echo REQUEST_IGNORE; ?>')">Ignore</div>
                                           <div class="vertical" id="block" onclick="requestInfo('<?php echo $value['userGuid'] ?>', '<?php echo REQUEST_BLOCK; ?>')">Block</div>
                                        </div>-->
                                        <div class="clearfix"> </div>
                                    </div>
                                </div>
                            <?php }
                            ?>
                        </div>
                        <?php
                    } else {
                        echo '<h4>' . $this->lang->line('details_not_found') . '</h4>';
                    }
                    ?>				
                </div>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
</div>


<SCRIPT language="javascript">
    $(document).ready(function () {
        $("#selectall").change(function () {
            $(".case").prop('checked', $(this).prop("checked"));
        });
    });
</SCRIPT>
