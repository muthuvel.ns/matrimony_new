
<script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/pagination/js/jquery.pajinate.js"></script>
<script>
    $(document).ready(function () {
        $('#paging_container_match').pajinate({
            num_page_links_to_display: 3,
            items_per_page: 4
        });

    });


    $(function () {
        $(".modal-closeshort").on('click', function () {
            $('#modalshort').hide();
            $('#response').hide();
        });
        $(".modal-closepaid").on('click', function () {
            $('#modalpaid').hide();
        });
        $("#mail_close").on('click', function () {
            $('#modalpaid').hide();
        });
        $("#close_modal").on('click', function () {
            $('#modal').hide();
        });
    });
</script>
<!--sendmailpaid -->
<div id="modalpaid" class="modal">
    <div class="modal-dialog animated sample" style=" margin-top: 9%;width: 440px;">
        <div class="row modal-contentpaid">
            <div class="sendhead">
                <span style="float:left;"></span>
                <span><h3 class="subtitle" style="color: #7e7e7e;"><?php echo sprintf($this->lang->line('to_text'), $this->lang->line('profile_send_mail')); ?></h3></span>
                <span class="modal-closepaid shtclose" ><a href="#"><img src="<?php echo base_url(); ?>assets/images/m/forgot-password-close.gif"></a></span>
            </div>

            <div id="paid_mail">
                <div id='loading' class="loader" style="display: block; text-align: center; margin: 100px;">
                    <img src="<?php echo base_url(); ?>assets/images/loader.gif"/>
                </div>
            </div>
            <div id="send_mail" style="display:none;">
                <form id="mail">
                    <div class="form-group row">
                        <div class="col-sm-10 short-text">
                            <textarea class="form-control" id="message" name="message" rows="6" maxlength="250" minlength="50"" required></textarea>
                        </div>
                        <input type="hidden" name="userguid" id="uid" value="">
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-12">
                            <div class="col-sm-2 pull-right">
                                <span class="btn btn-warning" id="mail_close"><?php echo $this->lang->line('cancel_admin_register'); ?></span>
                            </div>
                            <div class="col-sm-2 pull-right">
                                <button class="btn btn-primary" type="submit"><?php echo $this->lang->line('register_profile_for_submit'); ?></button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!--sendemailendpaid -->
<!--Interest msg-->
<div id="response" class="modal">
    <div class="modal-dialog animated sample" style=" margin-top: 9%;width: 440px;">
        <div class="modal-contentshort">
            <div class="row">
                <div>
                    <span style="float:left;"></span>
                    <span><h3 class="subtitle" id="title"><?php echo $this->lang->line('profile_request_send'); ?> </h3></span>
                    <span class="modal-closeshort shtclose" ><a href="#"><img src="<?php echo base_url(); ?>assets/images/m/forgot-password-close.gif"></a></span>
                </div>
                <div class="form-group row">
                    <div class="col-sm-10" id="msg_body" style="text-align:center;">
                        <div id='loader' style='display:block'>
                            <img src="<?php echo base_url(); ?>assets/images/loading.gif"/> Loding ...
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--sInterest msg end-->

<link href="<?php echo base_url(); ?>/assets/css/slider.css" rel='stylesheet' type='text/css' />
<!--  Modal Start -->
<div id="modal" class="modal">
    <div class="modal-dialog animated">
        <div class="modal-content">
            <div class="modal-header">
                <strong><?php echo $this->lang->line('register_profile_for_process2'); ?></strong>
            </div>
            <div class="modal-body" id="modal-body">

            </div>
            <div class="modal-footer">
                <button class="btn btn-default" type="button" id="close_modal"><?php echo $this->lang->line('close_text'); ?></button>
            </div>
        </div>
    </div>
</div>
<!--  Modal End -->
<div class="grid_3">
    <div class="container">
        <div class="breadcrumb1">
            <ul>
                <a href="<?php echo base_url(); ?>index.php/matches/view/"><i class="fa fa-home home_1"></i></a>
                <span class="divider">&nbsp;|&nbsp;</span>
                <li class="current-page"><?php echo $this->lang->line('new_matches_heading'); ?> (<?php echo (!empty($new) ? count($new) : 0) ?>)</li>
            </ul>
        </div>
        <div class="col-md-9 profile_left2">

            <?php if (!empty($new)) { ?>
                <div id="paging_container_match">
                    <?php foreach ($new as $key => $value) {//echo "<pre>";print_r($value);
                        ?>

                        <div class="content">
                            <div class="profile_top"><a href="<?php echo base_url() . 'profile/view?pid=' . $value['userGuid']; ?>">
                                    <h2><?php echo (!empty($value['userId']) ? constant('MEMBERID') . $value['userId'] : '') ?> | <?php echo (!empty($value['profile_created']) ? sprintf($this->lang->line('search_profile_create'), $this->lang->line('for_text')) . ' ' . $value['profile_created'] : 'Not Specified'); ?></h2>
                                    <div class="col-sm-3 profile_left-top">
                                        <a href="#" onclick="new_matche('<?php echo (!empty($value['userGuid']) ? $value['userGuid'] : ''); ?>')">
                                            <img src="<?php echo base_url() . 'assets/upload_images/' . $value['image']; ?>" class="img-responsive" alt=""/>
                                        </a>
                                        <div class="imgreqphoto">
                                            <?php
                                            if (!empty($value['image_request'])) {
                                                if (!empty($value['imagerequest'])) {
                                                    ?>
                                                    <div class="vertical" ><?php echo $this->lang->line('profile_request_send'); ?></div>
                                                <?php } else { ?>
                                                    <div class="vertical pointer "  onclick="requestProfile('<?php echo $value['userGuid']; ?>', '<?php echo REQUEST_IMAGE; ?>')"><?php echo $this->lang->line('profile_image_request'); ?></div>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <ul class="login_details1">
                                            <li><?php echo sprintf($this->lang->line('last_text'), $this->lang->line('text_login')); ?>  : <?php echo $value['log_in']; ?></li>
                                            <li><p><?php echo $value['summary']; ?></p></li>
                                        </ul>
                                    </div>
                                    <div class="col-sm-6">
                                        <table class="table_working_hours">
                                            <tbody>
                                                <tr class="opened">
                                                    <td class="day_label1"><?php echo $this->lang->line('register_profile_for_name'); ?>  :</td>
                                                    <td class="day_value"><?php echo (!empty($value['username']) ? $value['username'] : 'Not Specified'); ?></td>
                                                </tr>
                                                <tr class="opened">
                                                    <td class="day_label1"><?php echo $this->lang->line('search_profile_for_age'); ?> / <?php echo $this->lang->line('register_profile_for_height'); ?> :</td>
                                                    <td class="day_value"><?php echo (!empty($value['age']) ? $value['age'] : 'Not Specified') . ' / ' . (!empty($value['height']) ? $value['height'] . ' cm' : 'Not Specified'); ?></td>
                                                </tr>
                                                <tr class="opened">
                                                    <td class="day_label1"><?php echo sprintf($this->lang->line('last_text'), $this->lang->line('text_login')); ?> :</td>
                                                    <td class="day_value"><?php echo (!empty($value['log_in']) ? $value['log_in'] : 'Not Login') ?></td>
                                                </tr>
                                                <tr class="opened">
                                                    <td class="day_label1"><?php echo $this->lang->line('register_profile_for_religion'); ?> :</td>
                                                    <td class="day_value"><?php echo (!empty($value['religion']) ? $value['religion'] : 'Not Specified') ?></td>
                                                </tr>
                                                <tr class="opened">
                                                    <td class="day_label1"><?php echo $this->lang->line('register_profile_for_gender'); ?> :</td>
                                                    <td class="day_value"><?php echo (!empty($value['gender']) ? constant("GENDER_" . strtoupper($value['gender'])) : 'Not Specified'); ?></td>
                                                </tr>
                                                <tr class="opened">
                                                    <td class="day_label1"><?php echo $this->lang->line('register_profile_for_marriagestatus'); ?> :</td>
                                                    <td class="day_value"><?php echo (!empty($value['martial_status']) ? constant("MARITAL_STATUS_" . $value['martial_status']) : 'Not Specified'); ?></td>
                                                </tr>
                                                <tr class="closed">
                                                    <td class="day_label1"><?php echo sprintf($this->lang->line('search_profile_create'), $this->lang->line('by_text')); ?> :</td>
                                                    <td class="day_value closed"><span><?php echo (!empty($value['profile_created']) ? $value['profile_created'] : 'Not Specified'); ?></span></td>
                                                </tr>
                                                <tr class="closed">
                                                    <td class="day_label1"><?php echo $this->lang->line('search_education_title'); ?> :</td>
                                                    <td class="day_value closed"><span><?php echo (!empty($value['edu_name']) ? $value['edu_name'] : 'Not Specified') . ' - ' . (!empty($value['qualification']) ? ucfirst($value['qualification']) : 'Not Specified'); ?></span></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <p class="description"><a  class="m_3" href="<?php echo base_url() . 'index.php/profile/view?pid=' . $value['userGuid'] ?>" class="read-more"><?php echo $this->lang->line('text_view_profile'); ?></a></p>
                                        <div class="buttons">
                                            <div onclick="sendmail('<?php echo $value['userGuid']; ?>', '<?php echo $offer; ?>')" class="vertical pointer"><?php echo $this->lang->line('profile_send_mail'); ?></div>
                                            <?php
                                            if (!empty($value['interest'])) {
                                                if (($value['interest'] == REQUEST_INTEREST) || ($value['interest'] == REQUEST_ARRANGED) || ($value['interest'] == REQUEST_NOT_INETREST)) {
                                                    ?>
                                                    <div class="vertical"><?php echo $value['interest_name']; ?></div>
                                                <?php } else { ?>
                                                    <div class="vertical pointer" onclick="requestProfile('<?php echo $value['userGuid']; ?>', '<?php echo $value['interest']; ?>')"><?php echo $value['interest_name']; ?></div>
                                                <?php } ?>				
                                            <?php } else { ?>
                                                <div class="vertical pointer" onclick="requestProfile('<?php echo $value['userGuid']; ?>', '<?php echo REQUEST_INTEREST; ?>')"><?php echo $this->lang->line('profile_send_interest'); ?></div>
                                            <?php } if (!empty($value['shortlist'])) { ?>
                                                <div class="vertical pointer"  onclick="requestcancel('<?php echo $value['userGuid']; ?>', '<?php echo REQUEST_SHORTLIST; ?>')"><?php echo $this->lang->line('shortlished_text'); ?></div>
                                            <?php } else { ?>
                                                <div class="vertical pointer"  onclick="requestProfile('<?php echo $value['userGuid']; ?>', '<?php echo REQUEST_SHORTLIST; ?>')"><?php echo sprintf($this->lang->line('profile_shortlist'), ''); ?></div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <div class="clearfix"> </div>
                                </a></div>
                        </div>
                    <?php }
                    ?>
                    <div class="page_navigation pagination pagination_1"></div>
                </div>
                <?php
            } else {
                echo '<h4 style="text-align:center;"> ' . $this->lang->line("no_matches_text") . '' . $this->lang->line("no_new_matches") . '.</h4>';
            }
            ?>
        </div>

        <div class="col-md-3 match_right">
            <ul class="menu">
                <li class="item1"><h3 class="m_2"><?php echo $this->lang->line('search_title_1'); ?></h3>
                    <ul class="cute">

                        <li class="subitem1">
                            <form method="post" action="<?php echo base_url() . 'index.php/search/filtered'; ?>">
                                <input type="hidden" name="week" value="week">
                                <input type="hidden" name="caste" value="<?php echo (!empty($caste) ? $caste : ''); ?>">
                                <a onclick="$(this).closest('form').submit()"><?php echo sprintf($this->lang->line('search_with_text'), $this->lang->line('week_text')); ?> ( <?php echo $counts['week']; ?>  )</a>
                            </form>
                        </li>
                        <li class="subitem2">
                            <form method="post" action="<?php echo base_url() . 'index.php/search/filtered'; ?>">
                                <input type="hidden" name="month" value="month">
                                <input type="hidden" name="caste" value="<?php echo (!empty($caste) ? $caste : ''); ?>">
                                <a onclick="$(this).closest('form').submit()"><?php echo sprintf($this->lang->line('search_with_text'), $this->lang->line('month_text')); ?>  ( <?php echo $counts['month']; ?> ) </a>
                            </form>
                        </li>
                    </ul>
                </li>
                <!--<li class="item1"><h3 class="m_2">Profile type</h3>
                  <ul class="cute">
                        <li class="subitem1">
                                <form method="post" action="<?php echo base_url() . 'index.php/search/filtered'; ?>">
                                        <input type="hidden" name="photo" value="photo">
                                        <input type="hidden" name="caste" value="<?php echo (!empty($caste) ? $caste : ''); ?>">
                                        <a onclick="$(this).closest('form').submit()">with Photo </a>
                                </form>
                        </li>
                  </ul>
                </li>-->
                <li class="item1"><h3 class="m_2"><?php echo $this->lang->line('register_profile_for_marriagestatus'); ?></h3>
                    <ul class="cute">
                        <li class="subitem1">
                            <form method="post" action="<?php echo base_url() . 'index.php/search/filtered'; ?>">
                                <input type="hidden" name="maritalstatus" value="1">
                                <input type="hidden" name="caste" value="<?php echo (!empty($caste) ? $caste : ''); ?>">
                                <a onclick="$(this).closest('form').submit()"><?php echo $this->lang->line('register_profile_for_marriagestatus_1'); ?> ( <?php echo $counts['marital1']; ?> ) </a>
                            </form>
                        </li>
                        <li class="subitem1">
                            <form method="post" action="<?php echo base_url() . 'index.php/search/filtered'; ?>">
                                <input type="hidden" name="maritalstatus" value="2">
                                <input type="hidden" name="caste" value="<?php echo (!empty($caste) ? $caste : ''); ?>">
                                <a onclick="$(this).closest('form').submit()"><?php echo $this->lang->line('register_profile_for_marriagestatus_2'); ?> ( <?php echo $counts['marital2']; ?> ) </a>
                            </form>

                        </li>
                        <li class="subitem1">
                            <form method="post" action="<?php echo base_url() . 'index.php/search/filtered'; ?>">
                                <input type="hidden" name="maritalstatus" value="3">
                                <input type="hidden" name="caste" value="<?php echo (!empty($caste) ? $caste : ''); ?>">
                                <a onclick="$(this).closest('form').submit()"><?php echo $this->lang->line('register_profile_for_marriagestatus_3'); ?> ( <?php echo $counts['marital3']; ?> )</a>
                            </form>
                        </li>
                        <li class="subitem1">
                            <form method="post" action="<?php echo base_url() . 'index.php/search/filtered'; ?>">
                                <input type="hidden" name="maritalstatus" value="4">
                                <input type="hidden" name="caste" value="<?php echo (!empty($caste) ? $caste : ''); ?>">
                                <a onclick="$(this).closest('form').submit()"><?php echo $this->lang->line('register_profile_for_marriagestatus_4'); ?>( <?php echo $counts['marital4']; ?> )</a>
                            </form>
                        </li>
                    </ul>
                </li>
                <!-- <li class="item1"><h3 class="m_2">Mother Tongue</h3>
                  <ul class="cute">
                        <li class="subitem1">
                                <form method="post" action="<?php //echo base_url().'index.php/search/filtered';    ?>">
                                        <input type="hidden" name="created" value="myself">
                                        <input type="hidden" name="caste" value="<?php //echo (!empty( $caste )?$caste:'');    ?>">
                                        <a onclick="$(this).closest('form').submit()">Tamil </a>
                                </form>
                        </li>
                  </ul>
                </li>-->
                <li class="item1"><h3 class="m_2"><?php echo $this->lang->line('search_education_title'); ?></h3>
                    <ul class="cute">
                        <li class="subitem1">
                            <form method="post" action="<?php echo base_url() . 'index.php/search/filtered'; ?>">
                                <input type="hidden" name="education" value="1">
                                <input type="hidden" name="caste" value="<?php echo (!empty($caste) ? $caste : ''); ?>">
                                <a onclick="$(this).closest('form').submit()"><?php echo $this->lang->line('search_phd_text'); ?> ( <?php echo $counts['edu1']; ?> )			</a>
                            </form>
                        </li>
                        <li class="subitem1">
                            <form method="post" action="<?php echo base_url() . 'index.php/search/filtered'; ?>">
                                <input type="hidden" name="education" value="2">
                                <input type="hidden" name="caste" value="<?php echo (!empty($caste) ? $caste : ''); ?>">
                                <a onclick="$(this).closest('form').submit()"><?php echo $this->lang->line('search_post_graduate_text'); ?>  ( <?php echo $counts['edu2']; ?> )			</a>
                            </form>
                        </li>

                        <li class="subitem1">
                            <form method="post" action="<?php echo base_url() . 'index.php/search/filtered'; ?>">
                                <input type="hidden" name="education" value="3">
                                <input type="hidden" name="caste" value="<?php echo (!empty($caste) ? $caste : ''); ?>">
                                <a onclick="$(this).closest('form').submit()"><?php echo $this->lang->line('search_under_graduate_text'); ?>  ( <?php echo $counts['edu3']; ?> )			</a>
                            </form>
                        </li>
                        <li class="subitem1">
                            <form method="post" action="<?php echo base_url() . 'index.php/search/filtered'; ?>">
                                <input type="hidden" name="education" value="4">
                                <input type="hidden" name="caste" value="<?php echo (!empty($caste) ? $caste : ''); ?>">
                                <a onclick="$(this).closest('form').submit()"><?php echo $this->lang->line('search_diploma_text'); ?> ( <?php echo $counts['edu4']; ?> )			</a>
                            </form>
                        </li>
                        <li class="subitem1">
                            <form method="post" action="<?php echo base_url() . 'index.php/search/filtered'; ?>">
                                <input type="hidden" name="education" value="5">
                                <input type="hidden" name="caste" value="<?php echo (!empty($caste) ? $caste : ''); ?>">
                                <a onclick="$(this).closest('form').submit()"><?php echo $this->lang->line('register_profile_for_highesteducation'); ?> ( <?php echo $counts['edu5']; ?> )			</a>
                            </form>
                        </li>
                    </ul>
                </li>
                <li class="item1"><h3 class="m_2"><?php echo $this->lang->line('register_profile_for_occupation'); ?></h3>
                    <ul class="cute">
                        <li class="subitem1">
                            <form method="post" action="<?php echo base_url() . 'index.php/search/filtered'; ?>">
                                <input type="hidden" name="employeed" value="1">
                                <input type="hidden" name="caste" value="<?php echo (!empty($caste) ? $caste : ''); ?>">
                                <a onclick="$(this).closest('form').submit()"><?php echo $this->lang->line('register_profile_for_employeed_1'); ?> ( <?php echo $counts['emp_in1']; ?> )				</a>
                            </form>
                        </li>
                        <li class="subitem1">
                            <form method="post" action="<?php echo base_url() . 'index.php/search/filtered'; ?>">
                                <input type="hidden" name="employeed" value="2">
                                <input type="hidden" name="caste" value="<?php echo (!empty($caste) ? $caste : ''); ?>">
                                <a onclick="$(this).closest('form').submit()"><?php echo $this->lang->line('register_profile_for_employeed_2'); ?>	( <?php echo $counts['emp_in2']; ?> ) 					</a>
                            </form>
                        </li>
                        <li class="subitem1">
                            <form method="post" action="<?php echo base_url() . 'index.php/search/filtered'; ?>">
                                <input type="hidden" name="employeed" value="3">
                                <input type="hidden" name="caste" value="<?php echo (!empty($caste) ? $caste : ''); ?>">
                                <a onclick="$(this).closest('form').submit()"><?php echo $this->lang->line('register_profile_for_employeed_3'); ?> ( <?php echo $counts['emp_in3']; ?> ) 					</a>
                            </form>
                        </li>
                        <li class="subitem1">
                            <form method="post" action="<?php echo base_url() . 'index.php/search/filtered'; ?>">
                                <input type="hidden" name="employeed" value="4">
                                <input type="hidden" name="caste" value="<?php echo (!empty($caste) ? $caste : ''); ?>">
                                <a onclick="$(this).closest('form').submit()"><?php echo $this->lang->line('register_profile_for_employeed_4'); ?> ( <?php echo $counts['emp_in4']; ?> ) 					</a>
                            </form>
                        </li>
                    </ul>
                </li>
                <li class="item1"><h3 class="m_2"><?php echo $this->lang->line('register_profile_for_physicalstatus'); ?></h3>
                    <ul class="cute">
                        <li class="subitem1">
                            <form method="post" action="<?php echo base_url() . 'index.php/search/filtered'; ?>">
                                <input type="hidden" name="physicalstatus" value="1">
                                <input type="hidden" name="caste" value="<?php echo (!empty($caste) ? $caste : ''); ?>">
                                <a onclick="$(this).closest('form').submit()"><?php echo $this->lang->line('register_profile_for_physicalstatus_1'); ?> ( <?php echo $counts['physical1']; ?> ) 					</a>
                            </form>
                        </li>
                        <li class="subitem1">
                            <form method="post" action="<?php echo base_url() . 'index.php/search/filtered'; ?>">
                                <input type="hidden" name="physicalstatus" value="2">
                                <input type="hidden" name="caste" value="<?php echo (!empty($caste) ? $caste : ''); ?>">
                                <a onclick="$(this).closest('form').submit()"><?php echo $this->lang->line('register_profile_for_physicalstatus_2'); ?> ( <?php echo $counts['physical2']; ?> )  						</a>
                            </form>
                        </li>
                    </ul>
                </li>
                <li class="item1"><h3 class="m_2"><?php echo $this->lang->line('search_eatting_habit'); ?></h3>
                    <ul class="cute">
                        <li class="subitem1">
                            <form method="post" action="<?php echo base_url() . 'index.php/search/filtered'; ?>">
                                <input type="hidden" name="food" value="1">
                                <input type="hidden" name="caste" value="<?php echo (!empty($caste) ? $caste : ''); ?>">
                                <a onclick="$(this).closest('form').submit()"><?php echo $this->lang->line('register_profile_for_food_1'); ?> ( <?php echo $counts['food1']; ?> ) 						</a>
                            </form>
                        </li>
                        <li class="subitem1">
                            <form method="post" action="<?php echo base_url() . 'index.php/search/filtered'; ?>">
                                <input type="hidden" name="food" value="2">
                                <input type="hidden" name="caste" value="<?php echo (!empty($caste) ? $caste : ''); ?>">
                                <a onclick="$(this).closest('form').submit()"><?php echo $this->lang->line('register_profile_for_food_2'); ?> ( <?php echo $counts['food2']; ?> ) 						</a>
                            </form>
                        </li>
                        <li class="subitem1">
                            <form method="post" action="<?php echo base_url() . 'index.php/search/filtered'; ?>">
                                <input type="hidden" name="food" value="3">
                                <input type="hidden" name="caste" value="<?php echo (!empty($caste) ? $caste : ''); ?>">
                                <a onclick="$(this).closest('form').submit()"><?php echo $this->lang->line('register_profile_for_food_3'); ?> ( <?php echo $counts['food3']; ?> ) 						</a>
                            </form>
                        </li>
                    </ul>
                </li>
                <li class="item1"><h3 class="m_2"><?php echo $this->lang->line('register_profile_for_smoking'); ?></h3>
                    <ul class="cute">
                        <li class="subitem1">
                            <form method="post" action="<?php echo base_url() . 'index.php/search/filtered'; ?>">
                                <input type="hidden" name="smoking" value="1">
                                <input type="hidden" name="caste" value="<?php echo (!empty($caste) ? $caste : ''); ?>">
                                <a onclick="$(this).closest('form').submit()"><?php echo $this->lang->line('register_profile_for_smoking'); ?> ( <?php echo $counts['smoking1']; ?> ) 						</a>
                            </form>
                        </li>
                        <li class="subitem1">
                            <form method="post" action="<?php echo base_url() . 'index.php/search/filtered'; ?>">
                                <input type="hidden" name="smoking" value="2">
                                <input type="hidden" name="caste" value="<?php echo (!empty($caste) ? $caste : ''); ?>">
                                <a onclick="$(this).closest('form').submit()"><?php echo $this->lang->line('search_non_smoking'); ?>  ( <?php echo $counts['smoking2']; ?> ) 						</a>
                            </form>
                        </li>
                        <li class="subitem1">
                            <form method="post" action="<?php echo base_url() . 'index.php/search/filtered'; ?>">
                                <input type="hidden" name="smoking" value="3">
                                <input type="hidden" name="caste" value="<?php echo (!empty($caste) ? $caste : ''); ?>">
                                <a onclick="$(this).closest('form').submit()"><?php echo $this->lang->line('register_profile_for_smoking_3'); ?> ( <?php echo $counts['smoking3']; ?> ) 						</a>
                            </form>
                        </li>
                    </ul>
                </li>
                <li class="item1"><h3 class="m_2"><?php echo $this->lang->line('register_profile_for_drinking'); ?></h3>
                    <ul class="cute">
                        <li class="subitem1">
                            <form method="post" action="<?php echo base_url() . 'index.php/search/filtered'; ?>">
                                <input type="hidden" name="drinking" value="1">
                                <input type="hidden" name="caste" value="<?php echo (!empty($caste) ? $caste : ''); ?>">
                                <a onclick="$(this).closest('form').submit()"><?php echo $this->lang->line('register_profile_for_drinking'); ?> ( <?php echo $counts['drinking1']; ?> ) 						</a>
                            </form>
                        </li>
                        <li class="subitem1">
                            <form method="post" action="<?php echo base_url() . 'index.php/search/filtered'; ?>">
                                <input type="hidden" name="drinking" value="2">
                                <input type="hidden" name="caste" value="<?php echo (!empty($caste) ? $caste : ''); ?>">
                                <a onclick="$(this).closest('form').submit()"><?php echo $this->lang->line('search_never_drinking'); ?> ( <?php echo $counts['drinking2']; ?> ) 						</a>
                            </form>
                        </li>
                        <li class="subitem1">
                            <form method="post" action="<?php echo base_url() . 'index.php/search/filtered'; ?>">
                                <input type="hidden" name="drinking" value="3">
                                <input type="hidden" name="caste" value="<?php echo (!empty($caste) ? $caste : ''); ?>">
                                <a onclick="$(this).closest('form').submit()"><?php echo $this->lang->line('register_profile_for_drinking_3'); ?> ( <?php echo $counts['drinking3']; ?> ) 						</a>
                            </form>
                        </li>
                    </ul>
                </li>
                <li class="item1"><h3 class="m_2"><?php echo sprintf($this->lang->line('search_profile_create'), $this->lang->line('by_text')); ?></h3>
                    <ul class="cute">
                        <li class="subitem1">
                            <form method="post" action="<?php echo base_url() . 'index.php/search/filtered'; ?>">
                                <input type="hidden" name="created" value="myself">
                                <input type="hidden" name="caste" value="<?php echo (!empty($caste) ? $caste : ''); ?>">
                                <a onclick="$(this).closest('form').submit()"><?php echo $this->lang->line('search_self'); ?> (<?php echo $counts['self']; ?>) </a>
                            </form>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
        <div class="clearfix"> </div>
    </div>
</div>
<script src="<?php echo base_url() ?>assets/js/jquery-validate/jquery.validate.min.js"></script>
<script>
                                    $(document).ready(function () {
                                        $("#mail").validate({
                                            submitHandler: function (form) {
                                                var dataString = $('#mail').serialize();
                                                if (dataString == '' || dataString == null || dataString == 0) {
                                                    return false;
                                                }
                                                $('#send_mail').hide()
                                                $('#paid_mail').show();
                                                $.ajax({
                                                    type: 'POST',
                                                    url: baseurl + 'index.php/profile/sendmail',
                                                    data: dataString,
                                                    dataType: "json",
                                                    success: function (data) {
                                                        $('#loading').hide();
                                                        if (data.msg == 'success') {
                                                            $('#paid_mail').html('<h4 style="text-align:center; color:green;"><?php echo $this->lang->line("mail_send_successfully_text"); ?></h4>');
                                                        } else {
                                                            $('#paid_mail').html('<h4 style="text-align:center; color:red;">' + data.msg + '</h4>');
                                                        }
                                                        setTimeout(function () {
                                                            $('#modalpaid').hide();
                                                            location.reload();
                                                        }, 2000);
                                                    },
                                                    error: function (jqXHR, textStatus, errorThrown) {
                                                        console.log(textStatus, errorThrown);
                                                        $('#paid_mail').html('<h4 style="text-align:center; color:green;"> <?php echo $this->lang->line("error_occur_contact_admin"); ?></h4>');
                                                        setTimeout(function () {
                                                            $('#modalpaid').hide();
                                                            location.reload();
                                                        }, 2000);
                                                    }
                                                });
                                            }
                                        });

                                        $("#message").rules("add", {
                                            required: true,
                                            minlength: 50,
                                            maxlength: 250,
                                            messages: {
                                                required: "Please enter the text",
                                                minlength: jQuery.validator.format("Please, at least {0} characters are necessary"),
                                                maxlength: jQuery.validator.format("Please, at maximum {0} characters are allowed")
                                            }
                                        });

                                    });
                                    function requestProfile(pid, id) {
                                        if (pid == 0 || id == 0 || pid == '' || id == '') {
                                            return false;
                                        }
                                        $('#response').show();
                                        $.post(baseurl + 'index.php/profile/updateprofilerequestinfo?pid=' + pid + '&id=' + id,
                                                function (data) {
                                                    if (data.msg == 'success') {
                                                        $('#msg_body').html('<h4 style="text-align:center; color:green;">' + data.msg + '</h4>');
                                                    } else {
                                                        $('#msg_body').html('<h4 style="text-align:center; color:red;">' + data.msg + '</h4>');
                                                    }
                                                    setTimeout(function () {
                                                        $('#response').hide();
                                                        location.reload();
                                                    }, 3000);
                                                }, "json")
                                                .fail(function () {
                                                    $('#msg_body').html('<h4 style="text-align:center; color:green;"> <?php echo $this->lang->line("error_occur_contact_admin"); ?></h4>');
                                                    setTimeout(function () {
                                                        $('#response').hide();
                                                        location.reload();
                                                    }, 2000);
                                                });
                                    }

                                    function requestcancel(pid, id) {
                                        if (pid == 0 || id == 0 || pid == '' || id == '') {
                                            return false;
                                        }
                                        var reqdata = "'" + pid + "'" + ',' + "'" + id + "'";
                                        var htmldata = '<span class ="col-sm-10"><?php echo $this->lang->line("are_you_sure_the _shorlisted"); ?></span>';
                                        htmldata1 = '<div class="buttons pull-right col-sm-3">';
                                        htmldata2 = '<div onclick="requestcancelinfo(' + reqdata + ')" class="vertical pointer"><?php echo $this->lang->line('yes'); ?></div>';
                                        htmldata3 = '</div><div class="buttons pull-right col-sm-2">';
                                        htmldata4 = '<div onclick="cancle()" class="vertical pointer"><?php echo $this->lang->line('no'); ?></div>';
                                        htmldata5 = '</div>';
                                        $('#response').show();
                                        $('#msg_body').html(htmldata + htmldata1 + htmldata2 + htmldata3 + htmldata4 + htmldata5);
                                    }

                                    function requestcancelinfo(pid, id) {
                                        if (pid == 0 || id == 0 || pid == '' || id == '') {
                                            return false;
                                        }
                                        $.post(baseurl + 'index.php/profile/cancleprofilerequestinfo?pid=' + pid + '&id=' + id,
                                                function (data) {
                                                    if (data.msg == 'success') {
                                                        $('#msg_body').html('<h4 style="text-align:center; color:green;">' + data.msg + '</h4>');
                                                    } else {
                                                        $('#msg_body').html('<h4 style="text-align:center; color:red;">' + data.msg + '</h4>');
                                                    }
                                                    setTimeout(function () {
                                                        $('#response').hide();
                                                        location.reload();
                                                    }, 3000);
                                                }, "json")
                                                .fail(function () {
                                                    $('#msg_body').html('<h4 style="text-align:center; color:green;"><?php echo $this->lang->line("error_occur_contact_admin"); ?></h4>');
                                                    setTimeout(function () {
                                                        $('#response').hide();
                                                        location.reload();
                                                    }, 2000);
                                                });
                                    }

                                    function cancle() {
                                        $('#response').hide();
                                    }

                                    function new_matche(reqData) {

                                        $.ajax({
                                            type: "POST",
                                            url: baseurl + 'index.php/matches/modaldata?uid=' + reqData,
                                            success: function (data) {
                                                $("#modal").show();
                                                $("#modal-body").html(data);
                                            },
                                            error: function (jqXHR, textStatus, errorThrown) {
                                                console.log(textStatus, errorThrown);
                                                $('#modal-body').html('<h4 style="text-align:center; color:green;"> <?php echo $this->lang->line("error_occur_contact_admin"); ?></h4>');
                                                setTimeout(function () {
                                                    $('#modal').hide();
                                                    location.reload();
                                                }, 2000);
                                            }
                                        });
                                    }

                                    function sendmail(uid, offerid) {
                                        if (uid == 0 || offerid == 0 || uid == '' || offerid == '') {
                                            return false;
                                        }

                                        if (offerid == 2) { //need to show paid view
                                            $('#modalpaid').show();
                                            $('#loading').show();
                                            $.post(baseurl + 'index.php/profile/sendmailinfo?uid=' + uid,
                                                    function (data) {
                                                        $('#loading').hide();
                                                        $('#paid_mail').show();
                                                        $('#paid_mail').html(data);

                                                    }, "html")
                                                    .fail(function () {
                                                        $('#paid_mail').show();
                                                        $('#paid_mail').html('<h4 style="text-align:center; color:green;"> <?php echo $this->lang->line("error_occur_contact_admin"); ?></h4>');
                                                        setTimeout(function () {
                                                            $('#modalpaid').hide();
                                                            location.reload();
                                                        }, 2000);
                                                    });
                                        } else {//need to show send mail view
                                            if (offerid == 1) {
                                                $('#modalpaid').show();
                                                $('#paid_mail').hide();
                                                $('#send_mail').show();
                                                $('#uid').val(uid);


                                            }
                                        }
                                        //$("#modalshort").show();
                                    }
</script>
