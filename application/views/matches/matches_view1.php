
<link href="<?php echo base_url(); ?>/assets/css/tab.css" rel='stylesheet' type='text/css' />
<link href="<?php echo base_url(); ?>/assets/css/sample.css" rel='stylesheet' type='text/css' />
<script>
    $(document).ready(function () {
        $('#modal1').show();
    });
    $(function () {
        $(".shtclose").on('click', function () {
            $('#modal1').hide();
        });

    });

</script>
<?php if ($offer == 2) { ?>
    <div id="modal1" class="modal">
        <div class="modal-dialog animated sample" style=" margin-top: 9%;width: 440px;">
            <div class="row modal-contentpaid">
                <div class="">
                    <span style="float:left;"></span>
                    <span><h3 class="subtitle" style="color: #7e7e7e;"><?php echo $this->lang->line('payment_offer_text'); ?></h3></span>
                    <span class="modal-closepaid shtclose" ><a href="#"><img src="<?php echo base_url(); ?>assets/images/m/forgot-password-close.gif"></a></span>
                </div>
                <div class="col-md-4 offer_hour">
                    <h3 class="offer_head"><?php echo $this->lang->line('take_offer_text'); ?></h3>
                    <div class="off_hour">
                        <img src="<?php echo base_url(); ?>assets/images/m/24-7-365.jpg" class="img-hour" alt=""/>
                    </div>
                </div>	
                <div class="col-md-2">
                    <img src="<?php echo base_url(); ?>assets/images/m/offer.gif" class="lineimg img-div" alt=""/>
                </div>
                <div class="col-md-6">
                    <h3 class="off-head"><?php echo $this->lang->line('limited_time_text'); ?></h3>
                    <div class="txt_centre"><?php echo $this->lang->line('offer_content'); ?> </div>
                    <a href="<?php echo base_url(); ?>index.php/payment" class="btn btn-primary offer-btn"><?php echo $this->lang->line('offer_text'); ?></a>
                </div>						
            </div>
        </div>
    </div>
<?php } ?>
<div class="grid_3">
    <div class="container">
        <div class="breadcrumb1">
            <ul>
                <a href="#"><i class="fa fa-home home_1"></i></a>
                <span class="divider">&nbsp;|&nbsp;</span>
                <li class="current-page"><?php echo $this->lang->line('matches_title'); ?></li>
            </ul>
        </div>

        <div class="row">
            <div class="col-md-9 members_box2">
                <h3>Mathces view</h3>
                <ul class="nav nav-tabs panelnav" style=" margin-top: 25px;">
                    <li class="active tabcol"><a href="#tab_a" data-toggle="tab"><?php echo $this->lang->line('new_matches_title'); ?></a></li>
                    <li class="tabcol"><a class="white"href="#tab_b" data-toggle="tab"><?php echo $this->lang->line('mutual_matches_title'); ?></a></li>
                    <li class="tabcol"><a class="white"href="#tab_c" data-toggle="tab"><?php echo $this->lang->line('header_matches_3'); ?> </a></li>
                </ul>
                <div class="panelbody ">
                    <div class="tab-content ">
                        <div class="tab-pane active" id="tab_a" style="  margin-top: 5%;">
                            <h4><?php echo $this->lang->line('new_matches_title'); ?></h4>
                            <?php if (!empty($newmatches)) { ?>
                                <div class="row">
                                    <?php foreach ($newmatches as $k1 => $val1) { //echo "<pre>";print_r($val1);?>
                                        <div class=" col-md-3">
                                            <div class="panelimage center-block"><img src="<?php echo base_url() . 'assets/upload_images/' . $val1['image'] ?>" alt="" class="img-responsive panelimage"/></div>
                                            <a href="<?php echo base_url() . 'index.php/profile/view?pid=' . $val1['userGuid']; ?>"> <h3 class="size"><span class="m_3"><?php echo constant('MEMBERID') . '-' . $val1['userId']; ?></span></h3></a>
                                        </div> 
                                    <?php } ?>	
                                </div><!--row-->
                                <div class="vertical col-md-offset-10"> <a class="white" href="<?php echo base_url(); ?>index.php/matches/index">view all</a>	   </div>
                                <?php
                            } else {
                                echo'<h4 style="text-align:center;"> ' . $this->lang->line('matches_not_found') . ' </h4>';
                            }
                            ?> 
                        </div><!--tab-a-->
                        <div class="tab-pane " id="tab_b" style="  margin-top: 5%;">
                            <h4><?php echo $this->lang->line('mutual_matches_title'); ?></h4>
                            <?php if (!empty($similar)) { ?>
                                <div class="row">
                                    <?php foreach ($similar as $key1 => $values1) { ?>
                                        <div class=" col-md-3">
                                            <div class="panelimage center-block"><img src="<?php echo base_url() . 'assets/upload_images/' . $values1['image'] ?>" alt="" class="img-responsive panelimage"/></div>
                                            <a href="<?php echo base_url() . 'index.php/profile/view?pid=' . $values1['userGuid']; ?>"> <h3 class="size"><span class="m_3"> <?php echo $values1['username'] . ' ( ' . constant('MEMBERID') . '-' . $values1['userId'] . ' )'; ?></span></h3></a>
                                        </div>
                                    <?php } ?>
                                    <div class="vertical col-md-offset-10"><a class="white" href="<?php echo base_url(); ?>index.php/matches/mutual_matches"> <?php echo $this->lang->line('view_all'); ?>	</a>   </div>
                                </div><!--row-->
                                <?php
                            } else {
                                echo'<h4 style="text-align:center;"> ' . $this->lang->line("no_matches_text") . '  </h4>';
                            }
                            ?>
                        </div><!--tab-b-->

                        <div class="tab-pane " id="tab_c" style="  margin-top: 5%;">
                            <h4><?php echo $this->lang->line('header_matches_3'); ?> </h4>
                            <?php if (!empty($profile_viewed)) { ?>
                                <div class="row">
                                    <?php foreach ($profile_viewed as $viewed) { ?>
                                        <div class=" col-md-3">
                                            <div class="panelimage center-block"><img src="<?php echo base_url() . 'assets/upload_images/' . $viewed['image'] ?>" alt="" class="img-responsive panelimage"/></div>
                                            <a href="<?php echo base_url() . 'index.php/profile/view?pid=' . $viewed['userGuid']; ?>"> <h3 class="size"><span class="m_3"> <?php echo $viewed['username'] . ' ( ' . constant('MEMBERID') . '-' . $viewed['userId'] . ' )'; ?></span></h3></a>
                                        </div>
                                    <?php } ?>
                                    <div class="vertical col-md-offset-10"><a class="white" href="<?php echo base_url() . 'index.php/profile/profileviewed'; ?>"> <?php echo $this->lang->line('view_all'); ?></a></div>
                                </div><!--row-->
                                <?php
                            } else {
                                echo'<h4 style="text-align:center;"> ' . $this->lang->line("no_matches_text") . '  </h4>';
                            }
                            ?>
                        </div><!--tab-c-->
                    </div><!--tab content-->
                </div><!--panel-->
                <div class="mutualtext bottom-5">
                    <h3><?php echo $this->lang->line('who_viewed_profile'); ?></h3>
                    <p><?php echo $this->lang->line('who_viewed_profile') . '.' . $this->lang->line('to_view_the_list'); ?><a href="<?php echo base_url(); ?>index.php/profile/viewed"> <span class="m_3"><?php echo $this->lang->line('click_here'); ?></span></a></p>
                </div>
            </div><!--col-md-9-->
            <div class="col-md-3 "><!--col-3-->
                <?php if (!empty($recent)) { ?>
                    <div class="box_profile bottom-5 boxpad">
                        <h4 class="recentup"><?php echo $this->lang->line('recent_update_title'); ?></h4>
                        <div class="borderdash"></div>
                        <ul class="profile_item viewuser">
                            <?php foreach ($recent as $key => $values) { ?>
                                <li class="profile_item-img">
                                    <img  src="<?php echo base_url() . 'assets/upload_images/' . $values['image'] ?>" class="img-responsive" alt=""/>
                                </li>
                                <li class="profile_item-desc">
                                    <h4><?php echo constant('MEMBERID') . '-' . $values['userId']; ?></h4>
                                    <p> <?php echo $values['username']; ?></p>
                                    <h5><a href="<?php echo base_url() . 'index.php/profile/view?pid=' . $values['userGuid'] ?>"><?php echo $this->lang->line('text_view_profile'); ?></a></h5>
                                </li>
                                <div class="clearfix"> </div>
                            <?php } ?>
                        </ul>
                        <!--<div class="viewall pointer"><a href="<?php echo base_url(); ?>index.php/profile/viewed"> <?php echo $this->lang->line('view_all'); ?></a></div>-->
                    </div>
                <?php } ?>
                <?php if (!empty($success)) { ?>
                    <div class="box_profile boxpad">
                        <h4 class="recentup"><?php echo $this->lang->line('success_story_title'); ?></h4>
                        <div class="borderdash"></div>
                        <ul class="profile_item viewuser">
                            <?php foreach ($success as $data) { ?>
                                <li class="profile_item-img">
                                    <img src="<?php echo base_url() . 'assets/success_images/' . $data['photo']; ?>" class="img-responsive" alt=""/>
                                </li>
                                <li class="profile_item-desc">
                                    <h4><?php //echo constant('MEMBERID').'-'. $data['id'];    ?></h4>
                                    <p> <span><?php echo $this->lang->line('mr_text'); ?>.</span><?php echo $data['groom_name'] ?> & <span><?php echo $this->lang->line('mrs_text'); ?>.</span><?php echo $data['bride_name'] ?> </p>
                                    <h5><a href="<?php echo base_url() . 'index.php/success/read?ssid=' . $data['id']; ?>"><?php echo $this->lang->line('text_view_profile'); ?></a></h5>
                                </li>
                                <div class="clearfix"> </div>
                            <?php } ?>
                        </ul>
                        <div class="viewall pointer"><a href="<?php echo base_url() . 'index.php/success'; ?>"> <?php echo $this->lang->line('view_all'); ?></a></div>
                    </div>
                <?php } ?>
            </div><!--col-md-3 end-->
        </div><!--row-->
    </div><!--container end-->
</div><!--grid3 end-->
<style>
</style>
