
<link href="<?php echo base_url(); ?>/assets/css/matcheview.css" rel='stylesheet' type='text/css' /><link href="<?php echo base_url(); ?>/assets/css/tab.css" rel='stylesheet' type='text/css' />
<script src="http://codeorigin.jquery.com/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.anoslide.js"></script>

<style>
    .container{

    }
    .carousel .prev { margin-left:-60px; background:url(<?php echo base_url(); ?>assets/images/prev.png) 0 0 no-repeat;  }
    .carousel .next { margin-right:-60px;  background:url(<?php echo base_url(); ?>assets/images/next.png) 0 0 no-repeat; }
</style>	

<?php if (!empty($offer)) { ?>
    <script>
        $(document).ready(function () {
            $('#modal1').show();
        });
        $(function () {
            $(".shtclose").on('click', function () {
                $('#modal1').hide();
            });

        });

    </script>
<?php } ?>
<script>
    $(document).ready(function () {
        $('#content1').show();
    });
</script>
<?php if ($offer == 2) { ?>
    <div id="modal1" class="modal">
        <div class="modal-dialog animated sample" style=" margin-top: 9%;width: 440px;">
            <div class="row modal-contentpaid">
                <div class="">
                    <span style="float:left;"></span>
                    <span><h3 class="subtitle" style="color: #7e7e7e;"><?php echo $this->lang->line('payment_offer_text'); ?></h3></span>
                    <span class="modal-closepaid shtclose" ><a href="#"><img src="<?php echo base_url(); ?>assets/images/m/forgot-password-close.gif"></a></span>
                </div>
                <div class="col-md-4 offer_hour">
                    <h3 class="offer_head"><?php echo $this->lang->line('take_offer_text'); ?></h3>
                    <div class="off_hour">
                        <img src="<?php echo base_url(); ?>assets/images/m/24-7-365.jpg" class="img-hour" alt=""/>
                    </div>
                </div>	
                <div class="col-md-2">
                    <img src="<?php echo base_url(); ?>assets/images/m/offer.gif" class="lineimg img-div" alt=""/>
                </div>
                <div class="col-md-6">
                    <h3 class="off-head"><?php echo $this->lang->line('limited_time_text'); ?></h3>
                    <div class="txt_centre"><?php echo $this->lang->line('offer_content'); ?> </div>
                    <a href="<?php echo base_url(); ?>index.php/payment" class="btn btn-primary offer-btn"><?php echo $this->lang->line('offer_text'); ?></a>
                </div>						
            </div>
        </div>
    </div>
<?php } ?>

<!--Interest msg-->
<div id="response" class="modal">
    <div class="modal-dialog animated sample" style=" margin-top: 9%;width: 440px;">
        <div class="modal-contentshort">
            <div class="row">
                <div>
                    <span style="float:left;"></span>
                    <span><h3 class="subtitle" id="title"><?php echo $this->lang->line('profile_request_send'); ?> </h3></span>
                    <span class="modal-closeshort shtclose" ><a href="#"><img src="<?php echo base_url(); ?>assets/images/m/forgot-password-close.gif"></a></span>
                </div>
                <div class="form-group row">
                    <div class="col-sm-10" id="msg_body" style="text-align:center;">
                        <div id='loader' style='display:block'>
                            <img src="<?php echo base_url(); ?>assets/images/loading.gif"/> Loding ...
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--sInterest msg end-->


<div class="grid_3">
    <div class="container">

        <?php if ($offer == 2) { ?>
            <div class="col-md-12 ">
                <div class="col-md-offset-2 col-md-8 col-md-offset-2 sendleft" >
                    <span class="leftpaytxt pull_left" style="color:#fff;"><?php echo $this->lang->line("payment_text"); ?> </span>
                    <span class="btn btn-warning pull_right">
                        <a href="<?php echo base_url(); ?>index.php/payment" style="color:#fff;"><?php echo $this->lang->line("payment"); ?></a>
                    </span>
                </div>
            </div>
        <?php } ?>  

        <div class="breadcrumb1">
            <ul>
                <a href="<?php echo base_url(); ?>index.php/matches/view/"><i class="fa fa-home home_1"></i></a>
                <span class="divider">&nbsp;|&nbsp;</span>
                <li class="current-page"><?php echo $this->lang->line('matches_title'); ?> </li>
            </ul>
        </div>
        <!--col-md-2-->
        <div class="col-md-2 col_5" style="">
            <a href="<?php echo base_url(); ?>index.php/message/inbox">
                <input  type="radio" name="tabs" checked>
                <label style="background-color:#FFA417;"for="tab1"><?php echo $this->lang->line("header_message_1"); ?></label>
            </a>
            <a href="<?php echo base_url(); ?>index.php/message/send">
                <input  type="radio" name="tabs">
                <label  style="background-color:#FFA417;" for="tab2"><?php echo $this->lang->line("header_message_2"); ?></label></a>


        </div>

        <!--col-md-2 end-->
        <!--col-md-8-->
        <div class="col-md-8 members_box2">

            <div class="col_4 slider-color">
                <div class="sliderhead">
                    <h3 class="sli-head"><?php echo $this->lang->line("recent_update_title"); ?></h3>
                </div>   
                <div class="carousel" id="slider1" data-mixed="">
                    <a class="prev" id="prev1" data-prev=""></a>
                    <?php if (!empty($recent)) { ?>
                        <ul >
                            <?php foreach ($recent as $key => $values) { ?>
                                <li>
                                    <div class="wrap"><div><img data-src="<?php echo base_url() . 'assets/upload_images/' . $values['image'] ?>" ></div>
                                        <div class="slh ">   
                                            <h3 class="ht">
                                                <span class="pointer" onclick="window.open('<?php echo base_url() . 'index.php/profile/view?pid=' . $key ?>')">
                                                    <span class="m_3"><?php echo sprintf($this->lang->line('profile_text'), $this->lang->line('profile_id')) . '  : ' . constant('MEMBERID') . '-' . $values['userId']; ?></span>
                                                </span>
                                            </h3>
                                            <div class="sli-send pointer">
                                                <?php
                                                if (!empty($values['interest'])) {
                                                    /** validate activity is interest or arrange or not interest show the button name difference else show the sent interest */
                                                    if (($values['interest'] == REQUEST_INTEREST) || ($values['interest'] == REQUEST_ARRANGED) || ($values['interest'] == REQUEST_NOT_INETREST)) {
                                                        /** if activity not interest then shoe the suitable matches else show the $profile['interest_name'] */
                                                        if (($values['interest'] == REQUEST_NOT_INETREST)) {
                                                            ?>
                                                            <a href="<?php echo base_url() . 'index.php/matches/mutual_matches'; ?>"><span class="vertical"><?php echo $values['interest_name']; ?></span></a>
                                                        <?php } else { ?>
                                                            <span class="photo_view"><?php echo $values['interest_name']; ?></span> 
                                                        <?php } ?>
                                                    <?php } else { ?>
                                                        <div class="photo_view pointer" onclick="requestProfile('<?php echo $key; ?>', '<?php echo $values['interest']; ?>')"><?php echo $values['interest_name']; ?></span>
                                                        <?php } ?>
                                                    <?php } else { ?>
                                                        <span class="photo_view" onclick="requestProfile('<?php echo $key; ?>', '<?php echo REQUEST_INTEREST; ?>')"><i class="fa fa-heart-o"></i><?php echo $this->lang->line('profile_send_interest'); ?></span>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </div>
                                </li>
                            <?php } ?>	

                        </ul>
                        <div class="viewall pointer"><a href="<?php echo base_url(); ?>index.php/profile/viewed"><?php echo $this->lang->line('view_all'); ?></a></div>
                        <a class="next" id="next1" data-next=""></a>
                        <?php
                    } else {
                        echo '<h4 style="text-align:center;">' . $this->lang->line("no_matches_text") . ' </h4>';
                    }
                    ?>
                </div>
            </div>

            <?php //echo "<pre>";print_r($newmatches);   ?>
            <?php //echo count($newcount);  ?>
            <div class="col_4 slider-color">
                <div class="sliderhead">
                    <h3 class="sli-head"><?php echo $this->lang->line("new_matches_title"); ?></h3>
                </div>   
                <?php if (!empty($newmatches)) { ?>
                    <div class="carousel" id="slider2" data-mixed="">
                        <a class="prev" id="prev2" data-prev=""></a>
                        <ul >
                            <?php foreach ($newmatches as $k1 => $val1) { ?>
                                <li>
                                    <div class="wrap"><div><img data-src="<?php echo base_url() . 'assets/upload_images/' . $val1['image'] ?>" alt="" class="hover-animation image-zoom-in img-responsive"></div>
                                        <div class="slh ">   
                                            <h3 class="ht"><span class="pointer" onclick="window.open('<?php echo base_url() . 'index.php/profile/view?pid=' . $k1 ?>')">
                                                    <span class="m_3"><?php echo sprintf($this->lang->line('profile_text'), $this->lang->line('profile_id')) . '  : ' . constant('MEMBERID') . '-' . $val1['userId']; ?></span></span></h3>
                                            <div class="sli-send pointer">
                                                <?php /* if ( !empty( $val1['interest'] ) ) { ?>
                                                  <span class="photo_view" onclick="requestProfile('<?php echo $k1; ?>', '<?php echo $val1['interest'];?>')"><i class="fa fa-heart-o"></i><?php echo $val1['interest_name'];?></span>
                                                  <?php } else{ ?>
                                                  <span class="photo_view" onclick="requestProfile('<?php echo $k1; ?>', '<?php echo REQUEST_INTEREST;?>')"><i class="fa fa-heart-o"></i>Send interest</span>
                                                  <?php } */ ?>

                                                <?php
                                                if (!empty($val1['interest'])) {
                                                    /** validate activity is interest or arrange or not interest show the button name difference else show the sent interest */
                                                    if (($val1['interest'] == REQUEST_INTEREST) || ($val1['interest'] == REQUEST_ARRANGED) || ($val1['interest'] == REQUEST_NOT_INETREST)) {
                                                        /** if activity not interest then shoe the suitable matches else show the $profile['interest_name'] */
                                                        if (($val1['interest'] == REQUEST_NOT_INETREST)) {
                                                            ?>
                                                            <a href="<?php echo base_url() . 'index.php/matches/mutual_matches'; ?>"><div class="vertical"><?php echo $val1['interest_name']; ?></span></a>
                                                        <?php } else { ?>
                                                            <span class="photo_view"><?php echo $val1['interest_name']; ?></span> 
                                                        <?php } ?>
                                                    <?php } else { ?>
                                                        <span class="photo_view pointer" onclick="requestProfile('<?php echo $k1; ?>', '<?php echo $val1['interest']; ?>')"><?php echo $val1['interest_name']; ?></span>
                                                    <?php } ?>
                                                <?php } else { ?>
                                                    <span class="photo_view" onclick="requestProfile('<?php echo $k1; ?>', '<?php echo REQUEST_INTEREST; ?>')"><?php echo $this->lang->line('profile_send_interest'); ?></span>
                                                <?php } ?>
                                            </div>
                                        </div> 
                                    </div>
                                </li>
                            <?php } ?>

                        </ul>
                        <div class="viewall pointer"><a href="<?php echo base_url(); ?>index.php/matches/index"><?php echo $this->lang->line('view_all'); ?></a></div>
                        <a class="next" id="next2" data-next=""></a>

                    </div>
                    <?php
                } else {
                    echo'<h4 style="text-align:center;"> ' . $this->lang->line("no_matches_text") . '  </h4>';
                }
                ?>
            </div>

            <div class="col_4 slider-color">
                <div class="sliderhead">
                    <h3 class="sli-head"><?php echo $this->lang->line("profile_interest_title"); ?></h3>
                </div>   
                <?php if (!empty($similar)) { ?>
                    <div class="carousel" id="slider3" data-mixed="">
                        <a class="prev" id="prev3" data-prev=""></a>
                        <ul >
                            <?php foreach ($similar as $key1 => $values1) { ?>
                                <li>
                                    <div class="wrap">
                                        <div>
                                            <img data-src="<?php echo base_url() . 'assets/upload_images/' . $values1['image'] ?>" alt="" class="hover-animation image-zoom-in img-responsive" >
                                        </div>
                                        <div class="slh ">   
                                            <h3 class="ht">
                                                <span class="pointer" onclick="window.open('<?php echo base_url() . 'index.php/profile/view?pid=' . $key1 ?>')">
                                                    <span class="m_3"><?php echo sprintf($this->lang->line('profile_text'), $this->lang->line('profile_id')) . '  : ' . constant('MEMBERID') . '-' . $values1['userId']; ?></span>
                                                </span>
                                            </h3>
                                            <div class="sli-send pointer">
                                                <?php /* if ( !empty( $values1['interest'] ) ) { ?>
                                                  <span class="photo_view" onclick="requestProfile('<?php echo $key1; ?>', '<?php echo $values1['interest'];?>')"><i class="fa fa-heart-o"></i><?php echo $values1['interest_name'];?></span>
                                                  <?php } else{ ?>
                                                  <span class="photo_view" onclick="requestProfile('<?php echo $key1; ?>', '<?php echo REQUEST_INTEREST;?>')"><i class="fa fa-heart-o"></i>Send interest</span>
                                                  <?php } */ ?>

                                                <?php
                                                if (!empty($values1['interest'])) {
                                                    /** validate activity is interest or arrange or not interest show the button name difference else show the sent interest */
                                                    if (($values1['interest'] == REQUEST_INTEREST) || ($values1['interest'] == REQUEST_ARRANGED) || ($values1['interest'] == REQUEST_NOT_INETREST)) {
                                                        /** if activity not interest then shoe the suitable matches else show the $profile['interest_name'] */
                                                        if (($values1['interest'] == REQUEST_NOT_INETREST)) {
                                                            ?>
                                                            <a href="<?php echo base_url() . 'index.php/matches/mutual_matches'; ?>"><span class="vertical"><?php echo $values1['interest_name']; ?></span></a>
                                                        <?php } else { ?>
                                                            <span class="photo_view"><?php echo $values1['interest_name']; ?></span> 
                                                        <?php } ?>
                                                    <?php } else { ?>
                                                        <span class="photo_view pointer" onclick="requestProfile('<?php echo $key1; ?>', '<?php echo $values1['interest']; ?>')"><?php echo $values1['interest_name']; ?></span>
                                                    <?php } ?>
                                                <?php } else { ?>
                                                    <span class="photo_view" onclick="requestProfile('<?php echo $key1; ?>', '<?php echo REQUEST_INTEREST; ?>')"><?php echo $this->lang->line('profile_send_interest'); ?></span>
                                                <?php } ?>
                                            </div>
                                        </div> 
                                    </div>
                                </li>
                            <?php } ?>

                        </ul>
                        <div class="viewall pointer"><a href="<?php echo base_url(); ?>index.php/matches/mutual_matches"><?php echo $this->lang->line('view_all'); ?></a></div>
                        <a class="next" id="next3" data-next=""></a>
                    </div>
                    <?php
                } else {
                    echo'<h4 style="text-align:center;"> ' . $this->lang->line("no_matches_text") . '  </h4>';
                }
                ?>
            </div>

            <div class="col_4 slider-color">
                <div class="sliderhead">
                    <h3 class="sli-head"><?php echo $this->lang->line("who_viewed_profile"); ?></h3>
                </div>   
                <div class="carousel" id="slider4" data-mixed="">
                    <?php if (!empty($viewed)) { ?>
                        <a class="prev" id="prev4" data-prev=""></a>
                        <ul >
                            <?php foreach ($viewed as $key2 => $values2) { ?>
                                <li>
                                    <div class="wrap"><div><img data-src="<?php echo base_url() . 'assets/upload_images/' . $values2['image'] ?>" alt="" class="hover-animation image-zoom-in img-responsive" ></div>
                                        <div class="slh ">   
                                            <h3 class="ht">
                                                <span class="pointer" onclick="window.open('<?php echo base_url() . 'index.php/profile/view?pid=' . $key2 ?>')">
                                                    <span class="m_3"><?php echo sprintf($this->lang->line('profile_text'), $this->lang->line('profile_id')) . '  : ' . constant('MEMBERID') . '-' . $values2['userId']; ?></span></span></h3>
                                            <div class="sli-send pointer">
                                                <?php /* if ( !empty( $values2['interest'] ) ) { ?>
                                                  <span class="photo_view" onclick="requestProfile('<?php echo $key2; ?>', '<?php echo $values2['interest'];?>')"><i class="fa fa-heart-o"></i><?php echo $values2['interest_name'];?></span>
                                                  <?php } else{ ?>
                                                  <span class="photo_view" onclick="requestProfile('<?php echo $key2; ?>', '<?php echo REQUEST_INTEREST;?>')"><i class="fa fa-heart-o"></i>Send interest</span>
                                                  <?php } */ ?>
                                                <?php
                                                if (!empty($values2['interest'])) {
                                                    /** validate activity is interest or arrange or not interest show the button name difference else show the sent interest */
                                                    if (($values2['interest'] == REQUEST_INTEREST) || ($values2['interest'] == REQUEST_ARRANGED) || ($values2['interest'] == REQUEST_NOT_INETREST)) {
                                                        /** if activity not interest then shoe the suitable matches else show the $profile['interest_name'] */
                                                        if (($values2['interest'] == REQUEST_NOT_INETREST)) {
                                                            ?>
                                                            <a href="<?php echo base_url() . 'index.php/matches/mutual_matches'; ?>"><div class="photo_view"><?php echo $values2['interest_name']; ?></span></a>
                                                        <?php } else { ?>
                                                            <span class="photo_view"><?php echo $values2['interest_name']; ?></span> 
                                                        <?php } ?>
                                                    <?php } else { ?>
                                                        <span class="photo_view" onclick="requestProfile('<?php echo $key2; ?>', '<?php echo $values2['interest']; ?>')"><?php echo $values2['interest_name']; ?></span>
                                                    <?php } ?>
                                                <?php } else { ?>
                                                    <span class="photo_view" onclick="requestProfile('<?php echo $key2; ?>', '<?php echo REQUEST_INTEREST; ?>')"><?php echo $this->lang->line('profile_send_interest'); ?></span>
                                                <?php } ?>
                                            </div>
                                        </div> 
                                    </div>
                                </li>
                            <?php } ?>

                        </ul>
                        <div class="viewall pointer"><a href="<?php echo base_url(); ?>index.php/profile/viewed"><?php echo $this->lang->line('view_all'); ?></a></div>
                        <a class="next" id="next4" data-next=""></a>
                        <?php
                    } else {
                        echo'<h4 style="text-align:center;">' . $this->lang->line("no_matches_text") . '  </h4>';
                    }
                    ?>
                </div>
            </div>

        </div>
        <!--col-md-8-->

        <!--col-md-2 -->

        <div class="col-md-2 searchopt" >
            <ul class="menu">
                <li class="item1"><h3 class="m_2"><?php echo $this->lang->line('search_title_1'); ?></h3>
                    <ul class="cute">

                        <li class="subitem1">
                            <form method="post" action="<?php echo base_url() . 'index.php/search/filtered'; ?>">
                                <input type="hidden" name="week" value="week">
                                <input type="hidden" name="caste" value="<?php echo (!empty($caste) ? $caste : ''); ?>">
                                <a onclick="$(this).closest('form').submit()"><?php echo sprintf($this->lang->line('search_with_text'), $this->lang->line('week_text')); ?> ( <?php echo $counts['week']; ?>  )</a>
                            </form>
                        </li>
                        <li class="subitem2">
                            <form method="post" action="<?php echo base_url() . 'index.php/search/filtered'; ?>">
                                <input type="hidden" name="month" value="month">
                                <input type="hidden" name="caste" value="<?php echo (!empty($caste) ? $caste : ''); ?>">
                                <a onclick="$(this).closest('form').submit()"><?php echo sprintf($this->lang->line('search_with_text'), $this->lang->line('month_text')); ?> ( <?php echo $counts['month']; ?> ) </a>
                            </form>
                        </li>
                    </ul>
                </li>
                <!--<li class="item1"><h3 class="m_2"><?php echo $this->lang->line('search_title_2'); ?></h3>
                  <ul class="cute">
                        <li class="subitem1">
                                <form method="post" action="<?php echo base_url() . 'index.php/search/filtered'; ?>">
                                        <input type="hidden" name="photo" value="photo">
                                        <input type="hidden" name="caste" value="<?php echo (!empty($caste) ? $caste : ''); ?>">
                                        <a onclick="$(this).closest('form').submit()"><?php echo $this->lang->line('search_photo_text'); ?> </a>
                                </form>
                        </li>
                  </ul>
                </li>-->
                <li class="item1"><h3 class="m_2"><?php echo $this->lang->line('register_profile_for_marriagestatus'); ?></h3>
                    <ul class="cute">
                        <li class="subitem1">
                            <form method="post" action="<?php echo base_url() . 'index.php/search/filtered'; ?>">
                                <input type="hidden" name="maritalstatus" value="1">
                                <input type="hidden" name="caste" value="<?php echo (!empty($caste) ? $caste : ''); ?>">
                                <a onclick="$(this).closest('form').submit()"><?php echo $this->lang->line('register_profile_for_marriagestatus_1'); ?> ( <?php echo $counts['marital1']; ?> ) </a>
                            </form>
                        </li>
                        <li class="subitem1">
                            <form method="post" action="<?php echo base_url() . 'index.php/search/filtered'; ?>">
                                <input type="hidden" name="maritalstatus" value="2">
                                <input type="hidden" name="caste" value="<?php echo (!empty($caste) ? $caste : ''); ?>">
                                <a onclick="$(this).closest('form').submit()"><?php echo $this->lang->line('register_profile_for_marriagestatus_2'); ?> ( <?php echo $counts['marital2']; ?> ) </a>
                            </form>

                        </li>
                        <li class="subitem1">
                            <form method="post" action="<?php echo base_url() . 'index.php/search/filtered'; ?>">
                                <input type="hidden" name="maritalstatus" value="3">
                                <input type="hidden" name="caste" value="<?php echo (!empty($caste) ? $caste : ''); ?>">
                                <a onclick="$(this).closest('form').submit()"><?php echo $this->lang->line('register_profile_for_marriagestatus_3'); ?>( <?php echo $counts['marital3']; ?> )</a>
                            </form>
                        </li>
                        <li class="subitem1">
                            <form method="post" action="<?php echo base_url() . 'index.php/search/filtered'; ?>">
                                <input type="hidden" name="maritalstatus" value="4">
                                <input type="hidden" name="caste" value="<?php echo (!empty($caste) ? $caste : ''); ?>">
                                <a onclick="$(this).closest('form').submit()"><?php echo $this->lang->line('register_profile_for_marriagestatus_4'); ?> ( <?php echo $counts['marital4']; ?> )</a>
                            </form>
                        </li>
                    </ul>
                </li>
                <!-- <li class="item1"><h3 class="m_2">Mother Tongue</h3>
                  <ul class="cute">
                        <li class="subitem1">
                                <form method="post" action="<?php //echo base_url().'index.php/search/filtered';    ?>">
                                        <input type="hidden" name="created" value="myself">
                                        <input type="hidden" name="caste" value="<?php //echo (!empty( $caste )?$caste:'');    ?>">
                                        <a onclick="$(this).closest('form').submit()">Tamil </a>
                                </form>
                        </li>
                  </ul>
                </li>-->
                <li class="item1"><h3 class="m_2"><?php echo $this->lang->line('search_education_title'); ?></h3>
                    <ul class="cute">
                        <li class="subitem1">
                            <form method="post" action="<?php echo base_url() . 'index.php/search/filtered'; ?>">
                                <input type="hidden" name="education" value="1">
                                <input type="hidden" name="caste" value="<?php echo (!empty($caste) ? $caste : ''); ?>">
                                <a onclick="$(this).closest('form').submit()"><?php echo $this->lang->line('search_phd_text'); ?> ( <?php echo $counts['edu1']; ?> )			</a>
                            </form>
                        </li>
                        <li class="subitem1">
                            <form method="post" action="<?php echo base_url() . 'index.php/search/filtered'; ?>">
                                <input type="hidden" name="education" value="2">
                                <input type="hidden" name="caste" value="<?php echo (!empty($caste) ? $caste : ''); ?>">
                                <a onclick="$(this).closest('form').submit()"><?php echo $this->lang->line('search_post_graduate_text'); ?> ( <?php echo $counts['edu2']; ?> )			</a>
                            </form>
                        </li>
                        <li class="subitem1">
                            <form method="post" action="<?php echo base_url() . 'index.php/search/filtered'; ?>">
                                <input type="hidden" name="education" value="3">
                                <input type="hidden" name="caste" value="<?php echo (!empty($caste) ? $caste : ''); ?>">
                                <a onclick="$(this).closest('form').submit()"><?php echo $this->lang->line('search_under_graduate_text'); ?>  ( <?php echo $counts['edu3']; ?> )			</a>
                            </form>
                        </li>
                        <li class="subitem1">
                            <form method="post" action="<?php echo base_url() . 'index.php/search/filtered'; ?>">
                                <input type="hidden" name="education" value="4">
                                <input type="hidden" name="caste" value="<?php echo (!empty($caste) ? $caste : ''); ?>">
                                <a onclick="$(this).closest('form').submit()"><?php echo $this->lang->line('search_diploma_text'); ?> ( <?php echo $counts['edu4']; ?> )			</a>
                            </form>
                        </li>
                        <li class="subitem1">
                            <form method="post" action="<?php echo base_url() . 'index.php/search/filtered'; ?>">
                                <input type="hidden" name="education" value="5">
                                <input type="hidden" name="caste" value="<?php echo (!empty($caste) ? $caste : ''); ?>">
                                <a onclick="$(this).closest('form').submit()"><?php echo $this->lang->line('register_profile_for_highesteducation'); ?> ( <?php echo $counts['edu5']; ?> )			</a>
                            </form>
                        </li>
                    </ul>
                </li>
                <li class="item1"><h3 class="m_2"><?php echo $this->lang->line('register_profile_for_occupation'); ?></h3>
                    <ul class="cute">
                        <li class="subitem1">
                            <form method="post" action="<?php echo base_url() . 'index.php/search/filtered'; ?>">
                                <input type="hidden" name="employeed" value="1">
                                <input type="hidden" name="caste" value="<?php echo (!empty($caste) ? $caste : ''); ?>">
                                <a onclick="$(this).closest('form').submit()"><?php echo $this->lang->line('register_profile_for_employeed_1'); ?> ( <?php echo $counts['emp_in1']; ?> )				</a>
                            </form>
                        </li>
                        <li class="subitem1">
                            <form method="post" action="<?php echo base_url() . 'index.php/search/filtered'; ?>">
                                <input type="hidden" name="employeed" value="2">
                                <input type="hidden" name="caste" value="<?php echo (!empty($caste) ? $caste : ''); ?>">
                                <a onclick="$(this).closest('form').submit()"><?php echo $this->lang->line('register_profile_for_employeed_2'); ?>	( <?php echo $counts['emp_in2']; ?> ) 					</a>
                            </form>
                        </li>
                        <li class="subitem1">
                            <form method="post" action="<?php echo base_url() . 'index.php/search/filtered'; ?>">
                                <input type="hidden" name="employeed" value="3">
                                <input type="hidden" name="caste" value="<?php echo (!empty($caste) ? $caste : ''); ?>">
                                <a onclick="$(this).closest('form').submit()"><?php echo $this->lang->line('register_profile_for_employeed_3'); ?> ( <?php echo $counts['emp_in3']; ?> ) 					</a>
                            </form>
                        </li>
                        <li class="subitem1">
                            <form method="post" action="<?php echo base_url() . 'index.php/search/filtered'; ?>">
                                <input type="hidden" name="employeed" value="4">
                                <input type="hidden" name="caste" value="<?php echo (!empty($caste) ? $caste : ''); ?>">
                                <a onclick="$(this).closest('form').submit()"><?php echo $this->lang->line('register_profile_for_employeed_4'); ?> ( <?php echo $counts['emp_in4']; ?> ) 					</a>
                            </form>
                        </li>
                    </ul>
                </li>
                <li class="item1"><h3 class="m_2"><?php echo $this->lang->line('register_profile_for_physicalstatus'); ?></h3>
                    <ul class="cute">
                        <li class="subitem1">
                            <form method="post" action="<?php echo base_url() . 'index.php/search/filtered'; ?>">
                                <input type="hidden" name="physicalstatus" value="1">
                                <input type="hidden" name="caste" value="<?php echo (!empty($caste) ? $caste : ''); ?>">
                                <a onclick="$(this).closest('form').submit()"><?php echo $this->lang->line('register_profile_for_physicalstatus_1'); ?> ( <?php echo $counts['physical1']; ?> ) 					</a>
                            </form>
                        </li>
                        <li class="subitem1">
                            <form method="post" action="<?php echo base_url() . 'index.php/search/filtered'; ?>">
                                <input type="hidden" name="physicalstatus" value="2">
                                <input type="hidden" name="caste" value="<?php echo (!empty($caste) ? $caste : ''); ?>">
                                <a onclick="$(this).closest('form').submit()"><?php echo $this->lang->line('register_profile_for_physicalstatus_2'); ?> ( <?php echo $counts['physical2']; ?> )  						</a>
                            </form>
                        </li>
                    </ul>
                </li>
                <li class="item1"><h3 class="m_2"><?php echo $this->lang->line('search_eatting_habit'); ?></h3>
                    <ul class="cute">
                        <li class="subitem1">
                            <form method="post" action="<?php echo base_url() . 'index.php/search/filtered'; ?>">
                                <input type="hidden" name="food" value="1">
                                <input type="hidden" name="caste" value="<?php echo (!empty($caste) ? $caste : ''); ?>">
                                <a onclick="$(this).closest('form').submit()"><?php echo $this->lang->line('register_profile_for_food_1'); ?>( <?php echo $counts['food1']; ?> ) 						</a>
                            </form>
                        </li>
                        <li class="subitem1">
                            <form method="post" action="<?php echo base_url() . 'index.php/search/filtered'; ?>">
                                <input type="hidden" name="food" value="2">
                                <input type="hidden" name="caste" value="<?php echo (!empty($caste) ? $caste : ''); ?>">
                                <a onclick="$(this).closest('form').submit()"><?php echo $this->lang->line('register_profile_for_food_2'); ?> ( <?php echo $counts['food2']; ?> ) 						</a>
                            </form>
                        </li>
                        <li class="subitem1">
                            <form method="post" action="<?php echo base_url() . 'index.php/search/filtered'; ?>">
                                <input type="hidden" name="food" value="3">
                                <input type="hidden" name="caste" value="<?php echo (!empty($caste) ? $caste : ''); ?>">
                                <a onclick="$(this).closest('form').submit()"><?php echo $this->lang->line('register_profile_for_food_3'); ?> ( <?php echo $counts['food3']; ?> ) 						</a>
                            </form>
                        </li>
                    </ul>
                </li>
                <li class="item1"><h3 class="m_2"><?php echo $this->lang->line('register_profile_for_smoking'); ?></h3>
                    <ul class="cute">
                        <li class="subitem1">
                            <form method="post" action="<?php echo base_url() . 'index.php/search/filtered'; ?>">
                                <input type="hidden" name="smoking" value="1">
                                <input type="hidden" name="caste" value="<?php echo (!empty($caste) ? $caste : ''); ?>">
                                <a onclick="$(this).closest('form').submit()"><?php echo $this->lang->line('register_profile_for_smoking'); ?> ( <?php echo $counts['smoking1']; ?> ) 						</a>
                            </form>
                        </li>
                        <li class="subitem1">
                            <form method="post" action="<?php echo base_url() . 'index.php/search/filtered'; ?>">
                                <input type="hidden" name="smoking" value="2">
                                <input type="hidden" name="caste" value="<?php echo (!empty($caste) ? $caste : ''); ?>">
                                <a onclick="$(this).closest('form').submit()"><?php echo $this->lang->line('search_non_smoking'); ?> ( <?php echo $counts['smoking2']; ?> ) 						</a>
                            </form>
                        </li>
                        <li class="subitem1">
                            <form method="post" action="<?php echo base_url() . 'index.php/search/filtered'; ?>">
                                <input type="hidden" name="smoking" value="3">
                                <input type="hidden" name="caste" value="<?php echo (!empty($caste) ? $caste : ''); ?>">
                                <a onclick="$(this).closest('form').submit()"><?php echo $this->lang->line('register_profile_for_smoking_3'); ?> ( <?php echo $counts['smoking3']; ?> ) 						</a>
                            </form>
                        </li>
                    </ul>
                </li>
                <li class="item1"><h3 class="m_2"><?php echo $this->lang->line('register_profile_for_drinking'); ?></h3>
                    <ul class="cute">
                        <li class="subitem1">
                            <form method="post" action="<?php echo base_url() . 'index.php/search/filtered'; ?>">
                                <input type="hidden" name="drinking" value="1">
                                <input type="hidden" name="caste" value="<?php echo (!empty($caste) ? $caste : ''); ?>">
                                <a onclick="$(this).closest('form').submit()"><?php echo $this->lang->line('register_profile_for_drinking'); ?> ( <?php echo $counts['drinking1']; ?> ) 						</a>
                            </form>
                        </li>
                        <li class="subitem1">
                            <form method="post" action="<?php echo base_url() . 'index.php/search/filtered'; ?>">
                                <input type="hidden" name="drinking" value="2">
                                <input type="hidden" name="caste" value="<?php echo (!empty($caste) ? $caste : ''); ?>">
                                <a onclick="$(this).closest('form').submit()"><?php echo $this->lang->line('search_never_drinking'); ?> ( <?php echo $counts['drinking2']; ?> ) 						</a>
                            </form>
                        </li>
                        <li class="subitem1">
                            <form method="post" action="<?php echo base_url() . 'index.php/search/filtered'; ?>">
                                <input type="hidden" name="drinking" value="3">
                                <input type="hidden" name="caste" value="<?php echo (!empty($caste) ? $caste : ''); ?>">
                                <a onclick="$(this).closest('form').submit()"><?php echo $this->lang->line('register_profile_for_drinking_3'); ?> ( <?php echo $counts['drinking3']; ?> ) 						</a>
                            </form>
                        </li>
                    </ul>
                </li>
                <li class="item1"><h3 class="m_2"><?php echo sprintf($this->lang->line('search_profile_create'), $this->lang->line('by_text')); ?></h3>
                    <ul class="cute">
                        <li class="subitem1">
                            <form method="post" action="<?php echo base_url() . 'index.php/search/filtered'; ?>">
                                <input type="hidden" name="created" value="myself">
                                <input type="hidden" name="caste" value="<?php echo (!empty($caste) ? $caste : ''); ?>">
                                <a onclick="$(this).closest('form').submit()"><?php echo $this->lang->line('search_self'); ?> (<?php echo $counts['self']; ?>)</a>
                            </form>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    <script>

        $('#slider1 ul').anoSlide(
                {
                    items: <?php echo (!empty($recentcount) ? $recentcount : 0); ?>,
                    speed: 500,
                    prev: '#prev1',
                    next: '#next1',
                    lazy: true,
                    delay: 100
                })
        $('#slider2 ul').anoSlide(
                {
                    items: <?php echo (!empty($newcount) ? $newcount : 0); ?>,
                    speed: 500,
                    prev: '#prev2',
                    next: '#next2',
                    lazy: true,
                    delay: 100
                })
        $('#slider3 ul').anoSlide(
                {
                    items: <?php echo (!empty($similarcount) ? $similarcount : 0); ?>,
                    speed: 500,
                    prev: '#prev3',
                    next: '#next3',
                    lazy: true,
                    delay: 100
                })
        $('#slider4 ul').anoSlide(
                {
                    items: <?php echo (!empty($viewedcount) ? $viewedcount : 0); ?>,
                    speed: 500,
                    prev: '#prev4',
                    next: '#next4',
                    lazy: true,
                    delay: 100
                })

        function requestProfile(pid, id) {
            if (pid == 0 || id == 0 || pid == '' || id == '') {
                return false;
            }
            $('#response').show();
            $.post(baseurl + 'index.php/profile/updateprofilerequestinfo?pid=' + pid + '&id=' + id,
                    function (data) {
                        if (data.msg == 'success') {
                            $('#msg_body').html('<h4 style="text-align:center; color:green;">' + data.msg + '</h4>');
                        } else {
                            $('#msg_body').html('<h4 style="text-align:center; color:red;">' + data.msg + '</h4>');
                        }
                        setTimeout(function () {
                            $('#response').hide();
                            location.reload();
                        }, 5000);
                    }, "json")
                    .fail(function () {
                        $('#msg_body').html('<h4 style="text-align:center; color:green;"><?php echo $this->lang->line("error_occur_contact_admin"); ?></h4>');
                        setTimeout(function () {
                            $('#response').hide();
                            location.reload();
                        }, 2000);
                    });
        }

    </script>
