
<script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/pagination/js/jquery.pajinate.js"></script>
<script>
    $(document).ready(function () {
        $('#paging_container_short').pajinate({
            num_page_links_to_display: 3,
            items_per_page: 4
        });
    });
    $(function () {
        $(".modal-closeshort").on('click', function () {
            $('#modalshort').hide();
        });
        $(".modal-closepaid").on('click', function () {
            $('#modalpaid').hide();
        });

        $("#close").on('click', function () {
            $('#response').hide();
        });

        $("#mail_close").on('click', function () {
            $('#modalpaid').hide();
        });
        $("#close_modal").on('click', function () {
            $('#modal').hide();
        });
    });

</script>
<link href="<?php echo base_url(); ?>/assets/css/slider.css" rel='stylesheet' type='text/css' />
<!--  Modal Start -->
<div id="modal" class="modal">
    <div class="modal-dialog animated">
        <div class="modal-content">
            <div class="modal-header">
                <strong><?php echo $this->lang->line('register_profile_for_process2'); ?></strong>
            </div>
            <div class="modal-body" id="modal-body">

            </div>
            <div class="modal-footer">
                <button class="btn btn-default" type="button" id="close_modal"><?php echo $this->lang->line('close_text'); ?></button>
            </div>
        </div>
    </div>
</div>
<!--  Modal End -->
<!--sendmailpaid -->
<div id="modalpaid" class="modal">
    <div class="modal-dialog animated sample" style=" margin-top: 9%;width: 440px;">
        <div class="row modal-contentpaid">
            <div class="sendhead">
                <span style="float:left;"></span>
                <span><h3 class="subtitle" style="color: #7e7e7e;"><?php echo sprintf($this->lang->line('to_text'), $this->lang->line('profile_send_mail')); ?></h3></span>
                <span class="modal-closepaid shtclose" ><a href="#"><img src="<?php echo base_url(); ?>assets/images/m/forgot-password-close.gif"></a></span>
            </div>

            <div id="paid_mail">
                <div id='loading' class="loader" style='display: block; text-align: center; margin: 100px;'>
                    <img src="<?php echo base_url(); ?>assets/images/loader.gif"/>
                </div>
            </div>
            <div id="send_mail" style="display:none;">
                <form id="mail">
                    <div class="form-group row">
                        <div class="col-sm-10 short-text">
                            <textarea class="form-control" id="message" name="message" rows="6" maxlength="250" minlength="50"" required></textarea>
                        </div>
                        <input type="hidden" name="userguid" id="uid" value="">
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-12">
                            <div class="col-sm-2 pull-right">
                                <span class="btn btn-warning" id="mail_close"><?php echo $this->lang->line('cancel_admin_register'); ?></span>
                            </div>
                            <div class="col-sm-2 pull-right">
                                <button class="btn btn-primary" type="submit"><?php echo $this->lang->line('register_profile_for_submit'); ?></button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!--sendemailendpaid -->

<!--Interest msg-->
<div id="response" class="modal">
    <div class="modal-dialog animated sample" style=" margin-top: 9%;width: 440px;">
        <div class="modal-contentshort">
            <div class="row">
                <div>
                    <span style="float:left;"></span>
                    <span><h3 class="subtitle"><?php echo $this->lang->line('profile_request_send'); ?></h3></span>
                    <span class="modal-closeshort shtclose" id="close"><a href="#"><img src="<?php echo base_url(); ?>assets/images/m/forgot-password-close.gif"></a></span>
                </div>
                <div class="form-group row">
                    <div class="col-sm-10 short-text" id="msg_body" style="text-align:center;">
                        <div id='loader' style='display:block'>
                            <img src="<?php echo base_url(); ?>assets/images/loading.gif"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--sInterest msg end-->

<div class="grid_3">
    <div class="container">
        <div class="breadcrumb1">
            <ul>
                <a href="<?php echo base_url(); ?>index.php/matches/view/"><i class="fa fa-home home_1"></i></a>
                <span class="divider">&nbsp;|&nbsp;</span>
                <li class="current-page"><?php echo $this->lang->line('shortlished_text'); ?> <?php echo sprintf($this->lang->line('profile_text'), ''); ?>(<?php echo (!empty($shortList) ? count($shortList) : 0) ?>)</li>
            </ul>
        </div>
        <div class="col-md-3 col_5">
            <ul class="menu">
                <li class="item1"><h3 class="m_2"><?php echo $this->lang->line('activity_text'); ?></h3>
                    <ul class="cute">
                        <li class="subitem1"><a href="<?php echo base_url(); ?>index.php/matches/shortlist"><?php echo $this->lang->line('profiles_text') . ' ' . $this->lang->line('shortlished_text'); ?></a></li>
                        <li class="subitem2"><a href="<?php echo base_url(); ?>index.php/matches/ignore_list"><?php echo $this->lang->line('profiles_text') . ' ' . $this->lang->line('ignored_text'); ?></a></li>
                    </ul>
                </li>
            </ul>
            <?php if ($offer == 2) { ?>
                <div class="pay left sendleft">
                    <div class="leftpay">
                        <span class="leftpaytxt" style="color: #fff;"><?php echo $this->lang->line("payment_text"); ?> </span>
                    </div>	
                    <div class=" leftpay pointer btnlink" >
                        <span class="btnlinkalign ">
                            <a href="<?php echo base_url(); ?>index.php/payment"><?php echo $this->lang->line('payment'); ?></a></span>
                    </div>
                </div>
            <?php } ?>
        </div>
        <div class="col-md-9 profile_left">
            <div class="col_4">
                <div class="bs-example bs-example-tabs" role="tabpanel" data-example-id="togglable-tabs">

                    <div id="myTabContent" class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active" id="home" aria-labelledby="home-tab">

                            <div class="tab_box tab_box1">
                            </div>



                            <div id="paging_container_short">
                                <?php if (!empty($shortList)) { ?>

                                    <div class="page_navigation pagination pagination_1"></div>
                                    <div class="short_send">
                                        <input value="0" id="selectall" type="checkbox">
                                        <a onclick="sendBulkRequest()"><div class="short_btn vertical"><?php echo $this->lang->line('send_interest_all'); ?></div></a>
                                        <!-- <a href="#"><div class="short_btn_del vertical">delete</div></a> -->
                                    </div>
                                    <form id="short_form" method="post">
                                        <div class="clearfix"> </div>
                                        <?php foreach ($shortList as $key => $value) { ?>

                                            <div class="content">
                                                <div class="jobs-item with-thumb box_profile">
                                                    <div class="check_name">
                                                        <label class="short_checkbox">
                                                            <input value="<?php echo $value['userGuid']; ?>"  class="case" type="checkbox" name="shortlist[]">
                                                            <span class="sh_name"><?php echo (!empty($value['username']) ? $value['username'] : '') ?></span>
                                                        </label>
                                                    </div>
                                                    <div class="thumb_top">
                                                        <div class="thumb short_img"><a href="#" onclick="new_matche('<?php echo (!empty($value['userGuid']) ? $value['userGuid'] : ''); ?>')"><img src="<?php echo base_url() . 'assets/upload_images/' . $value['image'] ?>" class="img-responsive" alt=""/></a>
                                                            <div class="imgreqphoto">
                                                                <?php
                                                                if (!empty($value['image_request'])) {
                                                                    if (!empty($value['imagerequest'])) {
                                                                        ?>
                                                                        <div class="vertical   "  ><?php echo $this->lang->line('profile_request_send'); ?></div>
                                                                    <?php } else { ?>
                                                                        <div class="vertical pointer "  onclick="requestProfile('<?php echo $value['userGuid']; ?>', '<?php echo REQUEST_IMAGE; ?>')"><?php echo $this->lang->line('profile_image_request'); ?></div>
                                                                        <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="jobs_right short_right">
                                                            <h6 class="title"><a href="<?php echo base_url() . 'index.php/profile/view?pid=' . $value['userGuid'] ?>"><?php echo (!empty($value['username']) ? $value['username'] : '') . '(' . constant('MEMBERID') . $value['userId'] . ' )'; ?> </a></h6>

                                                            <ul class="login_details1">
                                                                <li><?php echo sprintf($this->lang->line('last_text'), $this->lang->line('text_login')); ?>  :  <?php echo $value['log_in']; ?></li>
                                                            </ul>
                                                            <table class="table_short">
                                                                <tbody>
                                                                    <tr class="opened_1">
                                                                        <td class="short_label1"><?php echo $this->lang->line('search_profile_for_age'); ?> / <?php echo $this->lang->line('register_profile_for_height'); ?> </td>
                                                                        <td>:</td>
                                                                        <td class="short_value"><?php echo (!empty($value['age']) ? $value['age'] . ' Yrs,' : 'not specified') . ' ' . (!empty($value['height']) ? $value['height'] . ' Cm' : 'not specified'); ?> </td>
                                                                    </tr>

                                                                    <tr class="opened">
                                                                        <td class="short_label1"><?php echo $this->lang->line('register_profile_for_religion'); ?> </td>
                                                                        <td>:</td>
                                                                        <td class="short_value"><?php echo (!empty($value['religion']) ? $value['religion'] : 'not specified') ?></td>
                                                                    </tr>
                                                                    <tr class="opened">
                                                                        <td class="short_label1"><?php echo $this->lang->line('register_profile_for_gender'); ?> </td>
                                                                        <td>:</td>
                                                                        <td class="short_value"><?php echo (!empty($value['gender']) ? constant("GENDER_" . strtoupper($value['gender'])) : 'Not Specified'); ?></td>
                                                                    </tr>
                                                                    <tr class="opened">
                                                                        <td class="short_label1"><?php echo $this->lang->line('register_profile_for_marriagestatus'); ?> </td>
                                                                        <td>:</td>
                                                                        <td class="short_value"><?php echo (!empty($value['martialstatus']) ? $value['martialstatus'] : 'not specified') ?></td>
                                                                    </tr>
                                                                    <tr class="opened">
                                                                        <td class="short_label1"><?php echo $this->lang->line('register_profile_for_caste'); ?> </td>
                                                                        <td>:</td>
                                                                        <td class="short_value"><?php echo (!empty($value['caste_name']) ? $value['caste_name'] : 'not specified') ?></td>
                                                                    </tr>
                                                                    <tr class="closed">
                                                                        <td class="short_label1"><?php echo sprintf($this->lang->line('search_profile_create'), $this->lang->line('by_text')); ?> </td>
                                                                        <td>:</td>
                                                                        <td class="short_value "><span><?php echo (!empty($value['profile_created']) ? $value['profile_created'] : 'not specified') ?></span></td>
                                                                    </tr>
                                                                    <tr class="closed">
                                                                        <td class="short_label1"><?php echo $this->lang->line('search_education_title'); ?> </td>
                                                                        <td>:</td>
                                                                        <td class="short_value "><span><?php echo (!empty($value['edu_name']) ? $value['edu_name'] : 'not specified') . ' - ' . (!empty($value['qualification']) ? $value['qualification'] : 'not specified'); ?></span></td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                            <p class="description"><a  class="m_3" href="<?php echo base_url() . 'index.php/profile/view?pid=' . $value['userGuid'] ?>" class="read-more"><?php echo $this->lang->line('text_view_profile'); ?></a></p>
                                                        </div>
                                                        <div class="clearfix"> </div>
                                                    </div>
                                                    <div class="thumb_bottom short_bottom">
                                                        <div class="buttons1">
                                                            <div class="vertical pointer"  onclick="sendmail('<?php echo $value['userGuid']; ?>', '<?php echo $offer; ?>')"><?php echo $this->lang->line('profile_send_mail'); ?></div>	
                                                            <?php
                                                            if (!empty($value['interest'])) {
                                                                if (($value['interest'] == REQUEST_INTEREST) || ($value['interest'] == REQUEST_ARRANGED)) {
                                                                    ?>
                                                                    <div class="vertical"><?php echo $value['interest_name']; ?></div>
                                                                <?php } else { ?>
                                                                    <div class="vertical pointer" onclick="requestProfile('<?php echo $value['userGuid']; ?>', '<?php echo $value['interest']; ?>')"><?php echo $value['interest_name']; ?></div>
                                                                <?php } ?>
                                                            <?php } else { ?>
                                                                <div class="vertical pointer" onclick="requestProfile('<?php echo $value['userGuid']; ?>', '<?php echo REQUEST_INTEREST; ?>')"><?php echo $this->lang->line('profile_send_interest'); ?></div>
                                                            <?php } ?>
                                           <!--<div class="vertical pointer" onclick="requestProfile('<?php //echo $value['userGuid']    ?>', '<?php //echo REQUEST_IGNORE;    ?>')">Ignore</div>
                                           <div class="vertical pointer">Block</div>-->
                                                        </div>

                                                        <div class="clearfix"> </div>
                                                    </div>
                                                </div>
                                            </div><!--content-->
                                            <?php
                                        }
                                        ?>
                                    </form>
                                </div>	
                                <?php
                            } else {
                                echo '<h4 class="tab_box tab_box1" style="text-align:center;">' . $this->lang->line("no_matches_text") . '</h4>';
                            }
                            ?>				

                        </div>

                    </div> 
                </div>
            </div>
        </div>
        <div class="clearfix"> </div>
    </div>
</div>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script>
                                                                    $(document).ready(function () {
                                                                        $("#mail").validate({
                                                                            submitHandler: function (form) {
                                                                                var dataString = $('#mail').serialize();
                                                                                if (dataString == '' || dataString == null || dataString == 0) {
                                                                                    return false;
                                                                                }
                                                                                $('#send_mail').hide()
                                                                                $('#paid_mail').show();
                                                                                $.ajax({
                                                                                    type: 'POST',
                                                                                    url: baseurl + 'index.php/profile/sendmail',
                                                                                    data: dataString,
                                                                                    dataType: "json",
                                                                                    success: function (data) {
                                                                                        $('#loading').hide();
                                                                                        if (data.msg == 'success') {
                                                                                            $('#paid_mail').html('<h4 style="text-align:center; color:green;"> <?php echo $this->lang->line("mail_send_successfully_text"); ?> </h4>');
                                                                                        } else {
                                                                                            $('#paid_mail').html('<h4 style="text-align:center; color:red;">' + data.msg + '</h4>');
                                                                                        }
                                                                                        setTimeout(function () {
                                                                                            $('#modalpaid').hide();
                                                                                            location.reload();
                                                                                        }, 2000);
                                                                                    },
                                                                                    error: function (jqXHR, textStatus, errorThrown) {
                                                                                        console.log(textStatus, errorThrown);
                                                                                        $('#paid_mail').html('<h4 style="text-align:center; color:green;"><?php echo $this->lang->line("error_occur_contact_admin"); ?></h4>');
                                                                                        setTimeout(function () {
                                                                                            $('#modalpaid').hide();
                                                                                            location.reload();
                                                                                        }, 2000);
                                                                                    }
                                                                                });
                                                                            }
                                                                        });

                                                                        $("#message").rules("add", {
                                                                            required: true,
                                                                            minlength: 50,
                                                                            maxlength: 250,
                                                                            messages: {
                                                                                required: "Please enter the text",
                                                                                minlength: jQuery.validator.format("Please, at least {0} characters are necessary"),
                                                                                maxlength: jQuery.validator.format("Please, at maximum {0} characters are allowed")
                                                                            }
                                                                        });

                                                                    });

                                                                    function new_matche(reqData) {

                                                                        $.ajax({
                                                                            type: "POST",
                                                                            url: baseurl + 'index.php/matches/modaldata?uid=' + reqData,
                                                                            success: function (data) {
                                                                                $("#modal").show();
                                                                                $("#modal-body").html(data);
                                                                            },
                                                                            error: function (jqXHR, textStatus, errorThrown) {
                                                                                console.log(textStatus, errorThrown);
                                                                                $('#modal-body').html('<h4 style="text-align:center; color:green;"> <?php echo $this->lang->line("error_occur_contact_admin"); ?></h4>');
                                                                                setTimeout(function () {
                                                                                    $('#modal').hide();
                                                                                    location.reload();
                                                                                }, 2000);
                                                                            }
                                                                        });
                                                                    }
                                                                    /*$(function(){
                                                                     
                                                                     
                                                                     $("#selectall").click(function () {
                                                                     $('.case').attr('checked', this.checked);
                                                                     });
                                                                     
                                                                     
                                                                     $(".case").click(function(){
                                                                     
                                                                     if($(".case").length == $(".case:checked").length) {
                                                                     $("#selectall").attr("checked", "checked");
                                                                     } else {
                                                                     $("#selectall").removeAttr("checked");
                                                                     }
                                                                     
                                                                     });
                                                                     });*/
                                                                    $(document).ready(function () {
                                                                        $("#selectall").change(function () {
                                                                            $(".case").prop('checked', $(this).prop("checked"));
                                                                        });
                                                                    });

                                                                    function sendBulkRequest() {
                                                                        var dataString = $('#short_form').serialize();
                                                                        if (dataString == '' || dataString == null || dataString == 0) {
                                                                            alert('Please select options');
                                                                            return false;
                                                                        }
                                                                        $('#response').show();
                                                                        $.ajax({
                                                                            type: 'POST',
                                                                            url: baseurl + 'index.php/profile/updatebulkrequestinfo?id=' +<?php echo REQUEST_INTEREST ?>,
                                                                            data: dataString,
                                                                            dataType: "json",
                                                                            success: function (data) {
                                                                                if (data.msg == 'success') {
                                                                                    $('#msg_body').html('<h4 style="text-align:center; color:green;">' + data.msg + '</h4>');
                                                                                } else {
                                                                                    $('#msg_body').html('<h4 style="text-align:center; color:red;">' + data.msg + '</h4>');
                                                                                }
                                                                                setTimeout(function () {
                                                                                    $('#response').hide();
                                                                                    location.reload();
                                                                                }, 2000);
                                                                            },
                                                                            error: function (jqXHR, textStatus, errorThrown) {
                                                                                console.log(textStatus, errorThrown);
                                                                                $('#msg_body').html('<h4 style="text-align:center; color:green;"> <?php echo $this->lang->line("error_occur_contact_admin"); ?></h4>');
                                                                                setTimeout(function () {
                                                                                    $('#msg_body').hide();
                                                                                    location.reload();
                                                                                }, 2000);
                                                                            }
                                                                        });
                                                                        return false;
                                                                    }

                                                                    function requestProfile(pid, id) {
                                                                        if (pid == 0 || id == 0 || pid == '' || id == '') {
                                                                            return false;
                                                                        }
                                                                        //console.log(pid);
                                                                        $('#response').show();
                                                                        $.post(baseurl + 'index.php/profile/updateprofilerequestinfo?pid=' + pid + '&id=' + id,
                                                                                function (data) {
                                                                                    if (data.msg == 'success') {
                                                                                        $('#msg_body').html('<h4 style="text-align:center; color:green;">' + data.msg + '</h4>');
                                                                                    } else {
                                                                                        $('#msg_body').html('<h4 style="text-align:center; color:red;">' + data.msg + '</h4>');
                                                                                    }
                                                                                    setTimeout(function () {
                                                                                        $('#response').hide();
                                                                                        location.reload();
                                                                                    }, 2000);
                                                                                }, "json")
                                                                                .fail(function () {
                                                                                    $('#msg_body').html('<h4 style="text-align:center; color:green;"> <?php echo $this->lang->line("error_occur_contact_admin"); ?></h4>');
                                                                                    setTimeout(function () {
                                                                                        $('#response').hide();
                                                                                        location.reload();
                                                                                    }, 2000);
                                                                                });
                                                                    }

                                                                    function sendmail(uid, offerid) {
                                                                        if (uid == 0 || offerid == 0 || uid == '' || offerid == '') {
                                                                            return false;
                                                                        }

                                                                        if (offerid == 2) { //need to show paid view
                                                                            $('#modalpaid').show();
                                                                            $('#loading').show();
                                                                            $.post(baseurl + 'index.php/profile/sendmailinfo?uid=' + uid,
                                                                                    function (data) {
                                                                                        $('#loading').hide();
                                                                                        $('#paid_mail').show();
                                                                                        $('#paid_mail').html(data);

                                                                                    }, "html")
                                                                                    .fail(function () {
                                                                                        $('#paid_mail').show();
                                                                                        $('#paid_mail').html('<h4 style="text-align:center; color:green;"> <?php echo $this->lang->line("error_occur_contact_admin"); ?></h4>');
                                                                                        setTimeout(function () {
                                                                                            $('#modalpaid').hide();
                                                                                            location.reload();
                                                                                        }, 2000);
                                                                                    });
                                                                        } else {//need to show send mail view
                                                                            if (offerid == 1) {
                                                                                $('#modalpaid').show();
                                                                                $('#paid_mail').hide();
                                                                                $('#send_mail').show();
                                                                                $('#uid').val(uid);


                                                                            }
                                                                        }
                                                                    }
</SCRIPT>
