
<div class="container">
    <div class="col-md-12">
        <div class="box-shadow ">
            <h4 class="gend">Choice Your Gender</h4>
            <form action="" method="post">
                <div class="form-group row">
                    <label for="gender" class="col-sm-5 form-control-label reglab"><?php echo $this->lang->line("register_profile_for_gender"); ?></label> 
                    <div class="col-sm-7">
                        <label class="radio-inline reglabrad">
                            <input type="radio" name="gender" id="male" value="M" required> <?php echo $this->lang->line('register_profile_for_male'); ?>
                        </label>
                        <label class="radio-inline reglabrad">
                            <input type="radio" name="gender" id="female" value="F" required> <?php echo $this->lang->line('register_profile_for_female'); ?>
                        </label>
                    </div>
                </div>
                <div class="btnsave">  <button type="submit" class="btn btn-success">Submit</button> </div>
            </form>
        </div>
    </div>
</div>
<style>
    .btnsave {
        text-align: center;
    }
    .box-shadow {
        background: #fff none repeat scroll 0 0;
        box-shadow: 0 6px 12px rgba(0, 0, 0, 0.176);
        margin: 5em auto 5em 10px;
        padding: 1em;
        position: relative;
        width: 70%;
    }
    .gend {color: #00923f !important;}
</style>
