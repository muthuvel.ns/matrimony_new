<script src="<?php echo base_url(); ?>assets/admin/js/pie-chart.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>/assets/admin/js/piechart/Chart.js"></script>
<!--bar chart-->
<script src="<?php echo base_url(); ?>assets/admin/js/barchart/canvasjs.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/js/barchart/loader.js"></script>
<!--pie chart-->
<script>
    /*pie chart*/
    var yeardata = [{
            value: 50,
            color: "#8bd22f",
            highlight: "#00abff",
            label: "yearUsers"
        }, {
            value: 50,
            color: "#f95372",
            highlight: "#FFC870",
            label: "non yearUsers"
        }];
    var data = [{
            value: <?php echo $usercount; ?>,
            color: "#8bd22f",
            highlight: "#00abff",
            label: "Users"
        }, {
            value: 0,
            color: "#f95372",
            highlight: "#FFC870",
            label: "non Users"
        }];
    var successdata = [{
            value: <?php echo $successcount; ?>,
            color: "#f95372",
            highlight: "#FFC870",
            label: "SuccessUsers"
        }, {
            value: <?php echo $usercount - $successcount; ?>,
            color: "#8bd22f",
            highlight: "#00abff",
            label: "non SuccessUsers"
        }];
    var femaledata = [{
            value: <?php echo $femalecount; ?>,
            color: "#210EED",
            highlight: "#8EC3DE",
            label: "FemaleUsers"
        }, {
            value: <?php echo $usercount - $femalecount; ?>,
            color: "#8bd22f",
            highlight: "#00abff",
            label: "non FemaleUsers"
        }];
    var maledata = [{
            value: <?php echo $malecount; ?>,
            color: "#E81A62",
            highlight: "#E8A2BB",
            label: "MaleUsers"
        }, {
            value: <?php echo $usercount - $malecount; ?>,
            color: "#8bd22f",
            highlight: "#00abff",
            label: "non MaleUsers"
        }];

    $(document).ready(function () {

        var ctx = document.getElementById("user").getContext("2d");
        var user = new Chart(ctx).Pie(data);
        $("#user").click(function (evt) {
            var activePoints = user.getSegmentsAtEvent(evt);
            var activeLabel = activePoints[0].label;
            if (activeLabel == "Red") {
                while (user.segments.length > 0) {
                    user.removeData()
                }
                for (i = 0; i < data2.length; i++) {
                    user.addData(data2[i])
                }
            }

        });


        var successctx = document.getElementById("success").getContext("2d");
        var success = new Chart(successctx).Pie(successdata);
        $("#success").click(function (evt) {
            var activePoints = success.getSegmentsAtEvent(evt);
            var activeLabel = activePoints[0].label;
            if (activeLabel == "Red") {
                while (success.segments.length > 0) {
                    success.removeData()
                }
                for (i = 0; i < data2.length; i++) {
                    success.addData(data2[i])
                }
            }

        });

        var famalectx = document.getElementById("female").getContext("2d");
        var famalevar = new Chart(famalectx).Pie(femaledata);
        $("#female").click(function (evt) {
            var activePoints = famalevar.getSegmentsAtEvent(evt);
            var activeLabel = activePoints[0].label;
            if (activeLabel == "Red") {
                while (famalevar.segments.length > 0) {
                    famalevar.removeData()
                }
                for (i = 0; i < data2.length; i++) {
                    famalevar.addData(data2[i])
                }
            }

        });

        var malectx = document.getElementById("male").getContext("2d");
        var malevar = new Chart(malectx).Pie(maledata);
        $("#male").click(function (evt) {
            var activePoints = malevar.getSegmentsAtEvent(evt);
            var activeLabel = activePoints[0].label;
            if (activeLabel == "Red") {
                while (malevar.segments.length > 0) {
                    malevar.removeData()
                }
                for (i = 0; i < data2.length; i++) {
                    malevar.addData(data2[i])
                }
            }

        });

        /* jQuery("#pie").radialPieChart("init", {
         'font-size': 13,
         'fill': 75,
         'data': [
         {'color': "#2DB1E4", 'perc': 40 },
         {'color': "#9CCA13", 'perc': 32 },
         {'color': "#A4075E", 'perc': 20 },
         {'color': "#A4075E", 'perc': 20 }
         ] 
         });*/

    });
    /*pie chart end*/
    /*bar chart*/
    <?php if( !empty( $chart )) { ?>
    google.charts.load('current', {'packages': ['bar']});
    google.charts.setOnLoadCallback(drawChart);
    function drawChart() {

        var data = google.visualization.arrayToDataTable([
            ['month', 'User', 'Free', 'Premium'],
                <?php echo $chart; ?>
            ]);
        var options = {
            chart: {
                title: 'Company Performance for <?php echo $year; ?>',
                //subtitle: 'Sales, Expenses, and Profit for ',
            }
        };
        var chart = new google.charts.Bar(document.getElementById('columnchart_material'));
        chart.draw(data, options);
    }
<?php }?>
    /*bar chart end*/

</script>
<div id="page-wrapper">
    <div class="graphs bgimage">
        <?php //echo $message;?>
        <content-top>
            <div class="content-top clearfix">
                <h1 class="al-title"><?php echo $this->lang->line('dashboard'); ?></h1>
                <ul class="breadcrumb al-breadcrumb">
                    <li><a href=""><?php echo $this->lang->line('home'); ?></a></li>
                    <li class=""><?php echo $this->lang->line('dashboard'); ?></li>
                </ul>
            </div>
        </content-top> 
        <div id="tabs-container">
            <ul class="tabs-menu printopt" id="pdfhidden">
             <!--   <li class="current printopt"><a href="<?php echo base_url(); ?>index.php/admin/dashboard"><?php echo $this->lang->line('month'); ?></a></li>-->
                <!--<li  class="printopt"><a href="<?php echo base_url(); ?>index.php/admin/yeardashboard">year</a></li>-->
            </ul>
            <div class="tab">
                <div id="tab-1" class="tab-content">
                    <div class="col_3">
                        <div class="col-md-6 widget widget1">
                            <div class="r3_counter_box">
                                <canvas id="user" class="pie-title-center pull-left" width="400" height="400"></canvas>
                                <a href="<?php echo base_url() . 'index.php/admin/userdetails?type=1'; ?>">
                                    <div class="stats">
                                        <div class="pull-left"><h5><strong><?php echo $usercount; ?></strong></h5>
                                            <span><?php echo $this->lang->line('users'); ?></span></div>
                                        <span class="pull-right"><i class="pull-left fa fa-user icon-rounded"></i></span>
                                    </div>
                            </div>
                            </a>
                        </div>
                        <div class="col-md-6 widget widget1">
                            <div class="r3_counter_box">
                                <canvas id="success" class="pie-title-center pull-left" width="400" height="400"></canvas>
                                <a href="<?php echo base_url(); ?>index.php/admin/successdetails?type=12">
                                    <div class="stats">
                                        <div class="pull-left"><h5><strong><?php echo $successcount; ?></strong></h5>
                                            <span><?php echo $this->lang->line('success'); ?></span></div>
                                        <span class="pull-right"><i class="pull-left fa fa-users user1 icon-rounded"></i></span>
                                    </div>
                            </div>
                            </a>
                        </div>
                        <div class="col-md-6 widget widget1">
                            <div class="r3_counter_box">
                                <canvas id="male" class="pie-title-center pull-left" width="400" height="400"></canvas>
                                <a href="<?php echo base_url() . 'index.php/admin/userdetails?type=2'; ?>">
                                    <div class="stats">
                                        <div class="pull-left"><h5><strong><?php echo $malecount; ?></strong></h5>
                                            <span><?php echo constant('GENDER_M'); ?></span></div>
                                        <span class="pull-right"><i class="pull-left fa fa-male icon-rounded"></i></span>
                                    </div>
                            </div>
                            </a>
                        </div>
                        <div class="col-md-6 widget">
                            <div class="r3_counter_box">
                                <canvas id="female" class="pie-title-center pull-left" width="400" height="400"></canvas>
                                <a href="<?php echo base_url() . 'index.php/admin/userdetails?type=3'; ?>">
                                    <div class="stats  ">
                                        <div class="pull-left"><h5><strong><?php echo $femalecount; ?></strong></h5>
                                            <span><?php echo constant('GENDER_F'); ?></span></div>
                                        <span class="pull-right"><i class="pull-left fa fa-female dollar1 icon-rounded"></i></span>
                                    </div>
                            </div>
                            </a>
                        </div>
                        <div class="clearfix"> </div>
                    </div>

                    <div class="span_11">
                        <div class="col-md-12 col_4">
                            <div class="map_container r3_counter_box">
                                <div id="columnchart_material" class="barchart" ></div>
                            </div> 
                        </div>
                        <!--<div class="col-md-6 col_5">
                          <div class="chart_container r3_counter_box">
                                <div id="pie" class="example" style="width: 100px;"></div>		  
                                </div>
                        </div>-->
                        <div class="clearfix"> </div>
                    </div>

                    <div class="content_bottom">
                        <div class="col-md-6 col_5">
                            <div class=" r3_counter_box">
                                <ba-card bacardclass="large-card with-scroll feed-panel" title="view profile"><div bacardblur="" zoom-in="" class="animated fadeIn card  large-card with-scroll feed-panel">
                                        <div class="card-header clearfix">  <h3 class="card-title"><?php echo $this->lang->line('recent_update'); ?></h3> </div>
                                        <div class="card-body">
                                            <feed>
                                                <?php
                                                if ( !empty( $recentUpdate )){
	                                                foreach ($recentUpdate as $value) { ?>
	                                                    <div class="feed-messages-container">
	                                                        <div class="feed-message">
	                                                            <div class="message-icon">
	                                                                <img class="photo-icon" src="<?php echo base_url() . 'assets/upload_images/' . $value['image']; ?>">
	                                                            </div>
	                                                            <div class="text-block text-message">
	                                                                <div class="message-header textcolor">
	                                                                    <span class="author"><?php echo $value['username']; ?></span>
	                                                                    <span><?php echo '(' . constant("MEMBERID") . $value['userId'] . ')'; ?></span>
	                                                                </div>
	                                                                <div class="message-content line-clamp line-clamp-2 textcolor">
	                                                                    <span class=""><?php echo $this->lang->line('register_profile_for_gender') ?>:<?php echo ($value['gender'] == 'M' ? 'Male' : 'Female'); ?></span><span class="textleft">|</span>
	                                                                    <span class="textleft"><?php echo sprintf($this->lang->line('search_profile_create'), $this->lang->line('by_text')) . ' : ' . $value['profile_created']; ?></span>
	                                                                </div>
	
	                                                                <div class="message-time textcolor">
	                                                                    <div class="post-time"><?php echo $value['date']; ?></div>
	                                                                    <div class="ago-time">
	                                                                        <a href="<?php echo base_url() . 'index.php/admin_profile/viewUser?uid=' . $value['userGuid'] . '&&type=' . constant('TYPE_1'); ?>"><?php echo $this->lang->line('view_profile'); ?></a>
	                                                                    </div>
	                                                                </div>
	                                                            </div>
	                                                        </div>			
	                                                    </div>
                                                <?php } }else{echo $this->lang->line('detail_not_found');} ?>
                                            </feed>
                                        </div>
                                    </div>
                                </ba-card>
                            </div>
                        </div>
                        <div class="col-md-6 col_5">
                            <div class=" r3_counter_box">
                                <ba-card bacardclass="large-card with-scroll feed-panel" title="view profile"><div bacardblur="" zoom-in="" class="animated fadeIn card  large-card with-scroll feed-panel">
                                        <div class="card-header clearfix">  <h3 class="card-title"><?php echo $this->lang->line('recent_premium_user'); ?></h3> </div>
                                        <div class="card-body">
                                            <feed>
                                                <?php 
                                                if (!empty( $userpayment )) {
	                                                foreach ($userpayment as $val) { ?>
	                                                    <div class="feed-messages-container">
	                                                        <div class="feed-message">
	                                                            <div class="message-icon">
	                                                                <img class="photo-icon" src="<?php echo base_url() . 'assets/upload_images/' . $val['image']; ?>">
	                                                            </div>
	                                                            <div class="text-block text-message">
	                                                                <div class="message-header textcolor">
	                                                                    <span class="author"><?php echo $val['username'] . '(' . constant("MEMBERID") . $val['userId'] . ')'; ?></span>
	                                                                </div>
	                                                                <div class="message-content line-clamp line-clamp-2 textcolor">
	                                                                    <span class=""><?php echo $this->lang->line('register_profile_for_email') ?>: <?php echo $val['email']; ?></span><span class="textleft"></span>
	                                                                    <!--<span class="textleft">Month:<?php // echo $val['month'];    ?></span>-->
	                                                                </div>
	                                                                <div class="message-time textcolor">
	                                                                    <div class="post-time"><?php echo $this->lang->line('register_profile_for_gender') ?> : <?php echo ($val['gender'] == 'M' ? 'Male' : 'Female'); ?></div>
	                                                                    <div class="ago-time">
	                                                                        <a class="textcolor" href="<?php echo base_url() . 'index.php/admin_profile/viewUser?uid=' . $val['userGuid'] . '&&type=' . constant('TYPE_4'); ?>"><?php echo $this->lang->line('view_profile'); ?></a>
	                                                                    </div>
	                                                                </div>
	                                                            </div>
	                                                        </div>			
	                                                    </div>
                                                <?php } }else{echo $this->lang->line('detail_not_found');} ?>
                                            </feed>
                                        </div>
                                    </div>
                                </ba-card>
                            </div>
                        </div>
                        <div class="clearfix"> </div>
                    </div>
                </div><!--tab1-->

            </div><!--tab -->
        </div><!--tab cont-->

        <script>


        </script>
        <style>
            .copy {
            /*    background: transparent none repeat scroll 0 0 !important; */
            }
            .tab {
                background-color: transparent !important;
            }
            /*#page-wrapper{min-height: 1340px !important;}*/
        </style>
