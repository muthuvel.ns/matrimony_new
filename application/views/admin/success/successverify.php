
<div id="page-wrapper">
    <div class="graphs bgimage" style="min-height:600px;">
        <content-top>
            <div class="content-top clearfix">
                <h1 class="al-title"><?php echo $this->lang->line('success_story_verfication_title'); ?></h1>
                <ul class="breadcrumb al-breadcrumb">
                    <li><a href="<?php echo base_url() . 'index.php/admin/dashboard'; ?>"><?php echo $this->lang->line('dashboard'); ?></a></li>
                    <li ><a href="<?php echo base_url() . 'index.php/admin/successdetails?type=' . $type; ?>"><?php echo $this->lang->line('success_story_title'); ?></a></li>
                    <li class=""><?php echo $this->lang->line('profile_verfication_text'); ?></li>
                </ul>
            </div>
        </content-top>
        <?php if (!empty($successdata)) { ?>
            <?php foreach ($successdata as $value) { //echo "<pre>";print_r($value);?> 
                <div class="col-md-12 box-content">
                    <div class="col-md-6 text-left margintop">	</div>
                    <div class="col-md-6 margintop  pull-right">	
                        <div class="btn btn-success" onclick="active('<?php echo $value['id']; ?>', '<?php echo $active = 1; ?>')"><?php echo $this->lang->line('active'); ?></div>
                        <div class="btn btn-primary" onclick="active('<?php echo $value['id']; ?>', '<?php echo $active = 2; ?>')"><?php echo $this->lang->line('deactive'); ?></div>
                        <a href="<?php echo base_url() . 'index.php/admin/successedit?mid=' . $value['id'] . '&type=' . $type; ?>"><div class="btn btn-danger" ><?php echo $this->lang->line('edit_text'); ?></div></a>
                    </div>	
                    <hr>				
                    <div class="clearfix"> </div>
                    <!--col-md-5-->
                    <div class="col-md-5 marginbottom">
                        <img height="300" width="350" src="<?php echo base_url() . 'assets/success_images/' . $value['image']; ?>">
                    </div>
                    <div class="col-md-6">
                        <table class="table">
                            <tbody>
                                <tr class="opened">
                                    <td class="day_label"><?php echo $this->lang->line('username'); ?> :</td>
                                    <td class="day_value"><?php echo (!empty($value['groom_name']) ? $value['groom_name'] : 'Not Specified'); ?> & <?php echo (!empty($value['bride_name']) ? $value['bride_name'] : 'Not Specified'); ?></td>
                                </tr>
                                <tr class="opened">
                                    <td class="day_label"><?php echo $this->lang->line('profile_id'); ?> :</td>
                                    <td class="day_value"><?php echo (!empty($value['member_id']) ? constant('MEMBERID') . $value['member_id'] : 'Not Specified'); ?> </td>
                                </tr>
                                <tr class="opened">
                                    <td class="day_label"><?php echo $this->lang->line('marriage_date'); ?>:</td>
                                    <td class="day_value"><?php echo (!empty($value['marriage_date']) ? date('d-M-Y', strtotime($value['marriage_date'])) : 'Not Specified'); ?></td>
                                </tr>
                                <tr class="opened">  
                                    <td class="day_label"><?php echo $this->lang->line('description_text'); ?>:</td>
                                    <td class="day_value"><?php echo $value['success_story']; ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                <?php } ?>
                <div class="clearfix"> </div>
                <?php
            } else {
                echo '<h4 class="textcolor" style="text-align:center;">' . $this->lang->line('success_not_found') . '</h4>';
            }
            ?>
        </div><!--row end-->

        <div class="clearfix"> </div>
    </div>
    <input type="hidden" id="baseurl" value="<?php echo base_url(); ?>"/>
    <script>
        var baseurl = $('#baseurl').val();
        function active(mid, active) {
            if (mid == 0 || mid == '' || active == 0 || active == '') {
                return false;
            }
            $.post(baseurl + 'index.php/admin/successactive?mid=' + mid + '&active=' + active,
                    function (data) {
                        alert(data.msg);
                        location.reload();
                    }, "json");
        }
    </script>
