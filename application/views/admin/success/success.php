<!--<script src="<?php echo base_url(); ?>assets/admin/js/search/animated-search-filter.js"></script>-->
<link src="<?php echo base_url();?>assets/admin/css/dataTables.bootstrap.min.css" rel="stylesheet" />
<script src="<?php echo base_url();?>assets/admin/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url();?>assets/admin/js/dataTables.bootstrap.min.js"></script> 
<script>
    $(document).ready(function () {
		
		$('#example').DataTable( );
		
        $("#selectall").change(function () {
            $(".case").prop('checked', $(this).prop("checked"));
        });
    });

</script>
<style>
.dataTables_length, .dataTables_filter, .dataTables_info, .dataTables_empty {
    color:#fff !important;
}
.sucessemail {
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
    width: 230px;
}
</style>
<script>
    $(document).ready(function () {
        $('#paging_container').pajinate({
            num_page_links_to_display: 3,
            items_per_page: 9
        });
        $("#selectall").change(function () {
            $(".case").prop('checked', $(this).prop("checked"));
        });
    });
</script>
<div id="page-wrapper">
    <div class="graphs bgimage">
        <content-top>
            <div class="content-top clearfix">
                <h1 class="al-title"><?php echo $this->lang->line('success_story_title'); ?></h1>
                <ul class="breadcrumb al-breadcrumb">
                    <li><a href="<?php echo base_url(); ?>index.php/admin/dashboard"><?php echo $this->lang->line('dashboard'); ?></a></li>
                    <li class=""><?php echo $this->lang->line('success_story_title'); ?></li>
                </ul>
            </div>
        </content-top>
        	<table id="example" class="" cellspacing="0" width="100%">
			<thead>
					<th></th>
				</thead>
				<tbody> 
        <?php if (!empty($success)) { ?> 
        
                    <?php foreach ($success as $value) { ?>
                  
                                	<tr class="col-md-4">
					<td class="col-md-4">
                                <div class=" text-left inbox_right space">      	
                                    <div class="mailbox-content profilecontent">
                                        <div class=" success_img">
                                            <div class="col-md-12"><a  href="#"> <img alt="" class="img-success" src="<?php echo base_url() . 'assets/success_images/' . $value['image']; ?>" width="100" height="100"></a></div>
                                        </div> 
                                        <div class=""><span class="email-title"><?php echo (!empty($value['member_id']) ? constant('MEMBERID') . $value['member_id'] : 'Not Specified'); ?> |<?php echo (!empty($value['member_id']) ? date('d-M-Y', strtotime($value['marriage_date'])) : 'Not Specified'); ?></span></div>
                                        <div class="sucessemail" title="<?php echo (!empty($value['email']) ? $value['email'] : 'Not Specified'); ?>"><?php echo $this->lang->line('register_profile_for_email') . ' : ' . (!empty($value['email']) ? $value['email'] : 'Not Specified'); ?></div>
                                        <div class="">
                                            <span class="email-title"><span><?php echo (!empty($value['groom_name']) ? $value['groom_name'] : 'Not Specified'); ?></span> & <span><?php echo (!empty($value['bride_name']) ? $value['bride_name'] : 'Not Specified'); ?></span></span>
                                        </div>
                                        <div class="successbottom">
                                            <?php if (empty($value['deleted'])) { ?>
                                                <span class="pull-left colorsucc">Verified</span>
                                            <?php } else { ?>
                                                <span class="pull-left colorsucc">Not Verified</span>
                                            <?php } ?>
                                            <span class="pull-right"><a href="<?php echo base_url() . 'index.php/admin/sucessverify?mid=' . $value['id'] . '&type=' . $type; ?>"><?php echo $this->lang->line('view_profile'); ?></a></span>
                                        </div>
                                    </div>
                                </div>
                                </td>
                                </tr>
                       
                    <?php } ?>	
          
            <?php
        } else {
            echo '<h4 class="textcolor" style="text-align:center;">' . $this->lang->line('success_not_found') . '</h4>';
        }
        ?>
        </tbody> 
        </table> 
        <div class="clearfix"> </div>
    </div>
