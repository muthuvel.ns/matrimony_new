<script src="<?php echo base_url() . 'assets/admin/js/register.js'; ?>"></script>

<div id="page-wrapper">
    <div class="graphs bgimage" style="min-height:600px;">
        <content-top>
            <div class="content-top clearfix">
                <h1 class="al-title"><?php echo sprintf($this->lang->line('add'), $this->lang->line('profile_admin')); ?></h1>
                <ul class="breadcrumb al-breadcrumb">
                    <li><a href="<?php echo base_url() . 'index.php/admin/dashboard'; ?>"><?php echo $this->lang->line('home'); ?></a></li>
                    <li><a href="<?php echo base_url() . 'index.php/admin/UsersAddInfo'; ?>"><?php echo sprintf($this->lang->line('add'), $this->lang->line('users')); ?></a></li>
                    <li class=""><?php echo sprintf($this->lang->line('add'), $this->lang->line('admin')), $this->lang->line('profile_admin'); ?></li>
                </ul>
            </div>
        </content-top>

        <div class="col-md-9 box-content">		
            <?php if (!empty($message)) { ?>
                <div class="alert alert-danger" style="margin-top:10px; text-align:center;"><?php echo $message; ?></div>
            <?php } ?>
            <!--col-md-4-->
            <form action="<?php echo base_url() . 'index.php/admin/adminadd'; ?>" method="post">	
                <div class="col-md-6 text-left margintop"><h3><?php echo sprintf($this->lang->line('add'), $this->lang->line('admin')), $this->lang->line('profile_admin'); ?></h3></div>

                <div class="col-md-10 text-left">	
                    <table class="table">
                        <tbody>

                            <tr class="opened">
                                <td class="day_label"><?php echo $this->lang->line('register_profile_for_name'); ?> :</td>
                                <td class="day_value"><input type="text" id="username" name="username" class="form-control" required></td>
                            </tr>
                            <tr class="opened">
                                <td class="day_label"><?php echo $this->lang->line('register_profile_for_gender'); ?> :</td>
                                <td class="day_value">
                                    <div class="">
                                        <div class="radio-inline"><label><input type="radio" name="gender" id="gender" value="M" required> <?php echo constant('GENDER_M'); ?></label></div>
                                        <div class="radio-inline"><label><input type="radio" name="gender" id="gender" value="F" required> <?php echo constant('GENDER_F'); ?></label></div>
                                    </div>
                                </td>
                            </tr>
                            <tr class="opened">
                                <td class="day_label"><?php echo $this->lang->line('register_profile_for_dob'); ?> :</td>
                                <td class="day_value">										
                                    <div class="col-md-4 dob"><select id="dobday" name="date" class="form-control input-sm" required></select></div>
                                    <div class="col-md-4 dob"><select id="dobmonth" name="month" class="form-control input-sm" required><div class="col-md-4 dob"></select></div>
                                    <div class="col-md-4 dob"><select id="dobyear" name="year" class="form-control input-sm" required><div class="col-md-4 dob"></select></div>
                                </td>
                            </tr>
                            <tr class="opened">
                                <td class="day_label"><?php echo$this->lang->line('register_profile_for_email'); ?> :</td>
                                <td class="day_value">		 					  
                                    <input type="text" style="text-transform: lowercase;" class="form-control" id="email" name="email" value="" placeholder="Email Id" onblur="emailavailablility()" required pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" oninvalid="this.setCustomValidity('Please enter valid email format')" oninput="setCustomValidity('')">
                                </td>
                            </tr>
                            <tr class="opened">
                                <td class="day_label"><?php echo $this->lang->line('register_profile_for_password'); ?> :</td>
                                <td class="day_value">
                                    <input type="password" class="form-control" id="password" name="password" placeholder="Password" pattern=".{6,}" required oninvalid="this.setCustomValidity('Please enter password minimum 6 character')" oninput="setCustomValidity('')">
                                </td>
                            </tr>


                            <tr class="opened">
                                <td class="day_label"><?php echo sprintf($this->lang->line('mobile_admin_register'), $this->lang->line('number_admin_register')) ?> :</td>
                                <td class="day_value">
                                    <input type="text" class="form-control" title="Enter numbers only" maxlength="12" id="mobile" name="mobile"  placeholder="Mobile Number" required oninvalid="this.setCustomValidity('Please enter mobile number')" oninput="setCustomValidity('')">
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div class=" text-right">
                        <button type="submit" class="btn btn-primary" name=""><?php echo $this->lang->line('register_profile_for_submit'); ?></button>

                    </div>		
                </div>					
            </form>

        </div><!--row end-->

        <div class="clearfix"> </div>
    </div>
    <script>
        $(document).ready(function () {
            $.dobPicker({
                daySelector: '#dobday', /* Required */
                monthSelector: '#dobmonth', /* Required */
                yearSelector: '#dobyear', /* Required */
                dayDefault: 'DD', /* Optional */
                monthDefault: 'MM', /* Optional */
                yearDefault: 'YY', /* Optional */
                minimumAge: 18, /* Optional */
                maximumAge: 40 /* Optional */
            });
        });

        $("#mobile").keypress(function (e) {
            //if the letter is not digit then display error and don't type anything
            if (e.which != 8 && e.which != 0 && ((e.which < 48) || (e.which > 57))) {
                //display error message
                // $("#mobile").html('<label for="mobile" class="error">Numbers Only</label>').show().fadeOut("slow");
                return false;
            }
        });
    </script>
