
<link href="<?php echo base_url(); ?>/assets/admin/css/process/style.css" rel='stylesheet' type='text/css' />
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/select2/select2.min.css" rel='stylesheet' type='text/css' />
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/progress-wizard.min.css" rel='stylesheet' type='text/css' />
<!-- Latest compiled and minified JavaScript -->
<script src="<?php echo base_url(); ?>assets/js/select2/select2.min.js"></script>
<script src="<?php echo base_url().'assets/js/jquery.validate.min.js'?>"></script>
<script>
    $(document).ready(function () {
        $("#register").validate();
    });
</script>
<div id="page-wrapper">
    <div class="graphs bgimage" style="min-height:600px;">
        <content-top>
            <div class="content-top clearfix">
                <h1 class="al-title"><?php echo $this->lang->line('user_information'); ?></h1>
                <ul class="breadcrumb al-breadcrumb">
                    <li><a href="<?php echo base_url() . 'index.php/admin/dashboard'; ?>"><?php echo $this->lang->line('home'); ?></a></li>
                    <li><a href="<?php echo base_url() . 'index.php/admin/UsersAddInfo'; ?>"><?php echo sprintf($this->lang->line('add'), $this->lang->line('users')); ?> </a></li>
                    <li class=""><?php echo $this->lang->line('user_information'); ?></li>
                </ul>
            </div>
        </content-top>
        <!--div class="checkout-wrap">
            <ul class="checkout-bar ">
                <li class="active"><span class="steps"><?php echo $this->lang->line("register_profile_for_process1"); ?></span></li> 
                <li class=""><span class="steps"><?php echo $this->lang->line("register_profile_for_process2"); ?></span></li>       
                <li class=""><span class="steps"><?php echo $this->lang->line("register_profile_for_process3"); ?></span></li>       
                <li class=""><span class="steps"><?php echo $this->lang->line("register_profile_for_process4"); ?></span></li>         
                <li class=""><span class="steps"><?php echo $this->lang->line("register_profile_for_process5"); ?></span></li>                  
            </ul>
        </div-->

		<ul class="progress-indicator">
            <li class="completed">
                <span class="bubble"></span>
                <?php echo $this->lang->line("register_profile_for_process1"); ?> <br><small>(complete)</small>
            </li>
            <li class="active">
                <span class="bubble"></span>
                <?php echo $this->lang->line("register_profile_for_process2"); ?> <br><small>(active)</small>
            </li>
            <li class="">
                <span class="bubble"></span>
                <?php echo $this->lang->line("register_profile_for_process3"); ?> 
            </li>
            <li>
                <span class="bubble"></span>
                <?php echo $this->lang->line("register_profile_for_process4"); ?>
            </li>
            <li>
                <span class="bubble"></span>
                <?php echo $this->lang->line("register_profile_for_process5"); ?>
            </li>
        </ul>


        <div class="col-md-12 box-content">
            <div class="col-md-12 text-left margintop">	<h3><?php echo $this->lang->line('user_information'); ?></h3>	</div>	
            <!--col-md-4-->
            <form method="post" action="<?php echo base_url() . 'index.php/admin/userRegistration?uid=' . $_GET['uid']; ?>" id="register">	
                <div class="col-md-6 text-left margintop">	<h4><?php echo $this->lang->line('basics_and_life_admin_register'); ?></h4>	</div>
                <div class="col-md-10 text-left">	
                    <table class="table">
                        <tbody>
                            <tr class="opened">
                                <td class="day_label"> <label for="maritalstatus" class="form-control-label"><?php echo $this->lang->line('register_profile_for_marriagestatus'); ?> <label class="red">*</label></label> </td>
                                <td class="day_value">
                                    <div class="col-md-3">
                                        <label class="radio-inline reglabrad">
                                            <input type="radio" name="maritalstatus" id="maritalstatus" value="1" required> <?php echo $this->lang->line('register_profile_for_marriagestatus_1'); ?>	
                                        </label>
                                    </div>
                                    <div class="col-md-3">
                                        <label class="radio-inline reglabrad">
                                            <input type="radio" name="maritalstatus" id="watch-me" value="2" required> <?php echo $this->lang->line('register_profile_for_marriagestatus_2'); ?>
                                        </label>
                                    </div>
                                    <div class="col-md-3">
                                        <label class="radio-inline reglabrad">
                                            <input type="radio" name="maritalstatus" id="watch-me" value="3" required> <?php echo $this->lang->line('register_profile_for_marriagestatus_3'); ?>
                                        </label>
                                    </div>
                                    <div class="col-md-3">
                                        <label class="radio-inline reglabrad">
                                            <input type="radio" name="maritalstatus" id="watch-me" value="4" required><?php echo $this->lang->line('register_profile_for_marriagestatus_4'); ?>
                                        </label>
                                    </div>
                                    <label class="error" for="maritalstatus"></label>
                                </td>
                            </tr>
                            <tr class="opened"  id='show-me' style='display:none'>
                                <td class="day_label"><?php echo $this->lang->line('no_of_children_admin_register') ?> </td>
                                <td class="day_value">
                                    <select class="form-control js-example-basic-single" id="child" name="child" required>
                                        <option value="none">None</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4 and above</option>
                                    </select> 
                                </td>
                            </tr>
                            <tr class="opened">
                                <td class="day_label"><label for="caste" class="form-control-label reglab"><?php echo $this->lang->line('register_profile_for_caste'); ?><label class="red">*</label></label> </td>
                                <td class="day_value">
                                    <select class="form-control js-example-data-array" id="caste" name="caste" onchange="casteInfo(this.value)" required>
                                        <option value=""> -- Select Caste -- </option>
                                    </select>
                                    <label class="error" for="caste"></label>
                                </td>
                            </tr>
                            <tr class="opened">
                                <td class="day_label"> <label for="subcaste" class="form-control-label"><?php echo $this->lang->line('register_profile_for_subcaste'); ?></label></td>
                                <td class="day_value">             
                                    <input type="text" class="form-control" name="subcaste" placeholder="<?php echo $this->lang->line("register_profile_for_subcaste"); ?>"></td>
                            </tr>
                            <tr class="opened">
                                <td class="day_label"><label for="gothram" class="form-control-label"><?php echo $this->lang->line('register_profile_for_gothram'); ?></label> </td>
                                <td class="day_value">
                                    <input type="text" class="form-control" name="gothram" placeholder="<?php echo $this->lang->line("register_profile_for_gothram"); ?>">
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <hr>
                    <h4><?php echo $this->lang->line('register_profile_for_location_title1'); ?> </h4>
                    <table class="table">
                        <tbody>
                            <tr class="closed">
                                <td class="day_label col-md-3"><label for="country" class="form-control-label"><?php echo $this->lang->line('register_profile_for_country'); ?><label class="red">*</label></label> </td>
                                <td class="day_value closed col-md-9">
                                    <select class="js-example-data-array form-control" id="country" name="country" onchange="stateInfo(this.value)" required>
                                        <option value=""> -- Select Country -- </option>
                                    </select>
                                    <label class="error" for="country"></label>
                                </td>
                            </tr>
                            <tr class="opened">
                                <td class="day_label"> <label for="state" class=" form-control-label"><?php echo $this->lang->line("register_profile_for_state"); ?> <label class="red">*</label></label> </td>
                                <td class="day_value closed">      
                                    <select class="form-control js-example-data-array"  name="state" id="state" onchange="cityInfo(this.value)" required>
                                        <option value=""> -- Select State -- </option>
                                    </select>
                                    <label class="error" for="state"></label>
                                </td>
                            </tr>
                            <tr class="closed">
                                <td class="day_label"><label for="city" class=" form-control-label"><?php echo $this->lang->line("register_profile_for_city"); ?><label class="red">*</label></label> </td>
                                <td class="day_value closed">   
                                    <select class="form-control js-example-data-array"  id ="city" name="city" required>
                                        <option value=""> -- Select City -- </option>
                                    </select>
                                    <label class="error" for="city"></label>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <hr>
                    <h4><?php echo $this->lang->line('register_profile_for_physical_title1'); ?></h4>
                    <table class="table">
                        <tbody>
                            <tr class="opened">
                                <td class="day_label"><label for="weight" class=" form-control-label"><?php echo $this->lang->line("register_profile_for_height"); ?> <label class="red">*</label></label>  </td>
                                <td class="day_value">
                                    <select class="form-control js-example-basic-single" name="height" id="height" required>
                                        <option value=""> -- <?php echo $this->lang->line("register_profile_for_height"); ?> -- </option>
                                        <option value="145" label="4' 9&quot; (145 cm)">4' 9" (145 cm)</option>
                                        <option value="147" label="4' 10&quot; (147 cm)">4' 10" (147 cm)</option>
                                        <option value="150" label="4' 11&quot; (150 cm)">4' 11" (150 cm)</option>
                                        <option value="152" label="5'  0&quot; (152 cm)">5'  0" (152 cm)</option>
                                        <option value="155" label="5' 1&quot; (155 cm)">5' 1" (155 cm)</option>
                                        <option value="157" label="5' 2&quot; (157 cm)">5' 2" (157 cm)</option>
                                        <option value="160" label="5' 3&quot; (160 cm)">5' 3" (160 cm)</option>
                                        <option value="162" label="5' 4&quot; (162 cm)">5' 4" (162 cm)</option>
                                        <option value="165" label="5' 5&quot; (165 cm)">5' 5" (165 cm)</option>
                                        <option value="167" label="5' 6&quot; (167 cm)">5' 6" (167 cm)</option>
                                        <option value="170" label="5' 7&quot; (170 cm)">5' 7" (170 cm)</option>
                                        <option value="173" label="5' 8&quot; (173 cm)">5' 8" (173 cm)</option>
                                        <option value="175" label="5' 9&quot; (175 cm)">5' 9" (175 cm)</option>
                                        <option value="178" label="5' 10&quot; (178 cm)">5' 10" (178 cm)</option>
                                        <option value="180" label="5' 11&quot; (180 cm)">5' 11" (180 cm)</option>
                                        <option value="183" label="6' 0&quot; (183 cm)">6' 0" (183 cm)</option>
                                        <option value="185" label="6' 1&quot; (185 cm)">6' 1" (185 cm)</option>
                                        <option value="188" label="6' 2&quot; (188 cm)">6' 2" (188 cm)</option>
                                        <option value="190" label="6' 3&quot; (190 cm)">6' 3" (190 cm)</option>
                                        <option value="193" label="6' 4&quot; (193 cm)">6' 4" (193 cm)</option>
                                        <option value="21">Above the Height </option>
                                    </select> 
                                    <label class="error" for="height"></label>
                                </td>
                            </tr>
                            <tr class="opened">
                                <td class="day_label"><label for="weight" class="form-control-label"><?php echo $this->lang->line("register_profile_for_weight"); ?> </label>   </td>
                                <td class="day_value">
                                    <select class="form-control js-example-basic-single" name="weight">
                                        <option value=""> -- <?php echo $this->lang->line("register_profile_for_weight"); ?> -- </option>
                                        <option value="41">41 kg</option>
                                        <option value="42">42 kg</option>
                                        <option value="43">43 kg</option>
                                        <option value="44">44 kg</option>
                                        <option value="45">45 kg</option>
                                        <option value="46">46 kg</option>
                                        <option value="47">47 kg</option>
                                        <option value="48">48 kg</option>
                                        <option value="49">49 kg</option>
                                        <option value="50">50 kg</option>
                                        <option value="51">51 kg</option>
                                        <option value="52">52 kg</option>
                                        <option value="53">53 kg</option>
                                        <option value="54">54 kg</option>
                                        <option value="55">55 kg</option>
                                        <option value="56">56 kg</option>
                                        <option value="57">57 kg</option>
                                        <option value="58">58 kg</option>
                                        <option value="59">59 kg</option>
                                        <option value="60">60 kg</option>
                                        <option value="61">61 kg</option>
                                        <option value="62">62 kg</option>
                                        <option value="63">63 kg</option>
                                        <option value="64">64 kg</option>
                                        <option value="65">65 kg</option>
                                        <option value="66">66 kg</option>
                                        <option value="67">67 kg</option>
                                        <option value="68">68 kg</option>
                                        <option value="69">69 kg</option>
                                        <option value="70">70 kg</option>
                                        <option value="71">71 kg</option>
                                        <option value="72">72 kg</option>
                                        <option value="73">73 kg</option>
                                        <option value="74">74 kg</option>
                                        <option value="75">75 kg</option>
                                        <option value="76">76 kg</option>
                                        <option value="77">77 kg</option>
                                        <option value="78">78 kg</option>
                                        <option value="79">79 kg</option>
                                        <option value="80">80 kg</option>
                                        <option value="81">81 kg</option>
                                        <option value="82">82 kg</option>
                                        <option value="83">83 kg</option>
                                        <option value="84">84 kg</option>
                                        <option value="85">85 kg</option>
                                        <option value="86">86 kg</option>
                                        <option value="87">87 kg</option>
                                        <option value="88">88 kg</option>
                                        <option value="89">89 kg</option>
                                        <option value="90">90 kg</option>
                                        <option value="91">91 kg</option>
                                        <option value="92">92 kg</option>
                                        <option value="93">93 kg</option>
                                        <option value="94">94 kg</option>
                                        <option value="95">95 kg</option>
                                        <option value="96">96 kg</option>
                                        <option value="97">97 kg</option>
                                        <option value="98">98 kg</option>
                                        <option value="99">99 kg</option>
                                        <option value="100">100 kg</option>
                                        <option value="101">Above the weight </option>
                                    </select> 
                                </td>
                            </tr>
                            <tr class="opened">
                                <td class="day_label"><label for="bodytype" class=" form-control-label"><?php echo $this->lang->line("register_profile_for_bodytype"); ?></label> </td>
                                <td class="day_value">
                                    <div class="col-md-3">
                                        <label class="radio-inline reglabrad">
                                            <input type="radio" name="bodytype"  value="1"><?php echo $this->lang->line('register_profile_for_bodytype_1'); ?>
                                        </label>
                                    </div>
                                    <div class="col-md-3">
                                        <label class="radio-inline reglabrad">
                                            <input type="radio" name="bodytype"  value="2"><?php echo $this->lang->line('register_profile_for_bodytype_2'); ?>
                                        </label>
                                    </div>
                                    <div class="col-md-3">
                                        <label class="radio-inline reglabrad">
                                            <input type="radio" name="bodytype"  value="3"><?php echo $this->lang->line('register_profile_for_bodytype_3'); ?>
                                        </label>
                                    </div>
                                    <div class="col-md-3">
                                        <label class="radio-inline reglabrad">
                                            <input type="radio" name="bodytype"  value="4"><?php echo $this->lang->line('register_profile_for_bodytype_4'); ?>
                                        </label>
                                    </div>
                                </td>
                            </tr>
                            <tr class="opened">
                                <td class="day_label"><label for="complexion" class="form-control-label"><?php echo $this->lang->line("register_profile_for_complexion"); ?></label> </td>
                                <td class="day_value">
                                    <div class="col-md-3">
                                        <label class="radio-inline reglabrad">
                                            <input type="radio" name="complexion"  value="1"><?php echo $this->lang->line('register_profile_for_complexion_1'); ?>	
                                        </label>
                                    </div> 
                                    <div class="col-md-3">
                                        <label class="radio-inline reglabrad">
                                            <input type="radio" name="complexion"  value="2"><?php echo $this->lang->line('register_profile_for_complexion_2'); ?>	
                                        </label>
                                    </div> 
                                    <div class="col-md-3">
                                        <label class="radio-inline reglabrad">
                                            <input type="radio" name="complexion"  value="3"> <?php echo $this->lang->line('register_profile_for_complexion_3'); ?>		
                                        </label>
                                    </div> 
                                    <div class="col-md-3">
                                        <label class="radio-inline reglabrad">
                                            <input type="radio" name="complexion"  value="4">	<?php echo $this->lang->line('register_profile_for_complexion_4'); ?>		
                                        </label>
                                    </div> 
                                </td>
                            </tr>
                            <tr class="opened">
                                <td class="day_label"><label for="physicalstatus" class="form-control-label"><?php echo $this->lang->line("register_profile_for_physicalstatus"); ?> <label class="red">*</label></label></td>
                                <td class="day_value closed">	
                                    <div class="col-md-6">
                                        <label class="radio-inline reglabrad">
                                            <input type="radio" name="physicalstatus"  value="1" required> <?php echo $this->lang->line('register_profile_for_physicalstatus_1'); ?> 
                                        </label>
                                    </div>
                                    <div class="col-md-6">
                                        <label class="radio-inline reglabrad">
                                            <input type="radio" name="physicalstatus"  value="2" required> <?php echo $this->lang->line('register_profile_for_physicalstatus_2'); ?>
                                        </label>
                                    </div>
                                    <label class="error" for="physicalstatus"></label>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <hr>
                    <h4><?php echo $this->lang->line('register_profile_for_education_title1'); ?></h4>
                    <table class="table">
                        <tbody>
                            <tr class="opened">
                                <td class="day_label"><label for="edu" class=" form-control-label"><?php echo $this->lang->line("register_profile_for_highesteducation"); ?> <label class="red">*</label></label>  </td>
                                <td class="day_value">
                                    <select class="form-control js-example-data-array" name="education" id ="education" required>
                                        <option value=""> -- Select education -- </option>
                                    </select>
                                    <label class="error" for="education"></label>	
                                </td>
                            </tr>
                            <tr class="opened">
                                <td class="day_label"><label for="occupation" class="form-control-label"><?php echo $this->lang->line("register_profile_for_educationqualification"); ?> <label class="red">*</label></label> </td>
                                <td class="day_value">
                                    <input type="text" class="form-control" name="qualification" placeholder="<?php echo $this->lang->line("register_profile_for_educationqualification"); ?>" required>
                                    <label class="error" for="qualification"></label>
                                </td>
                            </tr>
                            <tr class="opened">
                                <td class="day_label"><label for="occupation" class="form-control-label"><?php echo $this->lang->line("register_profile_for_occupation"); ?> <label class="red">*</label> </td>
                                <td class="day_value">
                                    <input type="text" class="form-control" name="occupation" placeholder="<?php echo $this->lang->line("register_profile_for_occupation"); ?> " required>
                                    <label class="error" for="occupation"></label>
                                </td>
                            </tr>
                            <tr class="opened">
                                <td class="day_label"><label for="empin" class="form-control-label"><?php echo $this->lang->line("register_profile_for_employeed"); ?> <label class="red">*</label></label> </td>
                                <td class="day_value">
                                    <div class="col-md-3">
                                        <label class="radio-inline reglabrad">
                                            <input type="radio" name="emp_in"  value="1" required> <?php echo $this->lang->line('register_profile_for_employeed_1'); ?>
                                        </label>
                                    </div>
                                    <div class="col-md-3">
                                        <label class="radio-inline reglabrad">
                                            <input type="radio" name="emp_in"  value="2" required> <?php echo $this->lang->line('register_profile_for_employeed_2'); ?>
                                        </label>
                                    </div>
                                    <div class="col-md-3">
                                        <label class="radio-inline reglabrad">
                                            <input type="radio" name="emp_in"  value="3" required> <?php echo $this->lang->line('register_profile_for_employeed_3'); ?>
                                        </label>
                                    </div>
                                    <div class="col-md-3">
                                        <label class="radio-inline reglabrad">
                                            <input type="radio" name="emp_in"  value="4" required> <?php echo $this->lang->line('register_profile_for_employeed_4'); ?>
                                        </label>
                                    </div>
                                    <label class="error" for="emp_in"></label>
                                </td>
                            </tr>
                            <tr class="opened">
                                <td class="day_label"><label for="star" class=" form-control-label"><?php echo $this->lang->line("register_profile_for_salary"); ?> </label> </td>
                                <td class="day_value">
                                    <select class="form-control js-example-basic-single" name="salary" id="salary">
                                        <option value=""> -- select salary -- </option>
                                        <option value="1">100000 or less</option>
                                        <option value="2">200000 to 300000</option>
                                        <option value="3">300000 to 400000</option>
                                        <option value="4">400000 to 500000</option>
                                        <option value="5">500000 to 600000</option>
                                        <option value="6">600000 to 700000</option>
                                        <option value="7">700000 to 800000</option>
                                        <option value="8">800000 to 900000</option>
                                        <option value="9">900000 Above</option>
                                        <option value="10">any</option>
                                    </select>  
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <hr>
                    <h4><?php echo $this->lang->line('register_profile_for_habbit_title1'); ?></h4>
                    <table class="table">
                        <tbody>
                            <tr class="closed">
                                <td class="day_label"><label for="food" class=" form-control-label"><?php echo $this->lang->line("register_profile_for_food"); ?> </label> </td>
                                <td class="day_value closed">
                                    <div class="col-md-4">
                                        <label class="radio-inline reglabrad">
                                            <input type="radio" name="food" value="1"> <?php echo $this->lang->line('register_profile_for_food_1'); ?> 
                                        </label>
                                    </div>
                                    <div class="col-md-4">
                                        <label class="radio-inline reglabrad">
                                            <input type="radio" name="food" value="2"> <?php echo $this->lang->line('register_profile_for_food_2'); ?>	
                                        </label>
                                    </div>
                                    <div class="col-md-4">
                                        <label class="radio-inline reglabrad">
                                            <input type="radio" name="food" value="3"> <?php echo $this->lang->line('register_profile_for_food_3'); ?>
                                        </label>
                                    </div>
                                </td>
                            </tr>
                            <tr class="closed">
                                <td class="day_label"> <label for="smoking" class=" form-control-label"> <?php echo $this->lang->line("register_profile_for_smoking"); ?> </label> </td>
                                <td class="day_value closed">			
                                    <div class="col-md-4">
                                        <label class="radio-inline reglabrad">
                                            <input type="radio" name="smoking" value="1">   <?php echo $this->lang->line('register_profile_for_yes'); ?>
                                        </label>
                                    </div>
                                    <div class="col-md-4">
                                        <label class="radio-inline reglabrad">
                                            <input type="radio" name="smoking" value="2">   <?php echo $this->lang->line('register_profile_for_no'); ?>
                                        </label>
                                    </div>
                                    <div class="col-md-4">
                                        <label class="radio-inline reglabrad">
                                            <input type="radio" name="smoking" value="3">  <?php echo $this->lang->line('register_profile_for_smoking_3'); ?> 
                                        </label>
                                    </div>
                                </td>
                            </tr>
                            <tr class="opened">
                                <td class="day_label"> <label for="drinking" class="form-control-label"><?php echo $this->lang->line("register_profile_for_drinking"); ?> </label>  </td>
                                <td class="day_value closed">
                                    <div class="col-md-4">
                                        <label class="radio-inline reglabrad">
                                            <input type="radio" name="drinking" value="1"> <?php echo $this->lang->line('register_profile_for_yes'); ?>  
                                        </label>
                                    </div>
                                    <div class="col-md-4">
                                        <label class="radio-inline reglabrad">
                                            <input type="radio" name="drinking" value="2"> <?php echo $this->lang->line('register_profile_for_no'); ?>
                                        </label>
                                    </div>
                                    <div class="col-md-4">
                                        <label class="radio-inline reglabrad">
                                            <input type="radio" name="drinking" value="3"> <?php echo $this->lang->line('register_profile_for_drinking_3'); ?>
                                        </label>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <hr>
                    <h4><?php echo $this->lang->line('register_profile_for_astrological_title1'); ?></h4>
                    <table class="table">
                        <tbody>
                            <tr class="opened_1">
                                <td class="day_label"><?php echo $this->lang->line('register_profile_for_dhosam'); ?> </td>
                                <td class="day_value">	
                                    <div class="col-md-4">
                                        <label class="radio-inline reglabrad">
                                            <input type="radio" name="dhosam" id="y" value="1"> <?php echo $this->lang->line('register_profile_for_yes'); ?> 
                                        </label>
                                    </div>
                                    <div class="col-md-4">
                                        <label class="radio-inline reglabrad">
                                            <input type="radio" name="dhosam" value="2"> <?php echo $this->lang->line('register_profile_for_no'); ?>	
                                        </label>
                                    </div>
                                    <div class="col-md-4">
                                        <label class="radio-inline reglabrad">
                                            <input type="radio" name="dhosam" value="3"> <?php echo $this->lang->line('register_profile_for_dhosam_3'); ?> 
                                        </label>
                                    </div>
                                </td>
                            </tr>
                            <tr class="opened">
                                <td class="day_label"><?php echo $this->lang->line('register_profile_for_raasi'); ?> </td>
                                <td class="day_value">
                                    <select class="form-control js-example-basic-single"  name="raasi" id="size">
                                        <option value="">-- select one -- </option>
                                        <option value="mesham" label="Mesham">Mesham</option>
                                        <option value="rishabam" label="Rishabam">Rishabam</option>
                                        <option value="mithunam" label="Mithunam">Mithunam</option>
                                        <option value="katagam" label="Katagam">Katagam</option>
                                        <option value="simham" label="Simham">Simham</option>
                                        <option value="kanni" label="Kanni">Kanni</option>
                                        <option value="thulam" label="Thulam">Thulam</option>
                                        <option value="vrichigam" label="Vrichigam">Vrichigam</option>
                                        <option value="dhanush" label="Dhanush">Dhanush</option>
                                        <option value="magaram" label="Magaram">Magaram</option>
                                        <option value="kumbham" label="Kumbham">Kumbham</option>
                                        <option value="meenam" label="Meenam">Meenam</option>	
                                    </select>
                                </td>
                            </tr>
                            <tr class="opened">
                                <td class="day_label"><?php echo $this->lang->line('register_profile_for_star'); ?> </td>
                                <td class="day_value">
                                    <select class="form-control js-example-basic-single" name="star" id="type">
                                        <option value=""> -- <?php echo $this->lang->line("register_profile_for_star"); ?> -- </option>
                                        <option value="ashwini">Ashwini</option>
                                        <option value="bharani">Bharani</option>
                                        <option value="karthigai">Karthigai</option>
                                        <option value="rohini">Rohini</option>
                                        <option value="mirigasirisham">Mirigasirisham</option>
                                        <option value="thiruvathirai">Thiruvathirai</option>
                                        <option value=punarpoosam">Punarpoosam</option>
                                        <option value="poosam">Poosam</option>
                                        <option value="ayilyam">Ayilyam</option>
                                        <option value="makam">Makam</option>
                                        <option value="pooram">Pooram</option>
                                        <option value="uthiram">Uthiram</option>
                                        <option value="hastham">Hastham</option>
                                        <option value="chithirai">Chithirai</option>
                                        <option value="swathi">Swathi</option>
                                        <option value="visakam">Visakam</option>
                                        <option value="anusham">Anusham</option>
                                        <option value="kettai">Kettai</option>
                                        <option value="moolam">Moolam</option>
                                        <option value="pooradam">Pooradam</option>
                                        <option value="uthradam">Uthradam</option>
                                        <option value="thiruvonam">Thiruvonam</option>
                                        <option value="avittam">Avittam</option>
                                        <option value="sadhayam">Sadhayam</option>
                                        <option value="puratathi">Puratathi</option>
                                        <option value="uthirattathi">Uthirattathi</option>
                                        <option value="revathi">Revathi</option>										
                                    </select>  
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <hr>
                    <h4><?php echo $this->lang->line('register_profile_for_family_title1'); ?></h4>
                    <table class="table">
                        <tbody>
                            <tr class="opened">
                                <td class="day_label"><label for="fstatus" class=" form-control-label"><?php echo $this->lang->line("register_profile_for_familystatus"); ?> <label class="red">*</label></label> </td>
                                <td class="day_value">
                                    <div class="col-md-3">
                                        <label class="radio-inline reglabrad">
                                            <input type="radio" name="fstatus" value="1" required> <?php echo $this->lang->line('register_profile_for_familystatus_1'); ?>
                                        </label>
                                    </div>
                                    <div class="col-md-3">
                                        <label class="radio-inline reglabrad">
                                            <input type="radio" name="fstatus" value="2" required> <?php echo $this->lang->line('register_profile_for_familystatus_2'); ?>	
                                        </label>
                                    </div>
                                    <div class="col-md-3">
                                        <label class="radio-inline reglabrad">
                                            <input type="radio" name="fstatus" value="3" required> <?php echo $this->lang->line('register_profile_for_familystatus_3'); ?>	
                                        </label>
                                    </div>
                                    <div class="col-md-3">
                                        <label class="radio-inline reglabrad">
                                            <input type="radio" name="fstatus" value="4" required> <?php echo $this->lang->line('register_profile_for_familystatus_4'); ?>	 
                                        </label>
                                    </div>
                                    <label for="fstatus" class="error"></label>
                                </td>
                            </tr>
                            <tr class="opened">
                                <td class="day_label"><label for="ftype" class=" form-control-label"><?php echo $this->lang->line("register_profile_for_familytype"); ?> <label class="red">*</label></label></td>
                                <td class="day_value">
                                    <div class="col-md-4">
                                        <label class="radio-inline reglabrad">
                                            <input type="radio" name="ftype" value="1" required> <?php echo $this->lang->line('register_profile_for_familytype_1'); ?> 
                                        </label>
                                    </div>
                                    <div class="col-md-4">
                                        <label class="radio-inline reglabrad">
                                            <input type="radio" name="ftype" value="2" required> <?php echo $this->lang->line('register_profile_for_familytype_2'); ?>
                                        </label>
                                    </div>
                                    <label for="ftype" class="error"></label>
                                </td>
                            </tr>
                            <tr class="opened">
                                <td class="day_label"><label for="fvalues" class="form-control-label"><?php echo $this->lang->line("register_profile_for_familyvalue"); ?> <label class="red">*</label></label> </td>
                                <td class="day_value closed">
                                    <div class="col-md-3">
                                        <label class="radio-inline reglabrad">
                                            <input type="radio" name="fvalues" value="1" required> <?php echo $this->lang->line('register_profile_for_familyvalue_1'); ?>
                                        </label>
                                    </div>
                                    <div class="col-md-3">
                                        <label class="radio-inline reglabrad">
                                            <input type="radio" name="fvalues" value="2" required> <?php echo $this->lang->line('register_profile_for_familyvalue_2'); ?>	
                                        </label>
                                    </div>
                                    <div class="col-md-3">
                                        <label class="radio-inline reglabrad">
                                            <input type="radio" name="fvalues" value="3" required> <?php echo $this->lang->line('register_profile_for_familyvalue_3'); ?>
                                        </label>
                                    </div> 
                                    <div class="col-md-3">
                                        <label class="radio-inline reglabrad">
                                            <input type="radio" name="fvalues" value="4" required> <?php echo $this->lang->line('register_profile_for_familyvalue_4'); ?>
                                        </label>
                                    </div>
                                    <label for="fvalues" class="error"></label>
                                </td>
                            </tr>

                        </tbody>
                    </table>
                    <hr>
                    <div class="marginbottom">
                        <h4><?php echo $this->lang->line('register_profile_for_something_title1'); ?></h4>
                        <textarea class="form-control" id="exampleTextarea" name="summary" rows="3"></textarea>

                    </div>
                    <div class=" text-right">
                        <button class="btn btn-primary" name="step2"><?php echo $this->lang->line('register_profile_for_next'); ?></button></a>
                    </div>		
                </div>					
            </form>

        </div><!--row end-->

        <div class="clearfix"> </div>
    </div>

    <script>
        $(document).ready(function () {
            $('input[name=maritalstatus]').click(function () {
                if (this.id == "watch-me") {
                    $("#show-me").show('slow');
                } else {
                    $("#show-me").hide('slow');
                }
            })


            $(".js-example-basic-single").select2();

            $("#caste").select2({
                data: <?php echo $caste; ?>
            });

            $("#country").select2({
                data: <?php echo $country; ?>
            })

            $("#state").select2({
                data: <?php echo $state; ?>
            })
            $("#city").select2({
                data: <?php echo $city; ?>
            })
            $("#education").select2({
                data: <?php echo $education; ?>
            })

        });
    </script>
    <input type="hidden" id="baseurl" value="<?php echo base_url(); ?>"/>
    <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
    <script>
        $(document).ready(function () {
            $("#register").validate();
        });


        function stateInfo(reqData) {
            if (reqData == 0) {
                return false
            }
            $("#city").html('');
            $("#state").html('');
            var baseurl = $('#baseurl').val();//alert(baseurl);
            $.ajax({
                type: "post",
                dataType: "json",
                url: baseurl + 'index.php/common/statelist/' + reqData,
                success: function (request) {
                    if (request.status == 0) {
                        $("#text-state").html('<input type="text" class="form-control" name="state_text" id="state" placeholder=" Enthe the state">');
                        $("#text-city").html('<input type="text" class="form-control" name="city_text" id="city" placeholder=" Enthe the city">');
                    } else {
                        $("#state").select2({
                            data: request
                        });
                    }
                },
                error: function (data) {

                },
            });

            return false;
        }

        function cityInfo(reqData) {
            if (reqData == 0) {
                return false
            }
            $("#city").html('');
            var baseurl = $('#baseurl').val();
            $.ajax({
                type: "post",
                dataType: "json",
                url: baseurl + 'index.php/common/citylist/' + reqData,
                success: function (request) {
                    if (request.status == 0) {
                        $("#text-city").html('<input type="text" class="form-control" name="city_text" id="city" placeholder=" Enthe the city">');
                    } else {
                        $("#city").select2({
                            data: request
                        });
                    }
                },
                error: function (data) {

                },
            });

            return false;
        }

        function casteInfo(reqData) {
            if (reqData == 0) {
                return false
            }
            if (reqData == 49) {
                $("#caste_others").show('slow');
            } else {
                $("#caste_others").hide('slow');
            }

        }
    </script>
    <style>
        label {
            font-weight: 700 !important;
        }
  
    </style>
