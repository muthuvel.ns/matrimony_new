			
<div id="page-wrapper">
    <div class="graphs bgimage" style="min-height: 530px;">
        <div id="mydiv"><!--print start-->	
            <content-top>
                <div class="content-top clearfix">
                    <h1 class="al-title"><?php echo sprintf($this->lang->line('add'), $this->lang->line('users')); ?></h1>
                    <ul class="breadcrumb al-breadcrumb printopt" id="pdfhidden">
                        <li><a href="<?php echo base_url() . 'index.php/admin/dashboard'; ?>"><?php echo $this->lang->line('home'); ?> </a></li>					
                        <li><?php echo sprintf($this->lang->line('add'), $this->lang->line('users')); ?> </li> 
                    </ul>
                </div>
            </content-top>
            <div class="col-md-12 contentinner" >


                <h2><?php echo sprintf($this->lang->line('add'), $this->lang->line('users')); ?></h2> 

                <?php if (!empty($message)) { ?>
                    <div class="alert alert-danger" style="margin-top:10px; text-align:center;"><?php echo $message; ?></div>
                <?php } ?>

                <div class="col-md-offset-1 col-md-4" style="margin-bottom: 10px;">
                    <div class="usersadd">
                        <a href="<?php echo base_url() . 'index.php/admin/adminadd'; ?>" style="color:#fff;">Admin</a>
                    </div>
                </div>
                <div class="col-md-4 col-md-offset-1" style="margin-bottom: 10px;">
                    <div class="usersadd">
                        <a href="<?php echo base_url() . 'index.php/admin/useradd'; ?>" style="color:#fff;">Member</a>
                    </div>
                </div>

            </div>
        </div>
        <div class="clearfix"> </div>

    </div>
