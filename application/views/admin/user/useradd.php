<script src="<?php echo base_url() . 'assets/admin/js/register.js'; ?>"></script>

<div id="page-wrapper">
    <div class="graphs bgimage" style="min-height:600px;">
        <content-top>
            <div class="content-top clearfix">
                <h1 class="al-title"><?php echo sprintf($this->lang->line('add'), $this->lang->line('profile_admin')); ?></h1>
                <ul class="breadcrumb al-breadcrumb">
                    <li><a href="<?php echo base_url() . 'index.php/admin/dashboard'; ?>"><?php echo $this->lang->line('home'); ?></a></li>
                    <li><a href="<?php echo base_url() . 'index.php/admin/UsersAddInfo'; ?>"><?php echo sprintf($this->lang->line('add'), $this->lang->line('users')); ?> </a></li>
                    <li class=""><?php echo sprintf($this->lang->line('add'), $this->lang->line('member_admin_register')), $this->lang->line('profile_admin'); ?></li>
                </ul>
            </div>
        </content-top>

        <div class="col-md-9 box-content">		
            <?php if (!empty($message)) { ?>
                <div class="alert alert-danger" style="margin-top:10px; text-align:center;"><?php echo $message; ?></div>
            <?php } ?>
            <!--col-md-4-->
            <form action="<?php echo base_url() . 'index.php/admin/userRegistration'; ?>" method="post">	
                <div class="col-md-6 text-left margintop"><h3><?php echo sprintf($this->lang->line('add'), $this->lang->line('member_admin_register')), $this->lang->line('profile_admin'); ?></h3></div>

                <div class="col-md-10 text-left">	
                    <table class="table">
                        <tbody>
                            <tr class="opened">
                                <td class="day_label"><?php echo sprintf($this->lang->line('search_profile_create'), $this->lang->line('by_text')); ?> </td>
                                <td class="day_value">
                                    <select class="form-control" id="profile_created" name="profile_created" required >
                                        <option value=""> -- Select Profile for -- </option>
                                        <option value="myself">Myself</option>
                                        <option value="son">Son</option>
                                        <option value="daughter">Daughter</option>
                                        <option value="brother">Brother</option>
                                        <option value="sister">Sister</option>
                                        <option value="relative">Relative</option>
                                        <option value="friend">Friend</option>
                                    </select>
                                </td>
                            </tr>
                            <tr class="opened">
                                <td class="day_label"><label id="label_name"><?php echo $this->lang->line('register_profile_for_name'); ?> </label></td>
                                <td class="day_value"><input type="text" id="username" name="username" class="form-control" required></td>
                            </tr>
                            <tr class="opened">
                                <td class="day_label"><?php echo $this->lang->line('register_profile_for_gender'); ?> </td>
                                <td class="day_value">
                                    <div class="">
                                        <div class="radio-inline"><label><input type="radio" name="gender" id="male" value="M"> Male</label></div>
                                        <div class="radio-inline"><label><input type="radio" name="gender" id="female" value="F" > Female</label></div>
                                        <input type="hidden" name="gender_hidden" id="gender_hidden" value="0" >
                                    </div>
                                </td>
                            </tr>
                            <tr class="opened">
                                <td class="day_label"><?php echo $this->lang->line('register_profile_for_dob'); ?> </td>
                                <td class="day_value">										
                                    <div class="col-md-4 dob"><select id="dobday" name="date" class="form-control input-sm" required></select></div>
                                    <div class="col-md-4 dob"><select id="dobmonth" name="month" class="form-control input-sm" required><div class="col-md-4 dob"></select></div>
                                    <div class="col-md-4 dob"><select id="dobyear" name="year" class="form-control input-sm" required><div class="col-md-4 dob"></select></div>
                                </td>
                            </tr>

                            <tr class="opened_1">
                                <td class="day_label"><?php echo $this->lang->line('register_profile_for_religion'); ?> </td>
                                <td class="day_value">	
                                    <select class="form-control" id="religion" name="religion" required >
                                        <!--<option value=""> -- Select Profile for -- </option>-->
                                        <option value="hindu">Hindu</option>
                                    </select>
                                </td>
                            </tr>
                            <tr class="opened">
                                <td class="day_label"><?php echo $this->lang->line('register_profile_for_tongue'); ?> </td>
                                <td class="day_value">
                                    <select class="form-control" id="mother_tongue" name="mother_tongue" required>
                                        <option value="">- Select your Mother Tongue -</option>
                                        <option value="tamil">Tamil</option>
                                        <option value="bengali">Bengali</option>
                                        <option value="english">English</option>
                                        <option value="gujarati">Gujarati</option>
                                        <option value="hindi">Hindi</option>
                                        <option value="kannada">Kannada</option>
                                        <option value="konkani">Konkani</option>
                                        <option value="malayalam">Malayalam</option>
                                        <option value="marathi">Marathi</option>
                                        <option value="marwari">Marwari</option>
                                        <option value="oriya">Oriya</option>
                                        <option value="punjabi">Punjabi</option>
                                        <option value="sindhi">Sindhi</option>
                                        <option value="telugu">Telugu</option>
                                        <option value="urdu">Urdu
                                        <option value="others">Others</option></option>
                                    </select>
                                </td>
                            </tr>
                            <tr class="opened">
                                <td class="day_label"><?php echo $this->lang->line('register_profile_for_email'); ?> </td>
                                <td class="day_value">							  
                                    <input type="text" style="text-transform: lowercase;" class="form-control" id="email" name="email" value="" placeholder="Email Id" onblur="emailavailablility()" required pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" oninvalid="this.setCustomValidity('Please enter valid email format')" oninput="setCustomValidity('')">
                                </td>
                            </tr>
                            <tr class="opened">
                                <td class="day_label"><?php echo $this->lang->line('register_profile_for_password'); ?> </td>
                                <td class="day_value">
                                    <input type="password" class="form-control" id="password" name="password" placeholder="Password" pattern=".{6,}" required oninvalid="this.setCustomValidity('Please enter password minimum 6 character')" oninput="setCustomValidity('')">
                                </td>
                            </tr>


                            <tr class="opened">
                                <td class="day_label"><?php echo sprintf($this->lang->line('mobile_admin_register'), $this->lang->line('number_admin_register')); ?> </td>
                                <td class="day_value">
                                    <input type="text" class="form-control" title="Enter numbers only" maxlength="12" id="mobile" name="mobile"  placeholder="Mobile Number" required oninvalid="this.setCustomValidity('Please enter mobile number')" oninput="setCustomValidity('')">
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div class=" text-right">
                        <button type="submit" class="btn btn-primary" name="step1"><?php echo $this->lang->line('register_profile_for_next'); ?></button>
                        <button class="btn btn-danger"><?php echo $this->lang->line('cancel_admin_register'); ?></button>
                    </div>		
                </div>					
            </form>

        </div><!--row end-->

        <div class="clearfix"> </div>
    </div>
    <script>
        $(document).ready(function () {
            $.dobPicker({
                daySelector: '#dobday', /* Required */
                monthSelector: '#dobmonth', /* Required */
                yearSelector: '#dobyear', /* Required */
                dayDefault: 'DD', /* Optional */
                monthDefault: 'MM', /* Optional */
                yearDefault: 'YY', /* Optional */
                minimumAge: 18, /* Optional */
                maximumAge: 40 /* Optional */
            });
        });


        $("#profile_created").change(function () {
            var create = $(this).val();
            if (create == "son" || create == "brother") {
                $("#male").prop("checked", true);
                $("#male").prop("disabled", true);
                $("#female").prop("disabled", true);
                $("#gender_hidden").val("M");
                if (create == "son") {
                    $("#label_name").html("Son's Name");
                } else {
                    $("#label_name").html("Groom's Name");
                }
            }

            if (create == "daughter" || create == "sister") {
                $("#female").prop("checked", true);
                $("#male").prop("disabled", true);
                $("#female").prop("disabled", true)
                $("#gender_hidden").val("F");
                if (create == "daughter") {
                    $("#label_name").html("Daughter's Name");
                } else {
                    $("#label_name").html("Bride's Name");
                }
            }

            if (create == "myself" || create == "friend" || create == "relative") {
                $("#male").prop("disabled", false);
                $("#female").prop("disabled", false);
                $("#male").prop("checked", false);
                $("#female").prop("checked", false);
                $("#label_name").html("Name");
                $("#gender_hidden").val("");
            }

            return false;
        });

        $("#mobile").keypress(function (e) {
            //if the letter is not digit then display error and don't type anything
            if (e.which != 8 && e.which != 0 && ((e.which < 48) || (e.which > 57))) {
                //display error message
                // $("#mobile").html('<label for="mobile" class="error">Numbers Only</label>').show().fadeOut("slow");
                return false;
            }
        });

    </script>

    <style>
        label {
            font-weight: 700 !important;
        }
    </style>
