<div class="container border">
    <div class="row about">
        <h1><?php echo $this->lang->line('header_about'); ?></h1>
        <div class="col-md-6">
            <h3 style=" font-size: 1.5em;"><?php echo $this->lang->line("text_matrimony_title"); ?></h3>
            <p><?php echo $this->lang->line("aboutus_page_step1_text"); ?></p>
        </div>

        <div class="col-md-6">
            <img src="<?php echo base_url(); ?>/assets/images/m/1.jpg">
        </div>
        <h3 style="font-size: 1.5em;"class="heading-primary" ><?php echo $this->lang->line("aboutus_page_step2_title"); ?></h3>
        <dl class="faq-list">
            <dt class="faq-list_h">
            <h4 class="marker"></h4>
            <h4><?php echo $this->lang->line("aboutus_page_step2_title1"); ?></h4>
            </dt>
            <dd>
                <h4 class="marker1"></h4>
                <p class="m_4"><?php echo $this->lang->line("aboutus_page_step2_title1_text"); ?></p>
            </dd>
            <dt class="faq-list_h">
            <h4 class="marker"></h4>
            <h4><?php echo $this->lang->line("aboutus_page_step2_title2"); ?></h4>
            </dt>
            <dd>
                <h4 class="marker1"></h4>
                <p class="m_4"><?php echo $this->lang->line("aboutus_page_step2_title2_text"); ?></p>
            </dd>
            <dt class="faq-list_h">
            <h4 class="marker"></h4>
            <h4><?php echo $this->lang->line("aboutus_page_step2_title3"); ?> </h4>
            </dt>
            <dd>
                <h4 class="marker1"></h4>
                <p class="m_4"><?php echo $this->lang->line("aboutus_page_step2_title3_text"); ?></p>
            </dd>
            <dt class="faq-list_h">
            <h4 class="marker"></h4>
            <h4><?php echo $this->lang->line("aboutus_page_step2_title4"); ?> </h4>
            </dt>
            <dd>
                <h4 class="marker1"></h4>
                <p class="m_4"><?php echo $this->lang->line("aboutus_page_step2_title4_text"); ?></p>
            </dd>
            <dt class="faq-list_h">
            <h4 class="marker"></h4>
            <h4><?php echo $this->lang->line("aboutus_page_step2_title5"); ?> </h4>
            </dt>
            <dd>
                <h4 class="marker1"></h4>
                <p class="m_4"><?php echo $this->lang->line("aboutus_page_step2_title5_text"); ?></p>
            </dd>
            <dt class="faq-list_h">
            <h4 class="marker"></h4>
            <h4><?php echo $this->lang->line("aboutus_page_step2_title6"); ?> </h4>
            </dt>
            <dd>
                <h4 class="marker1"></h4>
                <p class="m_4"><?php echo $this->lang->line("aboutus_page_step2_title6_text"); ?></p>
            </dd>
            <dt class="faq-list_h">
            <h4 class="marker"></h4>
            <h4><?php echo $this->lang->line("aboutus_page_step2_title7"); ?></h4>
            </dt>
            <dd>
                <h4 class="marker1"></h4>
                <p class="m_4"><?php echo $this->lang->line("aboutus_page_step2_title7_text"); ?></p>
            </dd>
            <dl>

                <p > <?php echo $this->lang->line("aboutus_page_step3_text"); ?></p>	
                </div>
                </div>



