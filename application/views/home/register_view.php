<!DOCTYPE HTML>
<html>
    <head>
        <title>Matrimonial</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="keywords" content="Marital Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template, 
              Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
        <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
        <link href="<?php echo base_url(); ?>/assets/css/bootstrap-3.1.1.min.css" rel='stylesheet' type='text/css' />
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="<?php echo base_url(); ?>/assets/js/jquery.min.js"></script>
        <script src="<?php echo base_url(); ?>/assets/js/bootstrap.min.js"></script>
        <!-- Custom Theme files -->
        <link href="<?php echo base_url(); ?>/assets/css/register.css" rel='stylesheet' type='text/css' />
        <link href="<?php echo base_url(); ?>/assets/css/style.css" rel='stylesheet' type='text/css' />
        <link href="<?php echo base_url(); ?>/assets/css/your.css" rel='stylesheet' type='text/css' />
        <link href='//fonts.googleapis.com/css?family=Oswald:300,400,700' rel='stylesheet' type='text/css'>
        <link href='//fonts.googleapis.com/css?family=Ubuntu:300,400,500,700' rel='stylesheet' type='text/css'>
        <!----font-Awesome----->
        <link href="<?php echo base_url(); ?>/assets/css/font-awesome.css" rel="stylesheet"> 
        <!----font-Awesome----->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">

        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/select2/select2.min.css">
        
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/progress-wizard.min.css" rel='stylesheet' type='text/css' />

        <!-- Latest compiled and minified JavaScript -->
        <script src="<?php echo base_url(); ?>assets/js/select2/select2.min.js"></script>

        <script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/pagination/js/jquery.pajinate.js"></script>
        
    <input type="hidden" id="baseurl" value="<?php echo base_url(); ?>"/>
    <script>
        $(document).ready(function () {
            $(".dropdown").hover(
                    function () {
                        $('.dropdown-menu', this).stop(true, true).slideDown("fast");
                        $(this).toggleClass('open');
                    },
                    function () {
                        $('.dropdown-menu', this).stop(true, true).slideUp("fast");
                        $(this).toggleClass('open');
                    }
            );
        });
    </script>
    <script>
        $(document).ready(function () {
            $('input[name=maritalstatus]').click(function () {
                if (this.id == "watch-me") {
                    $("#show-me").show('slow');
                } else {
                    $("#show-me").hide('slow');
                }
            })

            $('input[name=dhosam]').click(function () {
                if (this.id == "y") {
                    $("#dhosams").show('slow');
                } else {
                    $("#dhosams").hide('slow');
                }
            })
            $("#type").change(function () {

                var val = $(this).val();
                if (val == "1") {
                    $("#size").html("<option value='test'>raasi 1</option>");
                } else if (val == "2") {
                    $("#size").html("<option value='test'>raasi 2</option>");
                } else if (val == "3") {
                    $("#size").html("<option value='test'>raasi 3</option>");
                }
            });

            $(".js-example-basic-single").select2();

            $("#caste").select2({
                data: <?php echo $caste; ?>
            });

            $("#country").select2({
                data: <?php echo $country; ?>
            })

            $("#state").select2({
                data: <?php echo $state; ?>
            })
            $("#city").select2({
                data: <?php echo $city; ?>
            })
            $("#education").select2({
                data: <?php echo $education; ?>
            })
        });
    </script>
</head>
<body>
    <div class="navbar navbar-inverse-blue navbar">
        <div class="container">
            <div class="pull-left">
                <a class="brand" href=""><img src="<?php echo base_url(); ?>/assets/images/1.png" alt="logo" width="125"></a>
            </div>
        </div>
    </div>

    <!--process-->
    <div class="container">
        <link href="<?php echo base_url(); ?>/assets/css/process/style.css" rel='stylesheet' type='text/css' />

		<ul class="progress-indicator">
            <li class="completed">
                <span class="bubble"></span>
                <?php echo $this->lang->line("register_profile_for_process1"); ?> <br><small>(complete)</small>
            </li>
            <li class="active">
                <span class="bubble"></span>
                <?php echo $this->lang->line("register_profile_for_process2"); ?> <br><small>(active)</small>
            </li>
            <li class="">
                <span class="bubble"></span>
                <?php echo $this->lang->line("register_profile_for_process3"); ?> 
            </li>
            <li>
                <span class="bubble"></span>
                <?php echo $this->lang->line("register_profile_for_process4"); ?>
            </li>
            <li>
                <span class="bubble"></span>
                <?php echo $this->lang->line("register_profile_for_process5"); ?>
            </li>
        </ul>    </div>
    <!--processend-->


    <div class="grid">
        <div class="container gridview">
            <div class="col-md-9">
                <div style="float:left;">
                    <h2 class="registertitle"><?php echo $this->lang->line("register_profile_for_process2"); ?></h2></div>
              <!--<div style="float:right;"><a href="<?php echo base_url(); ?>index.php/home/index"class="btn btn-primary subbtn">Home</a></div>-->
                </br>
                </br>
                <hr>

                <div class="col-md-12 ">
                    <form action="<?php echo base_url(); ?>index.php/home/registration?uid=<?php echo $userGuid; ?>" method="POST" id="register">    
                        <div class="form-group row">
                            <label for="maritalstatus" class="col-sm-4 form-control-label"><?php echo $this->lang->line("register_profile_for_marriagestatus"); ?> <label class="red">*</label></label>
                            <div class="col-sm-8">
                                <div class="col-md-3">
                                    <label class="radio-inline reglabrad">
                                        <input type="radio" name="maritalstatus" id="maritalstatus" value="1" required> <?php echo $this->lang->line("register_profile_for_marriagestatus_1"); ?>
                                    </label>
                                </div>
                                <div class="col-md-3">
                                    <label class="radio-inline reglabrad">
                                        <input type="radio" name="maritalstatus" id="watch-me" value="2" required> <?php echo $this->lang->line("register_profile_for_marriagestatus_2"); ?>
                                    </label>
                                </div>
                                <div class="col-md-3">
                                    <label class="radio-inline reglabrad">
                                        <input type="radio" name="maritalstatus" id="watch-me" value="3" required> <?php echo $this->lang->line("register_profile_for_marriagestatus_3"); ?>
                                    </label>
                                </div>
                                <div class="col-md-3">
                                    <label class="radio-inline reglabrad">
                                        <input type="radio" name="maritalstatus" id="watch-me" value="4" required><?php echo $this->lang->line("register_profile_for_marriagestatus_4"); ?>
                                    </label>
                                </div>
                                <label class="error" for="maritalstatus"></label>
                            </div>
                        </div> 

                        <div class="form-group row" id='show-me' style='display:none'>
                            <label for="children" class="col-sm-4 form-control-label">No.of Children<label class="red">*</label></label>  
                            <div class="col-sm-8">
                                <select class="form-control js-example-basic-single" id="child" name="child" required>
                                    <option value="none">None</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4 and above</option>
                                </select>         
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="caste" class="col-sm-4 form-control-label reglab"><?php echo $this->lang->line("register_profile_for_caste"); ?> <label class="red">*</label></label>     
                            <div class="col-sm-8">
                                <select class="form-control js-example-data-array" id="caste" name="caste" onchange="casteInfo(this.value)" required>
                                    <option value=""> -- Select Caste -- </option>
                                </select>
                                <label class="error" for="caste"></label>
                            </div> 
                        </div>

                        <div class="form-group row" id='caste_others' style='display:none'>
                            <label for="children" class="col-sm-4 form-control-label"></label>  
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="caste_text" placeholder="Enter caste name">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="subcaste" class="col-sm-4 form-control-label"><?php echo $this->lang->line("register_profile_for_subcaste"); ?> </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="subcaste" placeholder="<?php echo $this->lang->line("register_profile_for_subcaste"); ?>">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="gothram" class="col-sm-4 form-control-label"><?php echo $this->lang->line("register_profile_for_gothram"); ?></label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="gothram" placeholder="<?php echo $this->lang->line("register_profile_for_gowtham"); ?>">
                            </div>
                        </div>

                        <div >
                            <span style="float:left;"><i class="fa fa-map-marker fa-2x" aria-hidden="true"></i></span>
                            <span><h3 class="subtitle"><?php echo $this->lang->line("register_profile_for_location_title1"); ?></h3></span>
                        </div>

                        <div class="form-group row" >
                            <label for="country" class="col-sm-4 form-control-label"><?php echo $this->lang->line("register_profile_for_country"); ?> <label class="red">*</label></label>  
                            <div class="col-sm-8">
                                <select class="js-example-data-array" id="country" name="country" onchange="stateInfo(this.value)" required>
                                    <option value=""> -- Select Country -- </option>
                                </select>
                                <label class="error" for="country"></label>
                            </div>
                        </div>

                        <div class="form-group row" >
                            <label for="state" class="col-sm-4 form-control-label"><?php echo $this->lang->line("register_profile_for_state"); ?> <label class="red">*</label></label>  
                            <div class="col-sm-8" id="text-state">
                                <select class="form-control js-example-data-array"  name="state" id="state" onchange="cityInfo(this.value)" required>
                                    <option value=""> -- Select State -- </option>
                                </select>
                                <label class="error" for="state"></label>        
                            </div>
                        </div>

                        <div class="form-group row" >
                            <label for="city" class="col-sm-4 form-control-label"><?php echo $this->lang->line("register_profile_for_city"); ?><label class="red">*</label></label>  
                            <div class="col-sm-8" id="text-city">
                                <select class="form-control js-example-data-array"  id ="city" name="city" required>
                                    <option value=""> -- Select City -- </option>
                                </select>
                                <label class="error" for="city"></label>         
                            </div>
                        </div>

                        <div>
                            <span style="float:left;"><i class="fa fa-user fa-2x" aria-hidden="true"></i></span>
                            <span><h3 class="subtitle"><?php echo $this->lang->line("register_profile_for_physical_title1"); ?></h3></span>
                        </div>


                        <div class="form-group row">
                            <label for="weight" class="col-sm-4 form-control-label"><?php echo $this->lang->line("register_profile_for_height"); ?> <label class="red">*</label></label>  
                            <div class="col-sm-8">
                                <select class="form-control js-example-basic-single" name="height" id="height" required>
                                    <option value=""> -- <?php echo $this->lang->line("register_profile_for_height"); ?> -- </option>
                                    <option value="145" label="4' 9&quot; (145 cm)">4' 9" (145 cm)</option>
                                    <option value="147" label="4' 10&quot; (147 cm)">4' 10" (147 cm)</option>
                                    <option value="150" label="4' 11&quot; (150 cm)">4' 11" (150 cm)</option>
                                    <option value="152" label="5'  0&quot; (152 cm)">5'  0" (152 cm)</option>
                                    <option value="155" label="5' 1&quot; (155 cm)">5' 1" (155 cm)</option>
                                    <option value="157" label="5' 2&quot; (157 cm)">5' 2" (157 cm)</option>
                                    <option value="160" label="5' 3&quot; (160 cm)">5' 3" (160 cm)</option>
                                    <option value="162" label="5' 4&quot; (162 cm)">5' 4" (162 cm)</option>
                                    <option value="165" label="5' 5&quot; (165 cm)">5' 5" (165 cm)</option>
                                    <option value="167" label="5' 6&quot; (167 cm)">5' 6" (167 cm)</option>
                                    <option value="170" label="5' 7&quot; (170 cm)">5' 7" (170 cm)</option>
                                    <option value="173" label="5' 8&quot; (173 cm)">5' 8" (173 cm)</option>
                                    <option value="175" label="5' 9&quot; (175 cm)">5' 9" (175 cm)</option>
                                    <option value="178" label="5' 10&quot; (178 cm)">5' 10" (178 cm)</option>
                                    <option value="180" label="5' 11&quot; (180 cm)">5' 11" (180 cm)</option>
                                    <option value="183" label="6' 0&quot; (183 cm)">6' 0" (183 cm)</option>
                                    <option value="185" label="6' 1&quot; (185 cm)">6' 1" (185 cm)</option>
                                    <option value="188" label="6' 2&quot; (188 cm)">6' 2" (188 cm)</option>
                                    <option value="190" label="6' 3&quot; (190 cm)">6' 3" (190 cm)</option>
                                    <option value="193" label="6' 4&quot; (193 cm)">6' 4" (193 cm)</option>
                                    <option value="21">Above the Height </option>
                                    <!--<option value="175" label="6' 5&quot; (196 cm)">6' 5" (196 cm)</option>
                                    <option value="176" label="6' 6&quot; (198 cm)">6' 6" (198 cm)</option>
                                    <option value="177" label="6' 7&quot; (201 cm)">6' 7" (201 cm)</option>
                                    <option value="178" label="6' 8&quot; (203 cm)">6' 8" (203 cm)</option>
                                    <option value="179" label="6' 9&quot; (206 cm)">6' 9" (206 cm)</option>
                                    <option value="180" label="6' 10&quot; (208 cm)">6' 10" (208 cm)</option>
                                    <option value="181" label="6' 11&quot; (211 cm)">6' 11" (211 cm)</option>
                                    <option value="182" label="7' 0&quot; (213 cm)">7' 0" (213 cm)</option>
                                    <option value="183" label="7' 1&quot; (216 cm)">7' 1" (216 cm)</option>
                                    <option value="184" label="7' 2&quot; (218 cm)">7' 2" (218 cm)</option>
                                    <option value="185" label="7' 3&quot; (221 cm)">7' 3" (221 cm)</option>
                                    <option value="186" label="7' 4&quot; (224 cm)">7' 4" (224 cm)</option>
                                    <option value="187" label="7' 5&quot; (226 cm)">7' 5" (226 cm)</option>
                                    <option value="188" label="7' 6&quot; (229 cm)">7' 6" (229 cm)</option>
                                    <option value="189" label="7' 7&quot; (231 cm)">7' 7" (231 cm)</option>
                                    <option value="190" label="7' 8&quot; (234 cm)">7' 8" (234 cm)</option>
                                    <option value="191" label="7' 9&quot; (236 cm)">7' 9" (236 cm)</option>	-->
                                </select> 
                                <label class="error" for="height"></label>        
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="weight" class="col-sm-4 form-control-label"><?php echo $this->lang->line("register_profile_for_weight"); ?> </label>  
                            <div class="col-sm-8">
                                <select class="form-control js-example-basic-single" name="weight">
                                    <option value=""> -- <?php echo $this->lang->line("register_profile_for_weight"); ?> -- </option>
                                    <option value="41">41 kg</option>
                                    <option value="42">42 kg</option>
                                    <option value="43">43 kg</option>
                                    <option value="44">44 kg</option>
                                    <option value="45">45 kg</option>
                                    <option value="46">46 kg</option>
                                    <option value="47">47 kg</option>
                                    <option value="48">48 kg</option>
                                    <option value="49">49 kg</option>
                                    <option value="50">50 kg</option>
                                    <option value="51">51 kg</option>
                                    <option value="52">52 kg</option>
                                    <option value="53">53 kg</option>
                                    <option value="54">54 kg</option>
                                    <option value="55">55 kg</option>
                                    <option value="56">56 kg</option>
                                    <option value="57">57 kg</option>
                                    <option value="58">58 kg</option>
                                    <option value="59">59 kg</option>
                                    <option value="60">60 kg</option>
                                    <option value="61">61 kg</option>
                                    <option value="62">62 kg</option>
                                    <option value="63">63 kg</option>
                                    <option value="64">64 kg</option>
                                    <option value="65">65 kg</option>
                                    <option value="66">66 kg</option>
                                    <option value="67">67 kg</option>
                                    <option value="68">68 kg</option>
                                    <option value="69">69 kg</option>
                                    <option value="70">70 kg</option>
                                    <option value="71">71 kg</option>
                                    <option value="72">72 kg</option>
                                    <option value="73">73 kg</option>
                                    <option value="74">74 kg</option>
                                    <option value="75">75 kg</option>
                                    <option value="76">76 kg</option>
                                    <option value="77">77 kg</option>
                                    <option value="78">78 kg</option>
                                    <option value="79">79 kg</option>
                                    <option value="80">80 kg</option>
                                    <option value="81">81 kg</option>
                                    <option value="82">82 kg</option>
                                    <option value="83">83 kg</option>
                                    <option value="84">84 kg</option>
                                    <option value="85">85 kg</option>
                                    <option value="86">86 kg</option>
                                    <option value="87">87 kg</option>
                                    <option value="88">88 kg</option>
                                    <option value="89">89 kg</option>
                                    <option value="90">90 kg</option>
                                    <option value="91">91 kg</option>
                                    <option value="92">92 kg</option>
                                    <option value="93">93 kg</option>
                                    <option value="94">94 kg</option>
                                    <option value="95">95 kg</option>
                                    <option value="96">96 kg</option>
                                    <option value="97">97 kg</option>
                                    <option value="98">98 kg</option>
                                    <option value="99">99 kg</option>
                                    <option value="100">100 kg</option>
                                    <option value="101">Above the weight </option>
                                </select>         
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="bodytype" class="col-sm-4 form-control-label"><?php echo $this->lang->line("register_profile_for_bodytype"); ?></label>
                            <div class="col-sm-8">
                                <div class="col-md-3">
                                    <label class="radio-inline reglabrad">
                                        <input type="radio" name="bodytype"  value="1"><?php echo $this->lang->line("register_profile_for_bodytype_1"); ?>
                                    </label>
                                </div>
                                <div class="col-md-3">
                                    <label class="radio-inline reglabrad">
                                        <input type="radio" name="bodytype"  value="2"><?php echo $this->lang->line("register_profile_for_bodytype_2"); ?>
                                    </label>
                                </div>
                                <div class="col-md-3">
                                    <label class="radio-inline reglabrad">
                                        <input type="radio" name="bodytype"  value="3"><?php echo $this->lang->line("register_profile_for_bodytype_3"); ?>
                                    </label>
                                </div>
                                <div class="col-md-3">
                                    <label class="radio-inline reglabrad">
                                        <input type="radio" name="bodytype"  value="4"><?php echo $this->lang->line("register_profile_for_bodytype_4"); ?>
                                    </label>
                                </div>
                            </div>
                        </div> 

                        <div class="form-group row">
                            <label for="complexion" class="col-sm-4 form-control-label"><?php echo $this->lang->line("register_profile_for_complexion"); ?></label>
                            <div class="col-sm-8">
                                <div class="col-md-3">
                                    <label class="radio-inline reglabrad">
                                        <input type="radio" name="complexion"  value="1"><?php echo $this->lang->line("register_profile_for_complexion_1"); ?>
                                    </label>
                                </div> 
                                <div class="col-md-3">
                                    <label class="radio-inline reglabrad">
                                        <input type="radio" name="complexion"  value="2"><?php echo $this->lang->line("register_profile_for_complexion_2"); ?>
                                    </label>
                                </div> 
                                <div class="col-md-3">
                                    <label class="radio-inline reglabrad">
                                        <input type="radio" name="complexion"  value="3"> <?php echo $this->lang->line("register_profile_for_complexion_3"); ?>	
                                    </label>
                                </div> 
                                <div class="col-md-3">
                                    <label class="radio-inline reglabrad">
                                        <input type="radio" name="complexion"  value="4">	<?php echo $this->lang->line("register_profile_for_complexion_4"); ?>	
                                    </label>
                                </div> 
                            </div>
                        </div> 

                        <div class="form-group row">
                            <label for="physicalstatus" class="col-sm-4 form-control-label"><?php echo $this->lang->line("register_profile_for_physicalstatus"); ?> <label class="red">*</label></label>
                            <div class="col-sm-8">
                                <div class="col-md-6">
                                    <label class="radio-inline reglabrad">
                                        <input type="radio" name="physicalstatus"  value="1" required> <?php echo $this->lang->line("register_profile_for_physicalstatus_1"); ?> 
                                    </label>
                                </div>
                                <div class="col-md-6">
                                    <label class="radio-inline reglabrad">
                                        <input type="radio" name="physicalstatus"  value="2" required> <?php echo $this->lang->line("register_profile_for_physicalstatus_2"); ?> 
                                    </label>
                                </div>
                                <label class="error" for="physicalstatus"></label>
                            </div>
                        </div> 

                        <div >
                            <span style="float:left;"><i class="fa fa-book fa-2x" aria-hidden="true"></i></span>
                            <span><h3 class="subtitle"><?php echo $this->lang->line("register_profile_for_education_title1"); ?></h3></span>
                        </div>


                        <div class="form-group row">
                            <label for="edu" class="col-sm-4 form-control-label"><?php echo $this->lang->line("register_profile_for_highesteducation"); ?> <label class="red">*</label></label>  
                            <div class="col-sm-8">
                                <select class="form-control js-example-data-array" name="education" id ="education" required>
                                    <option value=""> -- Select education -- </option>
                                </select>
                                <label class="error" for="education"></label>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="occupation" class="col-sm-4 form-control-label"><?php echo $this->lang->line("register_profile_for_educationqualification"); ?> <label class="red">*</label></label>  
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="qualification" placeholder="<?php echo $this->lang->line("register_profile_for_educationqualification"); ?>" required>
                                <label class="error" for="qualification"></label>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="occupation" class="col-sm-4 form-control-label"><?php echo $this->lang->line("register_profile_for_occupation"); ?> <label class="red">*</label></label>  
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="occupation" placeholder="<?php echo $this->lang->line("register_profile_for_occupation"); ?> " required>
                                <label class="error" for="occupation"></label>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="empin" class="col-sm-4 form-control-label"><?php echo $this->lang->line("register_profile_for_employeed"); ?> <label class="red">*</label></label>
                            <div class="col-sm-8">
                                <div class="col-md-3">
                                    <label class="radio-inline reglabrad">
                                        <input type="radio" name="emp_in"  value="1" required> <?php echo $this->lang->line("register_profile_for_employeed_1"); ?>
                                    </label>
                                </div>
                                <div class="col-md-3">
                                    <label class="radio-inline reglabrad">
                                        <input type="radio" name="emp_in"  value="2" required> <?php echo $this->lang->line("register_profile_for_employeed_2"); ?>
                                    </label>
                                </div>
                                <div class="col-md-3">
                                    <label class="radio-inline reglabrad">
                                        <input type="radio" name="emp_in"  value="3" required> <?php echo $this->lang->line("register_profile_for_employeed_3"); ?>
                                    </label>
                                </div>
                                <div class="col-md-3">
                                    <label class="radio-inline reglabrad">
                                        <input type="radio" name="emp_in"  value="4" required> <?php echo $this->lang->line("register_profile_for_employeed_4"); ?>
                                    </label>
                                </div>
                                <label class="error" for="emp_in"></label>
                            </div>
                        </div> 

                        <div class="form-group row">
                            <label for="star" class="col-sm-4 form-control-label"><?php echo $this->lang->line("register_profile_for_salary"); ?> </label>
                            <div class="col-sm-8">
                                <select class="form-control js-example-basic-single" name="salary" id="salary">
                                    <option value=""> -- select salary -- </option>
                                    <option value="1">100000 or less</option>
                                    <option value="2">200000 to 300000</option>
                                    <option value="3">300000 to 400000</option>
                                    <option value="4">400000 to 500000</option>
                                    <option value="5">500000 to 600000</option>
                                    <option value="6">600000 to 700000</option>
                                    <option value="7">700000 to 800000</option>
                                    <option value="8">800000 to 900000</option>
                                    <option value="9">900000 Above</option>
                                    <option value="10">any</option>
                                </select>   
                            </div>
                        </div>

                        <div>
                            <span style="float:left;"><i class="fa fa-behance-square fa-2x" aria-hidden="true"></i></span>
                            <span><h3 class="subtitle"><?php echo $this->lang->line("register_profile_for_habbit_title1"); ?></h3></span>
                        </div>


                        <div class="form-group row">
                            <label for="food" class="col-sm-4 form-control-label"><?php echo $this->lang->line("register_profile_for_food"); ?> </label>
                            <div class="col-sm-8">
                                <div class="col-md-4">
                                    <label class="radio-inline reglabrad">
                                        <input type="radio" name="food" value="1"> <?php echo $this->lang->line("register_profile_for_food_1"); ?>  
                                    </label>
                                </div>
                                <div class="col-md-4">
                                    <label class="radio-inline reglabrad">
                                        <input type="radio" name="food" value="2"> <?php echo $this->lang->line("register_profile_for_food_2"); ?> 
                                    </label>
                                </div>
                                <div class="col-md-4">
                                    <label class="radio-inline reglabrad">
                                        <input type="radio" name="food" value="3"> <?php echo $this->lang->line("register_profile_for_food_3"); ?> 
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="smoking" class="col-sm-4 form-control-label"> <?php echo $this->lang->line("register_profile_for_smoking"); ?> </label>
                            <div class="col-sm-8">
                                <div class="col-md-4">
                                    <label class="radio-inline reglabrad">
                                        <input type="radio" name="smoking" value="1">  <?php echo $this->lang->line("register_profile_for_yes"); ?> 
                                    </label>
                                </div>
                                <div class="col-md-4">
                                    <label class="radio-inline reglabrad">
                                        <input type="radio" name="smoking" value="2">  <?php echo $this->lang->line("register_profile_for_no"); ?> 
                                    </label>
                                </div>
                                <div class="col-md-4">
                                    <label class="radio-inline reglabrad">
                                        <input type="radio" name="smoking" value="3"> <?php echo $this->lang->line("register_profile_for_smoking_3"); ?>
                                    </label>
                                </div>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="drinking" class="col-sm-4 form-control-label"><?php echo $this->lang->line("register_profile_for_drinking"); ?> </label>
                            <div class="col-sm-8">
                                <div class="col-md-4">
                                    <label class="radio-inline reglabrad">
                                        <input type="radio" name="drinking" value="1"> <?php echo $this->lang->line("register_profile_for_yes"); ?>  
                                    </label>
                                </div>
                                <div class="col-md-4">
                                    <label class="radio-inline reglabrad">
                                        <input type="radio" name="drinking" value="2"> <?php echo $this->lang->line("register_profile_for_no"); ?> 
                                    </label>
                                </div>
                                <div class="col-md-4">
                                    <label class="radio-inline reglabrad">
                                        <input type="radio" name="drinking" value="3"> <?php echo $this->lang->line("register_profile_for_drinking_3"); ?>
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div>
                            <span style="float:left;"><i class="fa fa-asterisk fa-2x" aria-hidden="true"></i></span>
                            <span><h3 class="subtitle"><?php echo $this->lang->line("register_profile_for_astrological_title1"); ?></h3></span>
                        </div>

                        <div class="form-group row">
                            <label for="dhosam" class="col-sm-4 form-control-label"><?php echo $this->lang->line("register_profile_for_dhosam"); ?> </label>
                            <div class="col-sm-8">
                                <div class="col-md-4">
                                    <label class="radio-inline reglabrad">
                                        <input type="radio" name="dhosam" id="y" value="1"> <?php echo $this->lang->line("register_profile_for_yes"); ?>   
                                    </label>
                                </div>
                                <div class="col-md-4">
                                    <label class="radio-inline reglabrad">
                                        <input type="radio" name="dhosam" value="2"> <?php echo $this->lang->line("register_profile_for_no"); ?> 	
                                    </label>
                                </div>
                                <div class="col-md-4">
                                    <label class="radio-inline reglabrad">
                                        <input type="radio" name="dhosam" value="3"> <?php echo $this->lang->line("register_profile_for_dhosam_3"); ?>  
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row" id="dhosams" style="display:none;">
                            <div class="col-sm-offset-4 col-sm-8 ">
                                <input type="text" class="form-control" name="dhosam_type" placeholder="Enter dhosam">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="star" class="col-sm-4 form-control-label"><?php echo $this->lang->line("register_profile_for_star"); ?> </label>
                            <div class="col-sm-8">
                                <select class="form-control js-example-basic-single" name="star" id="type">
                                    <option value=""> -- <?php echo $this->lang->line("register_profile_for_star"); ?> -- </option>
                                    <option value="ashwini">Ashwini</option>
                                    <option value="bharani">Bharani</option>
                                    <option value="karthigai">Karthigai</option>
                                    <option value="rohini">Rohini</option>
                                    <option value="mirigasirisham">Mirigasirisham</option>
                                    <option value="thiruvathirai">Thiruvathirai</option>
                                    <option value=punarpoosam">Punarpoosam</option>
                                    <option value="poosam">Poosam</option>
                                    <option value="ayilyam">Ayilyam</option>
                                    <option value="makam">Makam</option>
                                    <option value="pooram">Pooram</option>
                                    <option value="uthiram">Uthiram</option>
                                    <option value="hastham">Hastham</option>
                                    <option value="chithirai">Chithirai</option>
                                    <option value="swathi">Swathi</option>
                                    <option value="visakam">Visakam</option>
                                    <option value="anusham">Anusham</option>
                                    <option value="kettai">Kettai</option>
                                    <option value="moolam">Moolam</option>
                                    <option value="pooradam">Pooradam</option>
                                    <option value="uthradam">Uthradam</option>
                                    <option value="thiruvonam">Thiruvonam</option>
                                    <option value="avittam">Avittam</option>
                                    <option value="sadhayam">Sadhayam</option>
                                    <option value="puratathi">Puratathi</option>
                                    <option value="uthirattathi">Uthirattathi</option>
                                    <option value="revathi">Revathi</option>										
                                </select>   
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="raasi" class="col-sm-4 form-control-label"><?php echo $this->lang->line("register_profile_for_raasi"); ?></label>
                            <div class="col-sm-8">
                                <select class="form-control js-example-basic-single"  name="raasi" id="size">
                                    <option value="">-- select one -- </option>
                                    <option value="mesham" label="Mesham">Mesham</option>
                                    <option value="rishabam" label="Rishabam">Rishabam</option>
                                    <option value="mithunam" label="Mithunam">Mithunam</option>
                                    <option value="katagam" label="Katagam">Katagam</option>
                                    <option value="simham" label="Simham">Simham</option>
                                    <option value="kanni" label="Kanni">Kanni</option>
                                    <option value="thulam" label="Thulam">Thulam</option>
                                    <option value="vrichigam" label="Vrichigam">Vrichigam</option>
                                    <option value="dhanush" label="Dhanush">Dhanush</option>
                                    <option value="magaram" label="Magaram">Magaram</option>
                                    <option value="kumbham" label="Kumbham">Kumbham</option>
                                    <option value="meenam" label="Meenam">Meenam</option>	
                                </select>
                            </div>
                        </div>

                        <div>
                            <span style="float:left;"><i class="fa fa-users fa-2x" aria-hidden="true"></i></span>
                            <span><h3 class="subtitle"><?php echo $this->lang->line("register_profile_for_family_title1"); ?></h3></span>
                        </div>

                        <div class="form-group row">
                            <label for="fstatus" class="col-sm-4 form-control-label"><?php echo $this->lang->line("register_profile_for_familystatus"); ?> <label class="red">*</label></label>
                            <div class="col-sm-8">
                                <div class="col-md-3">
                                    <label class="radio-inline reglabrad">
                                        <input type="radio" name="fstatus" value="1" required> <?php echo $this->lang->line("register_profile_for_familystatus_1"); ?>
                                    </label>
                                </div>
                                <div class="col-md-3">
                                    <label class="radio-inline reglabrad">
                                        <input type="radio" name="fstatus" value="2" required> <?php echo $this->lang->line("register_profile_for_familystatus_2"); ?>
                                    </label>
                                </div>
                                <div class="col-md-3">
                                    <label class="radio-inline reglabrad">
                                        <input type="radio" name="fstatus" value="3" required> <?php echo $this->lang->line("register_profile_for_familystatus_3"); ?>	
                                    </label>
                                </div>
                                <div class="col-md-3">
                                    <label class="radio-inline reglabrad">
                                        <input type="radio" name="fstatus" value="4" required> <?php echo $this->lang->line("register_profile_for_familystatus_4"); ?>	 
                                    </label>
                                </div>
                                <label for="fstatus" class="error"></label>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="ftype" class="col-sm-4 form-control-label"><?php echo $this->lang->line("register_profile_for_familytype"); ?> <label class="red">*</label></label>
                            <div class="col-sm-8">
                                <div class="col-md-4">
                                    <label class="radio-inline reglabrad">
                                        <input type="radio" name="ftype" value="1" required> <?php echo $this->lang->line("register_profile_for_familytype_1"); ?> 
                                    </label>
                                </div>
                                <div class="col-md-4">
                                    <label class="radio-inline reglabrad">
                                        <input type="radio" name="ftype" value="2" required> <?php echo $this->lang->line("register_profile_for_familytype_2"); ?>
                                    </label>
                                </div>
                            </div>
                            <label for="ftype" class="error"></label>
                        </div>

                        <div class="form-group row">
                            <label for="fvalues" class="col-sm-4 form-control-label"><?php echo $this->lang->line("register_profile_for_familyvalue"); ?> <label class="red">*</label></label>
                            <div class="col-sm-8">
                                <div class="col-md-3">
                                    <label class="radio-inline reglabrad">
                                        <input type="radio" name="fvalues" value="1" required> <?php echo $this->lang->line("register_profile_for_familyvalue_1"); ?>
                                    </label>
                                </div>
                                <div class="col-md-3">
                                    <label class="radio-inline reglabrad">
                                        <input type="radio" name="fvalues" value="2" required> <?php echo $this->lang->line("register_profile_for_familyvalue_2"); ?>	
                                    </label>
                                </div>
                                <div class="col-md-3">
                                    <label class="radio-inline reglabrad">
                                        <input type="radio" name="fvalues" value="3" required> <?php echo $this->lang->line("register_profile_for_familyvalue_3"); ?>
                                    </label>
                                </div> 
                                <div class="col-md-3">
                                    <label class="radio-inline reglabrad">
                                        <input type="radio" name="fvalues" value="4" required> <?php echo $this->lang->line("register_profile_for_familyvalue_4"); ?>
                                    </label>
                                </div>
                                <label for="fvalues" class="error"></label>
                            </div>
                        </div>

                        <div>
                            <span style="float:left;"><i class="fa fa-pencil-square fa-2x" aria-hidden="true"></i></span>
                            <span><h3 class="subtitle"><?php echo $this->lang->line("register_profile_for_something_title1"); ?></h3></span>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-12">
                                <textarea class="form-control" id="exampleTextarea" name="summary" rows="3"></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-offset-4 col-sm-8">
                                <button type="submit" class="btn btn-primary subbtn" name="step2"><?php echo $this->lang->line("register_profile_for_submit"); ?></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-3 per_img">
                <img src="<?php echo base_url(); ?>assets/images/register/1.png" width="100%" height="200">
                <img src="<?php echo base_url(); ?>assets/images/register/1.png" width="100%" height="200">
                <img src="<?php echo base_url(); ?>assets/images/register/1.png" width="100%" height="200">
                <img src="<?php echo base_url(); ?>assets/images/register/1.png" width="100%" height="200">
                <img src="<?php echo base_url(); ?>assets/images/register/1.png" width="100%" height="200">
                <img src="<?php echo base_url(); ?>assets/images/register/1.png" width="100%" height="200">
                <img src="<?php echo base_url(); ?>assets/images/register/1.png" width="100%" height="200">
                <img src="<?php echo base_url(); ?>assets/images/register/1.png" width="100%" height="200">
                <img src="<?php echo base_url(); ?>assets/images/register/1.png" width="100%" height="200">
            </div>
        </div>
    </div>
    <input type="hidden" id="baseurl" value="<?php echo base_url(); ?>"/>
    <script src="<?php echo base_url().'assets/js/jquery.validate.min.js'?>"></script>
    <script>
                                    $(document).ready(function () {
                                        $("#register").validate();
                                    });

                                    function stateInfo(reqData) {
                                        if (reqData == 0) {
                                            return false
                                        }
                                        $("#city").html('');
                                        $("#state").html('');
                                        var baseurl = $('#baseurl').val();//alert(baseurl);
                                        $.ajax({
                                            type: "post",
                                            dataType: "json",
                                            url: baseurl + 'index.php/common/statelist/' + reqData,
                                            success: function (request) {
                                                if (request.status == 0) {
                                                    $("#text-state").html('<input type="text" class="form-control" name="state_text" id="state" placeholder=" Enthe the state">');
                                                    $("#text-city").html('<input type="text" class="form-control" name="city_text" id="city" placeholder=" Enthe the city">');
                                                } else {
                                                    $("#state").select2({
                                                        data: request
                                                    });
                                                }
                                            },
                                            error: function (data) {
                                                alert("Error in connection");
                                                location.reload();
                                            },
                                        });

                                        return false;
                                    }

                                    function cityInfo(reqData) {
                                        if (reqData == 0) {
                                            return false
                                        }
                                        $("#city").html('');
                                        var baseurl = $('#baseurl').val();
                                        $.ajax({
                                            type: "post",
                                            dataType: "json",
                                            url: baseurl + 'index.php/common/citylist/' + reqData,
                                            success: function (request) {
                                                if (request.status == 0) {
                                                    $("#text-city").html('<input type="text" class="form-control" name="city_text" id="city" placeholder=" Enthe the city">');
                                                } else {
                                                    $("#city").select2({
                                                        data: request
                                                    });
                                                }
                                            },
                                            error: function (data) {
                                                alert("Error in connection");
                                                location.reload();
                                            },
                                        });

                                        return false;
                                    }

                                    function casteInfo(reqData) {
                                        if (reqData == 0) {
                                            return false
                                        }
                                        if (reqData == 49) {
                                            $("#caste_others").show('slow');
                                        } else {
                                            $("#caste_others").hide('slow');
                                        }

                                    }
    </script>



</body>
</html>
