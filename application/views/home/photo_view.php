<!DOCTYPE HTML>
<html>
    <head>
        <title>Matrimonial</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="keywords" content="Marital Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template, 
              Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
        <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
        <!-- Custom Theme files -->
        <link href="<?php echo base_url(); ?>/assets/css/style.css" rel='stylesheet' type='text/css' />
        <link href="<?php echo base_url(); ?>/assets/css/your.css" rel='stylesheet' type='text/css' />
        <link href='//fonts.googleapis.com/css?family=Oswald:300,400,700' rel='stylesheet' type='text/css'>
        <link href='//fonts.googleapis.com/css?family=Ubuntu:300,400,500,700' rel='stylesheet' type='text/css'>
        <!----font-Awesome----->
        <link href="<?php echo base_url(); ?>/assets/css/font-awesome.css" rel="stylesheet"> 
        <!----font-Awesome----->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">

        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/fileinput.css" media="all" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/progress-wizard.min.css" rel='stylesheet' type='text/css' />
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/fileinput.js" type="text/javascript"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js" type="text/javascript"></script>
    </head>
    <body>
        <!-- ============================  Navigation Start =========================== -->
        <div class="navbar navbar-inverse-blue navbar">
            <div class="container">

                <div class="pull-left"><a class="brand" href=""><img src="<?php echo base_url(); ?>assets/images/1.png" alt="logo" width="125"></a></div>
            </div> 

            <!--process-->
            <style>
                .checkout-wrap {
                    margin-bottom: 7% !important;
                }
            </style>
            <div class="container">
                <link href="<?php echo base_url(); ?>/assets/css/process/style.css" rel='stylesheet' type='text/css' />
					<ul class="progress-indicator">
						<li class="completed">
							<span class="bubble"></span>
							<?php echo $this->lang->line("register_profile_for_process1"); ?> <br><small>(complete)</small>
						</li>
						<li class="completed">
							<span class="bubble"></span>
							<?php echo $this->lang->line("register_profile_for_process2"); ?> <br><small>(complete)</small>
						</li>
						<li class="active">
							<span class="bubble"></span>
							<?php echo $this->lang->line("register_profile_for_process3"); ?> <br><small>(active)</small>
						</li>
						<li>
							<span class="bubble"></span>
							<?php echo $this->lang->line("register_profile_for_process4"); ?>
						</li>
						<li>
							<span class="bubble"></span>
							<?php echo $this->lang->line("register_profile_for_process5"); ?>
						</li>
					</ul>
            </div>
            <!--processend-->
            <div class="container add">
                <!--			<div class="btn btn-success part1 ">home </div>-->
                <div class="btn btn-primary part" style="margin-right: 20px;"><a style="color:#fff; "href="<?php echo base_url() . 'index.php/home/partnerinfo?uid=' . $userGuid; ?>">  <?php echo $this->lang->line("register_profile_for_next"); ?>
                    </a></div></br></br>
                <div class="photo">
                    <div class="row photo_upload">
                        <div class="container col-md-12">
                            <form enctype="multipart/form-data">
                                <div class="form-group">
                                    <input id="images" type="file" name="upfile" multiple class="file" data-overwrite-initial="false" data-min-file-count="1">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <input type="hidden" id="baseurl" value="<?php echo base_url(); ?>">
            <input type="hidden" id="userguid" value="<?php echo $userGuid; ?>">
            <script>
                var baseurl = $("#baseurl").val();
                var userguid = $("#userguid").val();
                $("#images").fileinput({
                    uploadUrl: baseurl + 'index.php/home/fileupload?uid=' + userguid, // you must set a valid URL here else you will get an error
                    allowedFileExtensions: ['jpg', 'png', 'jpeg'],
                    overwriteInitial: false,
                    maxFileSize: 1000,
                    maxFilesNum: 5,
                    //allowedFileTypes: ['image', 'video', 'flash'],
                    slugCallback: function (filename) {
                        return filename.replace('(', '_').replace(']', '_');
                    }
                });

                $(document).ready(function () {
                    /** overall upload button hide */
                    $(".fileinput-upload-button").hide();
                });
            </script>
