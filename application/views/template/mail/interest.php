<table cellspacing="0" cellpadding="0" border="0" style="background:#f1f1f2;width:100%;border-top:10px solid #f1f1f2; height: 600px;">
    <tbody><tr>
            <td align="center" valign="top">
                <table cellspacing="0" cellpadding="0" border="0" width="548" style="background:#fff;">
                    <tbody><tr>
                            <td align="left" bgcolor="" valign="top">
                                <table cellspacing="0" cellpadding="0" border="0" width="548"> 
                                    <tbody>
                                        <tr>
                                            <td align="left" valign="top" style="padding-top:18px;padding-bottom:18px;padding-left:15px;  border-bottom: 3px solid #ff9916;">
                                                <img width="125" border="0" title="" alt=" Matrimonial Site" src="<?php echo base_url() . 'assets/images/1.png' ?>" class="CToWUd">					
                                                <span style=" vertical-align: 155%;color: #FF9916;font-size: 22px;font-weight: 400;"><?php echo $this->lang->line("text_matrimony_title"); ?></span>
                                            </td>
                                        </tr>
                                    </tbody></table>
                            </td>
                        </tr>

                        <tr><td height="15" align="left" valign="top"></td></tr>
                        <tr><td height="15" valign="top"></td></tr>
                        <tr>
                            <td align="left" valign="top">
                                <table cellspacing="0" cellpadding="0" border="0" width="548" > 

                                    <tbody><tr>
                                            <td align="left" valign="top">
                                                <table cellspacing="0" cellpadding="0" border="0" width="528"> 
                                                    <tbody>
                                                        <tr><td align="left" valign="top"></td><td align="left" valign="top" style="font:bold 12px/13px arial;color:#363636; padding-left:13px;padding-bottom:10px;"><span><?php echo sprintf($this->lang->line("mail_interest_step1"), (!empty($partnername) ? ucwords($partnername) : ''), $partnerid); ?>,</span></td></tr>
                                                        <?php if ($interest != REQUEST_NOT_INETREST) { ?>
                                                            <tr><td align="left" valign="top"></td><td align="left" valign="top" style="padding-bottom:5px;font:normal 12px/18px arial;color:#777777;padding-top:5px;padding-left:25px;"><?php echo ucwords($username); ?><a  target="_blank" style="color:#0274cb;text-decoration:none" href="<?php echo base_url(); ?>"><span style="font-size:14px"><?php echo sprintf($this->lang->line('mail_member_id'), $userId) ?></span></a> <?php echo ( $interest == 1 ? $this->lang->line('mail_accept_profile') : $this->lang->line('mail_interest_profile')) ?>
                                                                    <a  target="_blank" style="color:#0274cb;text-decoration:none;font-size:12px" href="<?php echo base_url(); ?>"><?php echo $this->lang->line('mail_communiaction') ?></a></td></tr>
                                                        <?php } ?>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody></table>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" valign="top" style="padding:0px 15px 28px 15px">
                                <table cellspacing="0" cellpadding="0" border="0" width="518"> 
                                    <tbody><tr>
                                            <td align="left" valign="top">
                                                <table cellspacing="0" cellpadding="0" border="0" bgcolor="#f6f6f6" width="510" style=" border:1px solid #e9e9e9"> 
                                                    <tbody><tr><td height="10" colspan="4"><img height="10" border="0" style="" class="CToWUd"></td></tr>
                                                        <tr>
                                                            <td align="left" width="10" valign="top"><img border="0" width="10" style="display:block"  class="CToWUd"></td>
                                                            <td align="left" width="102" valign="top"><a  target="_blank" href="#"><img height="101" border="0" width="101" alt="" src="<?php echo base_url() . 'assets/upload_images/' . $image; ?>" class="CToWUd"></a></td>
                                                            <td align="left" width="8" valign="top"><img border="0" width="8" style="display:block"  class="CToWUd"></td>
                                                            <td align="left" width="354" valign="top" style="padding:0px 10px 30px 0px">
                                                                <table cellspacing="0" cellpadding="0" border="0" width="354" style="">
                                                                    <tbody><tr>
                                                                            <td align="left" valign="top" style="font:bold 13px arial;color:#363636;line-height:20px">
                                                                                <a  target="_blank" style="text-decoration:none;color:#363636" href="#"><?php echo ucwords($username) . ' ' . sprintf($this->lang->line("mail_member_id"), $userId); ?></a></td>
                                                                        </tr>
                                                                        <tr><td height="5" valign="top"></td></tr>
                                                                        <tr>
                                                                            <td align="left" valign="top" style="font:normal 13px/20px arial;color:#363636">
                                                                                <a target="_blank" style="text-decoration:none;color:#363636" href="#"><?php echo sprintf($this->lang->line("mail_content_data"), $age, $height, $religion, $state_name, $city_name, $edu_name . '-' . $qualification, $occupation); ?></a></td>
                                                                        </tr>
                                                                        <tr><td height="8" valign="top"></td></tr>
                                                                        <tr>
                                                                            <td align="left" width="354" valign="top">
                                                                                <table cellspacing="0" cellpadding="0" border="0" width="354">
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td align="left" valign="top" style="font:normal 12px/15px arial;color:#1b68b8">
                                                                                                <a  target="_blank" style="color:#0274cb;text-decoration:none" href="<?php echo base_url(); ?>"><?php echo $this->lang->line("text_view_profile"); ?></a></td>
                                                                                        </tr>
                                                                                        <?php if ($interest == REQUEST_INTEREST) { ?>
                                                                                            <tr><td align="right" valign="top"><a target="_blank" style="text-decoration:none;color:#0274cb" href="<?php echo base_url(); ?>"><img height="39" border="0" width="135" alt="" style="display:inline-block;" src="<?php echo base_url() . 'assets/images/send.gif'; ?>" class="CToWUd"></a></td></tr>
                                                                                        <?php } ?>

                                                                                        <?php if ($interest == REQUEST_NOT_INETREST) { ?>
                                                                                            <tr style="color:#363636"><td style="font:normal 12px/15px arial;">Your profile did not match his expectations. Your ideal match is still waiting for you. </td></tr>
                                                                                            <tr><td align="right" valign="top"><a target="_blank" style="text-decoration:none;color:#0274cb" href="<?php echo base_url(); ?>"><img height="39" border="0" width="135" alt="" style="display:inline-block;" src="<?php echo base_url() . 'assets/images/suitable.gif'; ?>" class="CToWUd"></a></td></tr>
                                                                                        <?php } ?>	
                                                                                    </tbody></table>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody></table>
                                                            </td>
                                                            <td align="left" width="12" valign="top"></td>
                                                        </tr>
                                                    </tbody></table>
                                            </td>
                                        </tr>
                                    </tbody></table>
                            </td>
                        </tr>
                        </td></tr>
                        <tr>
                            <td align="left" bgcolor="#ffffff" valign="top" style="font:normal 12px/18px arial;text-align:left;color:#363636;padding-left:28px">
                            </td>
                        </tr>
                        <tr><td height="15" valign="top"></td></tr>
                    </tbody></table>
            </td>
        </tr>
    </tbody></table>
