<table cellspacing="0" cellpadding="0" border="0" style="background:#f1f1f2;width:100%;border-top:10px solid #f1f1f2; height: 600px;">
    <tbody><tr>
            <td align="center" valign="top">
    <u></u>
    <table cellspacing="0" cellpadding="0" border="0" width="100%" style="background:#fff;max-width:550px;border:1px solid #dfe0e3 #dfe0e3;border-bottom:none">
        <tbody><tr><td><u></u></td></tr>
        <tr>
            <td align="left" valign="top" style="padding-top:18px;padding-bottom:18px;padding-left:15px">
                <img width="125" border="0" title="" alt="" src="<?php echo base_url() . 'assets/images/1.png' ?>" class="CToWUd">					
                <span style="vertical-align: 155%; color: #FF9916;font-size: 22px;font-weight: 400;"><?php echo $this->lang->line("text_matrimony_title"); ?></span>
            </td>
        </tr>
        </tbody></table>
    <u></u>
    <u></u>
    <table cellspacing="0" cellpadding="0" border="0" width="100%" style="max-width:550px; border-top:none;border-bottom:2px solid #fff;background:#fff">
        <tbody><tr><td><u></u>
        </td></tr><tr>
            <td valign="top" style="padding-top:24px;padding-right:40px;padding-left:40px; border-top: 3px solid #FF9916;">
                <table cellspacing="0" cellpadding="0" border="0" width="100%">
                    <tbody><tr>
                            <td valign="top" style="font:400 20px arial;line-height:21px;color:#333333;text-align:left;padding-bottom:13px">
                                <?php echo sprintf($this->lang->line("mail_activate_acc_step1"), (!empty($username) ? ucwords($username) : ''), $id); ?>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" style="font:normal 17px arial;line-height:21px;color:#555555;text-align:left;padding-bottom:16px">
                                <?php echo $this->lang->line("mail_activate_acc_step2"); ?><a target="_blank" style="outline:none;text-decoration:none;color:#FF9916;" href="<?php echo base_url() ?>"> <?php echo $this->lang->line("text_matrimony_title"); ?>.</a>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" valign="top">
                                <table cellspacing="0" cellpadding="0" border="0" width="177">
                                    <tbody><tr>
                                            <td valign="top" style="cursor: pointer;font:normal 18px arial;line-height:21px;color:#555555;border:1px solid #59B530;text-align:center;padding-top:8px;padding-bottom:8px">
                                                <span><img style="width:17px;" alt="" title="<?php echo $this->lang->line("mail_activate_acc_step3"); ?>"src="<?php echo base_url(); ?>assets/images/hand1.png"></span> <span><a  target="_blank" style="outline:none;text-decoration:none;color:#59B530" title="<?php echo $this->lang->line("mail_activate_acc_step3"); ?>" href="<?php echo base_url() . 'index.php/login/activate?id=' . $guid; ?>"><?php echo $this->lang->line("mail_activate_acc_step3"); ?></a></span>
                                            </td>
                                        </tr>
                                    </tbody></table>
                            </td>
                        </tr>

                        <tr>
                            <td valign="top" style="font:normal 14px arial;line-height:17px;color:#bbb;text-align:left;padding-bottom:11px">
                                <br>
                                <!--<a  target="_blank" style="outline:none;text-decoration:none;color:#69c" href="#">Matrimony</a>-->
                            </td>
                        </tr>
                    </tbody></table>
            </td>
        </tr>
        </tbody>
    </table>
</td>
</tr>
</tbody>
</table>


