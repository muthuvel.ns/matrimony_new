<table cellspacing="0" cellpadding="0" border="0" style="background:#f1f1f2;width:100%;border-top:10px solid #f1f1f2; height: 600px;">
    <tbody><tr>
            <td align="center" valign="top">
                <table cellspacing="0" cellpadding="0" border="0" width="548" style="background:#fff;">
                    <tbody><tr>
                            <td align="left" bgcolor="" valign="top">
                                <table cellspacing="0" cellpadding="0" border="0" width="548"> 
                                    <tbody>
                                        <tr>
                                            <td align="left" valign="top" style="padding-top:18px;padding-bottom:18px;padding-left:15px;  border-bottom: 3px solid #ff9916;">
                                                <img width="125" border="0" title="" alt=" Matrimonial Site" src="<?php echo base_url() . 'assets/images/1.png' ?>" class="CToWUd">					
                                                <span style=" vertical-align: 155%;color: #FF9916;font-size: 22px;font-weight: 400;"><?php echo $this->lang->line("text_matrimony_title"); ?></span>
                                            </td>
                                        </tr>
                                    </tbody></table>
                            </td>
                        </tr>
                        <tr><td height="15" align="left" valign="top"></td></tr>
                        <tr><td height="15" valign="top"></td></tr>
                        <tr>
                            <td align="left" valign="top">
                                <table cellspacing="0" cellpadding="0" border="0" width="548" > 
                                    <tbody><tr>
                                            <td align="left" valign="top">
                                                <table cellspacing="0" cellpadding="0" border="0" width="528" style="padding-left: 10px;"> 
                                                    <tbody><tr>	<td align="left" valign="top" style="font:bold 12px/13px arial;color:#363636"><span><?php echo sprintf($this->lang->line("mail_matches_step1"), $username, $userId) ?></span></td></tr>
                                                        <tr><td align="left" valign="top" style="font:normal 12px/18px arial;color:#777777;padding-top:10px"><?php echo $title; ?></td></tr>
                                                    </tbody></table>
                                            </td>
                                        </tr>
                                    </tbody></table>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" valign="top" style="padding:0px 15px 10px 15px">
                                <table cellspacing="0" cellpadding="0" border="0" width="518"> 
                                    <tbody>
                                        <?php
                                        if (!empty($matches)) {
                                            foreach ($matches as $val) {
                                                ?>
                                                <tr>
                                                    <td align="left" valign="top">
                                                        <table cellspacing="0" cellpadding="0" border="0" bgcolor="#f6f6f6" width="510" style=" border:1px solid #e9e9e9; margin-top:10px;padding:10px;"> 
                                                            <tbody>
                                                                <tr>
                                                                    <td align="left" width="102" valign="top"><a  target="_blank" href="#"><img height="101" border="0" width="101" alt="" src="<?php echo base_url() . 'assets/upload_images/' . $val['image']; ?>" class="CToWUd"></a></td>
                                                                    <td align="left" width="8" valign="top"><img border="0" width="8" style="display:block"  class="CToWUd"></td>
                                                                    <td align="left" width="354" valign="top" style="padding:0px 10px 0 0px">
                                                                        <table cellspacing="0" cellpadding="0" border="0" width="354" style="margin-bottom: 30px;">
                                                                            <tbody><tr>
                                                                                    <td align="left" valign="top" style="font:bold 13px arial;color:#363636;line-height:20px">
                                                                                        <a  target="_blank" style="text-decoration:none;color:#363636" href="#"><?php echo ucwords($val['username']) . ' ' . sprintf($this->lang->line("mail_member_id"), $val['userId']); ?></a></td>
                                                                                </tr>
                                                                                <tr><td height="5" valign="top"></td></tr>
                                                                                <tr>
                                                                                    <td align="left" valign="top" style="font:normal 13px/20px arial;color:#363636">
                                                                                        <a target="_blank" style="text-decoration:none;color:#363636" href="#"><?php echo sprintf($this->lang->line("mail_content_data"), $val['age'], $val['height'], $val['religion'], $val['state_name'], $val['city_name'], $val['edu_name'] . '-' . $val['qualification'], $val['occupation']); ?></a></td>
                                                                                </tr>
                                                                                <tr><td height="8" valign="top"></td></tr>
                                                                                <tr>
                                                                                    <td align="left" width="354" valign="top">
                                                                                        <table cellspacing="0" cellpadding="0" border="0" width="354">
                                                                                            <tbody><tr>
                                                                                                    <td align="left" valign="top" style="font:normal 12px/15px arial;color:#1b68b8">
                                                                                                        <a  target="_blank" style="color:#0274cb;text-decoration:none" href="<?php echo base_url(); ?>"><?php echo $this->lang->line("text_view_profile") ?></a></td>
                                                                                                                <!--<td align="right" valign="top"><a target="_blank" style="text-decoration:none;color:#0274cb" href="#"><img height="39" border="0" width="135" alt="" style="display:inline-block;padding-bottom:10px" src="http://localhost/matrimonial/assets/images/send.gif" class="CToWUd"></a></td>-->
                                                                                                </tr>
                                                                                            </tbody></table>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody></table>
                                                                    </td>
                                                                    <td align="left" width="12" valign="top"></td>
                                                                </tr>
                                                            </tbody></table>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                            <tr>
                                                <td align="right" valign="middle" style="vertical-align:middle;padding:10px">
                                                    <a  target="_blank" style="font:bold 12px arial;color:#0274cb;line-height:18px;text-decoration:none;margin-right:5%" href="<?php echo base_url(); ?>"><?php echo $this->lang->line("text_view_all_matches"); ?> <img height="10" border="0" width="5" src="http://localhost/matrimonial/assets/images/leftarrow.gif" class="CToWUd"></a>
                                                </td>
                                            </tr>	
                                            <?php
                                        }
                                        if (!empty($similar)) {
                                            ?>
                                            <tr>
                                                <td valign="top" align="left" style="font:normal 12px/18px arial;color:#777777;padding-top:10px"> <?php echo $title1 ?></td>
                                            </tr>
                                            <?php
                                            foreach ($similar as $val1) {
                                                ?>
                                                <tr>
                                                    <td align="left" valign="top">
                                                        <table cellspacing="0" cellpadding="0" border="0" bgcolor="#f6f6f6" width="510" style=" border:1px solid #e9e9e9; margin-top:10px;padding:10px;"> 
                                                            <tbody>
                                                                <tr>
                                                                    <td align="left" width="102" valign="top"><a  target="_blank" href="#"><img height="101" border="0" width="101" alt="" src="<?php echo base_url() . 'assets/upload_images/' . $val1['image']; ?>" class="CToWUd"></a></td>
                                                                    <td align="left" width="8" valign="top"><img border="0" width="8" style="display:block"  class="CToWUd"></td>
                                                                    <td align="left" width="354" valign="top" style="padding:0px 10px 0 0px">
                                                                        <table cellspacing="0" cellpadding="0" border="0" width="354" style="margin-bottom: 30px;">
                                                                            <tbody><tr>
                                                                                    <td align="left" valign="top" style="font:bold 13px arial;color:#363636;line-height:20px">
                                                                                        <a  target="_blank" style="text-decoration:none;color:#363636" href="#"><?php echo ucwords($val1['username']) . ' ' . sprintf($this->lang->line("mail_member_id"), $val1['userId']); ?></a></td>
                                                                                </tr>
                                                                                <tr><td height="5" valign="top"></td></tr>
                                                                                <tr>
                                                                                    <td align="left" valign="top" style="font:normal 13px/20px arial;color:#363636">
                                                                                        <a target="_blank" style="text-decoration:none;color:#363636" href="#"><?php echo sprintf($this->lang->line("mail_content_data"), $val1['age'], $val1['height'], $val1['religion'], $val1['state_name'], $val1['city_name'], $val1['edu_name'] . '-' . $val1['qualification'], $val1['occupation']); ?></a></td>
                                                                                </tr>
                                                                                <tr><td height="8" valign="top"></td></tr>
                                                                                <tr>
                                                                                    <td align="left" width="354" valign="top">
                                                                                        <table cellspacing="0" cellpadding="0" border="0" width="354">
                                                                                            <tbody><tr>
                                                                                                    <td align="left" valign="top" style="font:normal 12px/15px arial;color:#1b68b8">
                                                                                                        <a  target="_blank" style="color:#0274cb;text-decoration:none" href="<?php echo base_url(); ?>"><?php echo $this->lang->line("text_view_profile") ?></a></td>
                                                                                                                <!--<td align="right" valign="top"><a target="_blank" style="text-decoration:none;color:#0274cb" href="#"><img height="39" border="0" width="135" alt="" style="display:inline-block;padding-bottom:10px" src="http://localhost/matrimonial/assets/images/send.gif" class="CToWUd"></a></td>-->
                                                                                                </tr>
                                                                                            </tbody></table>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody></table>
                                                                    </td>
                                                                    <td align="left" width="12" valign="top"></td>
                                                                </tr>
                                                            </tbody></table>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                        </tbody></table>
                                </td>
                            </tr>

                            <tr>
                                <td align="right" valign="middle" style="vertical-align:middle;padding:0px">
                                    <a  target="_blank" style="font:bold 12px arial;color:#0274cb;line-height:18px;text-decoration:none;margin-right:5%" href="<?php echo base_url(); ?>"><?php echo $this->lang->line("text_view_all_matches"); ?> <img height="10" border="0" width="5" src="http://localhost/matrimonial/assets/images/leftarrow.gif" class="CToWUd"></a>
                                </td>
                            </tr>	
                        <?php } ?>
                        <tr><td height="15" valign="top"></td></tr>

                        <tr>
                            <td align="left" valign="top">
                                <table cellspacing="0" cellpadding="0" border="0" width="548"> 
                                    <tbody><tr>
                                            <td align="left" width="31" valign="top"></td>
                                            <td height="2" align="left" bgcolor="#ebebeb" width="486" valign="top"><img height="2" border="0" width="31" style="display:block" src="https://ci5.googleusercontent.com/proxy/08qGsLzwnsn4pe4HwNcGNPEwDWa_3m9SzGzuF7zwth1SQ6aFAMeg5UL7mAMdAPn80mTlzjU6nWxrf4hdd-zqZhSiba7G=s0-d-e1-ft#http://imgs.tamilmatrimony.com/bmimgs/trans.gif" class="CToWUd"></td>
                                            <td align="left" width="31" valign="top"></td>
                                        </tr>
                                    </tbody></table>
                            </td>
                        </tr>

                        <tr><td height="15" valign="top"></td></tr>
                        <tr>
                            <td align="left" bgcolor="#ffffff" valign="top" style="font:normal 12px/18px arial;text-align:left;color:#363636;padding-left:28px">
                            </td>
                        </tr>
                        <tr><td height="15" valign="top"></td></tr>

                    </tbody></table>
            </td>
        </tr>
    </tbody></table>
