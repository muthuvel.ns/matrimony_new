<table cellspacing="0" cellpadding="0" border="0" style="background:#f1f1f2;width:100%;border-top:10px solid #f1f1f2; height: 600px;">
    <tbody><tr>
            <td align="center" valign="top">
                <table align="center" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" width="100%" style="max-width:600px;border:1px solid #f1f1f1">
                    <tbody>
                        <tr>
                            <td align="left" valign="top" style="padding-top:18px;padding-bottom:18px;padding-left:15px;  border-bottom: 3px solid #ff9916;">
                                <img width="125" border="0" title="" alt=" Matrimonial Site" src="<?php echo base_url(); ?>assets/images/1.png" class="CToWUd">					
                                <span style=" vertical-align: 155%;color: #FF9916;font-size: 22px;font-weight: 400;"><?php echo $this->lang->line("text_matrimony_title"); ?></span>
                            </td>
                        </tr>
                        <tr>
                            <td width="100%" style="color:#333;text-align:center" colspan="2">
                                <table cellspacing="0" cellpadding="0" border="0" width="100%" style="text-align:center">
                                    <tbody>
                                        <tr>
                                            <td width="100%" style="height:344px;text-align:center" colspan="2">
                                                <div style="padding:40px 0 30px 0">
                                                    <img src="<?php echo base_url(); ?>assets/images/email.png" class="CToWUd">
                                                </div>
                                                <span style="font-size:14px;color:#bf2329;text-decoration:none"><?php echo sprintf($this->lang->line("mail_send_step1"), ucwords($partnername)); ?></span>
                                                <div style="padding-top:5px"></div>
                                                <p style="font-size:14px;color:#575656;margin:0;padding:0;"><span> <?php echo sprintf($this->lang->line("mail_send_step2"), ucwords($username)); ?></span></p>
                                                <div style="clear:both"></div>
                                                <div style="padding-top:15px"></div>
                                                <a target="_blank" style="margin:auto;background:#FFA417;width:115px;display:block;line-height:42px;min-height:42px;font-size:15px;color:#ffffff;text-align:center;text-decoration:none" href="<?php echo base_url(); ?>"><?php echo $this->lang->line("mail_send_step3") ?></a>
                                            </td>
                                        </tr>
                                    </tbody></table>
                            </td>
                        </tr>
                    </tbody></table>
            </td></tr>
    </tbody></table>
