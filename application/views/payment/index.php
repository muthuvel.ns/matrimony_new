<!DOCTYPE HTML>
<html>
    <head>
        <title>Matrimonial</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="keywords" content="Marital Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template, 
              Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
        <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
        <link href="<?php echo base_url(); ?>/assets/css/bootstrap-3.1.1.min.css" rel='stylesheet' type='text/css' />
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="<?php echo base_url(); ?>/assets/js/jquery.min.js"></script>
        <script src="<?php echo base_url(); ?>/assets/js/bootstrap.min.js"></script>
        <!-- Custom Theme files -->
        <link href="<?php echo base_url(); ?>/assets/css/register.css" rel='stylesheet' type='text/css' />
        <link href="<?php echo base_url(); ?>/assets/css/style.css" rel='stylesheet' type='text/css' />
        <link href="<?php echo base_url(); ?>/assets/css/your.css" rel='stylesheet' type='text/css' />
        <link href='//fonts.googleapis.com/css?family=Oswald:300,400,700' rel='stylesheet' type='text/css'>
        <link href='//fonts.googleapis.com/css?family=Ubuntu:300,400,500,700' rel='stylesheet' type='text/css'>
        <!----font-Awesome----->
        <link href="<?php echo base_url(); ?>/assets/css/font-awesome.css" rel="stylesheet"> 
        <!----font-Awesome----->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">

        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/css/select2.min.css">

        <!-- Latest compiled and minified JavaScript -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/js/select2.min.js"></script>
        <link href="<?php echo base_url(); ?>/assets/css/paid.css" rel='stylesheet' type='text/css' />
    </head>
    <body>
        <div class="navbar navbar-inverse-blue navbar">
            <div class="container">
                <div class="pull-left">
                    <a class="brand" href=""><img src="<?php echo base_url(); ?>/assets/images/1.png" alt="logo" width="125"></a>
                </div>
            </div>
        </div>
        <div class=" container someone_container">
            <ul>
                <li class="selfservice_tab_active_new"><?php echo $this->lang->line('self_service_plans'); ?></li>
            </ul>
            <?php if (!empty($membership)) { ?>		
                <ul class="membership_tabs">
                    <?php foreach ($membership as $val) { ?>
                                        <!--<li>Premium Plus <span class="normal">(<span class="bold">+</span>)</span></li>-->
                        <li><?php echo $val['month'] ?> <?php echo $this->lang->line('months_text'); ?></li>
                    <?php } ?>
                    <div class="clearfix"></div>
                </ul>
                <div class=" cart_shadow">
                    <div class="display_table">
                        <ul class="membership_tabs_inner">
                            <?php foreach ($membership as $value) { ?>
                                <li class="gc pointer" id ="payment_<?php echo $value['id']; ?>" onclick="paymentChanges('<?php echo $value['id']; ?>')">
                                    <form action="<?php echo base_url() . 'index.php/payment/buy' ?>" method="post">
                                        <input class="pointer" type="radio" value="<?php echo $value['id']; ?>" id="paycheck_<?php echo $value['id']; ?>" name="productcode">  
                                        <label for="ssp_gplus" class="cursor">
                                            <span class="font_18 green"><?php echo $value['name']; ?> <span class="plus_dark_grey"></span>  </span>
                                        </label>
                                        <div class="js_price_detail_block">
                                            <div class="spacer_5"></div>
                                            <span class="gray_price_new"><img height="10" width="6" title="Rs." alt="Rs." src=""> <?php echo $value['default_currency']; ?> </span>
                                            <div class="spacer_5"></div>
                                            <div>
                                                <span class="membership_price_new"><i class="fa fa-inr" ></i><?php echo $value['ind_currency']; ?></span>
                                            </div>
                                            <div>
                                                <span class="membership_price_new"><i class="fa fa-usd" ></i><?php echo $value['usd_currency']; ?></span>
                                            </div>
                                        </div>
                                        <div  id="make_<?php echo $value['id']; ?>" style="display:none;" class="photo_view"><a onclick="$(this).closest('form').submit()"><?php echo $this->lang->line('make_payment'); ?></a></div>
                                    </form>
                                </li>
                            <?php } ?>		
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <?php
            } else {
                echo '<h4>Please add membership info</h4>';
            }
            ?>
            <div class="clearfix"></div>
            </br></br>
            <div class="do_this_later">
                <a class="light_blue" href="<?php echo base_url(); ?>"><?php echo $this->lang->line('do_this_later'); ?></a><a href="#"><span class="gray_right_arrow"></span></a>
            </div>
        </div>    

        <script>

            function paymentChanges(reqdata) {
                if (reqdata == 1) {
                    $("#paycheck_1").prop("checked", true);
                    $("#paycheck_2").prop("checked", false);
                    $("#paycheck_3").prop("checked", false);
                    $('#make_1').show();
                    $('#make_2').hide();
                    $('#make_3').hide();
                    $('#payment_1').addClass('selected');
                    $('#payment_2').removeClass('selected');
                    $('#payment_3').removeClass('selected');
                }

                if (reqdata == 2) {
                    $("#paycheck_2").prop("checked", true);
                    $("#paycheck_1").prop("checked", false);
                    $("#paycheck_3").prop("checked", false);
                    $('#make_2').show();
                    $('#make_1').hide();
                    $('#make_3').hide();
                    $('#payment_2').addClass('selected');
                    $('#payment_1').removeClass('selected');
                    $('#payment_3').removeClass('selected');
                }

                if (reqdata == 3) {
                    $("#paycheck_3").prop("checked", true);
                    $("#paycheck_2").prop("checked", false);
                    $("#paycheck_1").prop("checked", false);
                    $('#make_3').show();
                    $('#make_2').hide();
                    $('#make_1').hide();
                    $('#payment_3').addClass('selected');
                    $('#payment_2').removeClass('selected');
                    $('#payment_1').removeClass('selected');
                }

            }


        </script>
