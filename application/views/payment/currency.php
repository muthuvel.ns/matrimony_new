<!DOCTYPE HTML>
<html>
    <head>
        <title>Matrimonial</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="keywords" content="Marital Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template, 
              Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
        <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
        <link href="<?php echo base_url(); ?>/assets/css/bootstrap-3.1.1.min.css" rel='stylesheet' type='text/css' />
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="<?php echo base_url(); ?>/assets/js/jquery.min.js"></script>
        <script src="<?php echo base_url(); ?>/assets/js/bootstrap.min.js"></script>
        <!-- Custom Theme files -->
        <link href="<?php echo base_url(); ?>/assets/css/register.css" rel='stylesheet' type='text/css' />
        <link href="<?php echo base_url(); ?>/assets/css/style.css" rel='stylesheet' type='text/css' />
        <link href="<?php echo base_url(); ?>/assets/css/your.css" rel='stylesheet' type='text/css' />
        <link href='//fonts.googleapis.com/css?family=Oswald:300,400,700' rel='stylesheet' type='text/css'>
        <link href='//fonts.googleapis.com/css?family=Ubuntu:300,400,500,700' rel='stylesheet' type='text/css'>
        <!----font-Awesome----->
        <link href="<?php echo base_url(); ?>/assets/css/font-awesome.css" rel="stylesheet"> 
        <!----font-Awesome----->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">

        <link href="<?php echo base_url(); ?>/assets/css/paid.css" rel='stylesheet' type='text/css' />
        <style>
            .frmalg {
                margin-top: 30px;
            }
        </style>
    </head>
    <body>
        <div class="navbar navbar-inverse-blue navbar">
            <div class="container">
                <div class="pull-left">
                    <a class="brand" href=""><img src="<?php echo base_url(); ?>/assets/images/1.png" alt="logo" width="125"></a>
                </div>
            </div>
        </div>
        <div class=" container someone_container">
            <?php if (!empty($message)) { ?>
                <script>
                    $(function () {
                        $('#success_msg').show();
                        setTimeout(function () {
                            $('#success_msg').hide();
                        }, 3000);
                    });
                </script>
                <div class="error" id="success_msg" style="color:blue; text-align: center; display:none; padding-buttom:10px;"><?php echo $message; ?></div>
            <?php } ?>
            <ul>
                <li class="selfservice_tab_active_new">Currency convertion</li>
            </ul>
            <div class=" cart_shadow">
                <div class="row">
                    <div class="frmalg">
                        <form id="currency" action="" method="post">
                            <div class="form-group col-md-8">
                                <label  class="col-md-3 form-control-label reglab">Membership </label>
                                <div class="col-md-8">
                                    <select class="form-control" name="member" id="member" required >
                                        <option value="">-- Select Membership -- </option>
                                        <option value="1">Platinum </option>
                                        <option value="2">Diamond </option>
                                        <option value="3">Gold</option>
                                    </select>
                                    <label class="error" for="member" style="display: none;"></label>
                                </div>
                            </div>
                            <div class="form-group col-md-8">
                                <label  class="col-md-3 form-control-label reglab">Month </label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control"  name="month"  id="month" placeholder="Enter the month " required >
                                    <label class="error" for="month" style="display: none;"></label>
                                </div>
                            </div>
                            <div class="form-group col-md-8">
                                <label  class="col-md-3 form-control-label reglab">INR </label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control"  name="ind_currency" onblur="currencyconvertion(this.value)" placeholder="Enter the amount INR" required >
                                    <label class="error" for="ind_currency" style="display: none;"></label>
                                </div>
                                <div id="loading" style="display: none; margin-left: 100px; margin-bottom: -28px;">
                                    <img class="pull-right" src="<?php echo base_url(); ?>assets/images/ajax-loader.gif"/>
                                </div>
                            </div>
                            <div class="form-group col-md-8">
                                <label  class="col-md-3 form-control-label reglab"> USD </label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control"  id="usd" name="usd_currency" placeholder="" readonly required>
                                    <label class="error" for="usd_currency" style="display: none;"></label>
                                </div>
                            </div>
                            <div class="form-group col-md-8">
                                <button name="step1" class="btn btn-default subbtn pull-right" id="btn" type="submit">Submit</button>
                            </div>
                        </form>
                    </div>	
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="clearfix"></div>
            </br></br>
        </div>    
        <input type="hidden" id="baseurl" value="<?php echo base_url(); ?>"/>
        <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
        <script>
                                        $(document).ready(function () {
                                            $("#currency").validate({
                                                rules: {
                                                    ind_currency: {
                                                        required: true,
                                                        number: true
                                                    },
                                                    month: {
                                                        required: true,
                                                        number: true
                                                    }
                                                }
                                            });
                                        });
                                        function currencyconvertion(reqData) {
                                            var intRegex = /[0-9 -()+]+$/;
                                            if (!reqData.match(intRegex)) {
                                                $('#usd').val('');
                                                return false
                                            }
                                            var baseurl = $('#baseurl').val();
                                            $('#loading').show();
                                            $('#btn').hide();
                                            $.ajax({
                                                type: "post",
                                                dataType: "text",
                                                url: baseurl + 'index.php/payment/currencyconvertion?price=' + reqData,
                                                success: function (request) {
                                                    $('#usd').val(request);
                                                    $('#loading').hide();
                                                    $('#btn').show();
                                                },
                                                error: function (data) {
                                                    // location.reload();
                                                },
                                            });

                                            return false;

                                        }
        </script>
