<style>
    .about_middle1 {

        padding: 5em 0;
    }
    .about_middle1 h2 {
        color: #fff;
        font-family: "Oswald",sans-serif;
        font-size: 2.5em;
        margin-bottom: 1em;
        text-align: center;
    }

    @media (max-width:1980px){
        .pull1.register{
            margin-left: 470px;
            width: 500px;
        }
    }
    @media (max-width:480px){
        .pull1.register{
            margin-left: 20px;
            width: 250px;
        }
    }

    .about_middle1{
        background: rgba(0, 0, 0, 0) url("<?php echo base_url(); ?>assets/images/m/fb.jpg") no-repeat scroll 0 0 / 100% 100%;
    }
</style>
<script type="text/javascript">
    $(document).ready(function () {
        $(".js-example-basic-single").select2();

        $("#country").select2({
            data: <?php echo $country; ?>
        })
    });
</script>
<div class="grid_3">
    <div class="container">
        <div class="breadcrumb1">
            <ul>
                <a href="index.html"><i class="fa fa-home home_1"></i></a>
                <span class="divider">&nbsp;|&nbsp;</span>
                <li class="current-page">feedback</li>
            </ul>
        </div>
        <div class="grid_5">
            <div class="about_middle1">
                <div class="container">
                    <h2>Feedback Form</h2>
                    <div class="pull1 register" style="">
                        <?php if (!empty($message)) { ?>
                            <script>
                                $(function () {
                                    $('#success_msg').show();
                                    setTimeout(function () {
                                        $('#success_msg').hide();
                                    }, 3000);
                                });
                            </script>
                            <div class="error" id="success_msg" style="color:blue; text-align: center; display:none; padding-buttom:10px;"><?php echo $message; ?></div>
                        <?php } ?>
                        <form action="<?php echo base_url(); ?>index.php/common/addfeedback" method="post" id="feedback">
                            <div class="form-group row">
                                <label for="username" class="col-sm-5 form-control-label reglab">Subject <span class="red"> *</span></label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="subject" name="subject" placeholder="Subject" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="username" class="col-sm-5 form-control-label reglab">Name <span class="red"> *</span></label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="username" name="username" placeholder="Username" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="email" class="col-sm-5 form-control-label reglab">Email <span class="red"> *</span></label>
                                <div class="col-sm-7">
                                    <input type="email" class="form-control" id="email" name="email" placeholder="Email Id" required>
                                </div>
                            </div>
                            <div class="form-group row" >
                                <label for="country" class="col-sm-5 form-control-label">Country <span class="red"> *</span></label>  
                                <div class="col-sm-7">
                                    <select class="js-example-data-array" id="country" name="country" required>
                                        <option value=""> -- Select Country -- </option>
                                    </select>  
                                    <label for="country" class="error"></label>       
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="mobile" class="col-sm-5 form-control-label reglab">Category <span class="red"> *</span></label>     
                                <div class="col-sm-7">
                                    <select class="form-control js-example-basic-single" id="category" name="category"  required >
                                        <option selected value="">--Select--</option>
                                        <option value="Problems related to the Website ">Problems related to the Website </option>
                                        <option value="Problems related to Profiles">Problems related to Profiles</option>
                                        <option value="Compliments and Suggestions">Compliments and Suggestions </option>
                                        <option value="Others">Others</option>
                                    </select>
                                    <label for="category" class="error"></label>
                                </div> 
                            </div>
                            <div class="form-group row">
                                <label for="gender" class="col-sm-5 form-control-label reglab">Suggestions / Feedback<span class="red"> *</span></label> 
                                <div class="col-sm-7">
                                    <textarea  class="form-control" name="message" id="message"  required></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-offset-7 col-sm-5">
                                    <button type="submit" class="btn btn-default subbtn" type="submit">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
            <script>
                                $("#feedback").validate();

                                $("#message").rules("add", {
                                    required: true,
                                    minlength: 30,
                                    maxlength: 250,
                                    messages: {
                                        required: "Please enter the text",
                                        minlength: jQuery.validator.format("Please, at least {0} characters are necessary"),
                                        maxlength: jQuery.validator.format("Please, at maximum {0} characters are allowed")
                                    }
                                });
            </script>
            <!-- Latest compiled and minified CSS -->
            <link rel="stylesheet" href="<?php echo base_url() . 'assets/css/select2/select2.min.css' ?>">

            <!-- Latest compiled and minified JavaScript -->
            <script src="<?php echo base_url() . 'assets/js/select2/select2.min.js' ?>"></script>

