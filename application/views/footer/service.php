<style>

    .accordion{
        width:100%;
        margin-left:0px;

    }
    .beark{
        font-size:14px;
        line-height: 1.5em;
        text-align: justify;
    }
    .txt{
        margin-bottom:16px !important;
    }
    .text_sample1 {
        margin-left: 30px !important;

    }
    .shadow{
        box-shadow: 0 1px 4px rgba(0, 0, 0, 0.3), 0 0 40px rgba(0, 0, 0, 0.1) inset;
    }
</style>  
<div class="grid_3">
    <div class="container">
        <div class="breadcrumb1">
            <ul>
                <a href="index.html"><i class="fa fa-home home_1"></i></a>
                <span class="divider">&nbsp;|&nbsp;</span>
                <li class="current-page">Service</li>
            </ul>
        </div>
        <div class="shadow">
            <h2>Service</h2></br>
            <!--<marquee direction="up" onmouseover="this.stop()" onmouseout="this.start()" scrollamount="3" style="height:500px;">-->
            <dl class="faq-list">
                <dt class="faq-list_h">
                <h4 style="color:#C32143;">Powerful Matching Options</h4>
                </dt>
                <dd>
                    <div class="m_4 text_sample1">
                        <div class="beark  txt">      Find your perfect match by using our powerful matching features.</div>

                        <div class="beark  txt">Use the Quick Search to find other members that are looking for someone just like you. Or use our Advanced Search to pick and choose from many different matching options. Once you've created the perfect search, save it so you can use the same search again with ease.</div>
                        <div class="beark  txt">
                            Still finding too many matches? Look for someone nearby. Narrow your search down to those living around your own zip code (not available in all areas).</div>
                        <div class="beark  txt">
                            Plan on being out of town? Search for other members living near your destination and meet up while away.
                        </div>
                    </div>
                </dd>
                <dt class="faq-list_h">

                <h4 style="color:#C32143;">Internal Mail Messaging</h4>
                </dt>
                <dd>
                    <div class="m_4 text_sample1">
                        <div class="beark  txt">
                            A fully integrated messaging system is available for you to email other members anonymously and safely. By using our messaging system, you won't have to reveal your real e-mail address or other personal information.
                        </div>
                        <div class="beark  txt">
                            You can also create custom mail templates if you find yourself sending the same message again and again. When creating a templates you can make use of "template variables" for you to personalize your messages. Automatically include your profile data with any message that you send.
                        </div>
                    </div>
                </dd>
                <dt class="faq-list_h">
                <h4 style="color:#C32143;">Send a Wink</h4>
                </dt>
                <dd>
                    <div class="m_4  text_sample1">
                        <div class="beark  txt">
                            See someone you're interested in but don't want to email? Send them a wink to let them know.
                        </div>
                    </div>
                </dd>
                <dt class="faq-list_h">
                <h4 style="color:#C32143;">Friends and Favourite People List</h4>
                </dt>
                <dd>
                    <div class="m_4  text_sample1">
                        <div class="beark  txt">Build your own private friends list to keep track of their usernames, and to share photos and events. Use it for your main contact list!</div>
                        <div class="beark  txt">
                            If you see someone you're interested in put them on your Hot People list and build your own Little Black Book.
                        </div>
                    </div>
                </dd>
                <dt class="faq-list_h">
                <h4 style="color:#C32143;" >Banned People</h4>
                </dt>
                <dd>
                    <div class="m_4  text_sample1">
                        <div class="beark  txt">
                            Tired of being bothered by the same losers over and over? Put them on your banned list and you become invisible to them.
                        </div>
                    </div>
                </dd>
                <dt class="faq-list_h">
                <h4 style="color:#C32143;">Profile Views and Winks List</h4>
                </dt>
                <dd>
                    <div class="m_4 text_sample1">
                        <div class="beark  txt">
                            Want to see who's checking you out? Look at the Views list to see who has visited your profile. And when you receive a Wink, check out the Winks List to see who's interested in you!
                        </div>
                    </div>
                </dd>
                <dt class="faq-list_h">

                <h4 style="color:#C32143;">Photo Gallery</h4>
                </dt>
                <dd>
                    <div class="m_4 text_sample1">
                        <div class="beark  txt">
                            Upload your photos and keep them private for you and your friends, or allow them to be viewed by everyone. Advanced gallery features include multiple album management and automatic thumbnails generation.
                        </div>
                        <div class="beark  txt">
                            Built in photo editing allows you to crop or resize your photos after uploading.
                        </div>
                    </div>
                </dd>
                <dt class="faq-list_h">
                <h4 style="color:#C32143;">Event Calendar</h4>
                </dt>
                <dd>
                    <div class="m_4 text_sample1">
                        <div class="beark  txt">
                            We offer a built in calendar so you can keep track of your events. See public events planned for members of the community, or track your own dates, such as birthdays or anniversaries.
                        </div>
                    </div>
                </dd>

            </dl>
        </div>
    </div>


</div>
