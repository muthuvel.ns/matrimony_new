<link href="<?php echo base_url(); ?>/assets/css/conversation.css" rel='stylesheet' type='text/css' />
<link href="<?php echo base_url(); ?>/assets/css/conven.css" rel='stylesheet' type='text/css' />
<script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/pagination/js/jquery.pajinate.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/outclick.js"></script>

<style>
    .tab_box{
        height:240px;
    }

</style>
<script>
    $(document).ready(function () {
        $('#ignore').click(function () {
            $('#listof').slideToggle("fast");
        })

        $('#ignore').outclick(function () {
            $('#listof').hide();
        });
    });
</script>
<link href="<?php echo base_url(); ?>/assets/css/tab.css" rel='stylesheet' type='text/css' />
<!--Interest msg-->
<div id="response" class="modal">
    <div class="modal-dialog animated sample" style=" margin-top: 9%;width: 440px;">
        <div class="modal-contentshort">
            <div class="row">
                <div>
                    <span style="float:left;"></span>
                    <span><h3 class="subtitle"><?php echo $this->lang->line('profile_request_send'); ?></h3></span>
                    <span class="modal-closeshort shtclose" id="close" ><a href="#"><img src="<?php echo base_url(); ?>assets/images/m/forgot-password-close.gif"></a></span>
                </div>
                <div class="form-group row">
                    <div class="col-sm-10 short-text" id="msg_body" style="text-align:center;">
                        <div id='loader' style='display:block'>
                            <img src="<?php echo base_url(); ?>assets/images/loading.gif"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--sInterest msg end-->

<!--sendmailpaid -->
<div id="modalpaid" class="modal">
    <div class="modal-dialog animated sample" style=" margin-top: 9%;width: 440px;">
        <div class="row modal-contentpaid">
            <div class="sendhead">
                <span style="float:left;"></span>
                <span><h3 class="subtitle" style="color: #7e7e7e;">><?php echo sprintf($this->lang->line('to_text'), $this->lang->line('profile_send_mail')); ?></h3></span>
                <span class="modal-closepaid shtclose" ><a href="#"><img src="<?php echo base_url(); ?>assets/images/m/forgot-password-close.gif"></a></span>
            </div>

            <div id="paid_mail">
                <div id='loading' class="loader" style='display: block; text-align: center; margin: 100px;'>
                    <img src="<?php echo base_url(); ?>assets/images/loader.gif"/>
                </div>
            </div>
            <div id="send_mail" style="display:none;">
                <form id="mail">
                    <div class="form-group row">
                        <div class="col-sm-10 short-text">
                            <textarea class="form-control" id="message" name="message" rows="6" maxlength="250" minlength="50"" required></textarea>
                        </div>
                        <input type="hidden" name="userguid" id="uid" value="">
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-12">
                            <div class="col-sm-2 pull-right">
                                <span class="btn btn-warning" id="mail_close"><?php echo $this->lang->line('cancel_admin_register'); ?></span>
                            </div>
                            <div class="col-sm-2 pull-right">
                                <button class="btn btn-primary" type="submit"><?php echo $this->lang->line('register_profile_for_submit'); ?></button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!--sendemailendpaid -->
<div class="grid_3">
    <div class="container">
        <div class="breadcrumb1">
            <ul>
                <a href="#"><i class="fa fa-home home_1"></i></a>
                <span class="divider">&nbsp;|&nbsp;</span>
                <li class="current-page"><?php echo $this->lang->line('inbox_text'); ?></li>
            </ul>
        </div>
        <div class="col-md-3 col_5">
            <input id="tab1" type="radio" name="tabs" checked>
            <label  class="inboxtablable" for="tab1" onclick="window.location = '<?php echo base_url() . 'index.php/message/inbox'; ?>';">Inbox</label>  
            <input id="tab2" type="radio" name="tabs">
            <label  class="inboxtablable" for="tab2" onclick="window.location = '<?php echo base_url() . 'index.php/message/send'; ?>';">Send</label>
              <!-- <section id="content1">
                    <ul class="match_box">
                            <h4>Inbox</h4>
                            <li><a href="<?php //echo base_url();    ?>index.php/message/inbox_detail">New</a></li>
                            <li><a href="<?php //echo base_url();    ?>index.php/message/inbox_detail">Accepted</a></li>
                            <li><a href="<?php //echo base_url();    ?>index.php/message/inbox_detail">Read or Not Request</a></li>
                     </ul>
              </section>
              <section id="content2">
                    <ul class="match_box">
                            <h4>Send</h4>
                            <li><a href="<?php //echo base_url();    ?>index.php/message/send_detail">Accepted</a></li>
                            <li><a href="<?php //echo base_url();    ?>index.php/message/send_detail">Not interest</a></li>
                     </ul>
              </section> -->     
            <img style=" width: 174px;  height: 135px;" src="<?php echo base_url() . 'assets/upload_images/' . (!empty($profile['image']) ? $profile['image'] : 'default.png'); ?>" class="img-responsive adj inboximageleft" alt=""/>
            <?php if ($offer == 2) { ?>
                <div class="pay left sendleft ">
                    <div class="leftpay">
                        <span class="leftpaytxt" style="color: #fff;"><?php echo $this->lang->line("payment_text"); ?> </span>
                    </div>	
                    <div class=" leftpay pointer btnlink" >
                        <span class="btnlinkalign ">
                            <a href="<?php echo base_url(); ?>index.php/payment"><?php echo $this->lang->line('payment'); ?></a></span>
                    </div>
                </div>
            <?php } ?>     
        </div>
        <?php if (!empty($conversation)) { ?>
            <div class="col-md-9 members_box2">
                <div class="box_profile">
                    <div class="jobs-item with-thumb" >
                        <!-- <a style="    margin-top: -32px;" class="fa fa-trash fa-short" href="#" title="Delete"></a> -->
                        <div class="thumb_top conmemborder">
                            <div class="thumb"><a href="#" onclick="showmodal_all()" >
                                    <img src="<?php echo base_url() . 'assets/upload_images/' . $conversation['image']; ?>" class="img-responsive" alt=""/></a>
                            </div>
                            <div class="jobs_right">
                                <h6 class="title"><a href="<?php echo base_url() . 'index.php/profile/view?pid=' . $conversation['userGuid']; ?>"><?php echo $conversation['username'] ?>( <?php echo constant('MEMBERID') . $conversation['userId']; ?> )</a></h6>
                                <ul class="login_details1">
                                    <li><?php echo sprintf($this->lang->line('last_text'), $this->lang->line('text_login')); ?>: <?php echo $conversation['log_in']; ?></li>
                                </ul>
                                <p class="description"><?php echo (!empty($conversation['age']) ? $conversation['age'] . ' years,' : 'not-specified') . ' ' . (!empty($conversation['height']) ? $conversation['height'] . ' Cms,' : 'not-specified') . ' | <span class="m_1">Reliogion</span> : ' . (!empty($conversation['religion']) ? $conversation['religion'] : 'not-specified') . '  | <span class="m_1">Education</span> :  ' . (!empty($conversation['edu_name']) ? $conversation['edu_name'] : 'not-specified') . ' - ' . (!empty($conversation['qualification']) ? $conversation['qualification'] : 'not-specified') . '  | <span class="m_1">Occupation</span> : ' . (!empty($conversation['occupation']) ? $conversation['occupation'] : 'not-specified') ?><br><a href="<?php echo base_url() . 'index.php/profile/view?pid=' . $conversation['userGuid'] ?>" class="read-more"><?php echo $this->lang->line('text_view_profile'); ?></a>
                            </div> 

                            <div class="descriptext">

                            </div>
                            <div class="clearfix"> </div>
                        </div>
                        <div class="thumb_bottom conmemborder">	   	  
                            <div class="thumb_but con-but">
                                <div class="vertical pointer"  onclick="sendmail('<?php echo $conversation['userGuid']; ?>', '<?php echo $offer; ?>')"><?php echo $this->lang->line('profile_send_mail'); ?></div>
                                <?php
                                /** login user block or ignore this user then don't show intrest ,send mail and other option */
                                if (empty($profile['ignore']) || empty($profile['block'])) {
                                    if (!empty($conversation['interest'])) {
                                        /** validate activity is interest or arrange or not interest show the button name difference else show the sent interest */
                                        if (($conversation['interest'] == REQUEST_INTEREST) || ($conversation['interest'] == REQUEST_ARRANGED) || ($conversation['interest'] == REQUEST_NOT_INETREST )) {
                                            /** if activity not interest then shoe the suitable matches else show the $profile['interest_name'] */
                                            if (($conversation['interest'] == REQUEST_NOT_INETREST)) {
                                                ?>
                                                <a href="<?php echo base_url() . 'index.php/matches/mutual_matches'; ?>"><div class="vertical pointer"><?php echo $conversation['interest_name']; ?></div></a>
                                            <?php } else { ?>
                                                <div class="vertical"><?php echo $conversation['interest_name']; ?></div> 
                                            <?php } ?>
                                        <?php } else { ?>
                                            <div class="vertical pointer" onclick="requestProfile('<?php echo $conversation['userGuid']; ?>', '<?php echo $conversation['interest']; ?>')"><?php echo $conversation['interest_name']; ?></div>
                                        <?php } ?>
                                    <?php } else { ?>
                                        <div class="vertical pointer" onclick="requestProfile('<?php echo $conversation['userGuid']; ?>', '<?php echo REQUEST_INTEREST; ?>')"><?php echo $this->lang->line('profile_send_interest'); ?></div>
                                    <?php } ?>
                                    <div class="ignordes" >
                                        <span class="btn-ignore-list" alt="Ignore this member" title="Ignore this member">
                                            <a style="text-decoration:none;" class=" txtsmall link" onclick="requestProfile('<?php echo $conversation['userGuid']; ?>', '<?php echo REQUEST_IGNORE; ?>')"><?php echo $this->lang->line('ignore'); ?></a>
                                        </span>
                                        <span id="ignore" class="dropdown-list"> 
                                            <span id="negarwico" class="dropdowniconeg downarw"></span>
                                        </span>
                                    </div>
                                    <div id="listof"  class="dropdowncls">
                                        <div class="ignstr" >
                                            <div class="ignoric1" onclick="requestProfile('<?php echo $conversation['userGuid']; ?>', '<?php echo REQUEST_IGNORE; ?>')" alt="Ignore this member" title="Ignore this member"><?php echo $this->lang->line('ignore'); ?></div>
                                            <div class="ignoric2" onclick="requestProfile('<?php echo $conversation['userGuid']; ?>', '<?php echo REQUEST_BLOCK; ?>')" alt="Block this member" title="Block this member"><?php echo $this->lang->line('block'); ?></div>
                                            <?php if (!empty($conversation['interest']) && ($conversation['interest'] != REQUEST_NOT_INETREST)) { ?>
                                                <div class="ignoric3" onclick="requestProfile('<?php echo $conversation['userGuid']; ?>', '<?php echo REQUEST_NOT_INETREST; ?>')" alt="not interest this member" title="not interest this member"><?php echo $this->lang->line('not_interest'); ?></div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                            <div class="clearfix"> </div>
                        </div>
                    </div>
                </div>

                <div class=" tab_box"> 	
                    <h1 ><?php echo $this->lang->line('text_other_converstation'); ?>( <?php echo (!empty($conversation['count']) ? $conversation['count'] : 0) ?> ) </h1>	
                    <?php if (!empty($conversation['message'])) { ?>
                        <div id="paging_container_mail">
                            <?php
                            foreach ($conversation['message'] as $value) {
                                ?>
                                <div class="content">	 	        
                                    <div class="col-md-12 charttxt ">	      
                                        <div class="col-md-1"><?php echo 'M' . $conversation['userId']; ?></div>
                                        <div class="col-md-7 mailtext charttxt">	
                                            <span class="show "><?php echo $value['message'] ?></span>									  
                                        </div>
                                        <div class="col-md-3 convenaccrec">
                                            <span class="smalltxt"><?php echo $this->lang->line('received'); ?>  : <?php echo $value['created'] ?></span>
                                        </div>

                                        <div class="col-md-1 "><a style=" margin-right: 10px;" class="fa fa-trash fa-short condel" onclick="requestDeleteMessage('<?php echo $value['id']; ?>')" title="Delete"></a></div>	
                                    </div></div>
                            <?php } ?>
                            <div class="page_navigation "></div></div>
                    </div><!--col-9-->
                <?php } ?>
            </div><!--container-->
            <?php
        } else {
            echo '<h4 style="text-align:center;">' . $this->lang->line('conversation_no') . '</h4>';
        }
        ?>
    </div><!--grid-->
    <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
    <script>
                                            $(document).ready(function () {
                                                $("#mail").validate({
                                                    submitHandler: function (form) {
                                                        var dataString = $('#mail').serialize();
                                                        if (dataString == '' || dataString == null || dataString == 0) {
                                                            return false;
                                                        }
                                                        $('#send_mail').hide()
                                                        $('#paid_mail').show();
                                                        $.ajax({
                                                            type: 'POST',
                                                            url: baseurl + 'index.php/profile/sendmail',
                                                            data: dataString,
                                                            dataType: "json",
                                                            success: function (data) {
                                                                $('#loading').hide();
                                                                if (data.msg == 'success') {
                                                                    $('#paid_mail').html('<h4 style="text-align:center; color:green;"> <?php echo $this->lang->line("mail_send_successfully_text"); ?> </h4>');
                                                                } else {
                                                                    $('#paid_mail').html('<h4 style="text-align:center; color:red;">' + data.msg + '</h4>');
                                                                }
                                                                setTimeout(function () {
                                                                    $('#modalpaid').hide();
                                                                    location.reload();
                                                                }, 2000);
                                                            },
                                                            error: function (jqXHR, textStatus, errorThrown) {
                                                                console.log(textStatus, errorThrown);
                                                                $('#paid_mail').html('<h4 style="text-align:center; color:green;"> <?php echo $this->lang->line("error_occur_contact_admin"); ?></h4>');
                                                                setTimeout(function () {
                                                                    $('#modalpaid').hide();
                                                                    location.reload();
                                                                }, 2000);
                                                            }
                                                        });
                                                    }
                                                });

                                                $("#message").rules("add", {
                                                    required: true,
                                                    minlength: 50,
                                                    maxlength: 250,
                                                    messages: {
                                                        required: "Please enter the text",
                                                        minlength: jQuery.validator.format("Please, at least {0} characters are necessary"),
                                                        maxlength: jQuery.validator.format("Please, at maximum {0} characters are allowed")
                                                    }
                                                });

                                                $('#paging_container_all').pajinate({
                                                    num_page_links_to_display: 3,
                                                    items_per_page: 6
                                                });
                                            });
                                            function reply() {
                                                $("#replycontent").show();
                                                $("#replybtn").hide();
                                            }
                                            function rlyclose() {
                                                $("#replycontent").hide();
                                                $("#replybtn").show();
                                            }
                                            function plusicon(reqdata) {
                                                $("#iconcontent_" + reqdata).show();
                                                $("#icon_" + reqdata).hide();
                                                $("#minsicon_" + reqdata).show();
                                            }
                                            function minusicon(reqdata) {
                                                $("#icon_" + reqdata).show();
                                                $("#iconcontent_" + reqdata).hide();
                                                $("#minsicon_" + reqdata).hide();
                                            }

                                            $(function () {
                                                $(".modal-closeshort").on('click', function () {
                                                    $('#modalshort').hide();
                                                });
                                                $(".modal-closepaid").on('click', function () {
                                                    $('#modalpaid').hide();
                                                });

                                                $("#mail_close").on('click', function () {
                                                    $('#modalpaid').hide();
                                                });
                                                $("#close").on('click', function () {
                                                    $('#response').hide();
                                                });
                                            });
                                            function requestProfile(pid, id) {
                                                if (pid == 0 || id == 0 || pid == '' || id == '') {
                                                    return false;
                                                }
                                                $('#response').show();
                                                $.post(baseurl + 'index.php/profile/updateprofilerequestinfo?pid=' + pid + '&id=' + id,
                                                        function (data) {
                                                            if (data.msg == 'success') {
                                                                $('#msg_body').html('<h4 style="text-align:center; color:green;">' + data.msg + '</h4>');
                                                            } else {
                                                                $('#msg_body').html('<h4 style="text-align:center; color:red;">' + data.msg + '</h4>');
                                                            }
                                                            setTimeout(function () {
                                                                $('#response').hide();
                                                                location.reload();
                                                            }, 3000);
                                                        }, "json")
                                                        .fail(function () {
                                                            $('#msg_body').html('<h4 style="text-align:center; color:green;"> <?php echo $this->lang->line("error_occur_contact_admin"); ?></h4>');
                                                            setTimeout(function () {
                                                                $('#response').hide();
                                                                location.reload();
                                                            }, 2000);
                                                        });
                                            }

                                            function sendmail(uid, offerid) {
                                                if (uid == 0 || offerid == 0 || uid == '' || offerid == '') {
                                                    return false;
                                                }

                                                if (offerid == 2) { //need to show paid view
                                                    $('#modalpaid').show();
                                                    $('#loading').show();
                                                    $.post(baseurl + 'index.php/profile/sendmailinfo?uid=' + uid,
                                                            function (data) {
                                                                $('#loading').hide();
                                                                $('#paid_mail').show();
                                                                $('#paid_mail').html(data);

                                                            }, "html")
                                                            .fail(function () {
                                                                $('#paid_mail').html('<h4 style="text-align:center; color:green;"> <?php echo $this->lang->line("error_occur_contact_admin"); ?></h4>');
                                                                setTimeout(function () {
                                                                    $('#modalpaid').hide();
                                                                    location.reload();
                                                                }, 2000);
                                                            });
                                                } else {//need to show send mail view
                                                    if (offerid == 1) {
                                                        $('#modalpaid').show();
                                                        $('#paid_mail').hide();
                                                        $('#send_mail').show();
                                                        $('#uid').val(uid);


                                                    }
                                                }
                                            }

                                            $(function () {
                                                var showTotalChar = 50, showChar = "Show (+)", hideChar = "Hide (-)";
                                                $('.show').each(function () {
                                                    var content = $(this).text();
                                                    if (content.length > showTotalChar) {
                                                        var con = content.substr(0, showTotalChar);
                                                        var hcon = content.substr(showTotalChar, content.length - showTotalChar);
                                                        var txt = con + '<span class="dots">...</span><span class="morectnt"><span>' + hcon + '</span>&nbsp;&nbsp;<a href="" class="showmoretxt">' + showChar + '</a></span>';
                                                        $(this).html(txt);
                                                    }
                                                });

                                                $(".showmoretxt").click(function () {
                                                    if ($(this).hasClass("sample")) {
                                                        $(this).removeClass("sample");
                                                        $(this).text(showChar);
                                                    } else {
                                                        $(this).addClass("sample");
                                                        $(this).text(hideChar);
                                                    }
                                                    $(this).parent().prev().toggle();
                                                    $(this).prev().toggle();
                                                    return false;
                                                });
                                            });

                                            $(document).ready(function () {
                                                $('#paging_container_mail').pajinate({
                                                    num_page_links_to_display: 2,
                                                    items_per_page: 5,
                                                    wrap_around: true,
                                                    show_first_last: false
                                                });
                                            });

                                            function requestDeleteMessage(id) {
                                                if (id == 0 || id == '') {
                                                    return false;
                                                }

                                                $('#response').show();
                                                $.post(baseurl + 'index.php/message/removeinboxmessage?msg_id=' + id,
                                                        function (data) {
                                                            if (data.msg == 'success') {
                                                                $('#msg_body').html('<h4 style="text-align:center; color:green;">' + data.msg + '</h4>');
                                                            } else {
                                                                $('#msg_body').html('<h4 style="text-align:center; color:red;">' + data.msg + '</h4>');
                                                            }
                                                            setTimeout(function () {
                                                                $('#response').hide();
                                                                location.reload();
                                                            }, 3000);
                                                        }, "json")
                                                        .fail(function () {
                                                            $('#msg_body').html('<h4 style="text-align:center; color:green;"> <?php echo $this->lang->line("error_occur_contact_admin"); ?></h4>');
                                                            setTimeout(function () {
                                                                $('#response').hide();
                                                                location.reload();
                                                            }, 2000);
                                                        });
                                            }
    </script>

