<link href="<?php echo base_url(); ?>/assets/css/tab.css" rel='stylesheet' type='text/css' />
<link href="<?php echo base_url(); ?>/assets/css/slider.css" rel='stylesheet' type='text/css' />
<script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/pagination/js/jquery.pajinate.js"></script>

<script>
    $(document).ready(function () {
        $('#paging_container_all').pajinate({
            num_page_links_to_display: 3,
            items_per_page: 6
        });
        $('#paging_container_new').pajinate({
            num_page_links_to_display: 3,
            items_per_page: 6
        });
        $('#paging_container_accepted').pajinate({
            num_page_links_to_display: 3,
            items_per_page: 6
        });
        $('#paging_container_notinterest').pajinate({
            num_page_links_to_display: 3,
            items_per_page: 6
        });
        $('#paging_container_Replied').pajinate({
            num_page_links_to_display: 3,
            items_per_page: 6
        });
    });

    function personal(reqData) {
        $.ajax({
            type: "POST",
            url: baseurl + 'index.php/matches/modaldata?uid=' + reqData,
            success: function (data) {
                $("#modal").show();
                $("#modal-body").html(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(textStatus, errorThrown);
                $('#modal-body').html('<h4 style="text-align:center; color:green;"><?php echo $this->lang->line("error_occur_contact_admin"); ?></h4>');
                setTimeout(function () {
                    $('#modal').hide();
                    location.reload();
                }, 2000);
            }
        });
    }
    $(function () {
        $("#close_modal").on('click', function () {
            $('#modal').hide();
        });
        $("#close").on('click', function () {
            $('#response').hide();
        });
    });
</script>
<!-- Modal Start -->
<div id="modal" class="modal">
    <div class="modal-dialog animated">
        <div class="modal-content">
            <div class="modal-header">
                <strong><?php echo $this->lang->line('register_profile_for_process2'); ?> </strong>
            </div>
            <div class="modal-body" id="modal-body">
            </div>
            <div class="modal-footer">
                <button class="btn btn-default" type="button" id="close_modal"><?php echo $this->lang->line('close_text'); ?></button>
            </div>
        </div>
    </div>
</div>
<!-- Modal End -->

<div class="grid_3">
    <div class="container">
        <?php if ($offer == 2) { ?>
            <div class="col-md-12 ">
                <div class="col-md-offset-2 col-md-8 col-md-offset-2 sendleft" >
                    <span class="leftpaytxt pull_left" style="color:#fff;"><?php echo $this->lang->line("payment_text"); ?> </span>
                    <span class="btn btn-warning pull_right">
                        <a href="<?php echo base_url(); ?>index.php/payment" style="color:#fff;"><?php echo $this->lang->line("payment"); ?></a>
                    </span>
                </div>
            </div>
        <?php } ?> 

        <div class="breadcrumb1">
            <ul>
                <a href="<?php echo base_url(); ?>index.php/matches/view/"><i class="fa fa-home home_1"></i></a>
                <span class="divider">&nbsp;|&nbsp;</span>
                <li class="current-page"><?php echo $this->lang->line('send_text'); ?></li>
            </ul>
        </div>
        <div class="col-md-3 col_5">
            <input id="tab1" type="radio" name="tabs" >
            <label  class="inboxtablable" for="tab1" onclick="window.location = '<?php echo base_url() . 'index.php/message/inbox'; ?>';"><?php echo $this->lang->line('inbox_text'); ?></label>  
            <input id="tab2" type="radio" name="tabs" checked>
            <label  class="inboxtablable" for="tab2" onclick="window.location = '<?php echo base_url() . 'index.php/message/send'; ?>';"><?php echo $this->lang->line('send_text'); ?></label>

            <img src="<?php echo base_url() . 'assets/upload_images/' . (!empty($profile['image']) ? $profile['image'] : 'default.png'); ?>" class="img-responsive adj inboximageleft" alt=""/>

        </div>
        <div class="col-md-9 members_box2">
            <h3>Sent</h3>
            <!--<p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College.</p>-->
            <div class="col_4">
                <div class="bs-example bs-example-tabs" role="tabpanel" data-example-id="togglable-tabs">
                    <ul id="myTab" class="nav nav-tabs nav-tabs1" role="tablist">
                        <li role="presentation" class="active"><a href="#home" id="home-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="true"><?php echo $this->lang->line('all_new'); ?></a></li>
                        <!-- <li role="presentation"><a href="#profile" role="tab" id="profile-tab" data-toggle="tab" aria-controls="profile">New</a></li> -->
                        <li role="presentation"><a href="#profile2" role="tab" id="profile-tab" data-toggle="tab" aria-controls="profile"><?php echo $this->lang->line('accepted'); ?></a></li>
                        <li role="presentation"><a href="#profile3" role="tab" id="profile-tab" data-toggle="tab" aria-controls="profile"><?php echo $this->lang->line('not_interest'); ?></a></li>
                        <li role="presentation"><a href="#profile4" role="tab" id="profile-tab" data-toggle="tab" aria-controls="profile"><?php echo $this->lang->line('replied'); ?></a></li>
                    </ul>
                    <div id="myTabContent" class="tab-content">
                        <!--All tab-->
                        <div role="tabpanel" class="tab-pane fade in active" id="home" aria-labelledby="home-tab">
                            <div id="paging_container_all">
                                <?php if (!empty($send)) { ?>
                                    <div class="page_navigation pagination pagination_1"></div>
                                    <div class="short_send">
                                        <div class="short_btn vertical pointer" onclick="sendBulkRequest('send')" ><?php echo $this->lang->line('delete'); ?></div>
                                        <input  style="display: initial;" value="0" id="selectall" type="checkbox">
                                    </div>
                                    <div class="clearfix"> </div>
                                    <div class="content">
                                        <form id="form_send" method="post">
                                            <?php foreach ($send as $value) { ?>
                                                <div class="jobs-item with-thumb  ">
                                                    <div class="thumb_top send_thump_top">
                                                        <div class="thumb">
                                                            <a href="#" onclick="personal('<?php echo (!empty($value['userGuid']) ? $value['userGuid'] : ''); ?>')" >
                                                                <img src="<?php echo base_url() . 'assets/upload_images/' . $value['image']; ?>" class="img-responsive inboximg" alt=""/>
                                                            </a>
                                                        </div>
                                                        <div class="jobs_right">
                                                            <h6 class="title">
                                                                <input style="display: initial;" value="<?php echo $value['userGuid']; ?>"  class="case" type="checkbox" name="inboxlist[]">
                                                                <a href="<?php echo base_url() . 'index.php/profile/view?pid=' . $value['userGuid'] ?>"><?php echo $value['username']; ?> (<?php echo constant('MEMBERID') . $value['userId'] ?>)</a>
                                                                <span class="date del pointer" onclick="sendRemoveRequest('<?php echo $value['userGuid']; ?>', 'inboxlist')"><i class="fa fa-trash fa-short del"  title="Delete"></i></span>
                                                                <span class="date" style="margin-right:10px;"><?php echo $days = $value['date'][0]; ?>	</span>
                                                            </h6>
                                                            <ul class="login_details1">
                                                                <li><?php echo sprintf($this->lang->line('last_text'), $this->lang->line('login')) . ' : ' . $value['log_in']; ?></li>
                                                            </ul>
                                                            <p class="description">
                                                                <?php echo (!empty($value['age']) ? $value['age'] . ' years,' : 'not-specified') . ' ' . (!empty($value['height']) ? $value['height'] . ' Cms,' : 'not-specified') ?> |<span class="m_1"><?php echo $this->lang->line('register_profile_for_gender'); ?></span> : <?php echo (!empty($value['gender']) ? constant("GENDER_" . strtoupper($value['gender'])) : 'not-specified'); ?> | <span class="m_1"><?php echo $this->lang->line('register_profile_for_religion'); ?></span> : <?php echo (!empty($value['religion']) ? $value['religion'] : 'not-specified') ?>  | <span class="m_1"><?php echo $this->lang->line('search_education_title'); ?></span> :  <?php echo (!empty($value['edu_name']) ? $value['edu_name'] : 'not-specified') . ' - ' . (!empty($value['qualification']) ? $value['qualification'] : 'not-specified') ?>  | <span class="m_1"><?php echo $this->lang->line('register_profile_for_occupation'); ?></span> : <?php echo (!empty($value['occupation']) ? $value['occupation'] : 'not-specified') ?><br><a href="<?php echo base_url() . 'index.php/profile/view?pid=' . $value['userGuid'] ?>" class="read-more"><?php echo $this->lang->line('text_view_profile'); ?></a>
                                                                <a href="<?php echo base_url() . 'index.php/message/sendconversation?uid=' . $value['userGuid'] ?>" target="_blank"><span class="conven">+<?php echo (!empty($value['activity']) ? count($value['activity']) : '') . ' ' . $this->lang->line('moreconversation'); ?></span></a></p>

                                                            <div class="thumb_bottom">
                                                                <?php
                                                                $active = $value['activity'];
                                                                if (!empty($active[0]) && $active[0] == REQUEST_INTEREST) {
                                                                    ?> 
                                                                    <div class="descriptext">
                                                                        <span class="inboxrequest"><a href="<?php echo base_url() . 'index.php/message/sendconversation?uid=' . $value['userGuid'] ?>" target="_blank"><?php echo $this->lang->line('profile_send_interest'); ?></a></span>
                                                                        <span class="dectxt"><?php echo $this->lang->line('text_send_interest'); ?>
                                                                        </span>
                                                                    </div>
                                                                <?php } elseif (!empty($active[0]) && $active[0] == REQUEST_IMAGE) { ?>
                                                                    <div class="descriptext">
                                                                        <span class="inboxrequest"><a href="<?php echo base_url() . 'index.php/message/sendconversation?uid=' . $value['userGuid'] ?>" target="_blank"><?php echo $this->lang->line('profile_image_request'); ?></a></span>
                                                                        <span class="dectxt"><?php echo $this->lang->line('text_send_view_image'); ?>
                                                                        </span>
                                                                    </div>
                                                                <?php } elseif (!empty($active[0]) && $active[0] == REQUEST_ACCEPT) { ?>
                                                                    <div class="descriptext">
                                                                        <span class="inboxrequest"><a href="<?php echo base_url() . 'index.php/message/sendconversation?uid=' . $value['userGuid'] ?>" target="_blank"><?php echo $this->lang->line('interest_accepted'); ?></a></span>
                                                                        <span class="dectxt"><?php echo $this->lang->line('text_send_interest_accept'); ?>
                                                                        </span>
                                                                    </div>
                                                                <?php } elseif (!empty($active[0]) && $active[0] == REQUEST_ARRANGED) { ?>
                                                                    <div class="descriptext">
                                                                        <span class="inboxrequest"><a href="<?php echo base_url() . 'index.php/message/sendconversation?uid=' . $value['userGuid'] ?>" target="_blank"><?php echo $this->lang->line('arranged'); ?></a></span>
                                                                        <span class="dectxt"><?php echo $this->lang->line('text_send_arranged'); ?> 
                                                                        </span>
                                                                    </div>
                                                                <?php } elseif (!empty($active[0]) && $active[0] == REQUEST_SEND) { ?>
                                                                    <div class="descriptext">
                                                                        <span class="inboxrequest"><a href="<?php echo base_url() . 'index.php/message/sendconversation?uid=' . $value['userGuid'] ?>" target="_blank"><?php echo $this->lang->line('profile_send_mail'); ?></a></span>
                                                                        <span class="dectxt"><?php echo $this->lang->line('text_send_mail'); ?>
                                                                        </span>
                                                                    </div>
                                                                <?php } ?>
                                                                <div class="clearfix"> </div>
                                                            </div>									

                                                        </div>
                                                        <div class="clearfix"> </div>
                                                    </div>

                                                </div>					  
                                            <?php } ?>
                                        </form>
                                    </div>
                                    <?php
                                } else {
                                    echo '<h4 style="text-align:center;">' . $this->lang->line('text_messages_not_found') . '</h4>';
                                }
                                ?>
                            </div>
                        </div>

                        <!--all tab end-->

                        <!--Accepted tab-->

                        <div role="tabpanel" class="tab-pane fade" id="profile2" aria-labelledby="profile-tab">
                            <div id="paging_container_accepted">
                                <?php if (!empty($accept)) { ?>                    
                                    <div class="page_navigation pagination pagination_1"></div>
                                    <div class="short_send">
                                        <div class="short_btn vertical" onclick="sendBulkRequest('accept')" ><?php echo $this->lang->line('delete'); ?></div>
                                        <input  style="display: initial;" value="0" id="selectaccept" type="checkbox">
                                    </div>
                                    <div class="clearfix"> </div>
                                    <div class="content">
                                        <form id="form_accept" method="post">
                                            <?php foreach ($accept as $val2) { ?>      
                                                <div class="jobs-item with-thumb">
                                                    <div class="thumb_top send_thump_top">
                                                        <div class="thumb">								
                                                            <a href="#" onclick="personal('<?php echo (!empty($val2['userGuid']) ? $val2['userGuid'] : ''); ?>')" >
                                                                <img src="<?php echo base_url() . 'assets/upload_images/' . $val2['image']; ?>" class="img-responsive inboximg" alt=""/>
                                                            </a>
                                                        </div>
                                                        <div class="jobs_right">
                                                            <h6 class="title">
                                                                <input style="display: initial;" value="<?php echo $val2['userGuid']; ?>"  class="caseaccept" type="checkbox" name="acceptlist[]">
                                                                <a href="<?php echo base_url() . 'index.php/profile/view?pid=' . $val2['userGuid'] ?>"><?php echo $val2['username']; ?> (<?php echo 'M' . $val2['userId'] ?>)</a>
                                                                <span class="date del pointer" onclick="sendRemoveRequest('<?php echo $val2['userGuid']; ?>', 'acceptlist')"><i class="fa fa-trash fa-short del"  title="Delete"></i></span>
                                                                <span class="date" style="margin-right:10px;"><?php echo $days = $val2['date_accept']; ?>	</span>
                                                            </h6>							
                                                            <ul class="login_details1">
                                                                <li><?php echo sprintf($this->lang->line('last_text'), $this->lang->line('login')); ?> : <?php echo $val2['log_in']; ?></li>
                                                            </ul>

                                                            <p class="description">
                                                                <?php echo (!empty($val2['age']) ? $val2['age'] . ' years,' : 'not-specified') . ' ' . (!empty($val2['height']) ? $val2['height'] . ' Cms,' : 'not-specified') ?> |<span class="m_1"><?php echo $this->lang->line('register_profile_for_gender'); ?></span> : <?php echo (!empty($val2['gender']) ? constant("GENDER_" . strtoupper($val2['gender'])) : 'not-specified'); ?> | <span class="m_1"><?php echo $this->lang->line('register_profile_for_religion'); ?></span> : <?php echo (!empty($val2['religion']) ? $val2['religion'] : 'not-specified') ?>  | <span class="m_1"><?php echo $this->lang->line('search_education_title'); ?></span> :  <?php echo (!empty($val2['edu_name']) ? $val2['edu_name'] : 'not-specified') . ' - ' . (!empty($val2['qualification']) ? $val2['qualification'] : 'not-specified') ?>  | <span class="m_1"><?php echo $this->lang->line('register_profile_for_occupation'); ?></span> : <?php echo (!empty($val2['occupation']) ? $val2['occupation'] : 'not-specified') ?><br><a href="<?php echo base_url() . 'index.php/profile/view?pid=' . $val2['userGuid'] ?>" class="read-more"><?php echo $this->lang->line('text_view_profile'); ?></a>
                                                                <a href="<?php echo base_url() . 'index.php/message/sendconversation?uid=' . $val2['userGuid'] ?>" target="_blank"><span class="conven">+<?php echo (!empty($val2['activity']) ? count($val2['activity']) : '') . ' ' . $this->lang->line('moreconversation'); ?></span></a></p>


                                                        </div>
                                                        <div class="clearfix"> </div>
                                                    </div>
                                                </div> 
                                            <?php } ?>
                                        </form>
                                    </div>
                                    <?php
                                } else {
                                    echo '<h4 style="text-align:center;">' . $this->lang->line('text_messages_not_found') . '</h4>';
                                }
                                ?>
                            </div>
                        </div>

                        <!--accepted tab end-->

                        <!--not interest-->
                        <div role="tabpanel" class="tab-pane fade" id="profile3" aria-labelledby="profile-tab">
                            <div id="paging_container_notinterest">
                                <?php if (!empty($notinterest)) { ?>                  
                                    <div class="page_navigation pagination pagination_1"></div>
                                    <div class="short_send">
                                        <div class="short_btn vertical pointer" onclick="sendBulkRequest('not')" ><?php echo $this->lang->line('delete'); ?></div>
                                        <input  style="display: initial;" value="0" id="selectnotinterest" type="checkbox">
                                    </div>
                                    <div class="clearfix"> </div>
                                    <div class="content">
                                        <form id="form_not" method="post">
                                            <?php foreach ($notinterest as $val3) { ?>
                                                <div class="jobs-item with-thumb">
                                                    <div class="thumb_top send_thump_top">
                                                        <div class="thumb">								
                                                            <a href="#" onclick="personal('<?php echo (!empty($val3['userGuid']) ? $val3['userGuid'] : ''); ?>')" >
                                                                <img src="<?php echo base_url() . 'assets/upload_images/' . $val3['image']; ?>" class="img-responsive inboximg" alt=""/>
                                                            </a>
                                                        </div>
                                                        <div class="jobs_right">
                                                            <h6 class="title">
                                                                <input style="display: initial;" value="<?php echo $val3['userGuid']; ?>"  class="casenotinterest" type="checkbox" name="notlist[]">
                                                                <a href="<?php echo base_url() . 'index.php/profile/view?pid=' . $val3['userGuid'] ?>"><?php echo $val3['username']; ?> (<?php echo constant('MEMBERID') . $val3['userId'] ?>)</a>
                                                                <span class="date del pointer" onclick="sendRemoveRequest('<?php echo $val3['userGuid']; ?>', 'notlist')"><i class="fa fa-trash fa-short del"  title="Delete"></i></span>
                                                                <span class="date" style="margin-right:10px;"><?php echo $days = $val3['date_notinterest']; ?>	</span>
                                                            </h6>						
                                                            <ul class="login_details1">
                                                                <li><?php echo sprintf($this->lang->line('last_text'), $this->lang->line('login')) . ' : ' . $val3['log_in']; ?></li>
                                                            </ul>
                                                            <p class="description">
                                                                <?php echo (!empty($val3['age']) ? $val3['age'] . ' years,' : 'not-specified') . ' ' . (!empty($val3['height']) ? $val3['height'] . ' Cms,' : 'not-specified') ?> |<span class="m_1"><?php echo $this->lang->line('register_profile_for_gender'); ?></span> : <?php echo (!empty($val3['gender']) ? constant("GENDER_" . strtoupper($val3['gender'])) : 'not-specified'); ?> | <span class="m_1"><?php echo $this->lang->line('register_profile_for_religion'); ?></span> : <?php echo (!empty($val3['religion']) ? $val3['religion'] : 'not-specified') ?>  | <span class="m_1"><?php echo $this->lang->line('search_education_title'); ?></span> :  <?php echo (!empty($val3['edu_name']) ? $val3['edu_name'] : 'not-specified') . ' - ' . (!empty($val3['qualification']) ? $val3['qualification'] : 'not-specified') ?>  | <span class="m_1"><?php echo $this->lang->line('register_profile_for_occupation'); ?></span> : <?php echo (!empty($val3['occupation']) ? $val3['occupation'] : 'not-specified') ?><br><a href="<?php echo base_url() . 'index.php/profile/view?pid=' . $val3['userGuid'] ?>" class="read-more"><?php echo $this->lang->line('text_view_profile'); ?></a>
                                                                <a href="<?php echo base_url() . 'index.php/message/sendconversation?uid=' . $val3['userGuid'] ?>" target="_blank"><span class="conven">+<?php echo (!empty($val3['activity']) ? count($val3['activity']) : '') . ' ' . $this->lang->line('moreconversation'); ?></span></a></p>
                                                        </div>
                                                        <div class="clearfix"> </div>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                        </form>
                                    </div>
                                    <?php
                                } else {
                                    echo '<h4 style="text-align:center;">' . $this->lang->line('text_messages_not_found') . '</h4>';
                                }
                                ?>
                            </div>
                        </div>
                        <!--not interest end-->
                        <!--reply-->

                        <div role="tabpanel" class="tab-pane fade" id="profile4" aria-labelledby="profile-tab">
                            <div id="paging_container_Replied">
                                <?php if (!empty($replied)) {   //echo "<pre>"; print_r($replied); echo "</pre>"  ?>                  
                                    <div class="page_navigation pagination pagination_1"></div>
                                    <div class="short_send">
                                        <div class="short_btn vertical pointer" onclick="sendBulkRequest('reply')" ><?php echo $this->lang->line('delete'); ?></div>
                                        <input  style="display: initial;" value="0" id="selectreplied" type="checkbox">
                                    </div>
                                    <div class="clearfix"> </div>
                                    <div class="content">
                                        <form id="form_reply" method="post">
                                            <?php foreach ($replied as $val4) { ?>
                                                <div class="jobs-item with-thumb">
                                                    <div class="thumb_top send_thump_top">
                                                        <div class="thumb">								
                                                            <a href="#" onclick="personal('<?php echo (!empty($val4['userGuid']) ? $val4['userGuid'] : ''); ?>')" >
                                                                <img src="<?php echo base_url() . 'assets/upload_images/' . $val4['image']; ?>" class="img-responsive inboximg" alt=""/>
                                                            </a>
                                                        </div>
                                                        <div class="jobs_right">
                                                            <h6 class="title">
                                                                <input style="display: initial;" value="<?php echo $val4['userGuid']; ?>"  class="casereplied" type="checkbox" name="replylist[]">
                                                                <a href="<?php echo base_url() . 'index.php/profile/view?pid=' . $val4['userGuid'] ?>"><?php echo $val4['username']; ?> (<?php echo 'M' . $val4['userId'] ?>)</a>
                                                                <span class="date del pointer" onclick="sendRemoveRequest('<?php echo $val4['userGuid']; ?>', 'replylist')"><i class="fa fa-trash fa-short del"  title="Delete"></i></span>
                                                                <span class="date" style="margin-right:10px;"><?php echo $days = $val4['date_replied']; ?>	</span>
                                                            </h6>						
                                                            <ul class="login_details1">
                                                                <li><?php echo sprintf($this->lang->line('last_text'), $this->lang->line('login')); ?> : <?php echo $val4['log_in']; ?></li>
                                                            </ul>
                                                            <p class="description">
                                                                <?php echo (!empty($val4['age']) ? $val4['age'] . ' years,' : 'not-specified') . ' ' . (!empty($val4['height']) ? $val4['height'] . ' Cms,' : 'not-specified') ?> |<span class="m_1"><?php echo $this->lang->line('register_profile_for_gender'); ?></span> : <?php echo (!empty($val4['gender']) ? constant("GENDER_" . strtoupper($val4['gender'])) : 'not-specified'); ?> | <span class="m_1"><?php echo $this->lang->line('register_profile_for_religion'); ?></span> : <?php echo (!empty($val4['religion']) ? $val4['religion'] : 'not-specified') ?>  | <span class="m_1"><?php echo $this->lang->line('search_education_title'); ?></span> :  <?php echo (!empty($val4['edu_name']) ? $val4['edu_name'] : 'not-specified') . ' - ' . (!empty($val4['qualification']) ? $val4['qualification'] : 'not-specified') ?>  | <span class="m_1"><?php echo $this->lang->line('register_profile_for_occupation'); ?></span> : <?php echo (!empty($val4['occupation']) ? $val4['occupation'] : 'not-specified') ?><br><a href="<?php echo base_url() . 'index.php/profile/view?pid=' . $val4['userGuid'] ?>" class="read-more"><?php echo $this->lang->line('text_view_profile'); ?></a>
                                                                <a href="<?php echo base_url() . 'index.php/message/sendmailconversation?uid=' . $val4['userGuid'] ?>" target="_blank"><span class="conven">+<?php echo (!empty($val4['activity']) ? count($val4['activity']) : '') . ' ' . $this->lang->line('moreconversation'); ?></span></a></p>
                                                        </div>
                                                        <div class="clearfix"> </div>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                        </form>
                                    </div>
                                    <?php
                                } else {
                                    echo '<h4 style="text-align:center;">' . $this->lang->line('text_messages_not_found') . '</h4>';
                                }
                                ?>
                            </div>
                        </div>


                        <!--end reply-->
                    </div> 
                </div>
            </div>
        </div>
        <div class="clearfix"> </div>
    </div>
</div>
<!--Interest msg-->
<div id="response" class="modal">
    <div class="modal-dialog animated sample" style=" margin-top: 9%;width: 440px;">
        <div class="modal-contentshort">
            <div class="row">
                <div>
                    <span style="float:left;"></span>
                    <span><h3 class="subtitle"> <?php echo $this->lang->line('profile_request_send'); ?> </h3></span>
                    <span class="modal-closeshort shtclose" id="close" ><a href="#"><img src="<?php echo base_url(); ?>assets/images/m/forgot-password-close.gif"></a></span>
                </div>
                <div class="form-group row">
                    <div class="col-sm-10 short-text" id="msg_body" style="text-align:center;">
                        <div id='loader' style='display:block'>
                            <img src="<?php echo base_url(); ?>assets/images/loading.gif"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--sInterest msg end-->
<script>
    $(document).ready(function () {
        $("#selectall").change(function () {
            $(".case").prop('checked', $(this).prop("checked"));
        });
        $("#selectunread").change(function () {
            $(".caseunread").prop('checked', $(this).prop("checked"));
        });
        $("#selectaccept").change(function () {
            $(".caseaccept").prop('checked', $(this).prop("checked"));
        });
        $("#selectnotinterest").change(function () {
            $(".casenotinterest").prop('checked', $(this).prop("checked"));
        });
        $("#selectreplied").change(function () {
            $(".casereplied").prop('checked', $(this).prop("checked"));
        });
    });

    function sendBulkRequest(ReqId) {
        var dataString = $('#form_' + ReqId).serialize();
        if (dataString == '' || dataString == null || dataString == 0) {
            alert('Please select options');
            return false;
        }
        $('#response').show();
        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/message/removemessage?id=2',
            data: dataString,
            dataType: "json",
            success: function (data) {
                if (data == 'success') {
                    $('#msg_body').html('<h4 style="text-align:center; color:green;"><?php echo $this->lang->line("conversation_remove_text"); ?></h4>');
                } else {
                    $('#msg_body').html('<h4 style="text-align:center; color:red;"><?php echo $this->lang->line("please_try_again_after_some_time"); ?></h4>');
                }
                setTimeout(function () {
                    $('#response').hide();
                    location.reload();
                }, 3000);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(textStatus, errorThrown);
                $('#msg_body').html('<h4 style="text-align:center; color:green;"> <?php echo $this->lang->line("error_occur_contact_admin"); ?></h4>');
                setTimeout(function () {
                    $('#response').hide();
                    location.reload();
                }, 3000);
            }
        });
        return false;
    }

    function sendRemoveRequest(uid, paramId) {
        if (uid == '' || uid == null || paramId == '' || paramId == null) {
            alert('Please try again');
            return false;
        }

        $('#response').show();
        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/message/removemessage?id=2&' + paramId + '=' + uid,
            dataType: "json",
            success: function (data) {
                if (data == 'success') {
                    $('#msg_body').html('<h4 style="text-align:center; color:green;"><?php echo $this->lang->line("conversation_remove_text"); ?></h4>');
                } else {
                    $('#msg_body').html('<h4 style="text-align:center; color:red;"><?php echo $this->lang->line("please_try_again_after_some_time"); ?></h4>');
                }
                setTimeout(function () {
                    $('#response').hide();
                    location.reload();
                }, 3000);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(textStatus, errorThrown);
                $('#msg_body').html('<h4 style="text-align:center; color:green;"> <?php echo $this->lang->line("error_occur_contact_admin"); ?></h4>');
                setTimeout(function () {
                    $('#response').hide();
                    location.reload();
                }, 3000);
            }
        });
        return false;
    }
</script>
