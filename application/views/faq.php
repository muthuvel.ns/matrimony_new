
<style>

    .accordion{
        width:100%;
        margin-left:0px;

    }
</style>  
<div class="grid_3">
    <div class="container">
        <div class="breadcrumb1">
            <ul>
                <a href="<?php echo base_url(); ?>index.php/home/index"><i class="fa fa-home home_1"></i></a>
                <span class="divider">&nbsp;|&nbsp;</span>
                <li class="current-page">FAQ</li>
            </ul>
        </div>
        <h2>FAQ</h2></br>
        <!--<marquee direction="up" onmouseover="this.stop()" onmouseout="this.start()" scrollamount="3" style="height:500px;">-->
        <dl class="faq-list">
            <dt class="faq-list_h">
            <h4 class="marker">Q?</h4>
            <h4><?php echo $this->lang->line("faq_page_step1_title_q1"); ?></h4>
            </dt>
            <dd>
                <h4 class="marker1">A.</h4>
                <p class="m_4"><?php echo $this->lang->line("faq_page_step1_title_a1"); ?></p>
            </dd>
            <dt class="faq-list_h">
            <h4 class="marker">Q?</h4>
            <h4><?php echo $this->lang->line("faq_page_step1_title_q2"); ?></h4>
            </dt>
            <dd>
                <h4 class="marker">A.</h4>
                <p class="m_4"><?php echo $this->lang->line("faq_page_step1_title_a2"); ?></p>
            </dd>
            <dt class="faq-list_h">
            <h4 class="marker">Q?</h4>
            <h4><?php echo $this->lang->line("faq_page_step1_title_q3"); ?></h4>
            </dt>
            <dd>
                <h4 class="marker">A.</h4>
                <p class="m_4"><?php echo $this->lang->line("faq_page_step1_title_a3"); ?></p>
            </dd>
            <dt class="faq-list_h">
            <h4 class="marker">Q?</h4>
            <h4><?php echo $this->lang->line("faq_page_step1_title_q4"); ?></h4>
            </dt>
            <dd>
                <h4 class="marker">A.</h4>
                <p class="m_4"><?php echo $this->lang->line("faq_page_step1_title_a4"); ?></p>
            </dd>
            <dt class="faq-list_h">
            <h4 class="marker">Q?</h4>
            <h4><?php echo $this->lang->line("faq_page_step1_title_q5"); ?></h4>
            </dt>
            <dd>
                <h4 class="marker">A.</h4>
                <p class="m_4"><?php echo $this->lang->line("faq_page_step1_title_a5"); ?></p>
            </dd>
            <dt class="faq-list_h">
            <h4 class="marker">Q?</h4>
            <h4><?php echo $this->lang->line("faq_page_step1_title_q6"); ?></h4>
            </dt>
            <dd>
                <h4 class="marker">A.</h4>
                <p class="m_4"><?php echo $this->lang->line("faq_page_step1_title_a6"); ?></p>
            </dd>

        </dl>
        <!--        </marquee>-->
        <!--<div class="accordion">
                 <dl>
                   <dt>
                     <a href="#accordion1" aria-expanded="false" aria-controls="accordion1" class="accordion-title accordionTitle js-accordionTrigger ">How do I change my email address or password?</a>
                   </dt>
                   <dd class="accordion-content accordionItem is-collapsed" id="accordion1" aria-hidden="true">
                     <p>You can change your email address on the "edit your profile" page. Here you can change some other information including search preferences, address and other preferences about your daily routines, habits, hobbies etc.
     After you've made your changes, click "Save" at the bottom of the page to save.</p>
                    
                   </dd>
                   <dt>
                     <a href="#accordion2" aria-expanded="false" aria-controls="accordion2" class="accordion-title accordionTitle js-accordionTrigger">
                       How does "My Matches" works?</a>
                   </dt>
                   <dd class="accordion-content accordionItem is-collapsed" id="accordion2" aria-hidden="true">
                     <p>My Matches works according to the basic profile and "search preferences" at the bottom of the "edit profile" page.</p>
                     
                   </dd>
                   <dt>
                     <a href="#accordion3" aria-expanded="false" aria-controls="accordion3" class="accordion-title accordionTitle js-accordionTrigger">
     Do people actually meet on your site? Do they ever get married?
                     </a>
                   </dt>
                   <dd class="accordion-content accordionItem is-collapsed" id="accordion3" aria-hidden="true">
                     <p>Thousands of people meet on our site daily and go on to relationships. We've also had hundreds of marriages across many borders. To see for yourself, check the site for real stories of real connections.</p>
                    
                   </dd>
      <dt>
                     <a href="#accordion4" aria-expanded="false" aria-controls="accordion3" class="accordion-title accordionTitle js-accordionTrigger">
     When someone emails me, where does the email go and how do I respond?
                     </a>
                   </dt>
                   <dd class="accordion-content accordionItem is-collapsed" id="accordion3" aria-hidden="true">
                     <p>When a member sends you a note, it goes straight into your onsite message Inbox. We then send an email to your personal email address to let you know that it's there.
     
     To read the note, simply login to the site and go to your Inbox. If you decide to write back, simply click "reply", write your own note and send away. Your note will go straight into the member's onsite Inbox, and we'll let them know it's there with an email to their personal email address.
     
     All communications with other members stay onsite so that you never have to give out any personal information until you feel completely ready. Then when you are, you can exchange phone numbers and even meet in person. </p>
                    
                   </dd>
     
      <dt>
                     <a href="#accordion5" aria-expanded="false" aria-controls="accordion3" class="accordion-title accordionTitle js-accordionTrigger">
     How do I edit my profile? How do I hide my profile?
                     </a>
                   </dt>
                   <dd class="accordion-content accordionItem is-collapsed" id="accordion3" aria-hidden="true">
                     <p>To update your profile, click "Edit Profile" in Members Panel menu. Be sure to save your changes at the bottom of the screen when you're done.
     
     Try to put your best foot forward with at least one photo and snappy, detailed essays about your interests. The photo is key since most people feel more comfortable writing to members they can see. Try to stay positive in your essays and let the real you shine.
     
     To hide your profile, select "No" for the option "Other members can see when I'm online?" on the "Edit Profile" page  . Keep in mind, however, that members who are most active on the site keep their profile visible in all places because it ups their chances of connecting with people. And you never know, someone special may be looking for you and never get to find you if your profile is hidden. </p>
                    
                   </dd>
      <dt>
                     <a href="#accordion5" aria-expanded="false" aria-controls="accordion3" class="accordion-title accordionTitle js-accordionTrigger">
     When I suspend my membership, does it end my subscription? Can I put my subscription on hold?
                     </a>
                   </dt>
                   <dd class="accordion-content accordionItem is-collapsed" id="accordion3" aria-hidden="true">
                     <p>No. Once you purchase a subscription, it will remain active for the remainder of your initial term. </p>
                    
                   </dd>
     
     
     
                 </dl>
               </div>
       </div>
     </div>
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
     <script>
     //uses classList, setAttribute, and querySelectorAll
     //if you want this to work in IE8/9 youll need to polyfill these
     (function(){
             var d = document,
             accordionToggles = d.querySelectorAll('.js-accordionTrigger'),
             setAria,
             setAccordionAria,
             switchAccordion,
       touchSupported = ('ontouchstart' in window),
       pointerSupported = ('pointerdown' in window);
       
       skipClickDelay = function(e){
         e.preventDefault();
         e.target.click();
       }
     
                     setAriaAttr = function(el, ariaType, newProperty){
                     el.setAttribute(ariaType, newProperty);
             };
             setAccordionAria = function(el1, el2, expanded){
                     switch(expanded) {
           case "true":
             setAriaAttr(el1, 'aria-expanded', 'true');
             setAriaAttr(el2, 'aria-hidden', 'false');
             break;
           case "false":
             setAriaAttr(el1, 'aria-expanded', 'false');
             setAriaAttr(el2, 'aria-hidden', 'true');
             break;
           default:
                                     break;
                     }
             };
     //function
     switchAccordion = function(e) {
       console.log("triggered");
             e.preventDefault();
             var thisAnswer = e.target.parentNode.nextElementSibling;
             var thisQuestion = e.target;
             if(thisAnswer.classList.contains('is-collapsed')) {
                     setAccordionAria(thisQuestion, thisAnswer, 'true');
             } else {
                     setAccordionAria(thisQuestion, thisAnswer, 'false');
             }
             thisQuestion.classList.toggle('is-collapsed');
             thisQuestion.classList.toggle('is-expanded');
                     thisAnswer.classList.toggle('is-collapsed');
                     thisAnswer.classList.toggle('is-expanded');
             
             thisAnswer.classList.toggle('animateIn');
             };
             for (var i=0,len=accordionToggles.length; i<len; i++) {
                     if(touchSupported) {
           accordionToggles[i].addEventListener('touchstart', skipClickDelay, false);
         }
         if(pointerSupported){
           accordionToggles[i].addEventListener('pointerdown', skipClickDelay, false);
         }
         accordionToggles[i].addEventListener('click', switchAccordion, false);
       }
     })();
     </script>-->
