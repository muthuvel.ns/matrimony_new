<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/css/select2.min.css">
<!--photo add-->
<script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/browser/js/jquery.filer.min.js?v=1.0.5"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/browser/js/custom.js?v=1.0.5"></script>
<!--photo end-->

<!-- Latest compiled and minified JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/js/select2.min.js"></script>
<link href="<?php echo base_url(); ?>assets/css/profile_edit.css" rel='stylesheet' type='text/css' />   
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script type="text/javascript">
    function SwapDivsWithClick(div1, div2)
    {
        d1 = document.getElementById(div1);
        d2 = document.getElementById(div2);
        if (d2.style.display == "none")
        {
            d1.style.display = "none";
            d2.style.display = "block";
        }
        else
        {
            d1.style.display = "block";
            d2.style.display = "none";
        }
    }
    function SwapDivsWithClick(div3, div4)
    {
        d3 = document.getElementById(div3);
        d4 = document.getElementById(div4);
        if (d4.style.display == "none")
        {
            d3.style.display = "none";
            d4.style.display = "block";
        }
        else
        {
            d3.style.display = "block";
            d4.style.display = "none";
        }
    }
    function SwapDivsWithClick(div5, div6)
    {
        d5 = document.getElementById(div5);
        d6 = document.getElementById(div6);
        if (d6.style.display == "none")
        {
            d5.style.display = "none";
            d6.style.display = "block";
        }
        else
        {
            d5.style.display = "block";
            d6.style.display = "none";
        }
    }
    function SwapDivsWithClick(div5, div6)
    {
        d5 = document.getElementById(div5);
        d6 = document.getElementById(div6);
        if (d6.style.display == "none")
        {
            d5.style.display = "none";
            d6.style.display = "block";
        }
        else
        {
            d5.style.display = "block";
            d6.style.display = "none";
        }
    }
    function SwapDivsWithClick(div7, div8)
    {
        d7 = document.getElementById(div7);
        d8 = document.getElementById(div8);
        if (d8.style.display == "none")
        {
            d7.style.display = "none";
            d8.style.display = "block";
        }
        else
        {
            d7.style.display = "block";
            d8.style.display = "none";
        }
    }
    function SwapDivsWithClick(div9, div10)
    {
        d9 = document.getElementById(div9);
        d10 = document.getElementById(div10);
        if (d10.style.display == "none")
        {
            d9.style.display = "none";
            d10.style.display = "block";
        }
        else
        {
            d9.style.display = "block";
            d10.style.display = "none";
        }
    }
    $(document).ready(function () {
        $(".js-example-basic-single").select2();
        $(".js-example-data-array-selected").select2();
        $("#caste").select2({
            data: <?php echo (!empty($casteList) ? $casteList : NULL) ?>
        });
        $("#country").select2({
            data: <?php echo (!empty($countryList) ? $countryList : NULL) ?>
        });
        $("#state").select2({
            data: <?php echo (!empty($stateList) ? $stateList : NULL) ?>
        });
        $("#city").select2({
            data: <?php echo (!empty($cityList) ? $cityList : NULL) ?>
        });
        $("#edu").select2({
            data: <?php echo (!empty($education) ? $education : NULL) ?>
        });

        $("#pre_caste").select2({
            data: <?php echo (!empty($casteList) ? $casteList : NULL) ?>
        });
        $("#pre_country").select2({
            data: <?php echo (!empty($countryList) ? $countryList : NULL) ?>
        });
        $("#pre_state").select2({
            data: <?php echo (!empty($stateList) ? $stateList : NULL) ?>
        });
        $("#pre_city").select2({
            data: <?php echo (!empty($cityList) ? $cityList : NULL) ?>
        });
        $("#pre_edu").select2({
            data: <?php echo (!empty($education) ? $education : NULL) ?>
        });
    });

    $(function () {
        $("#close").on('click', function () {alert('123');
            $('#response').hide();
        });
    });
</script>
<?php if (!empty($message)) {
    ?>
    <script>
        $(document).ready(function () {
            $('#response').show();
            $('#msg_body').html('<h4 style="text-align:center; color:green;"><?php echo $message; ?></h4>');
        });
    </script>
<?php } ?>

<!--Interest msg-->
<div id="response" class="modal">
    <div class="modal-dialog animated sample" style=" margin-top: 9%;width: 440px;">
        <div class="modal-contentshort">
            <div class="row">
                <div>
                    <span style="float:left;"></span>
                    <span><h3 class="subtitle"><?php echo $this->lang->line('text_alert_msg'); ?></h3></span>
                    <span class="modal-closeshort shtclose" id="close" ><a href="#"><img src="<?php echo base_url(); ?>assets/images/m/forgot-password-close.gif"></a></span>
                </div>
                <div class="form-group row">
                    <div class="col-sm-10 short-text" id="msg_body" style="text-align:center;">
                        <div id='loader' style='display:block'>
                            <img src="<?php echo base_url(); ?>assets/images/loading.gif"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--Interest msg end-->
</br></br></br>
<div class="container">
    <h3 class="registertitle pro-edit"><?php echo $this->lang->line('register_profile_for_process2'); ?></h3>
    <div class="col-md-12">
        <?php
        /** profile */
        $username = (!empty($profile['username']) ? $profile['username'] : '');
        $email = (!empty($profile['email']) ? $profile['email'] : '');
        $mobile = (!empty($profile['mobile']) ? $profile['mobile'] : '');
        $martialstatus = (!empty($profile['martial_status']) ? $profile['martial_status'] : '');
        $casteid = (!empty($profile['caste']) ? $profile['caste'] : '');
        $subCaste = (!empty($profile['sub_caste']) ? $profile['sub_caste'] : '');
        $gothram = (!empty($profile['gothram']) ? $profile['gothram'] : '');
        $dob = (!empty($profile['dob']) ? $profile['dob'] : '');
        $childs = (!empty($profile['childs']) ? $profile['childs'] : '');
        $gender = (!empty($profile['gender']) ? $profile['gender'] : '');
        $phystatus = (!empty($profile['physical_status']) ? $profile['physical_status'] : '');
        $bodytype = (!empty($profile['bodytype']) ? $profile['bodytype'] : '');
        $complexion = (!empty($profile['complexion']) ? $profile['complexion'] : '');
        $height = (!empty($profile['height']) ? $profile['height'] : '');
        $weight = (!empty($profile['weight']) ? $profile['weight'] : '');
        $country = (!empty($profile['country']) ? $profile['country'] : '');
        $state = (!empty($profile['state']) ? $profile['state'] : '');
        $city = (!empty($profile['city']) ? $profile['city'] : '');
        $country_name = (!empty($profile['country_name']) ? $profile['country_name'] : '');
        $state_name = (!empty($profile['state_name']) ? $profile['state_name'] : '');
        $city_name = (!empty($profile['city_name']) ? $profile['city_name'] : '');
        $education = (!empty($profile['education']) ? $profile['education'] : '');
        $occupation = (!empty($profile['occupation']) ? $profile['occupation'] : '');
        $qualification = (!empty($profile['qualification']) ? $profile['qualification'] : '');
        $employeed = (!empty($profile['employeed_in']) ? $profile['employeed_in'] : '');
        $salary = (!empty($profile['salary']) ? $profile['salary'] : '');
        $salary_view = (!empty($profile['salary_view']) ? $profile['salary_view'] : '');
        $religion = (!empty($profile['religion']) ? $profile['religion'] : '');
        $tongue = (!empty($profile['mother_tongue']) ? $profile['mother_tongue'] : '');
        $dhosam = (!empty($profile['dhosam']) ? $profile['dhosam'] : '');
        $dhosamtype = (!empty($profile['dhosam_type']) ? $profile['dhosam_type'] : '');
        $star = (!empty($profile['star']) ? $profile['star'] : '');
        $raasi = (!empty($profile['raasi']) ? $profile['raasi'] : '');
        $food = (!empty($profile['food']) ? $profile['food'] : '');
        $drink = (!empty($profile['drink']) ? $profile['drink'] : '');
        $smoke = (!empty($profile['smoke']) ? $profile['smoke'] : '');
        $ftype = (!empty($profile['family_type']) ? $profile['family_type'] : '');
        $fvalues = (!empty($profile['family_values']) ? $profile['family_values'] : '');
        $fstatus = (!empty($profile['family_status']) ? $profile['family_status'] : '');
        $castename = (!empty($profile['caste_name']) ? $profile['caste_name'] : '');
        $eduname = (!empty($profile['edu_name']) ? $profile['edu_name'] : '');

        /** partner prefrence */
        $ageFrom = (!empty($prefrenceData['age_from']) ? $prefrenceData['age_from'] : '');
        $ageTo = (!empty($prefrenceData['age_to']) ? $prefrenceData['age_to'] : '');
        $pre_maritalstatus = (!empty($prefrenceData['maritalstatus']) ? $prefrenceData['maritalstatus'] : '');
        $pre_height = (!empty($prefrenceData['height']) ? $prefrenceData['height'] : '');
        $pre_physicalstatus = (!empty($prefrenceData['physicalstatus']) ? $prefrenceData['physicalstatus'] : '');
        $pre_food = (!empty($prefrenceData['food']) ? $prefrenceData['food'] : '');
        $pre_smoke = (!empty($prefrenceData['smoking']) ? $prefrenceData['smoking'] : '');
        $pre_drink = (!empty($prefrenceData['drinking']) ? $prefrenceData['drinking'] : '');
        $pre_caste = (!empty($prefrenceData['caste']) ? $prefrenceData['caste'] : '');
        $pre_dhosam = (!empty($prefrenceData['dhosam']) ? $prefrenceData['dhosam'] : '');
        $pre_dhosam_type = (!empty($prefrenceData['dhosam_type']) ? $prefrenceData['dhosam_type'] : '');
        $pre_star = (!empty($prefrenceData['star']) ? $prefrenceData['star'] : '');
        $pre_country = (!empty($prefrenceData['country']) ? $prefrenceData['country'] : '');
        $pre_state = (!empty($prefrenceData['state']) ? $prefrenceData['state'] : '');
        $pre_city = (!empty($prefrenceData['city']) ? $prefrenceData['city'] : '');
        $pre_education = (!empty($prefrenceData['education']) ? $prefrenceData['education'] : '');
        $pre_qualification = (!empty($prefrenceData['qualification']) ? $prefrenceData['qualification'] : '');
        $pre_occupation = (!empty($prefrenceData['occupation']) ? $prefrenceData['occupation'] : '');
        $pre_salary = (!empty($prefrenceData['salary']) ? $prefrenceData['salary'] : '');
        $pre_employeed = (!empty($prefrenceData['employeed_in']) ? $prefrenceData['employeed_in'] : '');
        $pre_salaryView = (!empty($prefrenceData['salary_view']) ? $prefrenceData['salary_view'] : '');
        $pre_castename = (!empty($prefrenceData['caste_name']) ? $prefrenceData['caste_name'] : '');
        $pre_eduname = (!empty($prefrenceData['edu_name']) ? $prefrenceData['edu_name'] : '');
        $pre_country_name = (!empty($prefrenceData['country_name']) ? $prefrenceData['country_name'] : '');
        $pre_state_name = (!empty($prefrenceData['state_name']) ? $prefrenceData['state_name'] : '');
        $pre_city_name = (!empty($prefrenceData['city_name']) ? $prefrenceData['city_name'] : '');
        $pre_castename = (!empty($prefrenceData['caste_name']) ? $prefrenceData['caste_name'] : '');
        ?>
        <div class="drop-shadow ">
                      <!--<a id="showModal" href="" style="float: right;" class="btn btn-primary "><i class="fa fa-pencil"></i>Edit</a>-->
            <h3 class="pro-edit"><?php echo sprintf($this->lang->line('more_text'), $this->lang->line('register_profile_for_process2')); ?></h3>
            <div id="div1" style="display:block; ">
                <div class="edit_btn">
                    <a id="" href="javascript:SwapDivsWithClick('div1','div2')" style="float: right;" class="btn btn-primary ">
                        <i class="fa fa-pencil"></i><?php echo $this->lang->line('edit_text'); ?></a>
                </div>
                <table style="width:50%">
                    <tr>
                        <td><?php echo $this->lang->line('register_profile_for_email'); ?></td>
                        <td>:</td>		
                        <td><?php echo $email; ?></td>
                    </tr>
                    <tr>
                        <td><?php echo sprintf($this->lang->line('mobile_admin_register'), ''); ?></td>
                        <td>:</td>		
                        <td><?php echo $mobile; ?></td>
                    </tr>
                    <tr>
                        <td><?php echo $this->lang->line('register_profile_for_marriagestatus'); ?></td>
                        <td>:</td>		
                        <td><?php echo (!empty($martialstatus) ? constant("MARITAL_STATUS_" . $martialstatus) : ''); ?>
                        </td>
                    </tr>
                    <tr>
                        <td><?php echo $this->lang->line('register_profile_for_country'); ?> </td>
                        <td>:</td>		
                        <td><?php echo $country_name; ?></td>
                    </tr>
                    <tr>
                        <td><?php echo $this->lang->line('register_profile_for_state'); ?></td>
                        <td>:</td>		
                        <td><?php echo $state_name; ?></td>
                    </tr>
                    <tr>
                        <td><?php echo $this->lang->line('register_profile_for_city'); ?></td>
                        <td>:</td>		
                        <td><?php echo $city_name; ?></td>
                    </tr>

                </table>
            </div>
            <!--form-->
            <div id="div2" style="display:none; ">
                <a id="" href="javascript:SwapDivsWithClick('div1','div2')" style="float: right;" class="btn btn-primary ">
                    <i class="fa fa-times"></i><?php echo $this->lang->line('close_text'); ?></a>
                <form class="form-horizontal" id="pesonal" method="post">
                    <div class="modal-header">
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="col-sm-3 form-control-label"><?php echo $this->lang->line('register_profile_for_email'); ?><span class="red">*</span></label>
                            <div class="col-lg-8">
                                <input class="form-control" value="<?php echo $email; ?>" name="email" type="text" readonly  required	>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 form-control-label"><?php echo sprintf($this->lang->line('mobile_admin_register'), ''); ?><span class="red">*</span></label>
                            <div class="col-lg-8">
                                <input class="form-control" value="<?php echo $mobile; ?>" name="mobile" id ="mobile" type="text" required>
                            </div>
                        </div>
                        <?php
                        $martial1 = $martial2 = $martial3 = $martial4 = '';
                        if ($martialstatus == 1) {
                            $martial1 = 'checked';
                        } elseif ($martialstatus == 2) {
                            $martial2 = 'checked';
                        } elseif ($martialstatus == 3) {
                            $martial3 = 'checked';
                        } elseif ($martialstatus == 4) {
                            $martial4 = 'checked';
                        }
                        ?>
                        <div class="form-group">
                            <label class="col-sm-3 form-control-label"><?php echo $this->lang->line('register_profile_for_marriagestatus'); ?> <span class="red">*</span></label>
                            <div class="col-md-8">
                                <div class="col-md-3">
                                    <label class="radio-inline reglabrad">
                                        <input type="radio" name="maritalstatus" <?php echo $martial1; ?> id="maritalstatus" value="1" required> <?php echo $this->lang->line('register_profile_for_marriagestatus_1'); ?>
                                    </label>
                                </div>
                                <div class="col-md-3">
                                    <label class="radio-inline reglabrad">
                                        <input type="radio" name="maritalstatus" <?php echo $martial2; ?> id="watch-me" value="2" required> <?php echo $this->lang->line('register_profile_for_marriagestatus_2'); ?>	
                                    </label>
                                </div>
                                <div class="col-md-3">
                                    <label class="radio-inline reglabrad">
                                        <input type="radio" name="maritalstatus" <?php echo $martial3; ?> id="watch-me" value="3" required> <?php echo $this->lang->line('register_profile_for_marriagestatus_3'); ?> 
                                    </label>
                                </div>
                                <div class="col-md-3">
                                    <label class="radio-inline reglabrad">
                                        <input type="radio" name="maritalstatus" <?php echo $martial4; ?> id="watch-me" value="4" required> <?php echo $this->lang->line('register_profile_for_marriagestatus_4'); ?>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group" >
                            <label for="country" class="col-sm-3 form-control-label"><?php echo $this->lang->line('register_profile_for_country'); ?>  <span class="red">*</span></label>  
                            <div class="col-sm-8">
                                <select class="form-control js-example-data-array-selected" id="country" name="country" onchange="stateInfo(this.value)" required>
                                    <option value=""> -- Select Country -- </option>
                                    <option value="<?php echo $country ?>" selected="selected"> <?php echo $country_name; ?> </option>
                                </select>
                                <label class="error" for="country"></label>     
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="state" class="col-sm-3 form-control-label"><?php echo $this->lang->line('register_profile_for_state'); ?> <span class="red">*</span></label>  
                            <div class="col-sm-8"  id="text-state">
                                <select class="form-control js-example-data-array-selected"  name="state" id="state" onchange="cityInfo(this.value)" required>
                                    <option value=""> -- Select State -- </option>
                                    <option value="<?php echo $state ?>" selected="selected"> <?php echo $state_name; ?> </option>
                                </select>
                                <label class="error" for="state"></label>          
                            </div>
                        </div>

                        <div class="form-group" >
                            <label for="city" class="col-sm-3 form-control-label"><?php echo $this->lang->line('register_profile_for_city'); ?> <span class="red">*</span></label>  
                            <div class="col-sm-8" id="text-city">
                                <select class="form-control js-example-data-array-selected"  id ="city" name="city" required>
                                    <option value=""> -- Select City -- </option>
                                    <option value="<?php echo $city ?>" selected="selected"> <?php echo $city_name; ?> </option>
                                </select>
                                <label class="error" for="city"></label>         
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-default" type="button" ><?php echo $this->lang->line('cancel_admin_register'); ?></button>
                            <button class="btn btn-success" type="submit" id="pesonal_submit"><?php echo $this->lang->line('save_text'); ?></button>
                        </div>
                    </div>
                </form>
            </div>
            <!--form-->
        </div>
        <div class="drop-shadow ">
              <!--<a style="float: right;" class="btn btn-primary "><i class="fa fa-pencil"></i>Edit</a>-->
            <h3 class="pro-edit"><?php echo $this->lang->line('register_profile_for_physical_title1'); ?></h3>

            <div id="div5" style="display:block; ">
                <div class="edit_btn">
                    <a id="" href="javascript:SwapDivsWithClick('div5','div6')" style="float: right;" class="btn btn-primary ">
                        <i class="fa fa-pencil"></i><?php echo $this->lang->line('edit_text'); ?></a>
                </div>
                <table style="width:50%">
                    <tr>
                        <td><?php echo $this->lang->line('register_profile_for_height'); ?> </td>
                        <td>:</td>		
                        <td><?php echo $height . ' cm'; ?></td>
                    </tr>
                    <tr>
                        <td><?php echo $this->lang->line('register_profile_for_weight'); ?> </td>
                        <td>:</td>		
                        <td><?php echo $weight . ' kg'; ?></td>
                    </tr>
                    <tr>
                        <td><?php echo $this->lang->line('register_profile_for_bodytype'); ?> </td>
                        <td>:</td>		
                        <td><?php echo (!empty($bodytype) ? constant("BODY_TYPE_" . $bodytype) : ''); ?></td>
                    </tr>
                    <tr>
                        <td><?php echo $this->lang->line('register_profile_for_complexion'); ?> </td>
                        <td>:</td>		
                        <td><?php echo (!empty($complexion) ? constant("COMPLEXION_TYPE_" . $complexion) : ''); ?></td>
                    </tr>
                    <tr>
                        <td><?php echo $this->lang->line('register_profile_for_physicalstatus'); ?> </td>
                        <td>:</td>		
                        <td><?php echo (!empty($phystatus) ? constant("PHYSICAL_STATUS_" . $phystatus) : ""); ?></td>
                    </tr>
                </table>
            </div>
            <!--form-->
            <div id="div6" style="display:none; ">
                <a id="" href="javascript:SwapDivsWithClick('div5','div6')" style="float: right;" class="btn btn-primary ">
                    <i class="fa fa-times"></i><?php echo $this->lang->line('close_text'); ?></a>
                <form class="form-horizontal" role="form" id="physical_form">
                    <div class="modal-header">
                    </div>
                    <div class="modal-body">
                        <div class="form-group row">
                            <label for="height" class="col-sm-4 form-control-label"><?php echo $this->lang->line('register_profile_for_height'); ?> <span class="red">*</span></label>  
                            <div class="col-sm-8">
                                <select class="form-control js-example-basic-single" name="height" id="height" required>
                                    <option value=""> -- Height -- </option>
                                    <option value="145" label="4' 9&quot; (145 cm)">4' 9" (145 cm)</option>
                                    <option value="147" label="4' 10&quot; (147 cm)">4' 10" (147 cm)</option>
                                    <option value="150" label="4' 11&quot; (150 cm)">4' 11" (150 cm)</option>
                                    <option value="152" label="5'  0&quot; (152 cm)">5'  0" (152 cm)</option>
                                    <option value="155" label="5' 1&quot; (155 cm)">5' 1" (155 cm)</option>
                                    <option value="157" label="5' 2&quot; (157 cm)">5' 2" (157 cm)</option>
                                    <option value="160" label="5' 3&quot; (160 cm)">5' 3" (160 cm)</option>
                                    <option value="162" label="5' 4&quot; (162 cm)">5' 4" (162 cm)</option>
                                    <option value="165" label="5' 5&quot; (165 cm)">5' 5" (165 cm)</option>
                                    <option value="167" label="5' 6&quot; (167 cm)">5' 6" (167 cm)</option>
                                    <option value="170" label="5' 7&quot; (170 cm)">5' 7" (170 cm)</option>
                                    <option value="173" label="5' 8&quot; (173 cm)">5' 8" (173 cm)</option>
                                    <option value="175" label="5' 9&quot; (175 cm)">5' 9" (175 cm)</option>
                                    <option value="178" label="5' 10&quot; (178 cm)">5' 10" (178 cm)</option>
                                    <option value="180" label="5' 11&quot; (180 cm)">5' 11" (180 cm)</option>
                                    <option value="183" label="6' 0&quot; (183 cm)">6' 0" (183 cm)</option>
                                    <option value="185" label="6' 1&quot; (185 cm)">6' 1" (185 cm)</option>
                                    <option value="188" label="6' 2&quot; (188 cm)">6' 2" (188 cm)</option>
                                    <option value="190" label="6' 3&quot; (190 cm)">6' 3" (190 cm)</option>
                                    <option value="193" label="6' 4&quot; (193 cm)">6' 4" (193 cm)</option>
                                    <option value="21">Above the Height </option>
                                </select>         
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="weight" class="col-sm-4 form-control-label"><?php echo $this->lang->line('register_profile_for_weight'); ?> </label>  
                            <div class="col-sm-8">
                                <select class="form-control js-example-basic-single" name="weight" id="weight">
                                    <option value="0"> -- Weight -- </option>
                                    <option value="41">41 kg</option>
                                    <option value="42">42 kg</option>
                                    <option value="43">43 kg</option>
                                    <option value="44">44 kg</option>
                                    <option value="45">45 kg</option>
                                    <option value="46">46 kg</option>
                                    <option value="47">47 kg</option>
                                    <option value="48">48 kg</option>
                                    <option value="49">49 kg</option>
                                    <option value="50">50 kg</option>
                                    <option value="51">51 kg</option>
                                    <option value="52">52 kg</option>
                                    <option value="53">53 kg</option>
                                    <option value="54">54 kg</option>
                                    <option value="55">55 kg</option>
                                    <option value="56">56 kg</option>
                                    <option value="57">57 kg</option>
                                    <option value="58">58 kg</option>
                                    <option value="59">59 kg</option>
                                    <option value="60">60 kg</option>
                                    <option value="61">61 kg</option>
                                    <option value="62">62 kg</option>
                                    <option value="63">63 kg</option>
                                    <option value="64">64 kg</option>
                                    <option value="65">65 kg</option>
                                    <option value="66">66 kg</option>
                                    <option value="67">67 kg</option>
                                    <option value="68">68 kg</option>
                                    <option value="69">69 kg</option>
                                    <option value="70">70 kg</option>
                                    <option value="71">71 kg</option>
                                    <option value="72">72 kg</option>
                                    <option value="73">73 kg</option>
                                    <option value="74">74 kg</option>
                                    <option value="75">75 kg</option>
                                    <option value="76">76 kg</option>
                                    <option value="77">77 kg</option>
                                    <option value="78">78 kg</option>
                                    <option value="79">79 kg</option>
                                    <option value="80">80 kg</option>
                                    <option value="81">81 kg</option>
                                    <option value="82">82 kg</option>
                                    <option value="83">83 kg</option>
                                    <option value="84">84 kg</option>
                                    <option value="85">85 kg</option>
                                    <option value="86">86 kg</option>
                                    <option value="87">87 kg</option>
                                    <option value="88">88 kg</option>
                                    <option value="89">89 kg</option>
                                    <option value="90">90 kg</option>
                                    <option value="91">91 kg</option>
                                    <option value="92">92 kg</option>
                                    <option value="93">93 kg</option>
                                    <option value="94">94 kg</option>
                                    <option value="95">95 kg</option>
                                    <option value="96">96 kg</option>
                                    <option value="97">97 kg</option>
                                    <option value="98">98 kg</option>
                                    <option value="99">99 kg</option>
                                    <option value="100">100 kg</option>
                                    <option value="101">Above the weight </option>
                                </select>         
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="bodytype" class="col-sm-4 form-control-label"><?php echo $this->lang->line('register_profile_for_bodytype'); ?> </label>
                            <div class="col-sm-8">
                                <div class="col-md-3">
                                    <label class="radio-inline reglabrad">
                                        <input type="radio" name="bodytype"  id="bodytype_1" value="1"> <?php echo $this->lang->line('register_profile_for_bodytype_1'); ?> 
                                    </label>
                                </div>
                                <div class="col-md-3">
                                    <label class="radio-inline reglabrad">
                                        <input type="radio" name="bodytype"  id="bodytype_2" value="2"> <?php echo $this->lang->line('register_profile_for_bodytype_2'); ?> 	
                                    </label>
                                </div>
                                <div class="col-md-3">
                                    <label class="radio-inline reglabrad">
                                        <input type="radio" name="bodytype"  id="bodytype_3" value="3">	<?php echo $this->lang->line('register_profile_for_bodytype_3'); ?> 
                                    </label>
                                </div>
                                <div class="col-md-3">
                                    <label class="radio-inline reglabrad">
                                        <input type="radio" name="bodytype"  id="bodytype_4" value="4">	<?php echo $this->lang->line('register_profile_for_bodytype_4'); ?> 
                                    </label>
                                </div>
                            </div>
                        </div> 

                        <div class="form-group row">
                            <label for="complexion" class="col-sm-4 form-control-label"><?php echo $this->lang->line('register_profile_for_complexion'); ?> </label>
                            <div class="col-sm-8">
                                <div class="col-md-3">
                                    <label class="radio-inline reglabrad">
                                        <input type="radio" name="complexion"  id="complexion_1" value="1"> <?php echo $this->lang->line('register_profile_for_complexion_1'); ?> 
                                    </label>
                                </div> 
                                <div class="col-md-3">
                                    <label class="radio-inline reglabrad">
                                        <input type="radio" name="complexion"  id="complexion_2" value="2"> <?php echo $this->lang->line('register_profile_for_complexion_2'); ?>  
                                    </label>
                                </div> 
                                <div class="col-md-3">
                                    <label class="radio-inline reglabrad">
                                        <input type="radio" name="complexion"  id="complexion_3" value="3"> <?php echo $this->lang->line('register_profile_for_complexion_3'); ?> 	
                                    </label>
                                </div> 
                                <div class="col-md-3">
                                    <label class="radio-inline reglabrad">
                                        <input type="radio" name="complexion"  id="complexion_4" value="4">	<?php echo $this->lang->line('register_profile_for_complexion_4'); ?> 	
                                    </label>
                                </div> 
                            </div>
                        </div> 

                        <div class="form-group row">
                            <label for="physicalstatus" class="col-sm-4 form-control-label"><?php echo $this->lang->line('register_profile_for_physicalstatus'); ?>  <span class="red">*</span></label>
                            <div class="col-sm-8">
                                <div class="col-md-6">
                                    <label class="radio-inline reglabrad">
                                        <input type="radio" name="physicalstatus" id="physical_1" value="1" required> <?php echo $this->lang->line('register_profile_for_physicalstatus_1'); ?>  
                                    </label>
                                </div>
                                <div class="col-md-6">
                                    <label class="radio-inline reglabrad">
                                        <input type="radio" name="physicalstatus"  id="physical_2" value="2" required> <?php echo $this->lang->line('register_profile_for_physicalstatus_2'); ?> 
                                    </label>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-default" type="button" ><?php echo $this->lang->line('cancel_admin_register'); ?></button>
                        <button class="btn btn-primary" type="submit" id="physical_submit"><?php echo $this->lang->line('save_text'); ?></button>
                    </div>
                </form>
            </div>
            <!--form-->
        </div>
        <div class="drop-shadow ">
            <h3 class="pro-edit"><?php echo $this->lang->line('register_profile_for_education_title1'); ?></h3>
            <div id="div7" style="display:block; ">
                <div class="edit_btn">
                    <a id="" href="javascript:SwapDivsWithClick('div7','div8')" style="float: right;" class="btn btn-primary ">
                        <i class="fa fa-pencil"></i><?php echo $this->lang->line('edit_text'); ?></a>
                </div>
                <table style="width:50%">
                    <tr>
                        <td><?php echo $this->lang->line('register_profile_for_highesteducation'); ?> </td>
                        <td>:</td>		
                        <td><?php echo $eduname ?></td>
                    </tr>
                    <tr>
                        <td><?php echo $this->lang->line('qualification_text'); ?>  </td>
                        <td>:</td>		
                        <td><?php echo $qualification ?></td>
                    </tr>
                    <tr>
                        <td><?php echo $this->lang->line('register_profile_for_occupation'); ?>  </td>
                        <td>:</td>		
                        <td><?php echo $occupation ?></td>
                    </tr>
                    <tr>
                        <td><?php echo $this->lang->line('register_profile_for_employeed'); ?> </td>
                        <td>:</td>		
                        <td><?php echo (!empty($employeed) ? constant("EMPLOYEED_IN_" . $employeed) : ''); ?></td>
                    </tr>
                    <tr>
                        <td><?php echo $this->lang->line('register_profile_for_salary'); ?></td>
                        <td>:</td>		
                        <td><?php echo $salary_view; ?></td>
                    </tr>

                </table>
            </div>
            <!--form-->
            <div id="div8" style="display:none; ">
                <a id="" href="javascript:SwapDivsWithClick('div7','div8')" style="float: right;" class="btn btn-primary ">
                    <i class="fa fa-times"></i><?php echo $this->lang->line('close_text'); ?></a>
                <form class="form-horizontal" role="form" id="edu_form">
                    <div class="modal-header">
                    </div>
                    <div class="modal-body">
                        <div class="form-group row">
                            <label for="edu" class="col-sm-4 form-control-label"><?php echo $this->lang->line('register_profile_for_highesteducation'); ?> <span class="red">*</span></label>  
                            <div class="col-sm-8">
                                <select class="form-control js-example-data-array" name="edu" id ="edu" required>
                                    <?php if ($education == 0) { ?>
                                        <option value="0" selected="selected"> Any</option>
                                    <?php } else { ?>
                                        <option value="<?php echo $education ?>" selected="selected"> <?php echo $eduname; ?> </option>
                                    <?php } ?>
                                </select>
                                <label for="edu" class="error"></label>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="occupation" class="col-sm-4 form-control-label"><?php echo $this->lang->line('qualification_text'); ?> <span class="red">*</span></label>  
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="qualification" required placeholder="Qualification" value="<?php echo $qualification ?>">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="occupation" class="col-sm-4 form-control-label"><?php echo $this->lang->line('register_profile_for_occupation'); ?><span class="red">*</span></label>  
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="occupation" required placeholder="Occupation" value="<?php echo $occupation ?>">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="empin" class="col-sm-4 form-control-label"><?php echo $this->lang->line('register_profile_for_employeed'); ?> <span class="red">*</span></label>
                            <div class="col-sm-8">
                                <div class="col-md-3">
                                    <label class="radio-inline reglabrad">
                                        <input type="radio" name="emp_in"  id="emp_1" value="1" required> <?php echo $this->lang->line('register_profile_for_employeed_1'); ?>
                                    </label>
                                </div>
                                <div class="col-md-3">
                                    <label class="radio-inline reglabrad">
                                        <input type="radio" name="emp_in" id="emp_2" value="2" required> <?php echo $this->lang->line('register_profile_for_employeed_2'); ?>	
                                    </label>
                                </div>
                                <div class="col-md-3">
                                    <label class="radio-inline reglabrad">
                                        <input type="radio" name="emp_in"  id="emp_3"value="3" required> <?php echo $this->lang->line('register_profile_for_employeed_3'); ?> 	
                                    </label>
                                </div>
                                <div class="col-md-3">
                                    <label class="radio-inline reglabrad">
                                        <input type="radio" name="emp_in"  id="emp_4" value="4" required> <?php echo $this->lang->line('register_profile_for_employeed_4'); ?>
                                    </label>
                                </div>
                            </div>
                        </div> 
                        <div class="form-group row">
                            <label for="salary" class="col-sm-4 form-control-label"><?php echo $this->lang->line('register_profile_for_salary'); ?> </label>
                            <div class="col-sm-8">
                                <select class="form-control js-example-basic-single" name="salary" id="salary">
                                    <option value=""> -- select salary -- </option>
                                    <option value="1">100000 or less</option>
                                    <option value="2">200000 to 300000</option>
                                    <option value="3">300000 to 400000</option>
                                    <option value="4">400000 to 500000</option>
                                    <option value="5">500000 to 600000</option>
                                    <option value="6">600000 to 700000</option>
                                    <option value="7">700000 to 800000</option>
                                    <option value="8">800000 to 900000</option>
                                    <option value="9">900000 Above</option>
                                    <option value="10">any</option>
                                </select>   
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-default" type="button" ><?php echo $this->lang->line('cancel_admin_register'); ?></button>
                        <button class="btn btn-primary" type="submit" ><?php echo $this->lang->line('save_text'); ?></button>
                    </div>
                </form>
            </div>
            <!--form-->
        </div>
        <div class="drop-shadow ">
            <h3 class="pro-edit"><?php echo $this->lang->line('register_profile_for_habbit_title1'); ?></h3>
            <div id="div9" style="display:block; ">
                <div class="edit_btn">
                    <a id="" href="javascript:SwapDivsWithClick('div9','div10')" style="float: right;" class="btn btn-primary ">
                        <i class="fa fa-pencil"></i><?php echo $this->lang->line('edit_text'); ?></a>
                </div>
                <table style="width:50%">
                    <tr>
                        <td><?php echo $this->lang->line('register_profile_for_food'); ?> </td>
                        <td>:</td>		
                        <td><?php echo (!empty($food) ? constant("FOOD_TYPE_" . $food) : ''); ?> </td>
                    </tr>
                    <tr>
                        <td><?php echo $this->lang->line('register_profile_for_smoking'); ?>  </td>
                        <td>:</td>		
                        <td><?php echo (!empty($smoke) ? constant("SMOKE_" . $smoke) : ''); ?></td>
                    </tr>
                    <tr>
                        <td><?php echo $this->lang->line('register_profile_for_drinking'); ?></td>
                        <td>:</td>		
                        <td><?php echo (!empty($drink) ? constant("DRINK_" . $drink) : ''); ?></td>
                    </tr>
                </table>
            </div>
            <!--form-->
            <div id="div10" style="display:none; ">
                <a id="" href="javascript:SwapDivsWithClick('div9','div10')" style="float: right;" class="btn btn-primary ">
                    <i class="fa fa-times"></i><?php echo $this->lang->line('close_text'); ?></a>
                <form class="form-horizontal" role="form" id="habbit_form">
                    <div class="modal-header">
                    </div>
                    <div class="modal-body">
                        <div class="form-group row">
                            <label for="food" class="col-sm-4 form-control-label"><?php echo $this->lang->line('register_profile_for_food'); ?> </label>
                            <div class="col-sm-8">
                                <div class="col-md-4">
                                    <label class="radio-inline reglabrad">
                                        <input type="radio" name="food" id="food_1" value="1"> <?php echo $this->lang->line('register_profile_for_food_1'); ?> 
                                    </label>
                                </div>
                                <div class="col-md-4">
                                    <label class="radio-inline reglabrad">
                                        <input type="radio" name="food" id="food_2" value="2"> <?php echo $this->lang->line('register_profile_for_food_2'); ?>
                                    </label>
                                </div>
                                <div class="col-md-4">
                                    <label class="radio-inline reglabrad">
                                        <input type="radio" name="food" id="food_3" value="3"> <?php echo $this->lang->line('register_profile_for_food_3'); ?> 
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="smoking" class="col-sm-4 form-control-label"><?php echo $this->lang->line('register_profile_for_smoking'); ?> </label>
                            <div class="col-sm-8">
                                <div class="col-md-4">
                                    <label class="radio-inline reglabrad">
                                        <input type="radio" name="smoking" id="smoke_1" value="1"> <?php echo $this->lang->line('register_profile_for_yes'); ?> 
                                    </label>
                                </div>
                                <div class="col-md-4">
                                    <label class="radio-inline reglabrad">
                                        <input type="radio" name="smoking" id="smoke_2" value="2"> <?php echo $this->lang->line('register_profile_for_no'); ?> 	
                                    </label>
                                </div>
                                <div class="col-md-4">
                                    <label class="radio-inline reglabrad">
                                        <input type="radio" name="smoking" id="smoke_3" value="3"> <?php echo $this->lang->line('register_profile_for_smoking_3'); ?> 
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="drinking" class="col-sm-4 form-control-label"><?php echo $this->lang->line('register_profile_for_drinking'); ?> </label>
                            <div class="col-sm-8">
                                <div class="col-md-4">
                                    <label class="radio-inline reglabrad">
                                        <input type="radio" name="drinking" id="drink_1" value="1"> <?php echo $this->lang->line('register_profile_for_yes'); ?>  
                                    </label>
                                </div>
                                <div class="col-md-4">
                                    <label class="radio-inline reglabrad">
                                        <input type="radio" name="drinking" id="drink_2" value="2"> <?php echo $this->lang->line('register_profile_for_no'); ?> 	
                                    </label>
                                </div>
                                <div class="col-md-4">
                                    <label class="radio-inline reglabrad">
                                        <input type="radio" name="drinking" id="drink_3" value="3"> <?php echo $this->lang->line('register_profile_for_drinking_3'); ?> 
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-default" type="button" ><?php echo $this->lang->line('cancel_admin_register'); ?></button>
                        <button class="btn btn-primary" type="submit" ><?php echo $this->lang->line('save_text'); ?></button>
                    </div>
                </form>
            </div>
            <!--form-->
        </div>
        <div class="drop-shadow ">			    
            <h3 class="pro-edit"><?php echo $this->lang->line('register_profile_for_astrological_title1'); ?></h3>
            <div id="div11" style="display:block; ">
                <div class="edit_btn">
                    <a id="" href="javascript:SwapDivsWithClick('div11','div12')" style="float: right;" class="btn btn-primary ">
                        <i class="fa fa-pencil"></i><?php echo $this->lang->line('edit_text'); ?></a>
                </div>

                <table style="width:50%">
                    <tr>
                        <td><?php echo $this->lang->line('register_profile_for_caste'); ?></td>
                        <td>:</td>		
                        <td><?php echo $castename; ?></td>
                    </tr>
                    <tr>
                        <td><?php echo $this->lang->line('register_profile_for_subcaste'); ?> </td>
                        <td>:</td>		
                        <td><?php echo $subCaste; ?></td>
                    </tr>
                    <tr>
                        <td><?php echo $this->lang->line('register_profile_for_gothram'); ?>  </td>
                        <td>:</td>		
                        <td><?php echo $gothram; ?></td>
                    </tr>
                    <tr>
                        <td><?php echo $this->lang->line('register_profile_for_dhosam'); ?> </td>
                        <td>:</td>		
                        <td><?php echo (!empty($dhosam) ? constant("DHOSAM_TYPE_" . $dhosam) : ''); ?> </td>
                    </tr>
                    <tr>
                        <td><?php echo $this->lang->line('register_profile_for_star'); ?>    </td>
                        <td>:</td>		
                        <td><?php echo $star; ?></td>
                    </tr>
                    <tr>
                        <td><?php echo $this->lang->line('register_profile_for_raasi'); ?>  </td>
                        <td>:</td>		
                        <td><?php echo $raasi; ?></td>
                    </tr>
                </table>
            </div>

            <!--form-->
            <div id="div12" style="display:none; ">
                <a id="" href="javascript:SwapDivsWithClick('div11','div12')" style="float: right;" class="btn btn-primary ">
                    <i class="fa fa-times"></i><?php echo $this->lang->line('close_text'); ?></a>
                <form class="form-horizontal" role="form" id="religion_form">
                    <div class="modal-header">
                    </div>
                    <div class="modal-body">
                        <div class="form-group row">
                            <label class="col-sm-4 form-control-label"><?php echo $this->lang->line('register_profile_for_caste'); ?><span class="red">*</span></label>
                            <div class="col-md-8">
                                <select class="form-control js-example-data-array-selected" id="caste" name="caste" required>
                                    <option value=""> -- Select Caste -- </option>
                                </select> 
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 form-control-label"><?php echo $this->lang->line('register_profile_for_subcaste'); ?> </label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" name="subcaste" placeholder="Subcaste" value="<?php echo $subCaste ?>" >
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 form-control-label"><?php echo $this->lang->line('register_profile_for_gothram'); ?>   :</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="gothram" placeholder="Gothram" value="<?php echo $gothram ?>" >
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="dhosam" class="col-sm-4 form-control-label"><?php echo $this->lang->line('register_profile_for_dhosam'); ?> </label>
                            <div class="col-sm-8">
                                <div class="col-md-4">
                                    <label class="radio-inline reglabrad">
                                        <input type="radio" name="dhosam" id="dhosam_1" value="1"> <?php echo $this->lang->line('register_profile_for_yes'); ?>   
                                    </label>
                                </div>
                                <div class="col-md-4">
                                    <label class="radio-inline reglabrad">
                                        <input type="radio" name="dhosam" id="dhosam_2"value="2"> <?php echo $this->lang->line('register_profile_for_no'); ?>  	
                                    </label>
                                </div>
                                <div class="col-md-4">
                                    <label class="radio-inline reglabrad">
                                        <input type="radio" name="dhosam" id="dhosam_3" value="3"> <?php echo $this->lang->line('register_profile_for_dhosam_3'); ?>
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row" id="dhosams" style="display:none;">
                            <div class="col-sm-offset-4 col-sm-8 ">
                                <input type="text" placeholder="Enter dhosam" name="dhosam_type" class="form-control">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="star" class="col-sm-4 form-control-label"><?php echo $this->lang->line('register_profile_for_star'); ?>  </label>
                            <div class="col-sm-8">
                                <select class="form-control js-example-basic-single" name="star" id="star">
                                    <option value=""> -- Stars -- </option>
                                    <option value="ashwini">Ashwini</option>
                                    <option value="bharani">Bharani</option>
                                    <option value="karthigai">Karthigai</option>
                                    <option value="rohini">Rohini</option>
                                    <option value="mirigasirisham">Mirigasirisham</option
                                    ><option value="thiruvathirai">Thiruvathirai</option>
                                    <option value=punarpoosam">Punarpoosam</option>
                                    <option value="poosam">Poosam</option>
                                    <option value="ayilyam">Ayilyam</option>
                                    <option value="makam">Makam</option>
                                    <option value="pooram">Pooram</option>
                                    <option value="uthiram">Uthiram</option>
                                    <option value="hastham">Hastham</option>
                                    <option value="chithirai">Chithirai</option>
                                    <option value="swathi">Swathi</option>
                                    <option value="visakam">Visakam</option>
                                    <option value="anusham">Anusham</option>
                                    <option value="kettai">Kettai</option>
                                    <option value="moolam">Moolam</option>
                                    <option value="pooradam">Pooradam</option>
                                    <option value="uthradam">Uthradam</option>
                                    <option value="thiruvonam">Thiruvonam</option>
                                    <option value="avittam">Avittam</option>
                                    <option value="sadhayam">Sadhayam</option>
                                    <option value="puratathi">Puratathi</option>
                                    <option value="uthirattathi">Uthirattathi</option>
                                    <option value="revathi">Revathi</option>										
                                </select>   
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="raasi" class="col-sm-4 form-control-label"><?php echo $this->lang->line('register_profile_for_raasi'); ?> </label>
                            <div class="col-sm-8">
                                <select class="form-control js-example-basic-single"  name="raasi" id="raasi">
                                    <option value="">-- select one -- </option>
                                    <option value="mesham" label="Mesham">Mesham</option>
                                    <option value="rishabam" label="Rishabam">Rishabam</option>
                                    <option value="mithunam" label="Mithunam">Mithunam</option>
                                    <option value="katagam" label="Katagam">Katagam</option>
                                    <option value="simham" label="Simham">Simham</option>
                                    <option value="kanni" label="Kanni">Kanni</option>
                                    <option value="thulam" label="Thulam">Thulam</option>
                                    <option value="vrichigam" label="Vrichigam">Vrichigam</option>
                                    <option value="dhanush" label="Dhanush">Dhanush</option>
                                    <option value="magaram" label="Magaram">Magaram</option>
                                    <option value="kumbham" label="Kumbham">Kumbham</option>
                                    <option value="meenam" label="Meenam">Meenam</option>	
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-default" type="button" ><?php echo $this->lang->line('cancel_admin_register'); ?></button>
                        <button class="btn btn-primary" type="submit" ><?php echo $this->lang->line('save_text'); ?></button>
                    </div>
                </form><!--form-->
            </div>
        </div>
        <div class="drop-shadow ">			    
            <h3 class="pro-edit"><?php echo $this->lang->line('register_profile_for_family_title1'); ?></h3>
            <div id="div13" style="display:block; ">
                <div class="edit_btn">
                    <a id="" href="javascript:SwapDivsWithClick('div13','div14')" style="float: right;" class="btn btn-primary ">
                        <i class="fa fa-pencil"></i><?php echo $this->lang->line('edit_text'); ?></a>
                </div>
                <table style="width:50%">
                    <tr>
                        <td><?php echo $this->lang->line('register_profile_for_familystatus'); ?> </td>
                        <td>:</td>		
                        <td><?php echo (!empty($fstatus) ? constant("FAMILY_STATUS_" . $fstatus) : ''); ?></td>
                    </tr>
                    <tr>
                        <td><?php echo $this->lang->line('register_profile_for_familytype'); ?>   </td>
                        <td>:</td>		
                        <td><?php echo (!empty($ftype) ? constant("FAMILY_TYPE_" . $ftype) : ''); ?> </td>
                    </tr>
                    <tr>
                        <td><?php echo $this->lang->line('register_profile_for_familyvalue'); ?> </td>
                        <td>:</td>		
                        <td><?php echo (!empty($fvalues) ? constant("FAMILY_VALUES_" . $fvalues) : ''); ?> </td>
                    </tr>
                </table>
            </div>
            <!--form-->
            <div id="div14" style="display:none; ">
                <a id="" href="javascript:SwapDivsWithClick('div13','div14')" style="float: right;" class="btn btn-primary ">
                    <i class="fa fa-pencil"></i><?php echo $this->lang->line('close_text'); ?></a>
                <form class="form-horizontal" role="form" id="family_form">
                    <div class="modal-header">
                    </div>
                    <div class="modal-body">
                        <div class="form-group row">
                            <label for="fstatus" class="col-sm-4 form-control-label"><?php echo $this->lang->line('register_profile_for_familystatus'); ?> <span class="red">*</span></label>
                            <div class="col-sm-8">
                                <div class="col-md-3">
                                    <label class="radio-inline reglabrad">
                                        <input type="radio" name="fstatus" id="fstatus_1" value="1" required><?php echo $this->lang->line('register_profile_for_familystatus_1'); ?>
                                    </label>
                                </div>
                                <div class="col-md-3">
                                    <label class="radio-inline reglabrad">
                                        <input type="radio" name="fstatus" id="fstatus_2" value="2" required> <?php echo $this->lang->line('register_profile_for_familystatus_2'); ?>	
                                    </label>
                                </div>
                                <div class="col-md-3">
                                    <label class="radio-inline reglabrad">
                                        <input type="radio" name="fstatus" id="fstatus_3" value="3" required> <?php echo $this->lang->line('register_profile_for_familystatus_3'); ?>	
                                    </label>
                                </div>
                                <div class="col-md-3">
                                    <label class="radio-inline reglabrad">
                                        <input type="radio" name="fstatus" id="fstatus_4" value="4" required> <?php echo $this->lang->line('register_profile_for_familystatus_4'); ?>	 
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="ftype" class="col-sm-4 form-control-label"><?php echo $this->lang->line('register_profile_for_familytype'); ?><span class="red">*</span></label>
                            <div class="col-sm-8">
                                <div class="col-md-4">
                                    <label class="radio-inline reglabrad">
                                        <input type="radio" name="ftype" id="ftype_1" value="1" required> <?php echo $this->lang->line('register_profile_for_familytype_1'); ?> 
                                    </label>
                                </div>
                                <div class="col-md-4">
                                    <label class="radio-inline reglabrad">
                                        <input type="radio" name="ftype" id="ftype_2" value="2" required> <?php echo $this->lang->line('register_profile_for_familytype_2'); ?>	
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="fvalues" class="col-sm-4 form-control-label"><?php echo $this->lang->line('register_profile_for_familyvalue'); ?> <span class="red">*</span></label>
                            <div class="col-sm-8">
                                <div class="col-md-3">
                                    <label class="radio-inline reglabrad">
                                        <input type="radio" name="fvalues" id="fvalues_1" value="1" required> <?php echo $this->lang->line('register_profile_for_familyvalue_1'); ?> 
                                    </label>
                                </div>
                                <div class="col-md-3">
                                    <label class="radio-inline reglabrad">
                                        <input type="radio" name="fvalues" id="fvalues_2" value="2" required> <?php echo $this->lang->line('register_profile_for_familyvalue_2'); ?> 	
                                    </label>
                                </div>
                                <div class="col-md-3">
                                    <label class="radio-inline reglabrad">
                                        <input type="radio" name="fvalues" id="fvalues_2" value="3" required> <?php echo $this->lang->line('register_profile_for_familyvalue_3'); ?> 	
                                    </label>
                                </div>
                                <div class="col-md-3">
                                    <label class="radio-inline reglabrad">
                                        <input type="radio" name="fvalues" id="fvalues_4" value="4" required> <?php echo $this->lang->line('register_profile_for_familyvalue_4'); ?> 	
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-default" type="button" ><?php echo $this->lang->line('cancel_admin_register'); ?></button>
                        <button class="btn btn-primary" type="submit" ><?php echo $this->lang->line('save_text'); ?></button>
                    </div>
                </form><!--form-->
            </div>
        </div>
        <h3 class="registertitle pro-edit" id="pro_prefrence"> <?php echo $this->lang->line('register_profile_for_process4'); ?></h3>
        <div class="drop-shadow">

            <h3><?php echo $this->lang->line('basic_preference_text'); ?></h3>
            <div id="div15" style="display:block; ">
                <div class="edit_btn">
                    <a id="" href="javascript:SwapDivsWithClick('div15','div16')" style="float: right;" class="btn btn-primary ">
                        <i class="fa fa-pencil"></i><?php echo $this->lang->line('edit_text'); ?></a>
                </div>
                <table style="width:50%">
                    <tr>
                        <td><?php echo $this->lang->line('search_profile_for_age'); ?></td>
                        <td>:</td>		
                        <td><?php echo $ageFrom . ' - ' . $ageTo . ' Years' ?></td>
                    </tr>
                    <tr>
                        <td><?php echo $this->lang->line('register_profile_for_height'); ?></td>
                        <td>:</td>		
                        <td><?php echo $pre_height; ?></td>
                    </tr>
                    <tr>
                        <td><?php echo $this->lang->line('register_profile_for_marriagestatus'); ?></td>
                        <td>:</td>		
                        <td><?php echo (!empty($pre_maritalstatus) ? constant("MARITAL_STATUS_" . $pre_maritalstatus) : ''); ?></td>
                    </tr>
                    <tr>
                        <td><?php echo $this->lang->line('register_profile_for_physicalstatus'); ?></td>
                        <td>:</td>		
                        <td><?php echo (!empty($pre_physicalstatus) ? constant("PHYSICAL_STATUS_" . $pre_physicalstatus) : ''); ?></td>
                    </tr>
                    <tr>
                        <td><?php echo $this->lang->line('register_profile_for_country'); ?></td>
                        <td>:</td>		
                        <td><?php echo $pre_country_name; ?></td>
                    </tr>
                    <tr>
                        <td><?php echo $this->lang->line('register_profile_for_state'); ?></td>
                        <td>:</td>		
                        <td><?php echo $pre_state_name; ?></td>
                    </tr>
                    <tr>
                        <td><?php echo $this->lang->line('register_profile_for_city'); ?></td>
                        <td>:</td>		
                        <td><?php echo $pre_city_name; ?></td>
                    </tr>
                </table>
                <h3 class="pro-edit"><?php echo $this->lang->line('register_profile_for_food'), ' ', $this->lang->line('detail_text'); ?></h3>
                <table style="width:50%">
                    <tr>
                        <td><?php echo $this->lang->line('register_profile_for_food'), ' ', $this->lang->line('register_profile_for_habbit_title1'); ?></td>
                        <td>:</td>		
                        <td><?php echo (!empty($pre_food) ? constant("FOOD_TYPE_" . $pre_food) : ''); ?> </td>
                    </tr>
                    <tr>
                        <td><?php echo $this->lang->line('register_profile_for_smoking'), ' ', $this->lang->line('register_profile_for_habbit_title1'); ?></td>
                        <td>:</td>		
                        <td><?php echo (!empty($pre_smoke) ? constant("SMOKE_" . $pre_smoke) : ''); ?></td>
                    </tr>
                    <tr>
                        <td><?php echo $this->lang->line('register_profile_for_drinking'), ' ', $this->lang->line('register_profile_for_habbit_title1'); ?></td>
                        <td>:</td>		
                        <td><?php echo (!empty($pre_drink) ? constant("DRINK_" . $pre_drink) : ''); ?></td>
                    </tr>
                </table>
                <h3 class="pro-edit"><?php echo $this->lang->line('register_profile_for_caste'), ' ', $this->lang->line('detail_text'); ?></h3>
                <table style="width:50%">
                    <tr>
                        <td><?php echo $this->lang->line('register_profile_for_caste'); ?> </td>
                        <td>:</td>		
                        <td><?php echo $pre_castename; ?></td>
                    </tr>
                    <tr>
                        <td><?php echo $this->lang->line('register_profile_for_star'); ?> </td>
                        <td>:</td>		
                        <td><?php echo $pre_star; ?></td>
                    </tr>
                    <tr>
                        <td><?php echo $this->lang->line('dosham_text'); ?> </td>
                        <td>:</td>		
                        <td><?php echo (!empty($pre_dhosam) ? constant("DHOSAM_TYPE_" . $pre_dhosam) : ''); ?></td>
                    </tr>
                </table>
                <h3 class="pro-edit"> <?php echo $this->lang->line('search_education_title'), ' ', $this->lang->line('detail_text'); ?></h3>
                <table style="width:50%">
                    <tr>
                        <td> <?php echo $this->lang->line('search_education_title'); ?> </td>
                        <td>:</td>		
                        <td><?php echo $pre_eduname; ?></td>
                    </tr>
                    <tr>
                        <td><?php echo $this->lang->line('register_profile_for_occupation'); ?> </td>
                        <td>:</td>		
                        <td><?php echo $pre_occupation; ?></td>
                    </tr>
                    <tr>
                        <td> <?php echo $this->lang->line('register_profile_for_employeed'); ?> </td>
                        <td>:</td>		
                        <td><?php echo (!empty($pre_employeed) ? constant("EMPLOYEED_IN_" . $pre_employeed) : ''); ?></td>
                    </tr>
                    <tr>
                        <td><?php echo $this->lang->line('register_profile_for_salary'); ?></td>
                        <td>:</td>		
                        <td><?php echo $pre_salaryView; ?></td>
                    </tr>
                </table>
            </div>
            <!--form-->
            <div id="div16" style="display:none; ">
                <a id="" href="javascript:SwapDivsWithClick('div15','div16')" style="float: right;" class="btn btn-primary ">
                    <i class="fa fa-pencil"></i><?php echo $this->lang->line('close_text'); ?></a>
                <form class="form-horizontal" role="form" id="prefrence_form">
                    <div class="modal-header">
                    </div>
                    <div class="modal-body">
                        <div class="form-group row">
                            <label  class="col-sm-4 form-control-label"> <?php echo $this->lang->line('register_profile_for_age'); ?> <span class="red">*</span></label>
                            <div class="col-sm-8">
                                <label class="col-sm-2 form-control-label"><?php echo $this->lang->line('from_text'); ?></label>
                                <div class="col-md-3">
                                    <select class="form-control js-example-basic-single" id="pre_age_from" name="age_from" required>
                                        <option value="">From</option>
                                        <option value="21">21</option>
                                        <option value="22">22</option>
                                        <option value="23">23</option>
                                        <option value="24">24</option>
                                        <option  value="25">25</option>
                                        <option value="26">26</option>
                                        <option value="27">27</option>
                                        <option value="28">28</option>
                                        <option value="29">29</option>
                                        <option value="30">30</option>
                                        <option value="31">31</option>
                                        <option value="32">32</option>
                                        <option value="33">33</option>
                                        <option value="34">34</option>
                                        <option value="35">35</option>
                                        <option value="36">36</option>
                                        <option value="37">37</option>
                                        <option value="38">38</option>
                                        <option value="39">39</option>
                                        <option value="40">40</option>
                                        <option value="41">41</option>
                                        <option value="42">42</option>
                                        <option value="43">43</option>
                                        <option value="44">44</option>
                                        <option value="45">45</option>
                                        <option value="46">46</option>
                                        <option value="47">47</option>
                                        <option value="48">48</option>
                                        <option value="49">49</option>
                                        <option value="50">50</option>
                                        <option value="51">51</option>
                                        <option value="52">52</option>
                                        <option value="53">53</option>
                                        <option value="54">54</option>
                                        <option value="55">55</option>
                                        <option value="56">56</option>
                                        <option value="57">57</option>
                                        <option value="58">58</option>
                                        <option value="59">59</option>
                                        <option value="60">60</option>
                                        <option value="61">61</option>
                                        <option value="62">62</option>
                                        <option value="63">63</option>
                                        <option value="64">64</option>
                                        <option value="65">65</option>
                                        <option value="66">66</option>
                                        <option value="67">67</option>
                                        <option value="68">68</option>
                                        <option value="69">69</option>
                                        <option value="70">70</option>
                                    </select>  
                                    <label for="pre_age_from" class="error"></label>
                                </div>
                                <label class="col-sm-2 form-control-label"><?php echo $this->lang->line('to_text'); ?></label>
                                <div class="col-md-3">
                                    <select class="form-control js-example-basic-single" id="pre_age_to" name="age_to" required>
                                        <option value="">To</option>
                                        <option value="25">25</option>
                                        <option value="26">26</option>
                                        <option value="27">27</option>
                                        <option value="28">28</option>
                                        <option value="29">29</option>
                                        <option value="30">30</option>
                                        <option value="31">31</option>
                                        <option value="32">32</option>
                                        <option value="33">33</option>
                                        <option value="34">34</option>
                                        <option value="35">35</option>
                                        <option value="36">36</option>
                                        <option value="37">37</option>
                                        <option value="38">38</option>
                                        <option value="39">39</option>
                                        <option value="40">40</option>
                                        <option value="41">41</option>
                                        <option value="42">42</option>
                                        <option value="43">43</option>
                                        <option value="44">44</option>
                                        <option value="45">45</option>
                                        <option value="46">46</option>
                                        <option value="47">47</option>
                                        <option value="48">48</option>
                                        <option value="49">49</option>
                                        <option value="50">50</option>
                                        <option value="51">51</option>
                                        <option value="52">52</option>
                                        <option value="53">53</option>
                                        <option value="54">54</option>
                                        <option value="55">55</option>
                                        <option value="56">56</option>
                                        <option value="57">57</option>
                                        <option value="58">58</option>
                                        <option value="59">59</option>
                                        <option value="60">60</option>
                                        <option value="61">61</option>
                                        <option value="62">62</option>
                                        <option value="63">63</option>
                                        <option value="64">64</option>
                                        <option value="65">65</option>
                                        <option value="66">66</option>
                                        <option value="67">67</option>
                                        <option value="68">68</option>
                                        <option value="69">69</option>
                                        <option value="70">70</option>
                                    </select>
                                    <label for="pre_age_to" class="error"></label>  
                                </div>

                            </div>
                        </div> 
                        <div class="form-group row">
                            <label class="col-sm-4 form-control-label"> <?php echo $this->lang->line('register_profile_for_marriagestatus'); ?> <span class="red">*</span></label>
                            <div class="col-md-8">
                                <div class="col-md-3">
                                    <label class="radio-inline reglabrad">
                                        <input type="radio" name="maritalstatus" id="pre_maritalstatus_1" value="1" required>  <?php echo $this->lang->line('register_profile_for_marriagestatus_1'); ?> 
                                    </label>
                                </div>
                                <div class="col-md-3">
                                    <label class="radio-inline reglabrad">
                                        <input type="radio" name="maritalstatus" id="pre_maritalstatus_2" value="2" required> <?php echo $this->lang->line('register_profile_for_marriagestatus_2'); ?> 	
                                    </label>
                                </div>
                                <div class="col-md-3">
                                    <label class="radio-inline reglabrad">
                                        <input type="radio" name="maritalstatus" id="pre_maritalstatus_3" value="3" required> <?php echo $this->lang->line('register_profile_for_marriagestatus_3'); ?>  
                                    </label>
                                </div>
                                <div class="col-md-3">
                                    <label class="radio-inline reglabrad">
                                        <input type="radio" name="maritalstatus" id="pre_maritalstatus_4" value="4" required>  <?php echo $this->lang->line('register_profile_for_marriagestatus_4'); ?> 
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="height" class="col-sm-4 form-control-label"><?php echo $this->lang->line('register_profile_for_height'); ?></label>  
                            <div class="col-sm-8">
                                <select class="form-control js-example-basic-single" name="height" id="pre_height">
                                    <option value="0"> -- Height -- </option>
                                    <option value="145" label="4' 9&quot; (145 cm)">4' 9" (145 cm)</option>
                                    <option value="147" label="4' 10&quot; (147 cm)">4' 10" (147 cm)</option>
                                    <option value="150" label="4' 11&quot; (150 cm)">4' 11" (150 cm)</option>
                                    <option value="152" label="5'  0&quot; (152 cm)">5'  0" (152 cm)</option>
                                    <option value="155" label="5' 1&quot; (155 cm)">5' 1" (155 cm)</option>
                                    <option value="157" label="5' 2&quot; (157 cm)">5' 2" (157 cm)</option>
                                    <option value="160" label="5' 3&quot; (160 cm)">5' 3" (160 cm)</option>
                                    <option value="162" label="5' 4&quot; (162 cm)">5' 4" (162 cm)</option>
                                    <option value="165" label="5' 5&quot; (165 cm)">5' 5" (165 cm)</option>
                                    <option  value="167" label="5' 6&quot; (167 cm)">5' 6" (167 cm)</option>
                                    <option value="170" label="5' 7&quot; (170 cm)">5' 7" (170 cm)</option>
                                    <option value="173" label="5' 8&quot; (173 cm)">5' 8" (173 cm)</option>
                                    <option value="175" label="5' 9&quot; (175 cm)">5' 9" (175 cm)</option>
                                    <option value="178" label="5' 10&quot; (178 cm)">5' 10" (178 cm)</option>
                                    <option value="180" label="5' 11&quot; (180 cm)">5' 11" (180 cm)</option>
                                    <option value="183" label="6' 0&quot; (183 cm)">6' 0" (183 cm)</option>
                                    <option value="185" label="6' 1&quot; (185 cm)">6' 1" (185 cm)</option>
                                    <option value="188" label="6' 2&quot; (188 cm)">6' 2" (188 cm)</option>
                                    <option value="190" label="6' 3&quot; (190 cm)">6' 3" (190 cm)</option>
                                    <option value="193" label="6' 4&quot; (193 cm)">6' 4" (193 cm)</option>
                                    <option value="21">Above the Height </option>
                                </select>         
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="physicalstatus" class="col-sm-4 form-control-label"><?php echo $this->lang->line('register_profile_for_physicalstatus'); ?><span class="red">*</span></label>
                            <div class="col-sm-8">
                                <div class="col-md-6">
                                    <label class="radio-inline reglabrad">
                                        <input type="radio" name="physicalstatus" id="pre_physical_1" required value="1"> <?php echo $this->lang->line('register_profile_for_physicalstatus_1'); ?> 
                                    </label>
                                </div>
                                <div class="col-md-6">
                                    <label class="radio-inline reglabrad">
                                        <input type="radio" name="physicalstatus"  id="pre_physical_2" required value="2"> <?php echo $this->lang->line('register_profile_for_physicalstatus_2'); ?> 
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row" >
                            <label for="country" class="col-sm-4 form-control-label"><?php echo $this->lang->line('register_profile_for_country'); ?> </label>  
                            <div class="col-sm-8">
                                <select class="form-control js-example-data-array-selected" id="pre_country" name="country" onchange="stateInfo1(this.value)">
                                    <option value=""> -- Select Country -- </option>
                                    <?php if ($pre_country == 0) { ?>
                                        <option value="0" selected="selected"> Any </option>
                                    <?php } else { ?>
                                        <option value="<?php echo $pre_country ?>" selected="selected"> <?php echo $pre_country_name; ?> </option>
                                    <?php } ?>	           	 </select>
                                <label class="error" for="country"></label>     
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="state" class="col-sm-4 form-control-label"><?php echo $this->lang->line('register_profile_for_state'); ?></label>  
                            <div class="col-sm-8"  id="text-state">
                                <select class="form-control js-example-data-array-selected" name="state" id="pre_state" onchange="cityInfo1(this.value)">
                                    <option value=""> -- Select State -- </option>
                                    <?php if ($pre_state == 0) { ?>
                                        <option value="0" selected="selected"> Any </option>
                                    <?php } else { ?>
                                        <option value="<?php echo $pre_state ?>" selected="selected"> <?php echo $pre_state_name; ?> </option>
                                    <?php } ?>	
                                </select>
                                <label class="error" for="state"></label>          
                            </div>
                        </div>

                        <div class="form-group row" >
                            <label for="city" class="col-sm-4 form-control-label"><?php echo $this->lang->line('register_profile_for_city'); ?></label>  
                            <div class="col-sm-8" id="text-city">
                                <select class="form-control js-example-data-array-selected"  id ="pre_city" name="city">
                                    <option value=""> -- Select City -- </option>
                                    <?php if ($pre_city == 0) { ?>
                                        <option value="0" selected="selected"> Any </option>
                                    <?php } else { ?>
                                        <option value="<?php echo $pre_city ?>" selected="selected"> <?php echo $pre_city_name; ?> </option>
                                    <?php } ?>	
                                </select>
                                <label class="error" for="city"></label>         
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="edu" class="col-sm-4 form-control-label"> <?php echo $this->lang->line('register_profile_for_highesteducation'); ?> <span class="red">*</span></label>  
                            <div class="col-sm-8">
                                <select class="form-control js-example-data-array" name="education" id ="pre_edu" required>
                                    <?php if ($pre_education == 0) { ?>
                                        <option value="0" selected="selected"> Any </option>
                                    <?php } else { ?>
                                        <option value="<?php echo $pre_education ?>" selected="selected"> <?php echo $pre_eduname; ?> </option>
                                    <?php } ?>
                                </select>
                                <label class="error" for="pre_edu"></label>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="occupation" class="col-sm-4 form-control-label"><?php echo $this->lang->line('qualification_text'); ?> </label>  
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="qualification" placeholder="Qualification" value="<?php echo $pre_qualification; ?>">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="occupation" class="col-sm-4 form-control-label"><?php echo $this->lang->line('register_profile_for_occupation'); ?> </label>  
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="occupation" placeholder="Occupation" value="<?php echo $pre_occupation; ?>">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="empin" class="col-sm-4 form-control-label"><?php echo $this->lang->line('register_profile_for_employeed'); ?> </label>
                            <div class="col-sm-8">
                                <div class="col-md-3">
                                    <label class="radio-inline reglabrad">
                                        <input type="radio" name="emp_in"  id="pre_emp_1" value="1"> <?php echo $this->lang->line('register_profile_for_employeed_1'); ?>
                                    </label>
                                </div>
                                <div class="col-md-3">
                                    <label class="radio-inline reglabrad">
                                        <input type="radio" name="emp_in" id="pre_emp_2" value="2">  <?php echo $this->lang->line('register_profile_for_employeed_2'); ?>	
                                    </label>
                                </div>
                                <div class="col-md-3">
                                    <label class="radio-inline reglabrad">
                                        <input type="radio" name="emp_in"  id="pre_emp_3"value="3">  <?php echo $this->lang->line('register_profile_for_employeed_3'); ?>	 	
                                    </label>
                                </div>
                                <div class="col-md-3">
                                    <label class="radio-inline reglabrad">
                                        <input type="radio" name="emp_in"  id="pre_emp_4" value="4"> <?php echo $this->lang->line('register_profile_for_employeed_4'); ?>	 	
                                    </label>
                                </div>
                            </div>
                        </div> 
                        <div class="form-group row">
                            <label for="salary" class="col-sm-4 form-control-label"><?php echo $this->lang->line('register_profile_for_salary'); ?> </label>
                            <div class="col-sm-8">
                                <select class="form-control js-example-basic-single" name="salary" id="pre_salary">
                                    <option value=""> -- select salary -- </option>
                                    <option value="1">100000 or less</option>
                                    <option value="2">200000 to 300000</option>
                                    <option value="3">300000 to 400000</option>
                                    <option value="4">400000 to 500000</option>
                                    <option value="5">500000 to 600000</option>
                                    <option value="6">600000 to 700000</option>
                                    <option value="7">700000 to 800000</option>
                                    <option value="8">800000 to 900000</option>
                                    <option value="9">900000 Above</option>
                                    <option value="10">any</option>
                                </select>   
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="food" class="col-sm-4 form-control-label"><?php echo $this->lang->line('register_profile_for_food'); ?> </label>
                            <div class="col-sm-8">
                                <div class="col-md-4">
                                    <label class="radio-inline reglabrad">
                                        <input type="radio" name="food" id="pre_food_1" value="1"> <?php echo $this->lang->line('register_profile_for_food_1'); ?>  
                                    </label>
                                </div>
                                <div class="col-md-4">
                                    <label class="radio-inline reglabrad">
                                        <input type="radio" name="food" id="pre_food_2" value="2"><?php echo $this->lang->line('register_profile_for_food_2'); ?> 
                                    </label>
                                </div>
                                <div class="col-md-4">
                                    <label class="radio-inline reglabrad">
                                        <input type="radio" name="food" id="pre_food_3" value="3"> <?php echo $this->lang->line('register_profile_for_food_3'); ?>  
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="smoking" class="col-sm-4 form-control-label"><?php echo $this->lang->line('register_profile_for_smoking'); ?>   </label>
                            <div class="col-sm-8">
                                <div class="col-md-4">
                                    <label class="radio-inline reglabrad">
                                        <input type="radio" name="smoking" id="pre_smoke_1" value="1"> <?php echo $this->lang->line('register_profile_for_yes'); ?> 
                                    </label>
                                </div>
                                <div class="col-md-4">
                                    <label class="radio-inline reglabrad">
                                        <input type="radio" name="smoking" id="pre_smoke_2" value="2"> <?php echo $this->lang->line('register_profile_for_no'); ?> 	
                                    </label>
                                </div>
                                <div class="col-md-4">
                                    <label class="radio-inline reglabrad">
                                        <input type="radio" name="smoking" id="pre_smoke_3" value="3"> <?php echo $this->lang->line('register_profile_for_smoking_3'); ?> 	 
                                    </label>
                                </div>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="drinking" class="col-sm-4 form-control-label"><?php echo $this->lang->line('register_profile_for_drinking'); ?> </label>
                            <div class="col-sm-8">
                                <div class="col-md-4">
                                    <label class="radio-inline reglabrad">
                                        <input type="radio" name="drinking" id="pre_drink_1" value="1"> <?php echo $this->lang->line('register_profile_for_yes'); ?>  
                                    </label>
                                </div>
                                <div class="col-md-4">
                                    <label class="radio-inline reglabrad">
                                        <input type="radio" name="drinking" id="pre_drink_2" value="2"> <?php echo $this->lang->line('register_profile_for_no'); ?> 	
                                    </label>
                                </div>
                                <div class="col-md-4">
                                    <label class="radio-inline reglabrad">
                                        <input type="radio" name="drinking" id="pre_drink_3" value="3">   <?php echo $this->lang->line('register_profile_for_drinking_3'); ?> 	  
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 form-control-label"><?php echo $this->lang->line('register_profile_for_caste'); ?></label>
                            <div class="col-md-8">
                                <select class="form-control js-example-data-array-selected" id="pre_caste" name="caste">
                                    <option value=""> -- Select Caste -- </option>
                                </select> 
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="star" class="col-sm-4 form-control-label"><?php echo $this->lang->line('register_profile_for_star'); ?></label>
                            <div class="col-sm-8">
                                <select class="form-control js-example-basic-single" name="star" id="pre_star">
                                    <option value=""> -- Stars -- </option>
                                    <option value="ashwini">Ashwini</option>
                                    <option value="bharani">Bharani</option>
                                    <option value="karthigai">Karthigai</option>
                                    <option value="rohini">Rohini</option>
                                    <option value="mirigasirisham">Mirigasirisham</option>
                                    <option value="thiruvathirai">Thiruvathirai</option>
                                    <option value=punarpoosam">Punarpoosam</option>
                                    <option value="poosam">Poosam</option>
                                    <option value="ayilyam">Ayilyam</option>
                                    <option value="makam">Makam</option>
                                    <option value="pooram">Pooram</option>
                                    <option value="uthiram">Uthiram</option>
                                    <option value="hastham">Hastham</option>
                                    <option value="chithirai">Chithirai</option>
                                    <option value="swathi">Swathi</option>
                                    <option value="visakam">Visakam</option>
                                    <option value="anusham">Anusham</option>
                                    <option value="kettai">Kettai</option>
                                    <option value="moolam">Moolam</option>
                                    <option value="pooradam">Pooradam</option>
                                    <option value="uthradam">Uthradam</option>
                                    <option value="thiruvonam">Thiruvonam</option>
                                    <option value="avittam">Avittam</option>
                                    <option value="sadhayam">Sadhayam</option>
                                    <option value="puratathi">Puratathi</option>
                                    <option value="uthirattathi">Uthirattathi</option>
                                    <option value="revathi">Revathi</option>										
                                </select>   
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="dhosam" class="col-sm-4 form-control-label"><?php echo $this->lang->line('register_profile_for_dhosam'); ?> </label>
                            <div class="col-sm-8">
                                <div class="col-md-4">
                                    <label class="radio-inline reglabrad">
                                        <input type="radio" name="dhosam" id="pre_dhosam_1" value="1"> <?php echo $this->lang->line('register_profile_for_yes'); ?> 
                                    </label>
                                </div>
                                <div class="col-md-4">
                                    <label class="radio-inline reglabrad">
                                        <input type="radio" name="dhosam" id="pre_dhosam_2"value="2">  <?php echo $this->lang->line('register_profile_for_no'); ?> 	
                                    </label>
                                </div>
                                <div class="col-md-4">
                                    <label class="radio-inline reglabrad">
                                        <input type="radio" name="dhosam" id="pre_dhosam_3" value="3"> <?php echo $this->lang->line('register_profile_for_dhosam_3'); ?>
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row" id="pre_dhosams" style="display:none;">
                            <div class="col-sm-offset-4 col-sm-8 ">
                                <input type="text" placeholder="Enter dhosam" name="dhosam_type" class="form-control" value="<?php echo $pre_dhosam_type; ?>">
                            </div>
                        </div>	
                    </div> 
                    <div class="modal-footer">
                        <button class="btn btn-default" type="button" ><?php echo $this->lang->line('cancel_admin_register'); ?></button>
                        <button class="btn btn-primary" type="submit" ><?php echo $this->lang->line('save_text'); ?></button>
                    </div>
                </form>
                <!--form-->
            </div>
        </div>
    </div>
    <script>
        var baseurl = $('#baseurl').val();

        $('document').ready(function () {
            $("#height").val(<?php echo (!empty($height) ? $height : 0); ?>).trigger("change");
            $("#weight").val(<?php echo (!empty($weight) ? $weight : 0); ?>).trigger("change");
            $('#bodytype_' +<?php echo (!empty($bodytype) ? $bodytype : 0); ?>).prop('checked', true);
            $('#complexion_' +<?php echo (!empty($complexion) ? $complexion : 0); ?>).prop('checked', true);
            $('#physical_' +<?php echo (!empty($phystatus) ? $phystatus : 0); ?>).prop('checked', true);

            $('#pesonal').validate({
                rules: {
                    mobile: {
                        required: true,
                        number: true
                    }
                },
                //perform an AJAX post to ajax.php
                submitHandler: function () {
                    $.post(baseurl + 'index.php/profile/updateprofileinfo?step1=1',
                            $('#pesonal').serialize(),
                            function (data) {
                                alert(data.msg);
                                location.reload();
                            }, "json")
                            .fail(function () {
                                alert("<?php echo $this->lang->line("connection_error"); ?>");
                                location.reload();
                            });
                }
            });

            $('#physical_form').validate({
                //perform an AJAX post to ajax.php
                submitHandler: function () {
                    $.post(baseurl + 'index.php/profile/updateprofileinfo?step2=1',
                            $('#physical_form').serialize(),
                            function (data) {
                                alert(data.msg);
                                location.reload();
                            }, "json")
                            .fail(function () {
                                alert("<?php echo $this->lang->line("connection_error"); ?>");
                                location.reload();
                            });
                }
            });

            $("#edu").val(<?php echo (!empty($education) ? $education : 0); ?>).trigger("change");
            $("#salary").val(<?php echo (!empty($salary) ? $salary : 0); ?>).trigger("change");
            $('#emp_' +<?php echo (!empty($employeed) ? $employeed : 0); ?>).prop('checked', true);

            $('#edu_form').validate({
                //perform an AJAX post to ajax.php
                submitHandler: function () {
                    $.post(baseurl + 'index.php/profile/updateprofileinfo?step3=1',
                            $('#edu_form').serialize(),
                            function (data) {
                                alert(data.msg);
                                location.reload();
                            }, "json")
                            .fail(function () {
                                alert("<?php echo $this->lang->line("connection_error"); ?>");
                                location.reload();
                            });
                }
            });

            $('#food_' +<?php echo (!empty($food) ? $food : 0) ?>).prop('checked', true);
            $('#drink_' +<?php echo (!empty($drink) ? $drink : 0); ?>).prop('checked', true);
            $('#smoke_' +<?php echo (!empty($smoke) ? $smoke : 0); ?>).prop('checked', true);

            $('#habbit_form').validate({
                //perform an AJAX post to ajax.php
                submitHandler: function () {
                    $.post(baseurl + 'index.php/profile/updateprofileinfo?step4=1',
                            $('#habbit_form').serialize(),
                            function (data) {
                                alert(data.msg);
                                location.reload();
                            }, "json")
                            .fail(function () {
                                alert("<?php echo $this->lang->line("connection_error"); ?>");
                                location.reload();
                            });
                }
            });
            $('#dhosam_' +<?php echo (!empty($dhosam) ? $dhosam : 0) ?>).prop('checked', true);
            $("#caste").val(<?php echo (!empty($casteid) ? $casteid : 0); ?>).trigger("change");
            $("#star").val('<?php echo (!empty($star) ? $star : 0); ?>').trigger("change");
            $("#raasi").val('<?php echo (!empty($raasi) ? $raasi : 0); ?>').trigger("change");
            $('#fstatus_' +<?php echo (!empty($fstatus) ? $fstatus : 0) ?>).prop('checked', true);
            $('#fvalues_' +<?php echo (!empty($fvalues) ? $fvalues : 0) ?>).prop('checked', true);
            $('#ftype_' +<?php echo (!empty($ftype) ? $ftype : 0) ?>).prop('checked', true);

            $('#religion_form').validate({
                //perform an AJAX post to ajax.php
                submitHandler: function () {
                    $.post(baseurl + 'index.php/profile/updateprofileinfo?step5=1',
                            $('#religion_form').serialize(),
                            function (data) {
                                alert(data.msg);
                                location.reload();
                            }, "json")
                            .fail(function () {
                                alert("<?php echo $this->lang->line("connection_error"); ?>");
                                location.reload();
                            });
                }
            });

            $('#family_form').validate({
                //perform an AJAX post to ajax.php
                submitHandler: function () {
                    $.post(baseurl + 'index.php/profile/updateprofileinfo?step6=1',
                            $('#family_form').serialize(),
                            function (data) {
                                alert(data.msg);
                                location.reload();
                            }, "json")
                            .fail(function () {
                                alert("<?php echo $this->lang->line("connection_error"); ?>");
                                location.reload();
                            });
                }
            });
        });
        $('input[name=dhosam]').click(function () {
            if ($('#dhosam_1').is(':checked')) {
                $("#dhosams").show('slow');
            } else {
                $("#dhosams").hide('slow');
            }
        })

        function stateInfo(reqData) {
            if (reqData == 0) {
                $("#city").html('');
                $("#state").html('');
                return false
            }
            $("#city").html('');
            $("#state").html('');
            $.ajax({
                type: "post",
                dataType: "json",
                url: baseurl + 'index.php/common/statelist/' + reqData,
                success: function (request) {
                    if (request.status == 0) {
                        $("#text-state").html('<input type="text" class="form-control" name="state_text" id="text-state" placeholder=" Enthe the state">');
                        $("#text-city").html('<input type="text" class="form-control" name="city_text" id="text-city" placeholder=" Enthe the city">');
                    } else {
                        $("#state").select2({
                            data: request
                        });
                    }
                },
                error: function (data) {
                    alert("<?php echo $this->lang->line("connection_error"); ?>");
                    location.reload();
                },
            });

            return false;
        }

        function cityInfo(reqData) {
            if (reqData == 0) {
                return false
            }
            $("#city").html('');
            $.ajax({
                type: "post",
                dataType: "json",
                url: baseurl + 'index.php/common/citylist/' + reqData,
                success: function (request) {
                    if (request.status == 0) {
                        $("#text-city").html('<input type="text" class="form-control" name="city_text" id="text-city" placeholder=" Enthe the city">');
                    } else {
                        $("#city").select2({
                            data: request
                        });
                    }
                },
                error: function (data) {
                    alert("<?php echo $this->lang->line("connection_error"); ?>");
                    location.reload();
                },
            });

            return false;
        }

        /** partner prefrence */
        $('document').ready(function () {
            $('#prefrence_form').validate({
                //perform an AJAX post to ajax.php
                submitHandler: function () {
                    $.post(baseurl + 'index.php/profile/updateprefrenceinfo?step1=1',
                            $('#prefrence_form').serialize(),
                            function (data) {
                                alert(data.msg);
                                location.reload();
                            }, "json");
                }
            });
            $("#pre_age_from").val(<?php echo (!empty($ageFrom) ? $ageFrom : 0); ?>).trigger("change");
            $("#pre_age_to").val(<?php echo (!empty($ageTo) ? $ageTo : 0); ?>).trigger("change");
            $("#pre_height").val(<?php echo (!empty($pre_height) ? $pre_height : 0); ?>).trigger("change");
            $('#pre_physical_' +<?php echo (!empty($pre_physicalstatus) ? $pre_physicalstatus : 0); ?>).prop('checked', true);
            $('#pre_maritalstatus_' +<?php echo (!empty($pre_maritalstatus) ? $pre_maritalstatus : 0); ?>).prop('checked', true);
            $("#pre_edu").val(<?php echo $pre_education; ?>).trigger("change");
            $("#pre_salary").val(<?php echo (!empty($pre_salary)?$pre_salary:0); ?>).trigger("change");
            $('#pre_emp_' +<?php echo (!empty($pre_employeed) ? $pre_employeed : 0); ?>).prop('checked', true);
            $('#pre_food_' +<?php echo (!empty($pre_food) ? $pre_food : 0) ?>).prop('checked', true);
            $('#pre_drink_' +<?php echo (!empty($pre_drink) ? $pre_drink : 0); ?>).prop('checked', true);
            $('#pre_smoke_' +<?php echo (!empty($pre_smoke) ? $pre_smoke : 0); ?>).prop('checked', true);
            $('#pre_dhosam_' +<?php echo (!empty($pre_dhosam) ? $pre_dhosam : 0) ?>).prop('checked', true);
            $("#pre_caste").val(<?php echo (!empty($pre_caste) ? $pre_caste : 0); ?>).trigger("change");
            $("#pre_star").val('<?php echo (!empty($pre_star) ? $pre_star : 0); ?>').trigger("change");
            //		$("#pre_raasi").val('<?php //echo $pre_raasi;    ?>').trigger("change");
            //		$('#pre_fstatus_'+<?php //echo (!empty($pre_fstatus)?$pre_fstatus:0)    ?>).prop('checked',true);
            //		$('#pre_fvalues_'+<?php //echo (!empty($pre_fvalues)?$pre_fvalues:0)    ?>).prop('checked',true);
            //		$('#pre_ftype_'+<?php //echo (!empty($pre_ftype)?$pre_ftype:0)    ?>).prop('checked',true);

        });
        $('input[name=dhosam]').click(function () {
            if ($('#dhosam_1').is(':checked')) {
                $("#dhosams").show('slow');
            } else {
                $("#dhosams").hide('slow');
            }
        })

        function stateInfo1(reqData) {
            if (reqData == 0) {
                $("#pre_city").html('');
                $("#pre_state").html('');
                $("#pre_city").select2({
                    data: [{"id": "0", "text": "Any"}]
                });
                $("#pre_state").select2({
                    data: [{"id": "0", "text": "Any"}]
                });

                return false
            }
            $("#pre_state").html('');
            $.ajax({
                type: "post",
                dataType: "json",
                url: baseurl + 'index.php/common/statelist/' + reqData,
                success: function (request) {
                    if (request.status == 0) {
                        $("#text-state").html('<input type="text" class="form-control" name="state_text" id="text-state" placeholder=" Enthe the state">');
                        $("#text-city").html('<input type="text" class="form-control" name="city_text" id="text-city" placeholder=" Enthe the city">');
                    } else {
                        $("#pre_state").select2({
                            data: request
                        });
                    }
                },
                error: function (data) {
                    alert("<?php echo $this->lang->line("connection_error"); ?>");
                    location.reload();
                },
            });

            return false;
        }

        function cityInfo1(reqData) {
            if (reqData == 0) {
                $("#pre_city").html('');
                $("#pre_city").select2({
                    data: [{"id": "0", "text": "Any"}]
                });
                return false
            }
            $("#pre_city").html('');
            $.ajax({
                type: "post",
                dataType: "json",
                url: baseurl + 'index.php/common/citylist/' + reqData,
                success: function (request) {
                    if (request.status == 0) {
                        $("#text-city").html('<input type="text" class="form-control" name="city_text" id="text-city" placeholder=" Enthe the city">');
                    } else {
                        $("#pre_city").select2({
                            data: request
                        });
                    }
                },
                error: function (data) {
                    alert("<?php echo $this->lang->line("connection_error"); ?>");
                    location.reload();
                },
            });

            return false;
        }

    </script>
