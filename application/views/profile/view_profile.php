
<?php if (!empty($image_request)) { ?>
    <style>
        .button {
            margin-right: -120px;
        }
    </style>
<?php } ?>
<script>
    function sendmail() {
        $("#modalviewprofile").show();
    }
    $(function () {
        $(".modal-closeviewprofile").on('click', function () {
            $('#modalviewprofile').hide();
        });

        $("#close").on('click', function () {
            $('#response').hide();
        });
        $(".modal-closepaid").on('click', function () {
            $('#modalpaid').hide();
        });


    });

</script>
<!--sendmailpaid -->
<div id="modalpaid" class="modal">
    <div class="modal-dialog animated sample" style=" margin-top: 9%;width: 440px;">
        <div class="row modal-contentpaid">
            <div class="sendhead">
                <span style="float:left;"></span>
                <span><h3 class="subtitle" style="color: #7e7e7e;"><?php echo sprintf($this->lang->line('to_text'), $this->lang->line('profile_send_mail')); ?></h3></span>
                <span class="modal-closepaid shtclose" ><a href="#"><img src="<?php echo base_url(); ?>assets/images/m/forgot-password-close.gif"></a></span>
            </div>

            <div id="paid_mail">
                <div id='loading' class="loader" style='display: block; text-align: center; margin: 100px;'>
                    <img src="<?php echo base_url(); ?>assets/images/loader.gif"/>
                </div>
            </div>
            <div id="send_mail" style="display:none;">
                <form id="mail">
                    <div class="form-group row">
                        <div class="col-sm-10 short-text">
                            <textarea class="form-control" id="message" name="message" rows="6" maxlength="250" minlength="50" required></textarea>
                        </div>
                        <input type="hidden" name="userguid" id="uid" value="">
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-12">
                            <div class="col-sm-2 pull-right">
                                <span class="btn btn-warning" id="mail_close"><?php echo $this->lang->line('cancel_admin_register'); ?></span>
                            </div>
                            <div class="col-sm-2 pull-right">
                                <button class="btn btn-primary" type="submit"><?php echo $this->lang->line('register_profile_for_submit'); ?></button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!--sendemailendpaid -->
<!--Interest msg-->
<div id="response" class="modal">
    <div class="modal-dialog animated sample" style=" margin-top: 9%;width: 440px;">
        <div class="modal-contentshort">
            <div class="row">
                <div>
                    <span style="float:left;"></span>
                    <span><h3 class="subtitle"><?php echo $this->lang->line('profile_request_send'); ?> </h3></span>
                    <span class="modal-closeshort shtclose" id="close"><a href="#"><img src="<?php echo base_url(); ?>assets/images/m/forgot-password-close.gif"></a></span>
                </div>
                <div class="form-group row">
                    <div class="col-sm-10 short-text" id="msg_body" style="text-align:center;">
                        <div id='loader' style='display:block'>
                            <img src="<?php echo base_url(); ?>assets/images/loading.gif"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--sInterest msg end-->
<div class="grid_3">
    <div class="container">
        <div class="breadcrumb1">
            <ul>
                <a href="<?php echo base_url(); ?>"><i class="fa fa-home home_1"></i></a>
                <span class="divider">&nbsp;|&nbsp;</span>
                <li class="current-page"><?php echo $this->lang->line('view_profile'); ?> </li>
            </ul>
        </div>
        <div class="profile">
            <div class="col-md-8 profile_left"> 
                <h2><?php echo (!empty($profile['username']) ? ucwords($profile['username']) : '') ?>
                    <?php if (empty($profile['block']) && !empty($profile['ignore'])) { ?>
                        <img title ="You have ignored this member" src="<?php echo base_url(); ?>assets/images/m/ignore.png" class="ignore_img"></span>
                    <?php } if (empty($profile['ignore']) && !empty($profile['block'])) { ?>
                        <img title ="You have blocked this member" src="<?php echo base_url(); ?>assets/images/m/ignore.png" class="ignore_img"></span>
                    <?php } if (!empty($profile['ignore']) && !empty($profile['block'])) { ?>
                        <img title ="You have blocked and igonre this member" src="<?php echo base_url(); ?>assets/images/m/ignore.png" class="ignore_img"></span>
                    <?php } ?>
                </h2>
            </div>
            <div class="col-md-8 profile_left">
                <?php if (!empty($profile)) { ?>
                    <h2>Profile Id : <?php echo (!empty($profile['userId']) ? constant('MEMBERID') . $profile['userId'] : '') ?></h2>
                    <div class="thumb_bottom short_bottom viewbottom" style="float:right;">
                        <div class="button">
                            <div class="vertical pointer"  onclick="sendmail('<?php echo $profile['userGuid']; ?>', '<?php echo $offer; ?>')"><?php echo $this->lang->line('profile_send_mail'); ?></div>
                            <?php
                            /** login user block or ignore this user then don't show intrest ,send mail and other option */
                            if (empty($profile['ignore']) || empty($profile['block'])) {
                                /** validate empty or not */
                                if (!empty($profile['interest'])) {
                                    /** validate activity is interest or arrange or not interest show the button name difference else show the sent interest */
                                    if (($profile['interest'] == REQUEST_INTEREST) || ($profile['interest'] == REQUEST_ARRANGED) || ($profile['interest'] == REQUEST_NOT_INETREST)) {
                                        /** if activity not interest then shoe the suitable matches else show the $profile['interest_name'] */
                                        if (($profile['interest'] == REQUEST_NOT_INETREST)) {
                                            ?>
                                            <a href="<?php echo base_url() . 'index.php/matches/mutual_matches'; ?>"><div class="vertical"><?php echo $profile['interest_name']; ?></div></a>
                                        <?php } else { ?>
                                            <div class="vertical"><?php echo $profile['interest_name']; ?></div> 
                                        <?php } ?>
                                    <?php } else { ?>
                                        <div class="vertical pointer" onclick="requestProfile('<?php echo $profile['userGuid']; ?>', '<?php echo $profile['interest']; ?>')"><?php echo $profile['interest_name']; ?></div>
                                    <?php } ?>
                                <?php } else { ?>
                                    <div class="vertical pointer" onclick="requestProfile('<?php echo $profile['userGuid']; ?>', '<?php echo REQUEST_INTEREST; ?>')"><?php echo $this->lang->line('profile_send_interest'); ?></div>
                                <?php }if (!empty($profile['shortlist'])) { ?>
                                    <div class="vertical pointer"  onclick="requestcancel('<?php echo $profile['userGuid']; ?>', '<?php echo REQUEST_SHORTLIST; ?>')"><?php echo $this->lang->line('shortlished_text'); ?></div>
                                <?php } else { ?>
                                    <div class="vertical pointer"  onclick="requestProfile('<?php echo $profile['userGuid']; ?>', '<?php echo REQUEST_SHORTLIST; ?>')"><?php echo $this->lang->line('profile_shortlist'); ?></div>
                                    <?php
                                }
                                if (!empty($image_request)) {
                                    if (!empty($imagerequest)) {
                                        ?>
                                        <div class="vertical "><?php echo $this->lang->line('profile_request_send'); ?></div>
                                    <?php } else { ?>
                                        <div class="vertical pointer"  onclick="requestProfile('<?php echo $profile['userGuid']; ?>', '<?php echo REQUEST_IMAGE; ?>')"><?php echo $this->lang->line('profile_image_request'); ?></div>
                                        <?php
                                    }
                                }
                            }
                            ?>		
                        </div>
                    </div>
                    <div class="col_3">
                        <div class="col-sm-4 row_2">
                            <div class="flexslider">
                                <ul class="slides" >
                                    <?php
                                    if (!empty($profile_images)) {
                                        $image = '';
                                        foreach ($profile_images as $key => $val) {
                                            if (!empty($val)) {
                                                ?>
                                                <li data-thumb="<?php echo base_url() . '/assets/upload_images/' . $val; ?>">
                                                    <img src="<?php echo base_url() . '/assets/upload_images/' . $val; ?>" />
                                                </li>
                                                <?php
                                            }
                                        }
                                    } else {
                                        ?>
                                        <li data-thumb="<?php echo base_url() . '/assets/upload_images/default.png'; ?>">
                                            <img src="<?php echo base_url() . '/assets/upload_images/default.png'; ?>" />
                                        </li>
                                    <?php } ?>
                                    </	ul>
                            </div>
                        </div>
                        <div class="col-sm-8 row_1">
                            <table class="table_working_hours">
                                <tbody>
                                    <tr class="opened_1">
                                        <td class="day_label"><?php echo $this->lang->line('search_profile_for_age') . ' / ' . $this->lang->line('register_profile_for_height'); ?>:</td>
                                        <td class="day_value"><?php echo (!empty($profile['age']) ? $profile['age'] : 'Not Specified') . ' / ' . (!empty($profile['height']) ? $profile['height'] . ' cm' : 'Not Specified'); ?></td>
                                    </tr>
                                    <tr class="opened">
                                        <td class="day_label"><?php echo sprintf($this->lang->line('last_text'), $this->lang->line('text_login')); ?> :</td>
                                        <td class="day_value"><?php echo (!empty($profile['log_in']) ? $profile['log_in'] : 'Not Login') ?></td>
                                    </tr>
                                    <tr class="opened">
                                        <td class="day_label"><?php echo $this->lang->line('register_profile_for_religion'); ?> :</td>
                                        <td class="day_value"><?php echo (!empty($profile['religion']) ? $profile['religion'] : 'Not Specified') ?></td>
                                    </tr>
                                    <tr class="opened">
                                        <td class="day_label1"><?php echo $this->lang->line('register_profile_for_gender'); ?> :</td>
                                        <td class="day_value"><?php echo (!empty($profile['gender']) ? constant("GENDER_" . strtoupper($profile['gender'])) : 'Not Specified'); ?></td>
                                    </tr>
                                    <tr class="opened">
                                        <td class="day_label"><?php echo $this->lang->line('register_profile_for_marriagestatus'); ?> :</td>
                                        <td class="day_value"><?php echo (!empty($profile['martial_status']) ? constant("MARITAL_STATUS_" . $profile['martial_status']) : 'Not Specified'); ?></td>
                                    </tr>
                                    <tr class="opened">
                                        <td class="day_label"><?php echo $this->lang->line('location_text'); ?>  :</td>
                                        <td class="day_value"><?php echo (!empty($profile['country_name']) ? $profile['country_name'] : 'Not Specified'); ?></td>
                                    </tr>
                                    <tr class="closed">
                                        <td class="day_label"><?php echo sprintf($this->lang->line('search_profile_create'), $this->lang->line('by_text')); ?>  :</td>
                                        <td class="day_value closed"><span><?php echo (!empty($profile['profile_created']) ? $profile['profile_created'] : 'Not Specified'); ?></span></td>
                                    </tr>
                                    <tr class="closed">
                                        <td class="day_label"><?php echo $this->lang->line('search_education_title'); ?> :</td>
                                        <td class="day_value closed"><span><?php echo (!empty($profile['edu_name']) ? $profile['edu_name'] : 'Not Specified') . ' - ' . (!empty($profile['qualification']) ? ucfirst($profile['qualification']) : 'Not Specified'); ?></span></td>
                                    </tr>
                                </tbody>
                            </table>



                        </div>

                        <div class="clearfix"> </div>
                    </div>
                    <div class="col_4">
                        <div class="bs-example bs-example-tabs" role="tabpanel" data-example-id="togglable-tabs">
                            <ul id="myTab" class="nav nav-tabs nav-tabs1" role="tablist">
                                <li role="presentation" class="active"><a href="#home" id="home-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="true"><?php echo $this->lang->line('about_myself'); ?></a></li>
                                <li role="presentation"><a href="#profile" role="tab" id="profile-tab" data-toggle="tab" aria-controls="profile"><?php echo $this->lang->line('register_profile_for_family_title1'); ?></a></li>
                                <li role="presentation"><a href="#profile1" role="tab" id="profile-tab1" data-toggle="tab" aria-controls="profile1"><?php echo $this->lang->line('register_profile_for_process4'); ?></a></li>
                            </ul>
                            <div id="myTabContent" class="tab-content">
                                <div role="tabpanel" class="tab-pane fade in active" id="home" aria-labelledby="home-tab">
                                    <div class="tab_box">
                                        <h1><?php echo $this->lang->line('about_myself'); ?></h1>
                                        <p><?php echo (!empty($profile['summary']) ? $profile['summary'] : 'Not Specified'); ?></p>
                                    </div>
                                    <div class="basic_1">
                                        <h3><?php echo $this->lang->line('basic'); ?></h3>
                                        <div class="col-md-6 basic_1-left">
                                            <table class="table_working_hours">
                                                <tbody>
                                                    <tr class="opened_1">
                                                        <td class="day_label"><?php echo $this->lang->line('register_profile_for_name'); ?> :</td>
                                                        <td class="day_value"><?php echo (!empty($profile['username']) ? $profile['username'] : 'Not Specified'); ?></td>
                                                    </tr>
                                                    <tr class="opened">
                                                        <td class="day_label"><?php echo $this->lang->line('register_profile_for_marriagestatus'); ?> :</td>
                                                        <td class="day_value"><?php echo (!empty($profile['martial_status']) ? constant("MARITAL_STATUS_" . $profile['martial_status']) : 'Not Specified'); ?></td>
                                                    </tr>
                                                    <tr class="opened">
                                                        <td class="day_label"><?php echo $this->lang->line('register_profile_for_gender'); ?> :</td>
                                                        <td class="day_value"><?php echo (!empty($profile['gender']) ? constant("GENDER_" . strtoupper($profile['gender'])) : 'Not Specified'); ?></td>
                                                    </tr>
                                                    <tr class="opened">
                                                        <td class="day_label"><?php echo $this->lang->line('register_profile_for_bodytype'); ?>:</td>
                                                        <td class="day_value"><?php echo (!empty($profile['bodytype']) ? constant("BODY_TYPE_" . $profile['bodytype']) : 'Not Specified'); ?></td>
                                                    </tr>
                                                    <tr class="opened">
                                                        <td class="day_label"><?php echo $this->lang->line('register_profile_for_height'); ?> :</td>
                                                        <td class="day_value"><?php echo (!empty($profile['height']) ? $profile['height'] . ' cm' : 'Not Specified'); ?></td>
                                                    </tr>
                                                    <tr class="opened">
                                                        <td class="day_label"><?php echo $this->lang->line('register_profile_for_physicalstatus'); ?> :</td>
                                                        <td class="day_value closed"><span><?php echo (!empty($profile['physical_status']) ? constant("PHYSICAL_STATUS_" . $profile['physical_status']) : 'Not Specified'); ?></span></td>
                                                    </tr>
                                                    <tr class="opened">
                                                        <td class="day_label"><?php echo sprintf($this->lang->line('search_profile_create'), $this->lang->line('by_text')); ?> :</td>
                                                        <td class="day_value closed"><span><?php echo (!empty($profile['profile_created']) ? $profile['profile_created'] : 'Not Specified'); ?></span></td>
                                                    </tr>
                                                    <tr class="opened">
                                                        <td class="day_label"><?php echo $this->lang->line('register_profile_for_drinking'); ?>  :</td>
                                                        <td class="day_value closed"><span><?php echo (!empty($profile['drink']) ? constant("DRINK_" . $profile['drink']) : 'Not Specified'); ?></span></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="col-md-6 basic_1-left">
                                            <table class="table_working_hours">
                                                <tbody>
                                                    <tr class="opened_1">
                                                        <td class="day_label"><?php echo $this->lang->line('search_profile_for_age'); ?> :</td>
                                                        <td class="day_value"><?php echo (!empty($profile['age']) ? $profile['age'] . ' Yrs' : 'Not Specified'); ?></td>
                                                    </tr>
                                                    <tr class="opened">
                                                        <td class="day_label"><?php echo $this->lang->line('register_profile_for_tongue'); ?> :</td>
                                                        <td class="day_value"><?php echo (!empty($profile['mother_tongue']) ? $profile['mother_tongue'] : 'Not Specified'); ?></td>
                                                    </tr>
                                                    <tr class="opened">
                                                        <td class="day_label"><?php echo $this->lang->line('register_profile_for_complexion'); ?>  :</td>
                                                        <td class="day_value"><?php echo (!empty($profile['complexion']) ? constant("COMPLEXION_TYPE_" . $profile['complexion']) : 'Not Specified'); ?></td>
                                                    </tr>
                                                    <tr class="opened">
                                                        <td class="day_label"><?php echo $this->lang->line('register_profile_for_weight'); ?> :</td>
                                                        <td class="day_value"><?php echo (!empty($profile['weight']) ? $profile['weight'] . ' kg' : 'Not Specified'); ?> </td>
                                                    </tr>
                                                    <tr class="closed">
                                                        <td class="day_label"><?php echo $this->lang->line('search_eatting_habit'); ?> :</td>
                                                        <td class="day_value closed"><span><?php echo (!empty($profile['food']) ? constant("FOOD_TYPE_" . $profile['food']) : 'Not Specified'); ?></span></td>
                                                    </tr>
                                                    <tr class="closed">
                                                        <td class="day_label"><?php echo $this->lang->line('register_profile_for_smoking'); ?> :</td>
                                                        <td class="day_value closed"><span><?php echo (!empty($profile['smoke']) ? constant("SMOKE_" . $profile['smoke']) : 'Not Specified'); ?></span></td>
                                                    </tr>

                                                    <tr class="closed">
                                                        <td class="day_label"><?php echo $this->lang->line('register_profile_for_country'); ?> :</td>
                                                        <td class="day_value closed"><span><?php echo (!empty($profile['country_name']) ? $profile['country_name'] : 'Not Specified'); ?></span></td>
                                                    </tr>
                                                    <tr class="opened">
                                                        <td class="day_label"><?php echo $this->lang->line('register_profile_for_state'); ?> :</td>
                                                        <td class="day_value closed"><span><?php echo (!empty($profile['state_name']) ? $profile['state_name'] : 'Not Specified'); ?></span></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="clearfix"> </div>
                                    </div>
                                    <div class="basic_1">
                                        <h3><?php echo $this->lang->line('reg_social_title'); ?></h3>
                                        <div class="col-md-6 basic_1-left">
                                            <table class="table_working_hours">
                                                <tbody>
                                                    <tr class="opened">
                                                        <td class="day_label"><?php echo $this->lang->line('register_profile_for_dob'); ?> :</td>
                                                        <td class="day_value"><?php echo (!empty($profile['dob']) ? date("d-m-Y", strtotime($profile['dob'])) : 'Not Specified'); ?></td>
                                                    </tr>
                                                    <tr class="opened">
                                                        <td class="day_label"><?php echo $this->lang->line('register_profile_for_caste'); ?> :</td>
                                                        <td class="day_value"><?php echo (!empty($profile['caste_name']) ? $profile['caste_name'] : 'Not Specified'); ?></td>
                                                    </tr>
                                                    <tr class="opened_1">
                                                        <td class="day_label"><?php echo $this->lang->line('register_profile_for_religion'); ?> :</td>
                                                        <td class="day_value"><?php echo (!empty($profile['religion']) ? $profile['religion'] : 'Not Specified'); ?></td>
                                                    </tr>
                                                <!-- <tr class="opened">
                                                            <td class="day_label">Date of Birth :</td>
                                                            <td class="day_value closed"><span>01-05-1988</span></td>
                                                    </tr>
                                                <tr class="opened">
                                                            <td class="day_label">Place of Birth :</td>
                                                            <td class="day_value closed"><span>Not Specified</span></td>
                                                    </tr> -->
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="col-md-6 basic_1-left">
                                            <table class="table_working_hours">
                                                <tbody>
                                                    <tr class="opened">
                                                        <td class="day_label"><?php echo $this->lang->line('register_profile_for_subcaste'); ?> :</td>
                                                        <td class="day_value"><?php echo (!empty($profile['sub_caste']) ? $profile['sub_caste'] : 'Not Specified'); ?></td>
                                                    </tr>
                                                    <tr class="opened">
                                                        <td class="day_label"><?php echo $this->lang->line('register_profile_for_raasi'); ?> :</td>
                                                        <td class="day_value"><?php echo (!empty($profile['raasi']) ? $profile['raasi'] : 'Not Specified'); ?></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="clearfix"> </div>
                                    </div>
                                    <div class="basic_1 basic_2">
                                        <h3><?php echo $this->lang->line('edu_career_title'); ?></h3>
                                        <div class="basic_1-left">
                                            <table class="table_working_hours">
                                                <tbody>
                                                    <tr class="opened">
                                                        <td class="day_label"><?php echo $this->lang->line('search_education_title'); ?>   :</td>
                                                        <td class="day_value"><span><?php echo (!empty($profile['edu_name']) ? $profile['edu_name'] : 'Not Specified') . ' - ' . (!empty($profile['qualification']) ? ucfirst($profile['qualification']) : 'Not Specified'); ?></td>
                                                    </tr>
                                                    <tr class="opened">
                                                        <td class="day_label"><?php echo $this->lang->line('register_profile_for_occupation'); ?> :</td>
                                                        <td class="day_value closed"><span><?php echo (!empty($profile['occupation']) ? $profile['occupation'] : 'Not Specified'); ?></span></td>
                                                    </tr>
                                                    <tr class="opened">
                                                        <td class="day_label"><?php echo $this->lang->line('register_profile_for_salary'); ?> :</td>
                                                        <td class="day_value closed"><span><?php echo (!empty($profile['salary_view']) ? $profile['salary_view'] : 'Not Specified'); ?></span></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="clearfix"> </div>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="profile" aria-labelledby="profile-tab">
                                    <div class="basic_3">
                                        <div class="basic_1 basic_2">
                                            <h3><?php echo $this->lang->line('register_profile_for_family_title1'); ?></h3>
                                            <div class="col-md-6 basic_1-left">
                                                <table class="table_working_hours">
                                                    <tbody>
                                                        <tr class="opened">
                                                            <td class="day_label"><?php echo $this->lang->line('register_profile_for_familytype'); ?> :</td>
                                                            <td class="day_value"><?php echo (!empty($profile['family_type']) ? constant("FAMILY_TYPE_" . $profile['family_type']) : 'Not Specified') ?></td>
                                                        </tr>
                                                        <tr class="opened">
                                                            <td class="day_label"><?php echo $this->lang->line('register_profile_for_familystatus'); ?> :</td>
                                                            <td class="day_value"><?php echo (!empty($profile['family_status']) ? constant("FAMILY_STATUS_" . $profile['family_status']) : 'Not Specified') ?></td>
                                                        </tr>
                                                        <tr class="opened">
                                                            <td class="day_label"><?php echo $this->lang->line('register_profile_for_familyvalue'); ?> :</td>
                                                            <td class="day_value closed"><span><?php echo (!empty($profile['family_values']) ? constant("FAMILY_VALUES_" . $profile['family_values']) : 'Not Specified') ?></span></td>
                                                        </tr>
                                                <!-- <tr class="opened">
                                                                <td class="day_label">Father's Occupation :</td>
                                                                <td class="day_value">Not Specified</td>
                                                        </tr>
                                                <tr class="opened">
                                                                <td class="day_label">Mother's Occupation :</td>
                                                                <td class="day_value">Not Specified</td>
                                                        </tr>
                                                    <tr class="opened">
                                                                <td class="day_label">No. of Brothers :</td>
                                                                <td class="day_value closed"><span>Not Specified</span></td>
                                                        </tr>
                                                    <tr class="opened">
                                                                <td class="day_label">No. of Sisters :</td>
                                                                <td class="day_value closed"><span>Not Specified</span></td>
                                                        </tr> -->
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="profile1" aria-labelledby="profile-tab1">
                                    <div class="basic_1 basic_2">
                                        <h3><?php echo $this->lang->line('register_profile_for_process4'); ?> </h3>
                                        <div class="basic_1-left">
                                            <table class="table_working_hours">
                                                <tbody>
                                                    <tr class="opened">
                                                        <td class="day_label"><?php echo $this->lang->line('search_profile_for_age'); ?>   :</td>
                                                        <td class="day_value"><?php echo (!empty($prefrenceData['age_from']) ? $prefrenceData['age_from'] : 'Not Specified') . '-' . (!empty($prefrenceData['age_to']) ? $prefrenceData['age_to'] : 'Not Specified'); ?></td>
                                                    </tr>
                                                    <tr class="opened">
                                                        <td class="day_label"><?php echo $this->lang->line('register_profile_for_marriagestatus'); ?>  :</td>
                                                        <td class="day_value"><?php echo (!empty($prefrenceData['maritalstatus']) ? constant("MARITAL_STATUS_" . $prefrenceData['maritalstatus']) : 'Not Specified'); ?></td>
                                                    </tr>
                                                    <tr class="opened">
                                                        <td class="day_label"><?php echo $this->lang->line('register_profile_for_physicalstatus'); ?>  :</td>
                                                        <td class="day_value"><?php echo (!empty($prefrenceData['physicalstatus']) ? constant("PHYSICAL_STATUS_" . $prefrenceData['physicalstatus']) : 'Not Specified'); ?></td>
                                                    </tr>
                                              <!--  <tr class="opened">
                                                            <td class="day_label">Body Type :</td>
                                                            <td class="day_value closed"><span><?php //echo (!empty($prefrenceData['bodytype'])?constant("BODY_TYPE_".$prefrenceData['bodytype']):'Not Specified');    ?></span></td>
                                                    </tr>
                                                <tr class="opened">
                                                            <td class="day_label">Complexion :</td>
                                                            <td class="day_value closed"><span><?php //echo (!empty($prefrenceData['complexion'])?constant("COMPLEXION_TYPE_".$prefrenceData['complexion']):'Not Specified');    ?></span></td>
                                                    </tr>-->
                                                    <tr class="opened">
                                                        <td class="day_label"><?php echo $this->lang->line('register_profile_for_height'); ?>:</td>
                                                        <td class="day_value closed"><span><?php echo (!empty($prefrenceData['height']) ? $prefrenceData['height'] . ' cm' : 'Not Specified'); ?> </span></td>
                                                    </tr>
                                                    <tr class="opened">
                                                        <td class="day_label"><?php echo $this->lang->line('search_eatting_habit'); ?> :</td>
                                                        <td class="day_value closed"><span><?php echo (!empty($prefrenceData['food']) ? constant("FOOD_TYPE_" . $prefrenceData['food']) : 'Not Specified'); ?></span></td>
                                                    </tr>
                                                    <tr class="opened">
                                                        <td class="day_label"><?php echo $this->lang->line('register_profile_for_smoking'); ?> :</td>
                                                        <td class="day_value closed"><span><?php echo (!empty($prefrenceData['smoking']) ? constant("SMOKE_" . $prefrenceData['smoking']) : 'Not Specified'); ?></span></td>
                                                    </tr>
                                                    <tr class="opened">
                                                        <td class="day_label"><?php echo $this->lang->line('register_profile_for_drinking'); ?> :</td>
                                                        <td class="day_value closed"><span><?php echo (!empty($prefrenceData['drinking']) ? constant("DRINK_" . $prefrenceData['drinking']) : 'Not Specified'); ?></span></td>
                                                    </tr>
                                                    <tr class="opened">
                                                        <td class="day_label"><?php echo $this->lang->line('dhosam'); ?> :</td>
                                                        <td class="day_value closed"><span><?php echo (!empty($prefrenceData['dhosam']) ? constant("DHOSAM_TYPE_" . $prefrenceData['dhosam']) : 'Not Specified'); ?></span></td>
                                                    </tr>
                                                    <tr class="opened">
                                                        <td class="day_label"><?php echo $this->lang->line('register_profile_for_religion'); ?> :</td>
                                                        <td class="day_value closed"><span><?php echo (!empty($prefrenceData['religion']) ? $prefrenceData['religion'] : 'Not Specified'); ?></span></td>
                                                    </tr>
                                                    <tr class="opened">
                                                        <td class="day_label"><?php echo $this->lang->line('register_profile_for_caste'); ?> :</td>
                                                        <td class="day_value closed"><span><?php echo (!empty($prefrenceData['caste_name']) ? $prefrenceData['caste_name'] : 'Not Specified'); ?></span></td>
                                                    </tr>
                                                    <tr class="opened">
                                                        <td class="day_label"><?php echo $this->lang->line('register_profile_for_tongue'); ?> :</td>
                                                        <td class="day_value closed"><span><?php echo (!empty($prefrenceData['mother_tongue']) ? $prefrenceData['mother_tongue'] : 'Not Specified'); ?></span></td>
                                                    </tr>
                                                    <tr class="opened">
                                                        <td class="day_label"><?php echo $this->lang->line('search_education_title'); ?> :</td>
                                                        <td class="day_value closed"><span><?php echo (!empty($prefrenceData['edu_name']) ? $prefrenceData['edu_name'] : 'Not Specified') . ' - ' . (!empty($prefrenceData['qualification']) ? ucfirst($prefrenceData['qualification']) : 'Not Specified'); ?></span></td>
                                                    </tr>
                                                    <tr class="opened">
                                                        <td class="day_label"><?php echo $this->lang->line('register_profile_for_occupation'); ?> :</td>
                                                        <td class="day_value closed"><span><?php echo (!empty($prefrenceData['occupation']) ? $prefrenceData['occupation'] : 'Not Specified'); ?></span></td>
                                                    </tr>
                                                    <tr class="opened">
                                                        <td class="day_label"><?php echo $this->lang->line('salary'); ?> :</td>
                                                        <td class="day_value closed"><span><?php echo (!empty($prefrenceData['salary_view']) ? $prefrenceData['salary_view'] : 'Not Specified'); ?></span></td>
                                                    </tr>
                                                    <tr class="opened">
                                                        <td class="day_label"><?php echo $this->lang->line('register_profile_for_country'); ?>:</td>
                                                        <td class="day_value closed"><span><?php echo (!empty($prefrenceData['country_name']) ? $prefrenceData['country_name'] : 'Not Specified'); ?></span></td>
                                                    </tr>
                                                    <tr class="opened">
                                                        <td class="day_label"><?php echo $this->lang->line('register_profile_for_state'); ?> :</td>
                                                        <td class="day_value closed"><span><?php echo (!empty($prefrenceData['state_name']) ? $prefrenceData['state_name'] : 'Not Specified'); ?></span></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                } else {
                    echo '<h4 display="text-align:center;">' . $this->lang->line('no_inform_text') . ' </h4>';
                }
                ?>
            </div>
            <div class="col-md-4 profile_right">
                <!--<div class="newsletter">
                           <form>
                                  <input type="text" name="ne" size="30" required="" placeholder="Enter Profile ID :">
                                  <input type="submit" value="Go">
                           </form>
                </div>-->
                <div class="view_profile">
                    <h3><?php echo $this->lang->line('view_similar_profile'); ?></h3>
                    <?php
                    if (!empty($currentmatches)) {
                        foreach ($currentmatches as $key1 => $value) { //echo '<pre>';print_r($value['userGuid']);exit;
                            ?>

                            <ul class="profile_item">
                                <a href="<?php echo base_url() . 'index.php/profile/view?pid=' . $value['userGuid'] ?>">
                                    <li class="profile_item-img">
                                        <img src="<?php echo base_url() . 'assets/upload_images/' . $value['image']; ?>" class="img-responsive" alt=""/>
                                    </li>
                                    <li class="profile_item-desc">
                                        <h4><?php echo constant('MEMBERID') . $value['userId']; ?></h4>
                                        <p> <?php echo (!empty($value['age']) ? $value['age'] : '') . ' Yrs, ' . (!empty($value['height']) ? $value['height'] : '') . ' ' . (!empty($value['caste_name']) ? $value['caste_name'] : ''); ?></p>
                                        <h5><?php echo $this->lang->line('text_view_profile'); ?></h5>
                                    </li>
                                    <div class="clearfix"> </div>
                                </a>
                            </ul>
                            <?php
                        }
                    }
                    ?>
                </div>
                <div class="view_profile view_profile1">
                    <h3><?php echo $this->lang->line('mamber_who_viewed_this_profile_also_viewed'); ?></h3>
                    <?php
                    if (!empty($viewed)) {
                        foreach ($viewed as $key1 => $value1) {
                            ?>

                            <ul class="profile_item">
                                <a href="<?php echo base_url() . 'index.php/profile/view?pid=' . $value1['userGuid'] ?>">
                                    <li class="profile_item-img">
                                        <img src="<?php echo base_url() . 'assets/upload_images/' . $value1['image']; ?>" class="img-responsive" alt=""/>
                                    </li>
                                    <li class="profile_item-desc">
                                        <h4><?php echo constant('MEMBERID') . $value1['userId']; ?></h4>
                                        <p> <?php echo (!empty($value1['age']) ? $value1['age'] : '') . ' Yrs, ' . (!empty($value1['height']) ? $value1['height'] : '') . ' ' . (!empty($value1['caste_name']) ? $value1['caste_name'] : ''); ?></p>
                                        <h5><?php echo $this->lang->line('text_view_profile'); ?></h5>
                                    </li>
                                    <div class="clearfix"> </div>
                                </a>
                            </ul>

                            <?php
                        }
                    }
                    ?>
                </div>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
</div>

<!-- FlexSlider -->
<script defer src="<?php echo base_url(); ?>/assets/js/jquery.flexslider.js"></script>
<link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/flexslider.css" type="text/css" media="screen" />
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script>
                                            $(document).ready(function () {
                                                $("#mail").validate({
                                                    submitHandler: function (form) {
                                                        var dataString = $('#mail').serialize();
                                                        if (dataString == '' || dataString == null || dataString == 0) {
                                                            return false;
                                                        }
                                                        $('#send_mail').hide()
                                                        $('#paid_mail').show();
                                                        $.ajax({
                                                            type: 'POST',
                                                            url: baseurl + 'index.php/profile/sendmail',
                                                            data: dataString,
                                                            dataType: "json",
                                                            success: function (data) {
                                                                $('#loading').hide();
                                                                if (data.msg == 'success') {
                                                                    $('#paid_mail').html('<h4 style="text-align:center; color:green;">  <?php echo $this->lang->line("mail_send_successfully_text"); ?></h4>');
                                                                } else {
                                                                    $('#paid_mail').html('<h4 style="text-align:center; color:red;">' + data.msg + '</h4>');
                                                                }
                                                                setTimeout(function () {
                                                                    $('#modalpaid').hide();
                                                                    location.reload();
                                                                }, 2000);
                                                            },
                                                            error: function (jqXHR, textStatus, errorThrown) {
                                                                console.log(textStatus, errorThrown);
                                                                $('#paid_mail').html('<h4 style="text-align:center; color:green;"> <?php echo $this->lang->line("error_occur_contact_admin"); ?></h4>');
                                                                setTimeout(function () {
                                                                    $('#modalpaid').hide();
                                                                    location.reload();
                                                                }, 2000);
                                                            }
                                                        });
                                                    }
                                                });

                                                $("#message").rules("add", {
                                                    required: true,
                                                    minlength: 50,
                                                    maxlength: 250,
                                                    messages: {
                                                        required: "Please enter the text",
                                                        minlength: jQuery.validator.format("Please, at least {0} characters are necessary"),
                                                        maxlength: jQuery.validator.format("Please, at maximum {0} characters are allowed")
                                                    }
                                                });

                                            });
// Can also be used with $(document).ready()
                                            $(window).load(function () {
                                                $('.flexslider').flexslider({
                                                    animation: "slide",
                                                    controlNav: "thumbnails"
                                                });
                                            });
                                            function requestProfile(pid, id) {
                                                if (pid == 0 || id == 0 || pid == '' || id == '') {
                                                    return false;
                                                }
                                                $('#response').show();
                                                $.post(baseurl + 'index.php/profile/updateprofilerequestinfo?pid=' + pid + '&id=' + id,
                                                        function (data) {
                                                            if (data.msg == 'success') {
                                                                $('#msg_body').html('<h4 style="text-align:center; color:green;">' + data.msg + '</h4>');
                                                            } else {
                                                                $('#msg_body').html('<h4 style="text-align:center; color:red;">' + data.msg + '</h4>');
                                                            }

                                                        }, "json")
                                                        .fail(function () {
                                                            $('#msg_body').html('<h4 style="text-align:center; color:green;"> <?php echo $this->lang->line("error_occur_contact_admin"); ?></h4>');
                                                            setTimeout(function () {
                                                                $('#response').hide();
                                                                location.reload();
                                                            }, 2000);
                                                        });
                                            }

                                            function requestcancel(pid, id) {
                                                if (pid == 0 || id == 0 || pid == '' || id == '') {
                                                    return false;
                                                }
                                                var reqdata = "'" + pid + "'" + ',' + "'" + id + "'";
                                                var htmldata = '<span class ="col-sm-10"><?php echo $this->lang->line("are_you_sure_the _shorlisted"); ?></span>';
                                                htmldata1 = '<div class="buttons pull-right col-sm-3">';
                                                htmldata2 = '<div onclick="requestcancelinfo(' + reqdata + ')" class="vertical pointer"><?php echo $this->lang->line('yes'); ?></div>';
                                                htmldata3 = '</div><div class="buttons pull-right col-sm-2">';
                                                htmldata4 = '<div onclick="cancle()" class="vertical pointer"><?php echo $this->lang->line('no'); ?></div>';
                                                htmldata5 = '</div>';
                                                $('#response').show();
                                                $('#msg_body').html(htmldata + htmldata1 + htmldata2 + htmldata3 + htmldata4 + htmldata5);
                                            }

                                            function requestcancelinfo(pid, id) {
                                                if (pid == 0 || id == 0 || pid == '' || id == '') {
                                                    return false;
                                                }
                                                $.post(baseurl + 'index.php/profile/cancleprofilerequestinfo?pid=' + pid + '&id=' + id,
                                                        function (data) {
                                                            if (data.msg == 'success') {
                                                                $('#msg_body').html('<h4 style="text-align:center; color:green;">' + data.msg + '</h4>');
                                                            } else {
                                                                $('#msg_body').html('<h4 style="text-align:center; color:red;">' + data.msg + '</h4>');
                                                            }
                                                            setTimeout(function () {
                                                                $('#response').hide();
                                                                location.reload();
                                                            }, 3000);
                                                        }, "json")
                                                        .fail(function () {
                                                            $('#msg_body').html('<h4 style="text-align:center; color:green;"> <?php echo $this->lang->line("error_occur_contact_admin"); ?></h4>');
                                                            setTimeout(function () {
                                                                $('#response').hide();
                                                                location.reload();
                                                            }, 2000);
                                                        });
                                            }

                                            function cancle() {
                                                $('#response').hide();
                                            }
                                            function sendmail(uid, offerid) {
                                                if (uid == 0 || offerid == 0 || uid == '' || offerid == '') {
                                                    return false;
                                                }

                                                if (offerid == 2) { //need to show paid view
                                                    $('#modalpaid').show();
                                                    $('#loading').show();
                                                    $.post(baseurl + 'index.php/profile/sendmailinfo?uid=' + uid,
                                                            function (data) {
                                                                $('#loading').hide();
                                                                $('#paid_mail').show();
                                                                $('#paid_mail').html(data);

                                                            }, "html")
                                                            .fail(function () {
                                                                $('#paid_mail').show();
                                                                $('#paid_mail').html('<h4 style="text-align:center; color:green;"><?php echo $this->lang->line("error_occur_contact_admin"); ?></h4>');
                                                                setTimeout(function () {
                                                                    $('#modalpaid').hide();
                                                                    location.reload();
                                                                }, 2000);
                                                            });
                                                } else {//need to show send mail view
                                                    if (offerid == 1) {
                                                        $('#modalpaid').show();
                                                        $('#paid_mail').hide();
                                                        $('#send_mail').show();
                                                        $('#uid').val(uid);


                                                    }
                                                }
                                            }
</script>   

