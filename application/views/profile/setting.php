<div class="container">
    <div class="row setting_row">
        <h2 class="pro_settingtitle" > <?php echo $this->lang->line("setting_heading"); ?> </h2>
        <div class="col-md-9 bhoechie-tab-container">
            <div class="col-md-3 bhoechie-tab-menu">
                <div class="list-group">
                    <a href="#" class="list-group-item active text-center" id ="email_content">
                        <h4 class="fa fa-envelope-o group_text"></h4><br/><?php echo $this->lang->line("edit_email_address"); ?>
                    </a>
                    <a href="#" class="list-group-item text-center">
                        <h4 class="fa fa-key" ></h4><br/><?php echo $this->lang->line("change_password"); ?>
                    </a>
                    <!-- <a href="#" class="list-group-item text-center">
                      <h4 class="fa fa-american-sign-language-interpreting "></h4><br/>Deactivate Profile
                    </a>-->
                    <a href="#" class="list-group-item text-center">
                        <h4 class="fa fa-trash-o" ></h4><br/><?php echo $this->lang->line("delete_profile"); ?>
                    </a>

                </div>
            </div>
            <div class="col-md-9 bhoechie-tab">

                <!-- flight section -->
                <div class="bhoechie-tab-content active">
                    <center class="bhoechie_dot">
                        <h1  style="font-size:14em;"></h1>
                        <h2 style="margin-top: 0;"><?php echo $this->lang->line("email_address"); ?></h2>
                    </center>
                    <div class="set_pare">
                        <p id="email-text"><?php echo $this->lang->line("desciption_content"); ?></p>
                    </div>
                    <form id="email-form">
                        <div class="form-group row">
                            <label  class="col-sm-2 form-control-label reglab"><?php echo $this->lang->line("register_profile_for_email"), $this->lang->line("profile_id"); ?></label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control"  name="email" id="email" placeholder="Username" required pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" value="<?php echo $email; ?>" >
                                <label class="error" id="exits"></label>
                            </div>

                            <div class="col-sm-2"style="padding-left: 1px;" >
                                <button class="btn btn-primary set_save"><?php echo $this->lang->line("save_text"); ?></button>
                            </div>
                        </div>
                    </form>

                    <form id="pass-form" style="display: none">
                        <div class="form-group row">
                            <label  class="col-sm-2 form-control-label reglab"><?php echo $this->lang->line("register_profile_for_password"); ?></label>
                            <div class="col-sm-6">
                                <input type="password" class="form-control"  id="password" name="password" placeholder="Password" value="" required >
                            </div>
                            <div class="col-sm-2"style="padding-left: 1px;" >
                                <button class="btn btn-primary set_save" type="submit"><?php echo $this->lang->line("save_text"); ?></buttom>
                            </div>
                        </div>
                    </form>

                </div>


                <!-- train section -->
                <div class="bhoechie-tab-content">
                    <center class="bhoechie_dot">
                        <h1  style="font-size:14em;"></h1>
                        <h2 style="margin-top: 0;"><?php echo $this->lang->line("change_password"); ?></h2>
                    </center>
                    <div class="set_pare">
                        <p<?php echo $this->lang->line("your_change_content"); ?></p>
                    </div>
                    <form id="passchange-form">
                        <div class="form-group row">
                            <label  class="col-sm-4 form-control-label reglab"><?php echo $this->lang->line("enter_current_password"); ?></label>
                            <div class="col-sm-6" id="pass_error">
                                <input type="text" class="form-control" id="pass_change" name="password" placeholder="Password" required >
                                <label for="password" class="error"></label>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label  class="col-sm-4 form-control-label reglab"><?php echo $this->lang->line("enter_new_password"); ?></label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control"  id="new" name="new_password" placeholder="New Password" required >
                            </div>
                        </div>
                        <div class="form-group row">
                            <label  class="col-sm-4 form-control-label reglab"><?php echo $this->lang->line("conform_password"); ?></label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control"  id="confirm" name="conform_password" placeholder="Conform Password" required >
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-10 offset">
                                <div class="col-sm-2"style="padding-left: 1px;" >
                                    <button class="btn btn-primary set_save" type="submit"><?php echo $this->lang->line("save_text"); ?></buttom>
                                </div>
                            </div>
                        </div>
                    </form>

                </div>



                <!-- hotel search -->
                <!-- 
                <div class="bhoechie-tab-content">
                                <center class="bhoechie_dot">
                      <h1  style="font-size:14em;"></h1>
                      <h2 style="margin-top: 0;">Deactivate Profile</h2>
                    </center>
                                        <div class="set_pare">
                                                <p>You have  Deactive your Profile  </p>
                                        </div>
                    <form>
                                    <div class="form-group row">
                                                <label for="profile" class="col-sm-5 form-control-label reglab">Deactive Days</label>
                                                <div class="col-sm-7">
                                                <select class="form-control" id="profile_created" name="profile_created" required >
                                                        <option value=""> -- Select Deactive for -- </option>
                                                        <option value="1">1 Month</option>
                                                        <option value="2">2 Months</option>
                                                        <option value="3">3 Months</option>
                                                </select>
                                                </div>
                                   </div>
                                        <div class="form-group row">
                                                <div class="col-md-9 ">
                                                        <div class="col-sm-4"style="padding-left: 114%;" >
                                                                <a href="#"class="btn btn-primary set_save">Deactive</a>
                                                    </div>
                                              </div>
                                          </div>
                                        </form>
                </div> -->


                <div class="bhoechie-tab-content">
                    <center class="bhoechie_dot">
                        <h1  style="font-size:14em;"></h1>
                        <h2 style="margin-top: 0;"><?php echo $this->lang->line("delete"), ' ', $this->lang->line("profile_admin") ?></h2>
                    </center>
                    <div class="set_pare">
                        <p><?php echo $this->lang->line("you_have_delete_text"); ?>  </p>
                    </div>
                    <form method="post" id="delete_profile">
                        <div class="form-group row">
                            <label  class="col-sm-12 form-control-label reglab"><?php echo $this->lang->line("selete_reason_for_delete_text"); ?>  :</label> 
                            <div class="col-sm-12">
                                <div class="col-sm-4">
                                    <label class="radio-inline reglabrad">
                                        <input type="radio" name="reason" value="married" required> <?php echo $this->lang->line("married_text"); ?>
                                    </label>
                                </div>
                                <div class="col-sm-4">
                                    <label class="radio-inline reglabrad">
                                        <input type="radio" name="reason" value="fixed" required> <?php echo $this->lang->line("married_fixed_text"); ?>
                                    </label>
                                </div>
                                <div class="col-sm-4">
                                    <label class="radio-inline reglabrad">
                                        <input type="radio" name="reason" value="other" required> <?php echo $this->lang->line("reason_text"); ?>
                                    </label>
                                </div>
                                <label for="reason" class="error"></label>
                            </div>
                        </div>
                        <!--<div class="form-group row">
                    <label  class="col-sm-5 form-control-label reglab">Date of Mariage :</label> 
                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="departing" placeholder="Date" required>
                    </div>
                        </div>-->
                        <div class="form-group row">
                            <label  class="col-sm-5 form-control-label reglab"><?php echo $this->lang->line("give_here_details"); ?>:</label> 
                            <div class="col-sm-12">
                                <textarea class="form-control" name="message" id="message" rows="3"></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-10 offset">
                                <div class="col-sm-2"style="padding-left: 1px;" >
                                    <button class="btn btn-primary set_save" type="submit"><?php echo $this->lang->line("save_text"); ?></buttom>
                                </div>
                                <!--<div class="col-sm-2" style="padding-left: 1px;">
                                            <a href="#"class="btn btn-default set_reset">Cancle</a>
                                </div>-->
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!--Interest msg-->
<div id="response" class="modal">
    <div class="modal-dialog animated sample" style=" margin-top: 9%;width: 440px;">
        <div class="modal-contentshort">
            <div class="row">
                <div>
                    <span style="float:left;"></span>
                    <span><h3 class="subtitle">Deativate Profile</h3></span>
                    <span class="modal-closeshort shtclose" id="close"><a href="#"><img src="<?php echo base_url(); ?>assets/images/m/forgot-password-close.gif"></a></span>
                </div>
                <div class="form-group row">
                    <div class="col-sm-10 short-text" id="msg_body" style="text-align:center;">
                        <div id='loader' style='display:block'>
                            <img src="<?php echo base_url(); ?>assets/images/loading.gif"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--sInterest msg end-->

<input type="hidden" id="baseUrl" value="<?php echo base_url(); ?>"/>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script>
    var baseurl = $('#baseUrl').val();

    $('#email_content').click(function () {
        $("#email-text").html('<?php echo $this->lang->line('valid_email_member_to_member_communication'); ?>');
        $("#email-form").show();
        $("#pass-form").hide();
    });

    /*function correction(){
     if($('#pass_change').val() == ''){
     //$('#pass_change .error').html('password required');
     return false;
     }
     var myObject = new Object();
     myObject.password 	= $('#pass_change').val();
     $.post(baseurl+'index.php/profile/setting?step3=1', 
     myObject, 
     function(data){
     if(data.msg == 'success'){
     }else{
     $("#pass_error label.error").show();
     $('#pass_change').removeClass('valid');
     $('#pass_change').addClass('error');
     $("#pass_error label.error").html(data.msg);
     }
     }, "json");
     
     
     }*/
    $(document).ready(function () {

        $("#email-form").validate({
            //perform an AJAX post to ajax.php
            submitHandler: function () {
                $("#exits").hide();
                $.post(baseurl + 'index.php/profile/setting?step1=1',
                        $('#email-form').serialize(),
                        function (data) {
                            if (data.msg == 'success') {
                                $("#email-form").hide();
                                $("#pass-form").show();
                                $("#email-text").html('<?php echo $this->lang->line('pls_provide_your_password_for_security'); ?>');
                            } else {
                                $("#exits").show();
                                $('#exits').html(data.msg);
                            }
                        }, "json")
                        .fail(function () {
                            alert("<?php echo $this->lang->line('connection_error'); ?>");
                            location.reload();
                        });
            }
        });

        $("#pass-form").validate({
            //perform an AJAX post to ajax.php
            submitHandler: function () {
                var myObject = new Object();
                myObject.email = $('#email').val();
                myObject.password = $('#password').val();
                var reqData = JSON.stringify(myObject);
                $.post(baseurl + 'index.php/profile/setting?step2=1',
                        myObject,
                        function (data) {
                            $("#email-form").hide();
                            $("#pass-form").hide();
                            $("#email-text").html(data.msg);
                        }, "json")
                        .fail(function () {
                            alert("<?php echo $this->lang->line('connection_error'); ?>");
                            location.reload();
                        });
            }
        });

        $("#passchange-form").validate({
            rules: {
                new_password: {
                    minlength: 6
                },
                conform_password: {
                    equalTo: "#new"
                }
            },
            //perform an AJAX post to ajax.php
            submitHandler: function () {
                $.post(baseurl + 'index.php/profile/setting?step3=1',
                        $('#passchange-form').serialize(),
                        function (data) {
                            if (data.msg == 'success') {
                                alert('successfully updated');
                                $('#pass_change').val('');
                                $('#new').val('');
                                $('#confirm').val('');
                            } else {
                                $("#pass_error label.error").show();
                                $('#pass_change').removeClass('valid');
                                $('#pass_change').addClass('error');
                                $("#pass_error label.error").html(data.msg);
                                $('#pass_change').val('');
                            }
                        }, "json")
                        .fail(function () {
                            alert("<?php echo $this->lang->line('connection_error'); ?>");
                            location.reload();
                        });
            }
        });

        $("div.bhoechie-tab-menu>div.list-group>a").click(function (e) {
            e.preventDefault();
            $(this).siblings('a.active').removeClass("active");
            $(this).addClass("active");
            var index = $(this).index();
            $("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
            $("div.bhoechie-tab>div.bhoechie-tab-content").eq(index).addClass("active");
        });

        $("#delete_profile").validate({
            submitHandler: function () {
                $('#response').show();
                $.post(baseurl + 'index.php/profile/setting?step4=1',
                        $('#delete_profile').serialize(),
                        function (data) {
                            if (data.msg == 'success') {
                                $('#msg_body').html('<h4 style="text-align:center; color:green;"><?php echo $this->lang->line("deactive_successfully"); ?></h4>');
                            } else {
                                $('#msg_body').html('<h4 style="text-align:center; color:red;">' + data.msg + '</h4>');
                            }
                            setTimeout(function () {
                                $('#response').hide();
                                location.reload();
                            }, 3000);
                        }, "json")
                        .fail(function () {
                            alert("<?php echo $this->lang->line('connection_error'); ?>");
                            location.reload();
                        });
            }
        });

        $("#close").on('click', function () {
            $('#response').hide();
        });
    });
</script>
