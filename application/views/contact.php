
<style> 
    @media (max-width:1980px){
        .pull.register{
            margin-left: 280px;
            width: 500px;
        }
    }
    @media (max-width:480px){
        .pull.register{
            margin-left: 20px;
            width: 250px;
        }


    }

    .about_middle{
        background: rgba(0, 0, 0, 0) url("../../assets/images/backnew1.jpeg") no-repeat scroll 0 0 / 100% 100%;

    }
</style>
<div class="grid_3">
    <div class="container">
        <div class="breadcrumb1">
            <ul>
                <a href="<?php echo base_url(); ?>"><i class="fa fa-home home_1"></i></a>
                <span class="divider">&nbsp;|&nbsp;</span>
                <li class="current-page"><?php echo $this->lang->line('header_contact'); ?></li>
            </ul>
        </div>
        <div class="grid_5">

            <div class="col-md-6">
                <table align="left" border="0" width="100%">
                    <tbody>
                        <tr>
                            <td>
                                <p style="text-align: justify;"><span style="font-weight: bold;"><?php echo $this->lang->line('contact_title'); ?>:</span></p>
                                <p style="text-align: justify; padding-left: 60px;"><span style=""><?php echo $this->lang->line('contact_name'); ?></span></p>
                                <p style="text-align: justify; padding-left: 60px;"><span><?php echo $this->lang->line('contact_address'); ?></span></p>
                                <p style="text-align: justify; padding-left: 60px;"><span style=""><?php echo $this->lang->line('contact_address_1'); ?></span></p>
                                <p style="text-align: justify; padding-left: 60px;"><span style=""><?php echo $this->lang->line('contact_address_2'); ?><br></span></p>
                                <p style="text-align: justify; padding-left: 60px;"><span style=""><?php echo $this->lang->line('contact_address_3'); ?><br></span></p>
                                <p style="text-align: justify; padding-left: 60px;"><span style=""><?php echo $this->lang->line('contact_address_4'); ?><br></span></p>
                                <p style="padding-left: 60px;"><span style="">
                                        <script type="text/javascript" src="../javascript/tiny_mce/themes/advanced/langs/en.js"></script>
                                    </span><span style=""><?php echo $this->lang->line('contact_mobile'); ?></span></p>
                                <p style="padding-left: 60px;"><span style=""><?php echo $this->lang->line('contact_photo'); ?></span></p>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <div class="col-md-6">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d982.5014354721574!2d78.13033468803542!3d9.93347912055401!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3b00c5951d080b9f%3A0xe1eb64099cac373!2sHakim+Ajmal+Khan+Rd%2C+Chinna+Chokkikulam%2C+Chockikulam%2C+Madurai%2C+Tamil+Nadu+625002%2C+India!5e0!3m2!1sen!2sin!4v1462018452496" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
        </div>
    </div>
</div>
<!--<div class="map">

</div>-->


