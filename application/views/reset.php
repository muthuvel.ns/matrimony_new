<!DOCTYPE HTML>
<html>
    <head>
        <title>Matrimonial</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="keywords" content="Marital Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template, 
              Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
        <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
        <link href="<?php echo base_url(); ?>/assets/css/bootstrap-3.1.1.min.css" rel='stylesheet' type='text/css' />
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="<?php echo base_url(); ?>/assets/js/jquery.min.js"></script>
        <script src="<?php echo base_url(); ?>/assets/js/bootstrap.min.js"></script>
        <!-- Custom Theme files -->
        <link href="<?php echo base_url(); ?>/assets/css/register.css" rel='stylesheet' type='text/css' />
        <link href="<?php echo base_url(); ?>/assets/css/style.css" rel='stylesheet' type='text/css' />
        <link href="<?php echo base_url(); ?>/assets/css/your.css" rel='stylesheet' type='text/css' />
        <link href='//fonts.googleapis.com/css?family=Oswald:300,400,700' rel='stylesheet' type='text/css'>
        <link href='//fonts.googleapis.com/css?family=Ubuntu:300,400,500,700' rel='stylesheet' type='text/css'>
        <!----font-Awesome----->
        <link href="<?php echo base_url(); ?>/assets/css/font-awesome.css" rel="stylesheet"> 
        <!----font-Awesome----->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">

        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/css/select2.min.css">

        <!-- Latest compiled and minified JavaScript -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/js/select2.min.js"></script>


        <style>
            .text{
                font-weight: 700 !important;
            }
            .success{
                color:green;
            }
        </style>


    </head>
    <body>
        <div class="navbar navbar-inverse-blue navbar">
            <div class="container">
                <div class="pull-left">
                    <a class="brand" href=""><img src="<?php echo base_url(); ?>/assets/images/1.png" alt="logo" width="125"></a>
                </div>
            </div>
        </div>

        <div class="grid">
            <div class="container gridview" style="border: 1px solid #ddd;">
                <div class="col-md-9">
                    <div style="float:left;">
                        <h2 class="registertitle" style="margin-bottom: 20px;">Change Password</h2></div>
                    </br>
                    </br>

                    <div class="col-md-12 ">
                        <form action="<?php echo base_url(); ?>index.php/login/reset?id=<?php echo $userGuid; ?>" method="POST" id="reset"> 
                            <div class="form-group row control-group">
                                <label for="occupation" class="col-sm-4 form-control-label text" >New Password </label>  
                                <div class="col-sm-6">
                                    <input type="password" id="password" class="form-control" name="pass" placeholder="New Password" required>
                                    <label class="error" for=""></label>
                                </div>
                            </div>
                            <div class="form-group row control-group">
                                <label for="occupation" class="col-sm-4 form-control-label text">Re-type New Password </label>  
                                <div class="col-sm-6">
                                    <input type="password" id="retypepassword" class="form-control" name="retype" placeholder="Re-type New Password " required>
                                    <label class="error" for=""></label>
                                    <span id="error"></span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-offset-4 col-sm-8">
                                    <button type="submit" class="btn btn-primary subbtn"  id="btnSubmit" name="step3">Submit</button>
                                </div>
                            </div>


                        </form>
                    </div>
                </div>

            </div>
        </div>
        <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
        <script>
            $(document).ready(function () {
                $("#reset").validate();
            });

            $(function () {
                $("#btnSubmit").click(function () {
                    var password = $("#password").val();
                    var confirmPassword = $("#retypepassword").val();

                    if (password != confirmPassword) {
                        $("#error").html('<span style="color:red;">Passwords do not match!</span>');
                        return false;
                    }
                    $("#reset").submit();
                    return true;
                });
            });
        </script>


    </body>
</html>
