
<script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/pagination/js/jquery.pajinate.js"></script>
<link href="<?php echo base_url(); ?>/assets/css/sucessstory.css" rel='stylesheet' type='text/css' />
<div class="container">
    <div class="col-md-12 ">
        <ul class="nav nav-tabs" style=" margin-top: 25px;">
            <li class="active" id="tab1"><a data-toggle="tab"> <?php echo $this->lang->line('success_story_title'); ?></a></li>
            <li class=""><a href="<?php echo base_url() . 'index.php/success/index?tab=2' ?>" ><?php echo $this->lang->line('post_title'); ?></a></li>

        </ul>
        <div class="tab-content ">

            <div class="tab-pane active" id="tab_a"><!--tab a strat-->
                <div class="col-md-9 ">
                    <div  id="paging_container3"><!--page content3-->
                        <?php if (!empty($success)) { ?>
                            <div class="alt_page_navigation pull-right sucesscontent" ></div>
                            <div class="clearfix"></div>
                            <?php foreach ($success as $value) { //print_r($value);?>
                                <div class=" alt_content " >  <!--alt content-->                        
                                    <div class="susname"><h4 class="suctxt1"><?php echo $value['bride_name'] . ' & ' . $value['groom_name']; ?></h4><span class="sucloc"> <?php echo $this->lang->line('marriage_date') . ':' . $value['marriage_date']; ?></span></div>
                                    <div class="row ">
                                        <div class="col-md-6">
                                            <div class="border1"> 
                                                <div class="slides sucessreadimg">
                                                    <img class="sucessreadimg" src="<?php echo base_url() . 'assets/success_images/' . $value['photo']; ?>" />
                                                </div>
                                                <!--<div class="flexslider">
                                                 <ul class="slides sucessreadimg">
                                                        <li data-thumb="<?php // echo base_url();    ?>assets/images/p1.jpg">
                                                                <img src="<?php //echo base_url();    ?>assets/images/p1.jpg" />
                                                        </li>
                                                        <li data-thumb="<?php //echo base_url();    ?>assets/images/p2.jpg">
                                                                <img src="<?php //echo base_url();    ?>assets/images/p2.jpg" />
                                                        </li>
                                                        <li data-thumb="<?php //echo base_url();    ?>assets/images/p3.jpg">
                                                                <img src="<?php //echo base_url();    ?>assets/images/p3.jpg" />
                                                        </li>
                                                        <li data-thumb="<?php //echo base_url();    ?>assets/images/p4.jpg">
                                                                <img src="<?php //echo base_url();    ?>assets/images/p4.jpg" />
                                                        </li>
                                                 </ul>
                                          </div>-->						
                                            </div>	   
                                        </div>
                                        <div class="col-md-6">
                                            <div class="readcont">  
                                                <span style="height:225px;" class="sucessleft content"><p><?php echo $value['success_story']; ?></p>
                                                </span>
                                            </div>
                                        </div>
                                    </div><!--row end-->
                                </div>	<!--alt content end--> 
                            <?php } ?>
                        <?php } else { ?>
                            <div class="row sucesscontent">
                                <h4><?php echo $this->lang->line('success_not_found'); ?></h4>'; 
                            </div><!--row end-->
                        <?php } ?>
                    </div><!--page content3 end-->
                </div><!--col-md 9-->
                <div class="col-md-3 sucesscontent">
                    <div class="sucessstory">
                        <div class="sucessstycont"><?php echo $this->lang->line('post_your_text'); ?> </br><?php echo $this->lang->line('success_story_title'); ?></div>
                        <div class="postsucess"> <a href="<?php echo base_url() . 'index.php/success/index?tab=2' ?>"><span class="post-btn2" style="background: #FFA417;"><?php echo $this->lang->line('post_title'); ?></span></a></div>
                    </div> 
                </div>		 
            </div><!--tab a-->
        </div><!-- tab content -->

    </div><!-- col-md-9 end -->



</div><!-- end of container -->

<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script>
    $(document).ready(function () {
        pagination();
    });
    function pagination() {

        $('#paging_container3').pajinate({
            items_per_page: 2,
            item_container_id: '.alt_content',
            nav_panel_id: '.alt_page_navigation',
            nav_label_first: 'Prev',
            nav_label_last: 'Next'
                    /*nav_label_prev : '<',
                     nav_label_next : '>'*/

        });
    }
    function sucess() {
        //alert(123);
        $("#tab_b").addClass('active');
        $("#tab_a").removeClass('active');
        $("#tab2").addClass('active');
        $("#tab1").removeClass('active');

    }

</script>

<!-- FlexSlider -->
<script defer src="<?php echo base_url(); ?>/assets/js/jquery.flexslider.js"></script>
<link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/flexslider.css" type="text/css" media="screen" />
<script>
// Can also be used with $(document).ready()
    $(window).load(function () {
        $('.flexslider').flexslider({
            animation: "slide",
            controlNav: "thumbnails"
        });
    });
</script>  
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/select2/select2.min.css">
<!-- Latest compiled and minified JavaScript -->
<script src="<?php echo base_url(); ?>assets/js/select2/select2.min.js"></script>
