<?php
$lang['matches_title']					='Matches';
$lang['recent_update_title']			='Recent Updates';
$lang['new_matches_title']				='New Matches';
$lang['profile_interest_title']			='Profiles you might also be interested in';
$lang['who_viewed_profile']				='Who viewed your profile';
$lang['to_view_the_list']				='to view the list of member is';
$lang['no_matches_text']				='Matches Not Found ';
/*new matches*/
$lang['new_matches_heading']			='New Matches';
$lang['no_new_matches']					='Please Modify Your Partner Prefrence ';
$lang['viewed_profile_title']			='Viewed Profile';
$lang['profiles_text']					='Profiles';
$lang['ignored_text']					='Ignored';
$lang['send_interest_all']				='Send Interest All';
$lang['activity_text']					='Activity';
$lang['details_not_found']				='Details Not Found';
$lang['member_view_text']				='Members Viewded my profile';
/*photo edit*/
$lang['photo_edit_title']				='My photo edit';
$lang['add_text']						='Add';
$lang['edit_photo']						='Edit Photo';
$lang['edit_profile']					='Edit profile';
$lang['photo_change_title']				='My photo edit';
$lang['mutual_matches_profile_title']	='Mutual Matches Profile';
$lang['mutual_matches_title']			='Mutual Matches';
/*setting */
$lang['setting_heading']				='Profile Settings';
$lang['edit_email_address']				='Edit Email Address';
$lang['change_password']				='Change Passsword';
$lang['delete_profile']					='Delete Profile';
$lang['email_address']					='Email Address';
$lang['desciption_content']				='A valid e-mail id will be used to send you partner search mailers, member to member communication mailers and special offers.';
$lang['change_password']				='Change Password';
$lang['your_change_content']			='>Your change password must have  characters. ';
$lang['enter_current_password']			='Enter Current Password';
$lang['enter_new_password']				='Enter New Password';
$lang['conform_password']				='Conform Password';
$lang['you_have_delete_text']			='You have  Delete the your Profile';
$lang['selete_reason_for_delete_text']	='Select the reason for your profile deleting';
$lang['married_text']					='married';
$lang['married_fixed_text']				='marriage fixed';
$lang['reason_text']					='Other reason';
$lang['give_here_details']				='Give here details';
/*payment page*/
$lang['self_service_plans']				='Self Service Plans';
$lang['months_text'] 					='Months';
$lang['make_payment']					='Make Payment';
$lang['do_this_later']					='Ill do this later';
$lang['view_payment']					='View Payment';
$lang['no_data']='Please add membership info';



?>
