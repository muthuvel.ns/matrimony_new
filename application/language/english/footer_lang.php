<?php
$lang['footer_step1_title1']			= 'About Us';
$lang['footer_step1_title1_text']		= '"Madurai Somaeswar Matrimony is the fastest growing relationship site on the web. Create your Madurai Somaeswar Matrimony profile to begin the exciting journey towards finding your life partner."';
$lang['footer_step2_title1']			= 'Help & Support';
$lang['footer_step2_title1_text1']		= 'Contact us';
$lang['footer_step2_title1_text2']		= 'Feedback';
$lang['footer_step2_title1_text3']		= 'FAQs';
$lang['footer_step2_title1_text4']		= 'Success Story';
$lang['footer_step3_title1']			= 'Quick Links';
$lang['footer_step3_title1_text1']		= 'Privacy Policy';
$lang['footer_step3_title1_text2']		= 'Terms and Conditions';
$lang['footer_step3_title1_text3']		= 'Services';

/** Admin **/
$lang['copyright_admin']				=	'Copyright &copy; 2016. All Rights Reserved | Design by ';
$lang['kevellcorp']						=	'KevellCorp';
?>

