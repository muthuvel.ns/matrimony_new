<?php

/** Admin Login Page */
$lang['login'] 							= 	'Login';
$lang['dashboard']						=	'Dashboard';
$lang['home']							=	'Home';
$lang['month']							=	'Month';
$lang['users']							=	'Users';
$lang['premium']						='Premium';
$lang['payment']						='Payment';
$lang['active']							='Active';
$lang['deactive']						='Deactive';
$lang['delete']							='Delete';
$lang['deactivate']						='Deactivate';
$lang['about']							='About';
$lang['success']						=	'Success';
$lang['recent_update']					=	'Recent Update';
$lang['recent_premium_user']			=	'Recent Premium User';
$lang['view_profile']					=	'View Profile';
$lang['add']							=	'Add %s';
$land['profile_admin']					=	'Profile';
$lang['admin']							=	' Admin ';
$lang['mobile_admin_register']			= 	'Mobile %s';
$lang['number_admin_register']			=	'Number';
$lang['member_admin_register']			=	'Member ';
$lang['cancel_admin_register']			=	'Cancel';
$lang['basics_and_life_admin_register']	=	'Basics & Lifestyle';
$lang['no_of_children_admin_register']	=	'No.of Childrens';
$lang['detail_text']					='Details';
$lang['user_profile_title']				='User Profile';
$lang['platinum']						='Platinum';
$lang['diamond']						='Diamond';
$lang['gold']							='Gold';
$lang['print']							='Print';
$lang['username']						='Username';
$lang['user_information']				=	'User Information';
$lang['step1']							=	'Step1';
$lang['step2']							=	'Step2';
$lang['step3']							=	'Step3';
$lang['partner']						=	'Partner';

/*viewuserpage*/
$lang['about_myself']					='About Myself';
$lang['basic']							='Basics & Lifestyle';
$lang['reg_social_title']				='Religious / Social & Astro Background';
$lang['edu_career_title']				='Education & Career';
$lang['dhosam']							='Dhosam';
$lang['salary']							='Salary';
$lang['no_inform_text']					='No Information';
/*edit userpage*/
$lang['view_user_profile_text']			='View User profile';
$lang['profile_image']					='Profile Image';
$lang['add_photo']						='Add Photo';
$lang['main_photo']						='Main Photo';
$lang['annual_income']					='Annuual Income';
$lang['profile_approved']				='Profile Approved';
$lang['status_text']					='Status';
$lang['no_data']						='No Data';
/*succees story verfication*/
$lang['success_story_verfication_title']='Success Story Verification';
$lang['description_text']				='Description';
$lang['profile_verfication_text']		='Profile Verification';
$lang['your_text']						='Yours';
$lang['platinum_member_text']			='Platinum Member  Profile';
$lang['diamond_member_text']			='Diamond Member  Profile';
$lang['gold_member_text']				='Gold Member  Profile';
$lang['verify_image']					='Image Verification';
/*currency*/
$lang['membership_text']				='Membership';
$lang['default_amount']					='Default Amount';
$lang['inr_text']						='INR';
$lang['usd_text']						='USD';

?>
