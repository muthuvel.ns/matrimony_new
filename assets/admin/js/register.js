function emailavailablility(){
	var email	=	$.trim($("#email").val());
	var baseurl	=	$('#baseurl').val();
	
	// Check email status is valid or not
	//var emailstatus	=	is_email(email);
	
	// If email status is wrong OR has validation error then RETURN FALSE
	if(email =='' || email== null){
		alert("please enter email id");
		$("#email").val('');
		$("#password").val('');
		return false;
	}

 // Check email availability
	var jdata	=	$.ajax({
						type: "POST",
						url: baseurl+'index.php/admin/checkuseremailavailabilty?email='+email, 
						dataType: "html",
						async: false
					}).responseText;

	
	
	if( jdata == 1 ){
		alert("This email is already taken!");
		$("#email").val('');
		$("#password").val('');
		return false;
	} else if( jdata == 2 ){// For Success case : User can use this email 
		return true;
	}
	return true;
    
}

function is_email( data ) { 
	data = data.trim();
	
	if (data.charAt(0) == '_' || data.charAt(0) == '-') /* First character underscore and hyphens not allowed*/
	{
		return false;
	}
	
	var reg = /^((([a-z]|\d|[\_\-`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i;
    
 	if(reg.test(data) == false) {
  		return false;
 	} else {
		var reg_un = /^([^_]*(_[^_])?)*_?$/;	 /* consecutive underscore not allowed*/
		var reg_hy = /^([^-]*(-[^-])?)*-?$/; 	 /* consecutive hyphen not allowed*/
		if(reg_un.test(data) == false || reg_hy.test(data) == false) {
			return false;
		}
  		return true;
 	}
}
